//*--MessageConsole--*/myapi.MessageConsole.addText("");/*--MessageConsole--*/
package myapi;
import myapi.gui.MyConsole;
public class MessageConsole{
	private static MyConsole console;

	static{console=new MyConsole("Mensajes");}
	public static void setVisible(boolean visible){if(console!=null){console.setVisible(visible);}}
	public static void addText(String text){if(console!=null){console.addText(text);}}
	public static void addText(Object object){addText(object.toString());}
	public static void addText(Iterable<?>array){String output="";for(Object object:array){output+="\n"+object;}addText(output);}
	public static void addText(Object array[]){String output="";for(Object object:array){output+="\n"+object;}addText(output);}
	public static void addText(Object array[][]){
		String output="";
		for(Object objects[]:array){output+="\n";for(Object object:objects){output+="\t"+object;}}
		addText(output);
	}
}