package myapi.generics;
import java.io.Serializable;
import java.util.LinkedList;
public class MyLimitedList<T>implements Serializable{
	private static final long serialVersionUID=-3897429850846294541L;
	private final int limit;
	private LinkedList<T>list;

	public MyLimitedList(int limit){
		if(limit<1){limit=1;}
		this.limit=limit;
		list=new LinkedList<T>();
	}
	public void addLast(T object){
		if(list.size()==limit){list.removeFirst();}
		list.addLast(object);
	}
	public T get(int index){return list.get(index);}
	public int size(){return list.size();}
	public int limit(){return limit;}
	@Override public String toString(){return list.toString();}
	public int getNumberOf(T object){
		int count=0;
		for(T element:list){
			if(element!=null&&element.equals(object)){count++;}
		}
		return count;
	}
}