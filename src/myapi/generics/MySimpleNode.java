package myapi.generics;
import java.io.Serializable;
public class MySimpleNode<T>implements Serializable{
	private static final long serialVersionUID=-6146567388850960752L;
	private T data;
	private MySimpleNode<T>link;

	public MySimpleNode(T data){this.data=data;}
	public T getData(){return data;}
	public void setData(T data){this.data=data;}
	public MySimpleNode<T>getLink(){return link;}
	public void setLink(MySimpleNode<T>link){this.link=link;}
}