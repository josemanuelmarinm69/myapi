package myapi.generics;
import java.io.Serializable;
import java.util.Map.Entry;
public final class MyEntry<K,V>implements Entry<K,V>,Serializable{
	private static final long serialVersionUID=7111341100472416024L;
	private final K key;
	private V value;

	public MyEntry(K key,V value){
		this.key=key;
		this.value=value;
	}
	public K getKey(){return key;}
	public V getValue(){return value;}
	public V setValue(V value){
		V old=this.value;
		this.value=value;
		return old;
	}
	@Override public String toString(){return"MyEntry [key="+key+", value="+value+"]";}
}