package myapi.generics;
import java.util.ArrayList;
public interface MyStructure<T>{
	void insert(T element)throws Exception;
	ArrayList<T>list();
	boolean isEmpty();
}