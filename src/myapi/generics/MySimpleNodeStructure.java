package myapi.generics;
import java.io.Serializable;
public abstract class MySimpleNodeStructure<T>implements MyStructure<T>,Serializable{
	private static final long serialVersionUID=210431898309516437L;
	private MySimpleNode<T>root;

	public MySimpleNodeStructure(T element){
		try{insert(element);}
		catch(Exception e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
	}
	public final MySimpleNode<T>getRoot(){return root;}
	public final void setRoot(MySimpleNode<T>root){this.root=root;}
}