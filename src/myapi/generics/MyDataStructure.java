package myapi.generics;
public abstract class MyDataStructure<T>implements MyStructure<T>{
	public MyDataStructure(T element){
		try{insert(element);}
		catch(Exception e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
	}
	public abstract boolean delete(T element);
	public abstract T search(String criteria);
}