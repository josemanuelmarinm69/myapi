package myapi.gui.panels;
import static myapi.MyImageHelper.loadImage;
import static myapi.utils.OfData.isEmptyString;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
public abstract class MyTabbedSearchPanel<T extends myapi.generics.MyStructure<U>,U> extends JPanel{
	private static final long serialVersionUID=7392352436290245094L;
	public static class CriterioBusqueda{
		private String textoEtiqueta;
		private String textoTitulo;
		private ImageIcon icono;
		private String textoTip;

		public CriterioBusqueda(String textoEtiqueta,String textoTitulo,ImageIcon icono,String textoTip){
			setTextoEtiqueta(textoEtiqueta);
			setTextoTitulo(textoTitulo);
			setIcono(icono);
			setTextoTip(textoTip);
		}
		public String getTextoEtiqueta(){return textoEtiqueta;}
		public void setTextoEtiqueta(String textoEtiqueta){this.textoEtiqueta=textoEtiqueta;}
		public String getTextoTitulo(){return textoTitulo;}
		public void setTextoTitulo(String textoTitulo){this.textoTitulo=textoTitulo;}
		public ImageIcon getIcono(){return icono;}
		public void setIcono(ImageIcon icono){this.icono=icono;}
		public String getTextoTip(){return textoTip;}
		public void setTextoTip(String textoTip){this.textoTip=textoTip;}
	}
	private MyTextPanel panelTexto;
	private T estructura;

	public MyTabbedSearchPanel(T estructura,CriterioBusqueda criterios[]){
		this.estructura=estructura;
		setLayout(null);
		setPreferredSize(new Dimension(545,465));
		final JTabbedPane tabbedPane=new JTabbedPane(SwingConstants.TOP);
		tabbedPane.setBounds(10,10,525,445);
		add(tabbedPane);
		for(int i=0;i<criterios.length;i++){
			final MyXPanel panelX=new MyXPanel();
			panelX.getEtiqueta().setText(criterios[i].getTextoEtiqueta());
			panelX.setSize(525,455);
			panelX.getBuscar().addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0){
					String dato=panelX.getCaja().getText();
					if(!isEmptyString(dato)){funciones(dato,panelX.getEtiqueta().getText());}
					else{mostrar("No has ingresado nada");}
					panelX.setPanel(panelTexto);
				}
			});
			myapi.utils.OfEvent.addEnterButtonListener(panelX.getBuscar());
			tabbedPane.addTab(criterios[i].getTextoTitulo(),criterios[i].getIcono(),panelX,criterios[i].getTextoTip());
		}
	}
	@Override public void paint(java.awt.Graphics g){
		setOpaque(false);
		super.paint(g);
	}
	public final MyTextPanel getPanelTexto(){return panelTexto;}
	public final T getEstructura(){return estructura;}
	public final void mostrar(String mensaje){
		Dimension dimension=new Dimension(520,440);
		ImageIcon icono=loadImage("iconopmediano.png");
		panelTexto=new MyTextPanel(dimension,icono);
		panelTexto.addText(mensaje);
	}
	public abstract void funciones(String dato,String criterio);
}