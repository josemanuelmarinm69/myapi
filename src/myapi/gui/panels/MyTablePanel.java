package myapi.gui.panels;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfGUI.modifyDimension;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
public abstract class MyTablePanel<T> extends JPanel implements Runnable{
	private static final long serialVersionUID=-4225500162136573561L;
	private T elemento;
	private Thread hilo;
	private JTable tabla;
	private ModeloTabla modelo;
	private class ModeloTabla extends DefaultTableModel{
		private static final long serialVersionUID=-7115900779436580659L;
		private boolean[] columnasEditables;
		private int[] ancho;

		public ModeloTabla(Object datos[][],String encabezados[]){
			super(datos,encabezados);
			columnasEditables=new boolean[encabezados.length];
			ancho=new int[encabezados.length];
			for(int i=0;i<encabezados.length;i++){columnasEditables[i]=false;}
		}
		@Override public boolean isCellEditable(int fila,int columna){return columnasEditables[columna];}
		public int getColumnWidth(int columna){return ancho[columna];}
		public void setColumnWidth(int columna,int ancho){this.ancho[columna]=ancho;}
	}

	public MyTablePanel(T elemento){this(elemento,null);}
	public MyTablePanel(T elemento,java.awt.Dimension tamanoScroll){
		this.elemento=elemento;
		hilo=new Thread(this);
		setLayout(null);
		addResizableContainerListener(this);
		addSizeToParentListener(this);
		tabla=new JTable();
		tabla.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		llenarDatos();
		JScrollPane pane=new JScrollPane(tabla);
		if(tamanoScroll==null){tamanoScroll=modifyDimension(tabla.getPreferredSize(),25);}
		pane.setBounds(0,0,tamanoScroll.width,tamanoScroll.height);
		add(pane);
	}
	@Override public final void paint(java.awt.Graphics g){
		setOpaque(false);
		super.paint(g);
	}
	public final void run(){
		while(true){
			llenarDatos();
			try{Thread.sleep(2000);}
			catch(InterruptedException e){}
		}
	}
	public final Thread getHilo(){return hilo;}
	@Override public final void setPreferredSize(java.awt.Dimension d){super.setPreferredSize(d);setSize(d);}
	public abstract Object[][] llenarDatos(T elemento);
	public abstract String[] llenarEncabezados(T elemento);
	public void agregado(){hilo.start();}
	public void removido(){hilo.interrupt();}
	private void llenarDatos(){
		String encabezados[]=llenarEncabezados(elemento);
		Object datos[][]=llenarDatos(elemento);
		modelo=new ModeloTabla(datos,encabezados);
		tabla.setModel(modelo);
		for(int i=0;i<encabezados.length;i++){
			int ancho=getTextWidth(encabezados[i],tabla);
			if(ancho>modelo.getColumnWidth(i)){
				tabla.getColumnModel().getColumn(i).setPreferredWidth(ancho);
				tabla.getColumnModel().getColumn(i).setWidth(ancho);
				modelo.setColumnWidth(i,ancho);
			}
		}
		for(int i=0;i<datos.length;i++){
			for(int j=0;j<datos[i].length;j++){
				if(datos[i][j]!=null){
					int ancho=getTextWidth(datos[i][j].toString(),tabla);
					if(ancho>modelo.getColumnWidth(j)){
						tabla.getColumnModel().getColumn(j).setPreferredWidth(ancho);
						tabla.getColumnModel().getColumn(j).setWidth(ancho);
						modelo.setColumnWidth(j,ancho);
					}
				}
			}
		}
	}
}