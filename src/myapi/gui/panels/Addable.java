package myapi.gui.panels;
public interface Addable{
	public void added();
	public void removed();
}