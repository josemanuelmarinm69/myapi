package myapi.gui.panels;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
public class MyStartPanel extends JPanel{
	private static final long serialVersionUID=-2571490972066091586L;

	public MyStartPanel(){
		setLayout(null);
		setPreferredSize(new Dimension(220,255));
		JLabel lblNewLabel=new JLabel("Bienvenid@");
		lblNewLabel.setBounds(75,9,80,16);
		add(lblNewLabel);
		ImageIcon imagen=myapi.MyImageHelper.loadImage("iconoinicial.png");
		JLabel lblNewLabel_1=new JLabel("");
		lblNewLabel_1.setIcon(imagen);
		lblNewLabel_1.setBounds(10,36,270,203);
		add(lblNewLabel_1);
	}
}