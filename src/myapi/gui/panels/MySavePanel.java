package myapi.gui.panels;
public interface MySavePanel<T> extends Addable{
	public T get();
}