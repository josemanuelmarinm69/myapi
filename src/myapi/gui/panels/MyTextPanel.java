package myapi.gui.panels;
import static javax.swing.ListSelectionModel.SINGLE_SELECTION;
import static myapi.MyImageHelper.getScaledIcon;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addEnterButtonListener;
import static myapi.utils.OfGUI.getTextWidth;
/*--Debug--*//*import static myapi.Utilidades.Eventos.agregarOyenteSize;*//*--Debug--*/
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
public class MyTextPanel extends JPanel{
	private static final long serialVersionUID=-8117596312159732130L;
	private int counter;
	private JTextArea area;
	private JList list;
	private DefaultListModel model;
	private HashMap<Integer,String>headers;
	private HashMap<Integer,String>texts;

	public MyTextPanel(){this(new Dimension(300,200));}
	public MyTextPanel(Dimension dimension){this(dimension,null);}
	public MyTextPanel(Dimension dimension,ImageIcon icon){
		setLayout(null);
		addResizableContainerListener(this);
		/*--Debug--*//*agregarOyenteSize(this);*//*--Debug--*/
		int y=5,x=5;
		if(icon!=null){
			JLabel label=new JLabel();
			label.setBounds(5,5,60,60);
			label.setIcon(getScaledIcon(icon,label.getSize()));
			add(label);
			y+=label.getY()+label.getHeight();
			x+=label.getX()+label.getWidth();
		}
		JButton button=new JButton("Borrar Texto");
		button.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e){clearTexts();}});
		addEnterButtonListener(button);
		button.setBounds(x,5,getTextWidth(button),22);
		add(button);
		if(icon==null){y+=button.getY()+button.getHeight();}
		headers=new HashMap<Integer,String>();
		texts=new HashMap<Integer,String>();
		model=new DefaultListModel();
		counter=model.size();
		list=new JList(model);
		list.setSelectionMode(SINGLE_SELECTION);
		list.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				if(model.size()>0&&list.getMinSelectionIndex()>=0){area.setText(texts.get(new Integer(list.getMinSelectionIndex())));}
			}
		});
		area=new JTextArea();
		area.setEditable(false);
		JScrollPane listScroll=new JScrollPane(list);
		listScroll.setBounds(5,y,dimension.width/3,dimension.height);
		add(listScroll);
		JScrollPane areaScroll=new JScrollPane(area);
		areaScroll.setBounds(listScroll.getX()+listScroll.getWidth(),y,(dimension.width*2)/3,dimension.height);
		add(areaScroll);
	}
	public final void addText(String string){
		if(string==null||string.equals("\b")){clearTexts();return;}
		String header;
		if(string.length()<10){header=string;}
		else{header=string.substring(0,9)+"...";}
		headers.put(new Integer(counter),header);
		texts.put(new Integer(counter),string);
		model.addElement(header);
		counter=model.size();
	}
	public final void clearTexts(){
		model.clear();
		counter=model.size();
		area.setText("");
		headers=new HashMap<Integer,String>();
		texts=new HashMap<Integer,String>();
	}
	@Override public final void paint(Graphics graphics){setOpaque(false);super.paint(graphics);}
	@Override public final void setPreferredSize(Dimension dimension){super.setPreferredSize(dimension);setSize(dimension);}
}