package myapi.gui.panels;
import static myapi.MyOptionPane.showMessage;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfGUI.mouseOver;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfData.isEmptyString;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import myapi.generics.MyStructure;
public abstract class MyScrollSearchPanel<T extends MyStructure<U>,U extends Listable> extends JPanel implements CaretListener,ActionListener,Runnable,Addable{
	private static final long serialVersionUID=5814747106910118467L;
	private String textoToolTip;
	private T estructura;
	private ViewPanel viewPanel;
	private JScrollPane scrollPane;
	private JTextField textField;
	private JButton boton;
	private JCheckBox checks[];
	private Thread hilo;
	public static abstract class PanelContenido<U extends Listable> extends JPanel{
		private static final long serialVersionUID=-8803382173103635226L;
		private U elemento;
		private Class<?> clase;
		private LinkedHashMap<String,String>mapa;
		private boolean expandido;
		private boolean seleccionado;
		private int columnas;
		private int altoExpandido;
		private int altoContraido;
		private JLabel etiquetas[];
		private java.awt.Color fondo;

		public PanelContenido(U elemento){this(null,elemento);}
		public PanelContenido(javax.swing.border.Border borde,U elemento){this(borde,elemento,2);}
		public PanelContenido(javax.swing.border.Border borde,U elemento,int columnas){
			this.elemento=elemento;
			this.columnas=columnas;
			clase=elemento.getClass();
			mapa=elemento.parametrizar();
			setLayout(null);
			setFocusable(true);
			if(borde!=null){setBorder(borde);fondo=((javax.swing.border.LineBorder)borde).getLineColor();}
			addResizableContainerListener(this);
			addSizeToParentListener(this);
			int i=0,j=mapa.values().size();
			etiquetas=new JLabel[j];
			int y=5;
			for(Entry<String,String>entry:mapa.entrySet()){
				try{
					java.lang.reflect.Field atributo=clase.getDeclaredField(entry.getValue());
					atributo.setAccessible(true);
					Object valor=atributo.get(elemento);
					if(valor!=null){etiquetas[i]=new JLabel(valor.toString());}
					else{etiquetas[i]=new JLabel("null");}
				}
				catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(NoSuchFieldException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				int x=0;
				if(i%columnas==0){y=(i*22)+5;}
				else{x=etiquetas[i-1].getX()+etiquetas[i-1].getWidth()+5;}
				etiquetas[i].setBounds(x,y,getTextWidth(etiquetas[i]),17);
				etiquetas[i].setToolTipText(entry.getKey());
				addSizeToParentListener(etiquetas[i]);
				add(etiquetas[i]);
				if(i==columnas-1||(i<columnas-1&&i==j-1)){altoContraido=getHeight();}
				i++;
			}
			altoExpandido=getHeight();
			contraer();
		}
		protected final void setAdapter(MouseAdapter adapter){
			for(int i=0;i<etiquetas.length;i++){etiquetas[i].addMouseListener(adapter);}
		}
		private void mostrarEtiquetas(boolean mostrar){
			for(int i=0;i<etiquetas.length;i++){if(i/columnas>1){etiquetas[i].setVisible(mostrar);}}
			setExpandido(mostrar);
			repaint();
		}
		public final void agregar(JLabel etiqueta){
			JLabel ultima=etiquetas[etiquetas.length-1];
			etiqueta.setLocation(5,ultima.getY()+ultima.getHeight()+5);
			add(etiqueta);
			altoExpandido=getHeight();
			contraer();
		}
		public final void expandir(){
			setPreferredSize(new java.awt.Dimension(getWidth(),altoExpandido+5));
			setBackground(fondo);
			mostrarEtiquetas(true);
		}
		public final void contraer(){
			setPreferredSize(new java.awt.Dimension(getWidth(),altoContraido+5));
			setBackground(null);
			mostrarEtiquetas(false);
		}
		public final void actualizar(U elemento){
			this.elemento=elemento;
			int y=5;
			int i=0;
			for(Entry<String,String>entry:mapa.entrySet()){
				try{
					java.lang.reflect.Field atributo=clase.getDeclaredField(entry.getValue());
					atributo.setAccessible(true);
					Object valor=atributo.get(elemento);
					if(valor!=null){etiquetas[i].setText(valor.toString());}
					else{etiquetas[i].setText("null");}
				}
				catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(NoSuchFieldException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				int x=0;
				if(i%columnas==0){y=(i*22)+5;}
				else{x=etiquetas[i-1].getX()+etiquetas[i-1].getWidth()+5;}
				etiquetas[i].setBounds(x,y,getTextWidth(etiquetas[i]),17);
				i++;
			}
		}
		public final boolean isExpandido(){return expandido;}
		public final boolean isSeleccionado(){return seleccionado;}
		public final void setExpandido(boolean expandido){this.expandido=expandido;}
		public final void setSeleccionado(boolean seleccionado){
			this.seleccionado=seleccionado;
/*--MessageConsole--*/myapi.MessageConsole.addText(getClass().getName()+" seleccionado "+seleccionado);/*--MessageConsole--*/
		}
		@Override public final void setPreferredSize(java.awt.Dimension d){super.setPreferredSize(d);setSize(d);}
		public final U getElemento(){return elemento;}
	}
	public class ViewPanel extends MyLinearPanel<PanelContenido<U>>{
		private static final long serialVersionUID=-3407718645228275521L;

		public ViewPanel(){super(false);}
		@Override public java.awt.Component add(java.awt.Component componente){
			@SuppressWarnings("unchecked")
			final PanelContenido<U> c=(PanelContenido<U>)componente;
			MouseAdapter adapter=new MouseAdapter(){
				@Override public void mouseClicked(MouseEvent e){
					if(c.isSeleccionado()){c.setSeleccionado(false);}
					else{c.setSeleccionado(true);}
					ordenar();
				}
				@Override public void mouseEntered(MouseEvent e){
					if(!c.isExpandido()){c.expandir();ordenar();}
				}
				@Override public void mouseExited(MouseEvent e){
					if(!mouseOver(c)&&!c.isSeleccionado()){c.contraer();ordenar();}
				}
			};
			FocusListener listener=new FocusListener(){
				public void focusLost(FocusEvent arg0){
					if(!mouseOver(c)&&!c.isSeleccionado()){c.contraer();ordenar();}
				}
				public void focusGained(FocusEvent arg0){
					if(!c.isExpandido()){c.expandir();ordenar();}
				}
			};
			c.setToolTipText(textoToolTip);
			c.addMouseListener(adapter);
			c.setAdapter(adapter);
			c.addFocusListener(listener);
			return super.add(c);
		}
		public PanelContenido<U> getPrimerPanelSeleccionado(){
			ArrayList<PanelContenido<U>>paneles=getComponentes();
			for(PanelContenido<U> panel:paneles){if(panel.isSeleccionado()){return panel;}}
			return null;
		}
		public ArrayList<PanelContenido<U>> getPanelesSeleccionados(){
			ArrayList<PanelContenido<U>>array=new ArrayList<PanelContenido<U>>();
			ArrayList<PanelContenido<U>>paneles=getComponentes();
			for(PanelContenido<U> panel:paneles){if(panel.isSeleccionado()){array.add(panel);}}
			return array;
		}
		public ArrayList<PanelContenido<U>> getPaneles(){return getComponentes();}
		@Override public void paint(java.awt.Graphics g){setOpaque(false);super.paint(g);}
	}

	public MyScrollSearchPanel(T estructura,Object parametros[]){this(estructura,parametros,null,null);}
	public MyScrollSearchPanel(T estructura,Object parametros[],String textoToolTip){this(estructura,parametros,textoToolTip,null);}
	public MyScrollSearchPanel(T estructura,Object parametros[],java.awt.Dimension tamanoScroll){this(estructura,parametros,null,tamanoScroll);}
	public MyScrollSearchPanel(T estructura,Object parametros[],String textoToolTip,java.awt.Dimension tamanoScroll){
		if(textoToolTip==null){textoToolTip="Da click para seleccionar";}
		if(tamanoScroll==null){tamanoScroll=new java.awt.Dimension(300,400);}
		this.estructura=estructura;
		this.textoToolTip=textoToolTip;
		hilo=new Thread(this);
		setLayout(null);
		addResizableContainerListener(this);
		addSizeToParentListener(this);
		JLabel label=new JLabel("\u00BFQu\u00E9 buscas?");
		label.setBounds(5,10,getTextWidth(label),17);
		add(label);
		textField=new JTextField();
		textField.setBounds(5,label.getY()+label.getHeight()+5,label.getWidth(),20);
		textField.setColumns(10);
		add(textField);
		textField.addCaretListener(this);
		checks=new JCheckBox[parametros.length];
		int x=0;
		for(int i=0;i<parametros.length;i++){
			checks[i]=new JCheckBox(parametros[i].toString());
			checks[i].setLocation(5,(i*23+5)+textField.getY()+textField.getHeight());
			int ancho=getTextWidth(checks[i].getText(),checks[i])+15;
			if(ancho>x){x=ancho;}
			checks[i].setSize(ancho,23);
			checks[i].addActionListener(this);
			add(checks[i]);
		}
		if(textField.getX()+textField.getWidth()>x){x=textField.getX()+textField.getWidth();}
		viewPanel=new ViewPanel();
		viewPanel.setLayout(null);
		viewPanel.setAutoscrolls(true);
		scrollPane=new JScrollPane(viewPanel);
		scrollPane.setLocation(x+5,10);
		scrollPane.setSize(tamanoScroll.width,tamanoScroll.height);
		add(scrollPane);
		boton=new JButton();
	}
	@Override public void paint(java.awt.Graphics g){
		setOpaque(false);
		super.paint(g);
	}
	public void added(){hilo.start();}
	public void removed(){hilo.interrupt();}
	public abstract ArrayList<U> listar();
	public abstract void crearPaneles();
	public final void caretUpdate(CaretEvent e){
		if(hayCheckSeleccionado()&&!isEmptyString(textField.getText())){crearPaneles();if(boton!=null){boton.setEnabled(true);}return;}
		else if(!hayCheckSeleccionado()&&!isEmptyString(textField.getText())){
			showMessage("No seleccionaste ning\u00FAn par\u00E1metro","Error");
			getTextField().requestFocus();
		}
		if(boton!=null){boton.setEnabled(false);}
	}
	public final void actionPerformed(ActionEvent e){
		if(hayCheckSeleccionado()&&!isEmptyString(textField.getText())){crearPaneles();if(boton!=null){boton.setEnabled(true);}return;}
		if(boton!=null){boton.setEnabled(false);}
	}
	public final void run(){
		while(true){
			if(hayCheckSeleccionado()&&!isEmptyString(textField.getText())){
				ArrayList<PanelContenido<U>>array=getViewPanel().getPaneles();
				ArrayList<U>encontrados=listar();
				/*if(encontrados.size()<array.size()){
					for(int i=encontrados.size();i<array.size();i++){getViewPanel().remove();}
					array=getViewPanel().getPaneles(); crearPaneles();
					continue;
				}*/
				int i=0;
				for(PanelContenido<U> panel:array){panel.actualizar(encontrados.get(i));i++;}
			}
			try{Thread.sleep(2000);}
			catch(InterruptedException e){}
		}
	}
	public final boolean hayCheckSeleccionado(){
		for(int i=0;i<checks.length;i++){
			if(checks[i].isSelected()){return true;}
		}
		return false;
	}
	public final boolean[] checksSeleccionados(){
		boolean checksSeleccionados[]=new boolean[checks.length];
		for(int i=0;i<checks.length;i++){checksSeleccionados[i]=checks[i].isSelected();}
		return checksSeleccionados;
	}
	public final T getEstructura(){return estructura;}
	public final ViewPanel getViewPanel(){return viewPanel;}
	public final JScrollPane getScrollPane(){return scrollPane;}
	public final JTextField getTextField(){return textField;}
	public final Thread getHilo(){return hilo;}
	public final JButton getBoton(){return boton;}
	public final void setBoton(String texto){
		int y=checks[checks.length-1].getY()+checks[checks.length-1].getHeight();
		boton.setText(texto);
		boton.setBounds(5,y+5,getTextWidth(texto,boton),23);
		boton.setEnabled(false);
		add(boton);
	}
	public final PanelContenido<U> getPrimerPanelSeleccionado(){return viewPanel.getPrimerPanelSeleccionado();}
	public final ArrayList<PanelContenido<U>> getPanelesSeleccionados(){return viewPanel.getPanelesSeleccionados();}
	public final ArrayList<PanelContenido<U>> getPaneles(){return viewPanel.getPaneles();}
	@Override public final void setPreferredSize(java.awt.Dimension d){super.setPreferredSize(d);setSize(d);}
	public static Object[] convertirParametros(LinkedHashMap<String,String>mapa){
		Object parametros[]=new Object[mapa.values().size()]; int i=0;
		for(Entry<String,String>entry:mapa.entrySet()){parametros[i]=entry.getKey();i++;}
		return parametros;
	}
}