package myapi.gui.panels;
import static java.lang.Math.max;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.SOUTH;
import static java.awt.BorderLayout.EAST;
import static java.awt.BorderLayout.WEST;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
public class MyBorderLayoutPanel extends JPanel{
	private static final long serialVersionUID=598192945248345457L;
	public static final String norte=NORTH;
	public static final String centro=CENTER;
	public static final String sur=SOUTH;
	public static final String este=EAST;
	public static final String oeste=WEST;
	private boolean reemplazar;
	private int x1;
	private int x2;
	private int x3;
	private int x4;
	private int y1;
	private int y2;
	private int y3;
	private int y4;
	private MyLinearPanel<Component>pNorte;
	private MyLinearPanel<Component>pCentro;
	private MyLinearPanel<Component>pSur;
	private MyLinearPanel<Component>pEste;
	private MyLinearPanel<Component>pOeste;
	private JPanel panel;
	public static class PanelInterno extends JPanel{
		private static final long serialVersionUID=-7008077391610426652L;

		@Override public final void paint(java.awt.Graphics g){
			setOpaque(false);
			super.paint(g);
		}
		@Override public final void setPreferredSize(java.awt.Dimension d){super.setPreferredSize(d);setSize(d);}
	}

	public MyBorderLayoutPanel(Dimension dimension){this(dimension,false);}
	public MyBorderLayoutPanel(Dimension dimension,ImageIcon icono){this(dimension,icono,false);}
	public MyBorderLayoutPanel(Dimension dimension,boolean reemplazar){this(dimension,null,reemplazar);}
	public MyBorderLayoutPanel(Dimension dimension,final ImageIcon icono,boolean reemplazar){
		this.reemplazar=reemplazar;
		java.awt.event.ComponentListener listener=new java.awt.event.ComponentAdapter(){
			@Override public void componentShown(java.awt.event.ComponentEvent arg0){ordenar();}
			@Override public void componentResized(java.awt.event.ComponentEvent arg0){ordenar();}
		};
		panel=new JPanel(){
			private static final long serialVersionUID=-4844809652656052991L;

			@Override public void paint(java.awt.Graphics g){
				if(icono!=null){
					g.drawImage(icono.getImage(),0,0,getWidth(),getHeight(),null);
					setOpaque(false);
				}
				super.paint(g);
			}
			@Override public void setPreferredSize(java.awt.Dimension d){super.setPreferredSize(d);setSize(d);}
		};
		panel.setLayout(null);
		panel.setSize(dimension.width-20,dimension.height-20);
		panel.setAutoscrolls(true);
		pNorte=new MyLinearPanel<Component>(false);
		pNorte.addComponentListener(listener);
		pCentro=new MyLinearPanel<Component>(false);
		pCentro.addComponentListener(listener);
		pSur=new MyLinearPanel<Component>(false);
		pSur.addComponentListener(listener);
		pEste=new MyLinearPanel<Component>();
		pEste.addComponentListener(listener);
		pOeste=new MyLinearPanel<Component>();
		pOeste.addComponentListener(listener);
		panel.add(pNorte);
		panel.add(pCentro);
		panel.add(pSur);
		panel.add(pEste);
		panel.add(pOeste);
		JScrollPane pane=new JScrollPane(panel);
		pane.setPreferredSize(dimension);
		super.add(pane);
	}
	@Override public final Component add(Component c){add(c,centro);return c;}
	@Override public final void add(Component componente,Object ubicacion){
		if(ubicacion==norte){
			if(reemplazar&&!pNorte.getComponentes().isEmpty()){
				//pNorte=new PanelLineal<Component>(false);
				pNorte.removeAll();
/*--MessageConsole--*/myapi.MessageConsole.addText("Reemplazado en "+ubicacion);/*-MessageConsole--*/
			}
			pNorte.add(componente);
		}
		else if(ubicacion==centro){
			if(reemplazar&&!pCentro.getComponentes().isEmpty()){
				//pCentro=new PanelLineal<Component>(false);
				pCentro.removeAll();
/*--MessageConsole--*/myapi.MessageConsole.addText("Reemplazado en "+ubicacion);/*--MessageConsole--*/
			}
			pCentro.add(componente);
		}
		else if(ubicacion==sur){
			if(reemplazar&&!pSur.getComponentes().isEmpty()){
				//pSur=new PanelLineal<Component>(false);
				pSur.removeAll();
/*--MessageConsole--*/myapi.MessageConsole.addText("Reemplazado en "+ubicacion);/*--MessageConsole--*/
			}
			pSur.add(componente);
		}
		else if(ubicacion==este){
			if(reemplazar&&!pEste.getComponentes().isEmpty()){
				//pEste=new PanelLineal<Component>();
				pEste.removeAll();
/*--MessageConsole--*/myapi.MessageConsole.addText("Reemplazado en "+ubicacion);/*--MessageConsole--*/
			}
			pEste.add(componente);
		}
		else if(ubicacion==oeste){
			if(reemplazar&&!pOeste.getComponentes().isEmpty()){
				pOeste=new MyLinearPanel<Component>();
/*--MessageConsole--*/myapi.MessageConsole.addText("Reemplazado en "+ubicacion);/*--MessageConsole--*/
			}
			pOeste.add(componente);
		}
		java.awt.event.ComponentListener listener=new java.awt.event.ComponentAdapter(){
			@Override public void componentShown(java.awt.event.ComponentEvent e){
				if(e.getComponent().getParent()!=null&&e.getComponent().getParent() instanceof MyLinearPanel){
					MyLinearPanel<?> lineal=(MyLinearPanel<?>)e.getComponent().getParent();
					lineal.ordenar();
				}
			}
			@Override public void componentResized(java.awt.event.ComponentEvent e){
				if(e.getComponent().getParent()!=null&&e.getComponent().getParent() instanceof MyLinearPanel){
					MyLinearPanel<?> lineal=(MyLinearPanel<?>)e.getComponent().getParent();
					lineal.ordenar();
				}
			}
		};
		componente.addComponentListener(listener);
		ordenar();
/*--MessageConsole--*/myapi.MessageConsole.addText("A\u00F1adido "+componente.getClass().getName()+" en "+ubicacion);/*--MessageConsole--*/
	}
	@Override public final void remove(Component componente){
		if(reemplazar){
			if(pNorte.getComponentes().contains(componente)){
				pNorte=new MyLinearPanel<Component>(false);
			}
			else if(pCentro.getComponentes().contains(componente)){
				pCentro=new MyLinearPanel<Component>(false);
			}
			else if(pSur.getComponentes().contains(componente)){
				pSur=new MyLinearPanel<Component>(false);
			}
			else if(pEste.getComponentes().contains(componente)){
				pEste=new MyLinearPanel<Component>();
			}
			else if(pOeste.getComponentes().contains(componente)){
				pOeste=new MyLinearPanel<Component>();
			}
		}
		else{
			pNorte.remove(componente);
			pCentro.remove(componente);
			pSur.remove(componente);
			pEste.remove(componente);
			pOeste.remove(componente);
		}
		ordenar();
/*--MessageConsole--*/myapi.MessageConsole.addText("Eliminado "+componente.getClass().getName());/*--MessageConsole--*/
		System.gc();
	}
	public final void removeIn(Object ubicacion){
		if(ubicacion==centro&&!pNorte.getComponentes().isEmpty()){
			if(reemplazar){pNorte=new MyLinearPanel<Component>(false);}
			else{pNorte.remove();}
		}
		else if(ubicacion==norte&&!pCentro.getComponentes().isEmpty()){
			if(reemplazar){pCentro=new MyLinearPanel<Component>(false);}
			else{pCentro.remove();}
		}
		else if(ubicacion==sur&&!pSur.getComponentes().isEmpty()){
			if(reemplazar){pSur=new MyLinearPanel<Component>(false);}
			else{pSur.remove();}
		}
		else if(ubicacion==este&&!pEste.getComponentes().isEmpty()){
			if(reemplazar){pEste=new MyLinearPanel<Component>();}
			else{pEste.remove();}
		}
		else if(ubicacion==oeste&&!pOeste.getComponentes().isEmpty()){
			if(reemplazar){pOeste=new MyLinearPanel<Component>();}
			else{pOeste.remove();}
		}
/*--MessageConsole--*/else{myapi.MessageConsole.addText("Nada eliminado en "+ubicacion);}/*--MessageConsole--*/
		ordenar();
		System.gc();
	}
	@Override public final void removeAll(){
		pNorte.removeAll();
		pCentro.removeAll();
		pSur.removeAll();
		pEste.removeAll();
		pOeste.removeAll();
		ordenar();
		System.gc();
	}
	public final boolean componentIn(Object ubicacion){
		if(ubicacion==norte){return pNorte.getComponentes().size()>0;}
		else if(ubicacion==centro){return pCentro.getComponentes().size()>0;}
		else if(ubicacion==sur){return pSur.getComponentes().size()>0;}
		else if(ubicacion==este){return pEste.getComponentes().size()>0;}
		else if(ubicacion==oeste){return pOeste.getComponentes().size()>0;}
		return false;
	}
	public final boolean componentIsIn(Component componente,Object ubicacion){
		if(ubicacion==norte){return pNorte.getComponentes().contains(componente);}
		else if(ubicacion==centro){return pCentro.getComponentes().contains(componente);}
		else if(ubicacion==sur){return pSur.getComponentes().contains(componente);}
		else if(ubicacion==este){return pEste.getComponentes().contains(componente);}
		else if(ubicacion==oeste){return pOeste.getComponentes().contains(componente);}
		return false;
	}
	public final Component getComponentIn(Object ubicacion){
		if(ubicacion==norte){return pNorte.getComponentes().get(0);}
		else if(ubicacion==centro){return pCentro.getComponentes().get(0);}
		else if(ubicacion==sur){return pSur.getComponentes().get(0);}
		else if(ubicacion==este){return pEste.getComponentes().get(0);}
		else if(ubicacion==oeste){return pOeste.getComponentes().get(0);}
		return null;
	}
	@Override public final void setPreferredSize(Dimension d){super.setPreferredSize(d);setSize(d);}
	private void ordenar(){
		x4=x3=x2=x1=0;y4=y3=y2=y1=0;
		if(!pOeste.getComponentes().isEmpty()){x4=x3=x2=pOeste.getWidth()+1;}
		if(!pNorte.getComponentes().isEmpty()
				||!pCentro.getComponentes().isEmpty()
				||!pSur.getComponentes().isEmpty()){
			int w1=pNorte.getWidth()+1,w2=pCentro.getWidth()+1,w3=pSur.getWidth()+1;
			int w4=max(w1,w2),w=max(w3,w4);
			x4=x3=x2+w;
		}
		if(!pEste.getComponentes().isEmpty()){x4=x3+pEste.getWidth()+1;}
		if(!pNorte.getComponentes().isEmpty()){y4=y3=y2=pNorte.getHeight()+1;}
		if(!pOeste.getComponentes().isEmpty()
				||!pCentro.getComponentes().isEmpty()
				||!pEste.getComponentes().isEmpty()){
			int h1=pOeste.getHeight()+1,h2=pCentro.getHeight()+1,h3=pEste.getHeight()+1;
			int h4=max(h1,h2),h=max(h3,h4);
			y4=y3=y2+h;
		}
		if(!pSur.getComponentes().isEmpty()){y4=y3+pSur.getHeight()+1;}
		pNorte.setLocation(x1,y1);
		pCentro.setLocation(x2,y2);
		pSur.setLocation(x1,y3);
		pEste.setLocation(x3,y2);
		pOeste.setLocation(x1,y2);
		panel.setPreferredSize(new Dimension(x4,y4));
		panel.revalidate();
		panel.repaint();
		revalidate();
		repaint();
	}
}