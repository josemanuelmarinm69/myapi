package myapi.gui.panels;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfGUI.getTextWidth;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
public abstract class MyButtonModifyPanel<T extends myapi.generics.MyStructure<U>,U>extends JPanel implements ActionListener{
	private static final long serialVersionUID=-7172905755854429525L;
	private U elemento;
	private JButton botones[];

	public MyButtonModifyPanel(U elemento){this(elemento,null,null);}
	public MyButtonModifyPanel(U elemento,Object atributos[],Object datos[]){
		setLayout(null);
		setElemento(elemento);
		addResizableContainerListener(this);
		addSizeToParentListener(this);
		if(atributos!=null&&datos!=null){iniciarComponentes(atributos,datos);}
	}
	@Override public void paint(java.awt.Graphics g){
		setOpaque(false);
		super.paint(g);
	}
	private void iniciarComponentes(Object atributos[],Object datos[]){
		JLabel etiquetas[]=new JLabel[atributos.length];
		botones=new JButton[atributos.length];
		for(int i=0;i<atributos.length;i++){
			etiquetas[i]=new JLabel(atributos[i].toString());
			etiquetas[i].setBounds(10,(i*38)+10,getTextWidth(etiquetas[i]),14);
			add(etiquetas[i]);
			botones[i]=new JButton(datos[i].toString());
			int x=etiquetas[i].getX()+etiquetas[i].getWidth()+10;
			botones[i].setBounds(x,(i*38)+10,getTextWidth(botones[i]),23);
			botones[i].addActionListener(this);
			addSizeToParentListener(botones[i]);
			add(botones[i]);
		}
	}
	@Override public final void setPreferredSize(java.awt.Dimension d){super.setPreferredSize(d);setSize(d);}
	public final U getElemento(){return elemento;}
	public final void setElemento(U elemento){this.elemento=elemento;}
	public final JButton getBoton(int i){return botones[i];}
}