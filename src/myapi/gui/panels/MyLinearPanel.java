package myapi.gui.panels;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfEvent.addResizableContainerListener;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JPanel;
public class MyLinearPanel<T extends Component> extends JPanel{
	private static final long serialVersionUID=-8517802145864062106L;
	private ArrayList<Component> array;
	private boolean alineacionHorizontal;

	public MyLinearPanel(boolean alineacionHorizontal){
		this.alineacionHorizontal=alineacionHorizontal;
		array=new ArrayList<Component>();
		setLayout(null);
		addResizableContainerListener(this);
		addSizeToParentListener(this);
	}
	public MyLinearPanel(){this(true);}
	public final void ordenar(){
		int alto=0,ancho=0;
		for(int i=0;i<array.size();i++){
			Component componente=array.get(i);
			int x=0,y=0;
			if(i>0){
				Component anterior=array.get(i-1);
				if(alineacionHorizontal){x=anterior.getX()+anterior.getWidth()+1;}
				else{y=anterior.getY()+anterior.getHeight()+1;}
			}
			componente.setLocation(x,y);
			if(alineacionHorizontal&&alto<y+componente.getHeight()){alto=y+componente.getHeight();}
			if(alineacionHorizontal&&i==array.size()-1){ancho=x+componente.getWidth();}
			if(!alineacionHorizontal&&ancho<x+componente.getWidth()){ancho=x+componente.getWidth();}
			if(!alineacionHorizontal&&i==array.size()-1){alto=y+componente.getHeight();}
		}
		setPreferredSize(new Dimension(ancho,alto));
		validate();
		repaint();
	}
	@Override public Component add(Component componente){
		array.add(componente);
		super.add(componente);
		ordenar();
		if(componente instanceof Addable){
			Addable agregable=(Addable)componente;
/*--MessageConsole--*/myapi.MessageConsole.addText("Iniciando "+agregable.getClass().getName());/*--MessageConsole--*/
			agregable.added();
		}
		return componente;
	}
	@Override public final void remove(Component componente){
		super.remove(componente);
		array.remove(componente);
		ordenar();
		if(componente instanceof Addable){
			Addable agregable=(Addable)componente;
/*--MessageConsole--*/myapi.MessageConsole.addText("Finalizando "+agregable.getClass().getName());/*--MessageConsole--*/
			agregable.removed();
		}
	}
	public final void remove(){remove(array.get(array.size()-1));}
	@Override public final void removeAll(){while(array.size()>0){remove();}}
	@Override public final void setPreferredSize(Dimension dimension){super.setPreferredSize(dimension);setSize(dimension);}
	@SuppressWarnings("unchecked")
	public final ArrayList<T> getComponentes(){
		ArrayList<T> array=new ArrayList<T>();
		for(Component componente:this.array){array.add((T)componente);}
		return array;
	}
}