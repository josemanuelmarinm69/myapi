package myapi.gui.panels;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class MyXPanel extends JPanel{
	private static final long serialVersionUID=856517681361242968L;
	private JLabel etiqueta;
	private JTextField caja;
	private JButton buscar;

	public MyXPanel(){
		setLayout(null);
		etiqueta=new JLabel("Introduce");
		etiqueta.setBounds(10,10,150,20);
		add(etiqueta);
		caja=new JTextField();
		caja.setBounds(170,10,100,20);
		add(caja);
		caja.setColumns(10);
		buscar=new JButton("Buscar");
		buscar.setBounds(280,10,100,20);
		add(buscar);
	}
	public JLabel getEtiqueta(){return etiqueta;}
	public JTextField getCaja(){return caja;}
	public JButton getBuscar(){return buscar;}
	public void setPanel(JPanel panel){
		if(getComponentCount()-1>2){
			remove(getComponentCount()-1);
			validate();
		}
		add(panel);
		panel.setBounds(10,40,505,400);
		revalidate();
	}
	@Override public void paint(java.awt.Graphics g){
		setOpaque(false);
		super.paint(g);
	}
}