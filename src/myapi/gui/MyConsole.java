package myapi.gui;
import static java.awt.Toolkit.getDefaultToolkit;
import static java.lang.System.exit;
import static myapi.MyImageHelper.loadImage;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import myapi.gui.panels.MyTextPanel;
public class MyConsole extends JFrame{
	private static final long serialVersionUID=-717714288625477503L;
	private MyTextPanel panel;

	public MyConsole(){this(null);}
	public MyConsole(String title){this(title,false);}
	public MyConsole(boolean errors){this(null,errors);}
	public MyConsole(String title,boolean errors){
		super();
		ImageIcon icon=loadImage("iconopmyoption.png");
		Dimension screenSize=getDefaultToolkit().getScreenSize();
		panel=new MyTextPanel();
		setTitle(title==null?"MyConsle":"MyConsle -- "+title);
		/*addMinimizeToTray(this,icon,title);*/
		setContentPane(panel);
		pack();
		Point location=new Point();
		if(errors){location=new Point(screenSize.width-getWidth(),0);}
		else{location=new Point(screenSize.width-getWidth(),screenSize.height-getHeight());}
		setLocation(location);
		setIconImage(icon.getImage());
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			@Override public void windowClosing(WindowEvent e){
				setVisible(false);
				/*if(!isSystemTrayEmpty()){return;}*/
				Frame frames[]=getFrames();
				for(Frame frame:frames){
					if(frame.isShowing()){myapi.MessageConsole.addText("Frame "+frame.getTitle()+" is avoinding to close");return;}
				}
				exit(0);
			}
		});
		setResizable(false);
	}
	public synchronized void addText(String texto){panel.addText(texto);System.out.println(texto);}
}