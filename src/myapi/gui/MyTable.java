package myapi.gui;
import static java.awt.Color.black;
import static javax.swing.SwingConstants.CENTER;
import static javax.swing.BorderFactory.createLineBorder;
import static myapi.utils.OfGUI.getTextWidth;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyTable extends JPanel{
	private static final long serialVersionUID=-9062511990635563614L;
	private static final int labelHeight=17;
	private Color borderColor;
	private ArrayList<ArrayList<ArrayList<JLabel>>>labels;
	private ArrayList<Integer>x;
	private ArrayList<String>headers;
	private ArrayList<ArrayList<Object>>data;
	private ArrayList<ArrayList<Integer>>y;

	public MyTable(ArrayList<String>headers,ArrayList<ArrayList<Object>>data){this(headers,data,black);}
	public MyTable(ArrayList<String>headers,ArrayList<ArrayList<Object>>data,Color borderColor){
		super(null);
		this.borderColor=borderColor;
		this.headers=headers;
		this.data=data;
		labels=new ArrayList<ArrayList<ArrayList<JLabel>>>();
		labels.add(new ArrayList<ArrayList<JLabel>>());
		x=new ArrayList<Integer>();
		y=new ArrayList<ArrayList<Integer>>();
		calculate();
	}
	public void addColumn(String column,Object data[]){
		ArrayList<Object>array=new ArrayList<Object>();
		for(Object object:data){array.add(object);}
		addColumn(column,array);
	}
	public void addColumn(String column,ArrayList<Object>data){
		removeAll();
		headers.add(column);
		int i=1;
		for(ArrayList<Object>array:this.data){array.add(data.get(i));i++;}
		calculate();
	}
	public void addRow(Object row[]){
		ArrayList<Object>array=new ArrayList<Object>();
		for(Object object:row){array.add(object);}
		addRow(array);
	}
	public void addRow(ArrayList<Object>row){
		removeAll();
		data.add(row);
		calculate();
	}
	@Override public final void setPreferredSize(Dimension dimension){super.setPreferredSize(dimension);setSize(dimension);}
	private void calculate(){
		int i=0;
		x.add(new Integer(i));
		y.add(new ArrayList<Integer>());
		y.get(0).add(new Integer(i));
		for(String header:headers){
			labels.get(0).add(new ArrayList<JLabel>());
			JLabel label=new JLabel(header);
			label.setBorder(createLineBorder(borderColor));
			int width=getTextWidth(label);
			label.setSize(width,labelHeight);
			int xA=x.get(i).intValue();
			int xN=xA+label.getWidth();
			x.add(new Integer(xN));
			labels.get(0).get(i).add(label);
			i++;
		}
		y.add(new ArrayList<Integer>());
		y.get(1).add(new Integer(labelHeight));
		i=1;
		for(ArrayList<Object>array:data){
			labels.add(new ArrayList<ArrayList<JLabel>>());
			y.add(new ArrayList<Integer>());
			int j=0,yN=j;
			for(Object object:array){
				labels.get(i).add(new ArrayList<JLabel>());
				int yA=y.get(i).get(y.get(i).size()-1).intValue();
				if(object==null){
					JLabel label=new JLabel("(null)");
					label.setBorder(createLineBorder(borderColor));
					int width=getTextWidth(label);
					label.setSize(width,labelHeight);
					int xA=x.get(j).intValue(),xAN=x.get(j+1).intValue(),xN=xA+label.getWidth();
					if(xN>xAN){updateX(j+1,new Integer(xN));}
					if(yN<yA+label.getHeight()){yN=yA+label.getHeight();}
					labels.get(i).get(j).add(label);
				}
				else if(object instanceof Iterable){
					Iterable<?>data=(Iterable<?>)object;
					for(Object objectN:data){
						JLabel label=new JLabel(objectN.toString());
						label.setBorder(createLineBorder(borderColor));
						int ancho=getTextWidth(label);
						label.setSize(ancho,labelHeight);
						int xA=x.get(j).intValue(),xAN=x.get(j+1).intValue(),xN=xA+label.getWidth();
						if(xN>xAN){updateX(j+1,new Integer(xN));}
						int yAN=yA+label.getHeight();
						y.get(i+1).add(new Integer(yAN));
						yA=yAN;
						labels.get(i).get(j).add(label);
					}
					if(yN<yA){yN=yA;}
				}
				else if(object instanceof ImageIcon){
					ImageIcon image=(ImageIcon)object;
					JLabel label=new JLabel(image);
					label.setBorder(createLineBorder(borderColor));
					label.setSize(image.getIconWidth(),image.getIconHeight());
					int xA=x.get(j).intValue(),xAN=x.get(j+1).intValue(),xN=xA+label.getWidth();
					if(xN>xAN){updateX(j+1,new Integer(xN));}
					if(yN<yA+label.getHeight()){yN=yA+label.getHeight();}
					labels.get(i).get(j).add(label);
				}
				else{
					JLabel label=new JLabel(object.toString());
					label.setBorder(createLineBorder(borderColor));
					int ancho=getTextWidth(label);
					label.setSize(ancho,labelHeight);
					int xA=x.get(j).intValue(),xAN=x.get(j+1).intValue(),xN=xA+label.getWidth();
					if(xN>xAN){updateX(j+1,new Integer(xN));}
					if(yN<yA+label.getHeight()){yN=yA+label.getHeight();}
					labels.get(i).get(j).add(label);
				}
				j++;
			}
			y.get(i+1).add(new Integer(yN));
			i++;
		}
		order();
	}
	private void order(){
/*--MessageConsole--*/myapi.MessageConsole.addText(x);/*--MessageConsole--*/
/*--MessageConsole--*/myapi.MessageConsole.addText(y);/*--MessageConsole--*/
		int i=0,j=i;
		for(ArrayList<ArrayList<JLabel>>array1:labels){
			j=0;
			for(ArrayList<JLabel>array2:array1){
				int k=0;boolean unique=array2.size()<2;
				for(JLabel label:array2){
					int x=this.x.get(j).intValue(),width=this.x.get(j+1).intValue()-x,y,height;
					if(k>0){y=this.y.get(i+1).get(k-1).intValue();}
					//else if(i>0&&datos.get(i-1).get(0) instanceof Iterable){y=this.y.get(i+1).get(k).intValue();}
					else{y=this.y.get(i).get(this.y.get(i).size()-1).intValue();}
					if(!unique){height=label.getHeight();}
					else{height=this.y.get(i+1).get(this.y.get(i+1).size()-1).intValue()-y;}
					label.setLocation(x,y);
					//if(etiqueta.getText().equals("(null)")){System.out.println(i+"\n"+j+"\n"+k);}
					label.setSize(width,height);
					label.setVerticalAlignment(CENTER);
					label.setHorizontalAlignment(CENTER);
					add(label);
					k++;
				}
				j++;
			}
			i++;
		}
		int x=this.x.get(j).intValue(),y=this.y.get(i).get(this.y.get(i).size()-1).intValue();
		setPreferredSize(new Dimension(x+1,y+1));
		repaint();
	}
	private void updateX(int index,Integer value){
		int fisrt=x.get(index).intValue(),difference=value.intValue()-fisrt;
		x.set(index,value);
		for(index+=1;index<x.size();index++){
			int newValue=x.get(index).intValue()+difference;
			x.set(index,new Integer(newValue));
		}
	}
	/*private void updateY(int index1,int index2,Integer value){
		int first=y.get(index1).get(index2).intValue(),difference=value.intValue()-first;
		y.get(index1).set(index2,value);
		for(index1+=1;index1<y.size();index1++){
			for(index2+=1;index2<y.get(index1).size();index2++){
				int newValue=y.get(index1).get(index2).intValue()+difference;
				y.get(index1).set(index2,new Integer(newValue));
			}
		}
	}*/
	public static MyTable createMyTable(String headers[],Object data[][]){return createMyTable(headers,data,black);}
	public static MyTable createMyTable(String headers[],Object data[][],Color borderColor){
		ArrayList<String>headersArray=new ArrayList<String>();
		ArrayList<ArrayList<Object>>dataArray=new ArrayList<ArrayList<Object>>();
		for(String header:headers){headersArray.add(header);}
		for(Object[]array:data){
			ArrayList<Object>objectArray=new ArrayList<Object>();
			for(Object objeto:array){objectArray.add(objeto);}
			dataArray.add(objectArray);
		}
		return new MyTable(headersArray,dataArray,borderColor);
	}
}