package myapi.gui;
import static java.lang.Class.forName;
import static myapi.MyImageHelper.loadImage;
import static myapi.utils.OfFile.getFilesResourceBundle;
import static myapi.utils.OfGUI.getLocaleByLanguageAndCountry;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
public class MyWindow extends JFrame{
	private static final long serialVersionUID=-6600535303511740579L;
	private boolean maximized;
	private Font font;
	private HashMap<JRadioButtonMenuItem,Locale>languageMap;
	private JRadioButtonMenuItem languageItems[];
	private JMenu languageMenu;
	protected ResourceBundle bundle;
	protected JMenuBar bar;

	public MyWindow(Locale locale){this(locale,false);}
	public MyWindow(Locale locale,ImageIcon icon){this(locale,icon,false);}
	public MyWindow(Locale locale,boolean resizable){this(locale,resizable,(Dimension)null);}
	public MyWindow(Locale locale,Container contentPane){this(locale,false,contentPane);}
	public MyWindow(Locale locale,Dimension size){this(locale,false,size);}
	public MyWindow(Locale locale,ImageIcon icon,boolean resizable){this(locale,icon,resizable,(Dimension)null);}
	public MyWindow(Locale locale,ImageIcon icon,Container contentPane){this(locale,icon,false,contentPane);}
	public MyWindow(Locale locale,ImageIcon icon,Dimension size){this(locale,icon,false,size);}
	public MyWindow(Locale locale,boolean resizable,Container contentPane){this(locale,null,resizable,contentPane);}
	public MyWindow(Locale locale,boolean resizable,Dimension size){this(locale,null,resizable,size);}
	public MyWindow(Locale locale,ImageIcon icon,boolean resizable,Container contentPane){
		super();
		init(/*locale,*/icon,resizable);
		if(contentPane==null&&resizable){setMaximized();}
		else if(contentPane!=null){setContentPane(contentPane);}

		init();
		if(locale==null){return;}
		updateLocale(locale);
	}
	public MyWindow(Locale locale,ImageIcon icon,boolean resizable,Dimension size){
		super();
		init(/*locale,*/icon,resizable);
		if(size==null&&resizable){setMaximized();}
		else if(size!=null){setPreferredSize(size);setLocationRelativeTo(null);}

		init();
		if(locale==null){return;}
		updateLocale(locale);
	}
	private void init(/*Locale locale,*/ImageIcon icon,boolean resizable){
		setIconImage((icon==null?loadImage("iconopmyoption.png"):icon).getImage());
		setResizable(resizable);
		try{forName("myapi.utils.OfEvent");}
		//----ErrorConsole----//
		catch(ClassNotFoundException e){myapi.ErrorConsole.addText(e);}
		setJMenuBar(bar=new JMenuBar());
		bar.add(languageMenu=new JMenu());
	}
	private final synchronized void setMaximized(){
		setExtendedState(MAXIMIZED_BOTH);
		addComponentListener(new ComponentAdapter(){
			@Override public void componentShown(ComponentEvent e){
				if(maximized){
					setExtendedState(MAXIMIZED_BOTH);
					//----MessageConsole----//
					myapi.MessageConsole.addText("Maximized "+getSize());
				}
			}
		});
	}
	private final synchronized void updateLocale(Locale locale){
		if(locale==null){return;}
		setLocale(locale);
		updateBundle(locale);
		languageMenu.setText(bundle.getString("languageMenuText"));
		languageMenu.removeAll();
		languageMap=new HashMap<JRadioButtonMenuItem,Locale>();
		String languageTexts[]=bundle.getString("languages").split(",");
		languageItems=new JRadioButtonMenuItem[languageTexts.length];
		ButtonGroup languageOptions=new ButtonGroup();
		for(int i=0;i<languageTexts.length;i++){
			languageItems[i]=new JRadioButtonMenuItem();
			languageItems[i].setFont(font);
			languageItems[i].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){updateLocale(languageMap.get(e.getSource()));}
			});
			languageMenu.add(languageItems[i]);
			languageOptions.add(languageItems[i]);
			String languageText[]=languageTexts[i].split("_");
			Locale displayLocale=getLocaleByLanguageAndCountry(languageText[0],languageText[1]);
			if(displayLocale.getCountry().equalsIgnoreCase(locale.getCountry())){languageItems[i].setSelected(true);}
			languageItems[i].setText(displayLocale.getDisplayName(locale));
			languageMap.put(languageItems[i],displayLocale);
		}
		updateLocale();
	}
	public synchronized void updateBundle(Locale locale){bundle=getFilesResourceBundle("MyWindow",locale);}
	public synchronized void updateLocale(){setTitle(bundle.getString("title"));}
	public synchronized void init(){}
	@Override public void setFont(Font font){
		if(bar!=null){bar.setFont(font);}
		if(languageMenu!=null){languageMenu.setFont(font);}
		if(languageItems!=null){for(JMenuItem item:languageItems){item.setFont(font);}}
		super.setFont(this.font=font);
	}
	@Override public final synchronized void setContentPane(Container container){
		if(container==null){return;}
//		System.out.println(getPreferredSize());
		super.setContentPane(container);
//		System.out.println(getPreferredSize());
		if(!maximized){pack();setLocationRelativeTo(null);}
//		System.out.println(getPreferredSize());
		repaint();
//		System.out.println(getPreferredSize());
	}
	@Override public final synchronized void setSize(Dimension dimension){}
	/*	if(dimension==null){return;}
		maximized=false;
		super.setSize(dimension);
	}*/
	@Override public final synchronized void setPreferredSize(Dimension dimension){
		if(dimension==null){return;}
		//setSize(dimension);
		super.setPreferredSize(dimension);
	}
	@Override public final synchronized void validate(){
		if(getExtendedState()==MAXIMIZED_BOTH){maximized=true;}
		else{maximized=false;}
		super.validate();
	}
	@Override public final synchronized void setExtendedState(int estado){
		if(estado==MAXIMIZED_BOTH){maximized=true;}
		super.setExtendedState(estado);
	}
}