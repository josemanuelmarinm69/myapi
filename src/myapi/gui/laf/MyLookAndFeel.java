package myapi.gui.laf;
import static java.io.File.separatorChar;
import static java.lang.Class.forName;
import static java.lang.Thread.currentThread;
import static javax.swing.UIManager.setLookAndFeel;
import static myapi.utils.OfData.isEmptyString;
import static myapi.utils.OfFile.getClassLoader;
import static myapi.utils.OfFile.getLibrariesFolder;
import java.io.IOException;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.ParseException;
import java.util.ResourceBundle;
import javax.swing.LookAndFeel;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.synth.SynthLookAndFeel;
public class MyLookAndFeel extends SynthLookAndFeel{
	private static final long serialVersionUID=-3171201954862218464L;

	public MyLookAndFeel(){this("laf.xml");}
	public MyLookAndFeel(String filename){this(MyLookAndFeel.class.getResource(filename),MyLookAndFeel.class);}
	public MyLookAndFeel(String filename,String...urls){this(filename,getClassLoader(urls));}
	public MyLookAndFeel(String filename,File...files){this(filename,getClassLoader(files));}
	private MyLookAndFeel(String filename,URLClassLoader loader){this(loader.getResource(filename),loader);}
	private MyLookAndFeel(URL resource,URLClassLoader loader){
		super();
		try{load(resource.openStream(),loader.loadClass("MyExternalSynthLAFLoader"));}
		catch(ClassNotFoundException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(ParseException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(IOException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
	}
	private MyLookAndFeel(URL resource,Class<?>baseClass){
		super();
		try{load(resource.openStream(),baseClass);}
		catch(ParseException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(IOException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
	}
	public static SynthLookAndFeel parseLAF(String filename,String...urls){return new MyLookAndFeel(filename,urls);}
	public static void installLookAndFeel(LookAndFeel laf){
		try{setLookAndFeel(laf);}
		catch(UnsupportedLookAndFeelException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
	}
	public static void installLookAndFeel(String laf){
		try{setLookAndFeel(laf);}
		catch(UnsupportedLookAndFeelException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(ClassNotFoundException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(InstantiationException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
	}
	public static void installLookAndFeel(){installLookAndFeel(new MyLookAndFeel());}
	public static LookAndFeel loadExternalLAF(String lafName,URLClassLoader loader)
			throws ClassNotFoundException,IllegalAccessException,InstantiationException{
		currentThread().setContextClassLoader(loader);
		Class<?>lafClass=forName(lafName,true,loader);
		return(LookAndFeel)lafClass.newInstance();
	}
	public static void checkExternalLAFS(ResourceBundle bundle){
		String lafs=bundle.getString("lafs");
		if(lafs==null||isEmptyString(lafs)){return;}
		for(String lafName:lafs.split(",")){
			try{
				String lafFiles[]=bundle.getString(lafName).split(",");
				String lafPaths[]=new String[lafFiles.length];
				for(int i=0;i<lafPaths.length;i++){lafPaths[i]=getLibrariesFolder()+separatorChar+lafFiles[i];}
				installLookAndFeel(loadExternalLAF(lafName,getClassLoader(lafPaths)));
				break;
			}
			catch(Throwable e){myapi.ErrorConsole.addText(e);}
		}
	}
}