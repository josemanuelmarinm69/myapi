package myapi.gui.laf;
public class MyLAFParserGUI{
	private String lafPath;
	private java.awt.Font font;
	private java.util.ResourceBundle bundle;
	private myapi.gui.MyWindow demo,frame;
	private MySimpleLAFPropertiesPanel xmlPanel;
	private MyGraphicLAFPropertiesPanel graphicPanel;
	//pending radiobutton, checkbox, textarea, popup, jscrollbar
	private class MyDemoWindow extends myapi.gui.MyWindow{
		private static final long serialVersionUID=-4228480418200871717L;
		private javax.swing.JTextField text;
		private javax.swing.JButton button;
		private javax.swing.JComboBox combo;
		private javax.swing.JLabel label;
		private javax.swing.JSlider slider;
		private javax.swing.JProgressBar progress;

		public MyDemoWindow(java.util.Locale locale){super(locale,true);}
		@Override public synchronized void updateBundle(java.util.Locale locale){
			bundle=myapi.utils.OfFile.getFilesResourceBundle("DemoWindow",locale);
		}
		@Override public synchronized void updateLocale(){
			/*text.setText(bundle.getString("text"));
			text.setSize(myapi.utils.OfGUI.getTextWidth(text),myapi.utils.OfGUI.getTextHeight(text));*/
			button.setText(bundle.getString("button"));
			//button.setSize(myapi.utils.OfGUI.getTextWidth(button),myapi.utils.OfGUI.getTextHeight(button));
			button.setSize(200,50);
			button.setLocation(text.getX(),text.getY()+text.getHeight());
			/*String options[]=bundle.getString("combo").split(",");
			combo.removeAllItems();
			myapi.utils.OfEvent.fetchArrayIntoJComboBox(combo,myapi.utils.OfData.convertArrayToList(options));
			combo.setBounds(button.getX(),button.getY()+button.getHeight(),button.getWidth(),button.getHeight());
			slider.setLocation(combo.getX(),combo.getY()+combo.getHeight());
			label.setText(bundle.getString("label"));
			label.setSize(myapi.utils.OfGUI.getTextWidth(label),myapi.utils.OfGUI.getTextHeight(label));
			label.setLocation(slider.getX(),slider.getY()+slider.getHeight());
			progress.setLocation(text.getX()+text.getWidth(),text.getY());*/
			super.updateLocale();
			getContentPane().setSize(getContentPane().getPreferredSize());
			pack();
		}
		@Override public synchronized void init(){
			setContentPane(new javax.swing.JPanel(null));
			myapi.utils.OfEvent.addResizableContainerListener(getContentPane());
			myapi.utils.OfEvent.addSizeToParentListener(text=new javax.swing.JTextField());
			text.setLocation(0,0);
			//text.setFont(font);
			myapi.utils.OfEvent.addSizeToParentListener(button=new javax.swing.JButton());
			//button.setFont(font);
			myapi.utils.OfEvent.addSizeToParentListener(combo=new javax.swing.JComboBox());
			//combo.setFont(font);
			myapi.utils.OfEvent.addSizeToParentListener(slider=new javax.swing.JSlider());
			slider.setSize(200,50);
			//slider.setFont(font);
			myapi.utils.OfEvent.addSizeToParentListener(label=new javax.swing.JLabel());
			//label.setFont(font);
			myapi.utils.OfEvent.addSizeToParentListener(progress=new javax.swing.JProgressBar());
			progress.setSize(slider.getWidth(),slider.getHeight());
			progress.setValue(60);
			//progress.setFont(font);
			//getContentPane().add(text);
			getContentPane().add(button);
			/*getContentPane().add(combo);
			getContentPane().add(slider);
			getContentPane().add(label);
			getContentPane().add(progress);*/
			getContentPane().setSize(getContentPane().getPreferredSize());
			pack();
			setLocationRelativeTo(null);
		}
		@Override public void setFont(java.awt.Font font){
			if(text!=null){text.setFont(font);}
			if(button!=null){button.setFont(font);}
			if(combo!=null){combo.setFont(font);}
			if(slider!=null){slider.setFont(font);}
			if(label!=null){label.setFont(font);}
			if(progress!=null){progress.setFont(font);}
			super.setFont(font);
		}
	}
	private class MySimpleLAFPropertiesPanel extends javax.swing.JPanel{
		private static final long serialVersionUID=90432830481764050L;
		private javax.swing.JTextArea xmlArea;
		private javax.swing.JScrollPane xmlPane;
		private javax.swing.JButton xmlButton;

		public MySimpleLAFPropertiesPanel(final java.awt.Font font){
			super(null);
			myapi.utils.OfEvent.addResizableContainerListener(this);
			lafPath=myapi.utils.OfFile.createPath(myapi.utils.OfFile.getProgramFolder(),"data","laf");
			String xml="";
			for(String line:myapi.MyFileHelper.loadStrings(myapi.utils.OfFile.createPath(lafPath,"laf.xml"))){xml+=line+'\n';}
			xmlArea=new javax.swing.JTextArea(xml);
			xmlPane=new javax.swing.JScrollPane(xmlArea);
			xmlPane.setBounds(0,0,600,400);
			xmlButton=new javax.swing.JButton();
			xmlButton.setLocation(xmlPane.getWidth()/2-40,xmlPane.getY()+xmlPane.getHeight());
			xmlButton.addActionListener(new java.awt.event.ActionListener(){
				public void actionPerformed(java.awt.event.ActionEvent e){
					try{
						java.io.FileWriter writer=new java.io.FileWriter(myapi.utils.OfFile.createPath(lafPath,"laf.xml"));
						writer.write(xmlArea.getText());
						writer.close();
					}
					catch(Exception ex){}
					java.awt.Point position=demo.getLocation();
					demo.dispose();
					myapi.gui.laf.MyLookAndFeel.installLookAndFeel(new myapi.gui.laf.MyLookAndFeel("laf.xml",
							lafPath,myapi.utils.OfFile.createPath(lafPath,"imagenes")));
					demo=new MyDemoWindow(java.util.Locale.US);
					//demo.setFont(font);
					demo.pack();
					demo.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
					demo.setLocation(position);
					demo.setVisible(true);
				}
			});
			myapi.utils.OfEvent.addSizeToParentListener(xmlButton);
			add(xmlPane);
			add(xmlButton);
			setSize(getPreferredSize());
		}
		@Override public void setFont(java.awt.Font font){
			if(xmlArea!=null){xmlArea.setFont(font);}
			if(xmlButton!=null){xmlButton.setFont(font);}
			super.setFont(font);
		}
	}
	private class MyGraphicLAFPropertiesPanel extends javax.swing.JPanel{
		private static final long serialVersionUID=-8664043155649471168L;
		private java.util.ArrayList<javax.swing.JPanel>panels;
		private org.w3c.dom.Document document;
		private myapi.gui.panels.MyLinearPanel<javax.swing.JPanel>container;
		private javax.swing.JButton moreButton;
		private javax.swing.JButton xmlButton;

		public MyGraphicLAFPropertiesPanel(final java.awt.Font font)throws java.lang.Exception{
			super(null);
			javax.xml.parsers.DocumentBuilderFactory dbf=javax.xml.parsers.DocumentBuilderFactory.newInstance();
			javax.xml.parsers.DocumentBuilder db=dbf.newDocumentBuilder();
			org.xml.sax.InputSource is=new org.xml.sax.InputSource();
			is.setCharacterStream(new java.io.StringReader("<synth></synth>"));
			document=db.parse(is);
			myapi.utils.OfEvent.addResizableContainerListener(this);
			lafPath=myapi.utils.OfFile.createPath(myapi.utils.OfFile.getProgramFolder(),"data","laf");
			container=new myapi.gui.panels.MyLinearPanel<javax.swing.JPanel>(false);
			panels=new java.util.ArrayList<javax.swing.JPanel>();
			moreButton=new javax.swing.JButton();
			myapi.utils.OfEvent.addSizeToParentListener(moreButton);
			moreButton.addActionListener(new java.awt.event.ActionListener(){
				private javax.swing.plaf.ComboBoxUI ui;

				public void actionPerformed(java.awt.event.ActionEvent e){
					javax.swing.JPanel panel=new javax.swing.JPanel(null);
					final javax.swing.JLabel label=new javax.swing.JLabel();
					javax.swing.JLabel fontLabel=new javax.swing.JLabel(bundle.getString("fontText"));
					javax.swing.JLabel insetsLabel=new javax.swing.JLabel(bundle.getString("insetsText"));
					javax.swing.JLabel foregroundLabel=new javax.swing.JLabel(bundle.getString("foregroundText"));
					javax.swing.JLabel backgroundLabel=new javax.swing.JLabel(bundle.getString("backgroundText"));
					final javax.swing.JLabel foregroundColor=new javax.swing.JLabel();
					final javax.swing.JLabel backgroundColor=new javax.swing.JLabel();
					final javax.swing.JSpinner size=new javax.swing.JSpinner();
					myapi.utils.OfEvent.addListenerToJSpinner(size,4,30);
					size.setValue(new Integer(4));
					final javax.swing.JSpinner right=new javax.swing.JSpinner();
					myapi.utils.OfEvent.addListenerToJSpinner(right,0,20);
					final javax.swing.JSpinner left=new javax.swing.JSpinner();
					myapi.utils.OfEvent.addListenerToJSpinner(left,0,20);
					final javax.swing.JSpinner top=new javax.swing.JSpinner();
					myapi.utils.OfEvent.addListenerToJSpinner(top,0,20);
					final javax.swing.JSpinner bottom=new javax.swing.JSpinner();
					myapi.utils.OfEvent.addListenerToJSpinner(bottom,0,20);
					final javax.swing.JComboBox properties=new javax.swing.JComboBox();
					java.util.List<String>array=myapi.utils.OfData.convertArrayToList(bundle.getString("properties").split(","));
					myapi.utils.OfEvent.fetchArrayIntoJComboBox(properties,array);
					properties.addItemListener(new java.awt.event.ItemListener(){
						private boolean flag;

						public void itemStateChanged(java.awt.event.ItemEvent e){
							flag=!flag;
							if(flag){return;}
							javax.swing.JComboBox source=(javax.swing.JComboBox)e.getSource();
							String key;
							if(source.getSelectedIndex()==0||(key=source.getSelectedItem().toString())==null){return;}
							try{
								javax.xml.parsers.DocumentBuilderFactory dbf=javax.xml.parsers.DocumentBuilderFactory.newInstance();
								javax.xml.parsers.DocumentBuilder db=dbf.newDocumentBuilder();
								org.xml.sax.InputSource is=new org.xml.sax.InputSource();

								is.setCharacterStream(new java.io.StringReader(bundle.getString(key+"-style")));
								org.w3c.dom.Document bind=db.parse(is);
								java.util.ArrayList<org.w3c.dom.Node>nodes;
								if((nodes=myapi.xml.MyXMLEditor.getNodesFrom(document.getChildNodes(),
										"synth",(java.util.List<String>)null))!=null){
									org.w3c.dom.Node imported=document.importNode(bind.getFirstChild(),true);
									nodes.get(0).appendChild(imported);
								}

								is.setCharacterStream(new java.io.StringReader(bundle.getString(key+"-bind")));
								bind=db.parse(is);
								if((nodes=myapi.xml.MyXMLEditor.getNodesFrom(document.getChildNodes(),
										"synth",(java.util.List<String>)null))!=null){
									org.w3c.dom.Node imported=document.importNode(bind.getFirstChild(),true);
									nodes.get(0).appendChild(imported);
								}

								String xml=bundle.getString("fontXML");
								xml=xml.replaceFirst(",",""+size.getValue());
								is.setCharacterStream(new java.io.StringReader(xml));
								bind=db.parse(is);
								if((nodes=myapi.xml.MyXMLEditor.getNodesFrom(document.getChildNodes(),
										"style",new String[]{"synth"}))!=null){
									org.w3c.dom.Node imported=document.importNode(bind.getFirstChild(),true);
									nodes.get(0).appendChild(imported);
								}

								xml=bundle.getString("insetsXML");
								xml=xml.replaceFirst(",",""+top.getValue());
								xml=xml.replaceFirst(",",""+left.getValue());
								xml=xml.replaceFirst(",",""+right.getValue());
								xml=xml.replaceFirst(",",""+bottom.getValue());
								is.setCharacterStream(new java.io.StringReader(xml));
								bind=db.parse(is);
								if((nodes=myapi.xml.MyXMLEditor.getNodesFrom(document.getChildNodes(),
										"style",new String[]{"synth"}))!=null){
									org.w3c.dom.Node imported=document.importNode(bind.getFirstChild(),true);
									nodes.get(0).appendChild(imported);
								}
							}
							catch(org.xml.sax.SAXException ex){}
							catch(java.io.IOException ex){}
							catch(javax.xml.parsers.ParserConfigurationException ex){}
						}
					});
					properties.setBounds(0,0,150,30);
					//if(ui==null){ui=properties.getUI();}
					//else{properties.setUI(ui);}
					label.setBounds(0,40,150,50);
					fontLabel.setBounds(0,100,myapi.utils.OfGUI.getTextWidth(fontLabel),myapi.utils.OfGUI.getTextHeight(fontLabel));
					size.setBounds(100,100,50,20);
					insetsLabel.setBounds(0,120,myapi.utils.OfGUI.getTextWidth(insetsLabel),myapi.utils.OfGUI.getTextHeight(insetsLabel));
					right.setBounds(100,120,50,20);
					left.setBounds(150,120,50,20);
					top.setBounds(200,120,50,20);
					bottom.setBounds(250,120,50,20);
					foregroundLabel.setBounds(0,140,myapi.utils.OfGUI.getTextWidth(foregroundLabel),myapi.utils.OfGUI.getTextHeight(foregroundLabel));
					backgroundLabel.setBounds(0,160,myapi.utils.OfGUI.getTextWidth(backgroundLabel),myapi.utils.OfGUI.getTextHeight(backgroundLabel));
					panel.add(properties);
					//panel.add(label);
					panel.add(fontLabel);
					panel.add(size);
					panel.add(insetsLabel);
					panel.add(right);
					panel.add(left);
					panel.add(top);
					panel.add(bottom);
					//panel.add(foregroundLabel);
					//panel.add(backgroundLabel);
					panel.setSize(320,160);
					panels.add(panel);
					container.add(panel);
					moreButton.setLocation(container.getX()+container.getWidth()/2-moreButton.getWidth()/2,
							container.getY()+container.getHeight());
					xmlButton.setLocation(container.getX()+container.getWidth()/2-xmlButton.getWidth()/2,
							moreButton.getY()+moreButton.getHeight());
					setSize(getPreferredSize());
					if(frame!=null){frame.pack();}
					java.lang.System.out.println(":"+getPreferredSize()+":");
					java.lang.System.out.println(":"+moreButton.getLocation()+":");
					java.lang.System.out.println(":"+xmlButton.getLocation()+":");
				}
			});
			xmlButton=new javax.swing.JButton();
			xmlButton.addActionListener(new java.awt.event.ActionListener(){
				public void actionPerformed(java.awt.event.ActionEvent e){
					try{
						java.io.FileWriter writer=new java.io.FileWriter(myapi.utils.OfFile.createPath(lafPath,"laf.xml"));
						javax.xml.transform.Transformer transformer=
								javax.xml.transform.TransformerFactory.newInstance().newTransformer();
						transformer.transform(new javax.xml.transform.dom.DOMSource(document),
								new javax.xml.transform.stream.StreamResult(writer));
						writer.close();
					}
					catch(Exception ex){}
					java.awt.Point position=demo.getLocation();
					demo.dispose();
					myapi.gui.laf.MyLookAndFeel.installLookAndFeel(new myapi.gui.laf.MyLookAndFeel("laf.xml",
							lafPath,myapi.utils.OfFile.createPath(lafPath,"imagenes")));
					demo=new MyDemoWindow(java.util.Locale.US);
					demo.setFont(font);
					demo.pack();
					demo.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
					demo.setLocation(position);
					demo.setVisible(true);
				}
			});
			myapi.utils.OfEvent.addSizeToParentListener(xmlButton);
			moreButton.doClick();
			add(container);
			add(xmlButton);
			add(moreButton);
			setSize(getPreferredSize());
		}
		@Override public void setFont(java.awt.Font font){
			if(moreButton!=null){moreButton.setFont(font);}
			if(xmlButton!=null){xmlButton.setFont(font);}
			super.setFont(font);
		}
	}

	public MyLAFParserGUI()throws java.lang.Exception{
		bundle=myapi.utils.OfFile.getFilesResourceBundle("MyLAFParser",java.util.Locale.US);
		font=myapi.MyFontHelper.changeSize(myapi.utils.OfGUI.dialog,
				java.lang.Integer.parseInt(bundle.getString("font")),false);

		graphicPanel=new MyGraphicLAFPropertiesPanel(font);
		graphicPanel.setFont(font);
		//xmlPanel=new MySimpleLAFPropertiesPanel(font);
		//xmlPanel.setFont(font);
		final javax.swing.JMenu modeMenu=new javax.swing.JMenu();
		modeMenu.setFont(font);
		javax.swing.ButtonGroup group=new javax.swing.ButtonGroup();
		final javax.swing.JRadioButtonMenuItem advancedMode=new javax.swing.JRadioButtonMenuItem();
		advancedMode.setFont(font);
		final javax.swing.JRadioButtonMenuItem graphicMode=new javax.swing.JRadioButtonMenuItem();
		graphicMode.setFont(font);
		graphicMode.setSelected(true);
		modeMenu.add(advancedMode);
		modeMenu.add(graphicMode);
		group.add(advancedMode);
		group.add(graphicMode);

		javax.swing.JFrame.setDefaultLookAndFeelDecorated(true);
		frame=new myapi.gui.MyWindow(java.util.Locale.US,true,/*xmlPanel*/graphicPanel){
			private static final long serialVersionUID=3449325240337902690L;

			@Override public synchronized void updateBundle(java.util.Locale locale){
				bundle=myapi.utils.OfFile.getFilesResourceBundle("MyLAFParser",locale);
			}
			@Override public synchronized void updateLocale(){
				if(xmlPanel!=null){
					xmlPanel.xmlButton.setText(bundle.getString("button"));
					xmlPanel.xmlButton.setSize(myapi.utils.OfGUI.getTextWidth(xmlPanel.xmlButton),
							myapi.utils.OfGUI.getTextHeight(xmlPanel.xmlButton));
					xmlPanel.xmlButton.setLocation(xmlPanel.xmlPane.getWidth()/2-xmlPanel.xmlButton.getWidth()/2,
							xmlPanel.xmlPane.getY()+xmlPanel.xmlPane.getHeight());
					xmlPanel.setSize(xmlPanel.getPreferredSize());
				}
				else{
					graphicPanel.moreButton.setText(bundle.getString("addPanel"));
					graphicPanel.moreButton.setSize(myapi.utils.OfGUI.getTextWidth(graphicPanel.moreButton),
							myapi.utils.OfGUI.getTextHeight(graphicPanel.moreButton));
					graphicPanel.moreButton.setLocation(graphicPanel.container.getX()+graphicPanel.container.getWidth()/2-
							graphicPanel.moreButton.getWidth()/2,graphicPanel.container.getY()+graphicPanel.container.getHeight());
					graphicPanel.xmlButton.setText(bundle.getString("button"));
					graphicPanel.xmlButton.setSize(myapi.utils.OfGUI.getTextWidth(graphicPanel.xmlButton),
							myapi.utils.OfGUI.getTextHeight(graphicPanel.xmlButton));
					graphicPanel.xmlButton.setLocation(graphicPanel.container.getX()+graphicPanel.container.getWidth()/2-
							graphicPanel.xmlButton.getWidth()/2,graphicPanel.moreButton.getY()+graphicPanel.moreButton.getHeight());
					graphicPanel.setSize(graphicPanel.getPreferredSize());
				}
				super.updateLocale();
				modeMenu.setText(bundle.getString("mode"));
				advancedMode.setText(bundle.getString("advanced"));
				graphicMode.setText(bundle.getString("graphic"));
				pack();
			}
		};
		java.awt.event.ActionListener listener=new java.awt.event.ActionListener(){
			public void actionPerformed(java.awt.event.ActionEvent e){
				try{
					javax.swing.JPanel panel;
					frame.setContentPane(panel=e.getSource()==advancedMode?new MySimpleLAFPropertiesPanel(font):
						new MyGraphicLAFPropertiesPanel(font));
					panel.setFont(font);
					frame.pack();
				}
				catch(java.lang.Throwable ex){}
			}
		};
		advancedMode.addActionListener(listener);
		graphicMode.addActionListener(listener);
		frame.getJMenuBar().add(modeMenu,0);
		frame.setFont(font);
		//System.out.println(frame.getSize());
		//System.out.println(frame.getPreferredSize());
		frame.pack();
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);

		java.util.ResourceBundle lafsBundle=myapi.utils.OfFile.getFilesResourceBundle("MyLAFParser",java.util.Locale.US);
		myapi.gui.laf.MyLookAndFeel.checkExternalLAFS(lafsBundle);

		demo=new MyDemoWindow(java.util.Locale.US);
		demo.setFont(font);
		demo.pack();
		demo.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		demo.setVisible(true);
	}
}