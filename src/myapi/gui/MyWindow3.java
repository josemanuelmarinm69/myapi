package myapi.gui;
import static myapi.MyImageHelper.loadImage;
import java.util.Locale;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import myapi.gui.panels.MyTextPanel;
public abstract class MyWindow3 extends MyWindow{
	private static final long serialVersionUID=-1798115700972392951L;

	protected JMenu mnUsuarios;
	protected JMenuItem menuItem;
	protected JMenuItem menuItem_1;
	protected JMenuItem menuItem_2;
	protected JMenuItem menuItem_3;
	protected JMenuItem menuItem_4;
	protected JMenu mnLibros1;
	protected JMenuItem mntmRegistrar;
	protected JMenuItem mntmConsultar_1;
	protected JMenuItem mntmListado1;
	protected JMenuItem mntmConsultar;
	protected JMenuItem mntmEliminar;
	protected JMenu mnPrestamos;
	protected JMenuItem menuItem_5;
	protected JMenuItem menuItem_6;
	protected JMenuItem menuItem_7;
	protected JMenuItem menuItem_8;
	protected JMenuItem menuItem_9;
	protected JMenuBar menuBar11;
	private MyTextPanel textPanel;

	public MyWindow3(Locale locale,javax.swing.ImageIcon icon){super(locale,icon);}
	public MyWindow3(Locale locale,javax.swing.ImageIcon icon,java.awt.Dimension size){super(locale,icon,size);}
	public abstract void loadData();
	public abstract void createFileMenu();
	public abstract void createKeyListeners();
	public abstract void enableDisable();
	public final void showText(String message){
		if(textPanel==null){
			textPanel=new MyTextPanel(new java.awt.Dimension(getPreferredSize().width-5,getPreferredSize().height-5),loadImage("iconopmediano.png"));
		}
		textPanel.addText(message);
		setContentPane(textPanel);
	}
}