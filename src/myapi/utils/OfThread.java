package myapi.utils;
import static java.lang.System.arraycopy;
import static java.lang.Thread.currentThread;
import static myapi.utils.OfData.copyOf;
public class OfThread{
	public static ThreadGroup getRootThreadGroup(){
		ThreadGroup currentThreadGroup=currentThread().getThreadGroup();
		ThreadGroup parentThreadGroup;
		while((parentThreadGroup=currentThreadGroup.getParent())!=null){currentThreadGroup=parentThreadGroup;}
		return currentThreadGroup;
	}
	public static ThreadGroup[]getAllThreadGroups(){
		ThreadGroup root=getRootThreadGroup();
		int number=root.activeGroupCount();
		int length=0;
		ThreadGroup[]groups;
		do{
			number*=2;
			groups=new ThreadGroup[number];
			length=root.enumerate(groups,true);
		}while(length==number);
		ThreadGroup[]allGroups=new ThreadGroup[length+1];
		allGroups[0]=root;
		arraycopy(groups,0,allGroups,1,length);
		return allGroups;
	}
	public static ThreadGroup getThreadGroup(String name){
		if(name==null){throw new IllegalArgumentException("Null thread group name");}
		ThreadGroup[]groups=getAllThreadGroups();
		for(ThreadGroup group:groups){if(group.getName().equals(name)){return group;}}
		return null;
	}
	public static Thread[]getGroupThreads(ThreadGroup group){
		if(group==null){throw new IllegalArgumentException("Null thread group");}
		int number=group.activeCount();
		int length=0;
		Thread[]threads;
		do{
			number*=2;
			threads=new Thread[number];
			length=group.enumerate(threads);
		}while(length==number);
		return copyOf(threads,length);
	}
}