package myapi.utils;
import static java.io.File.separatorChar;
import static java.lang.Thread.sleep;
import static java.util.ResourceBundle.getBundle;
import static javax.sound.sampled.AudioSystem.getAudioInputStream;
import static javax.sound.sampled.AudioSystem.getClip;
import static javax.swing.filechooser.FileSystemView.getFileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.sound.sampled.Clip;
import javazoom.jl.player.Player;
public class OfFile{
	public static final String dat1="dat1";
	public static final String dat2="dat2";
	public static final String dat3="dat3";
	public static final String dat="dat";

	public static String getExtension(File file){return(file==null?null:getExtension(file.getName()));}
	public static String getExtension(String name){
		if(name==null){return null;}
		int i=name.lastIndexOf('.');
		return(i>0&&i<name.length()-1?name.substring(i+1):null);
	}
	public static String getFileName(File file){
		if(file==null){return null;}
		String name=null;
		String path=file.getAbsolutePath();
		String extension=getExtension(file);
		int i=path.lastIndexOf(separatorChar);
		if(i>0&&i<path.length()-1){name=path.substring(i+1);}
		if(name!=null&&extension!=null){name=name.replaceAll("."+extension.toLowerCase(),"").replaceAll("."+extension.toUpperCase(),"");}
		return name;
	}
	public static String getParentFolder(File file){
		if(file==null){return null;}
		String parent=null;
		String path=file.getAbsolutePath();
		int j=path.lastIndexOf(separatorChar);
		path=path.substring(0,j);
		int i=path.lastIndexOf(separatorChar);
		if(i>0&&i<path.length()-1&&j>0&&j<path.length()-1){parent=path.substring(i+1,j);}
		return parent;
	}
	public static boolean fileHasExtensionAndDependants(File file,String extension,String dependants[]){
		if(file==null||extension==null){return false;}
		if(dependants!=null){
			for(int i=0;i<dependants.length;i++){
				File dependant=new File(getParentFolder(file)+separatorChar+getFileName(file)+"."+dependants[i]);
				if(!dependant.exists()||dependant.isDirectory()){return false;}
			}
		}
		return extension.equalsIgnoreCase(getExtension(file));/*&&!getFileName(file).equalsIgnoreCase("Temporal");*/
	}
	public static boolean isValidFilePath(String path){
		if(path==null){return false;}
		File file=new File(path);
		return file.exists()&&file.isFile();
	}
	public static String createPath(String...dirs){
		if(dirs==null||dirs.length==0){return null;}
		String path="";
		for(int i=0;i<dirs.length;i++){
			if(i>0){path+=separatorChar;}
			path+=dirs[i];
		}
		return path;
	}
	public static String getProgramFolder(){
		try{return new File(".").getCanonicalPath();}
		/*--ErrorConsole--*/catch(IOException e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
		return null;
	}
	public static String getFilesFolder(){
		String folder=getProgramFolder();
		if(folder!=null){return folder+separatorChar+"data"+separatorChar+"files";}
		return null;
	}
	public static String getFontsFolder(){
		String folder=getProgramFolder();
		if(folder!=null){return folder+separatorChar+"data"+separatorChar+"fonts";}
		return null;
	}
	public static String getImagesFolder(){
		String folder=getProgramFolder();
		if(folder!=null){return folder+separatorChar+"data"+separatorChar+"images";}
		return null;
	}
	public static String getLibrariesFolder(){
		String folder=getProgramFolder();
		if(folder!=null){return folder+separatorChar+"data"+separatorChar+"libraries";}
		return null;
	}
	public static String getHomeDirectory(){return getFileSystemView().getHomeDirectory().getAbsolutePath();}
	public static ResourceBundle getFilesResourceBundle(String fileName,Locale locale){
		return getResourceBundleFor(fileName,locale,new File(getFilesFolder()));
	}
	public static ResourceBundle getResourceBundleFor(String fileName,Locale locale,File...locations){
		URLClassLoader loader;
		if((loader=getClassLoader(locations))==null){return null;}
		return getBundle(fileName,locale,loader);
	}
	public static URLClassLoader getClassLoader(String...urls){
		if(urls.length==0){return null;}
		File files[]=new File[urls.length];
		for(int i=0;i<files.length;i++){files[i]=new File(urls[i]);}
		return getClassLoader(files);
	}
	public static URLClassLoader getClassLoader(File...files){
		if(files.length==0){return null;}
		try{
			URL urls[]=new URL[files.length];
			for(int i=0;i<urls.length;i++){urls[i]=files[i].toURI().toURL();}
			return new URLClassLoader(urls);
		}
		catch(MalformedURLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return null;
	}
	public static void playAudio(String file){playAudio(new File(file).toURI());}
	public static void playAudio(final URI uri){
		final String extension=getExtension(new File(uri.getPath()));
		Thread thread=new Thread(new Runnable(){
			public void run(){
				try{
					if(extension.equalsIgnoreCase("wav")){
						Clip clip=getClip();
						clip.open(getAudioInputStream(uri.toURL()));
						clip.start();
						sleep(1000);
						clip.close();
					}
					else if(extension.equalsIgnoreCase("mp3")){
						Player p=new Player(new FileInputStream(new File(uri.getPath())));
						p.play();
					}
				}
				/*--ErrorConsole--*/catch(Exception e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
			}
		});
		thread.start();
	}
}