package myapi.utils;
import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.WEST;
import static java.awt.Color.black;
import static java.lang.Double.parseDouble;
import static java.lang.Double.valueOf;
import static java.lang.Integer.parseInt;
import static java.lang.Integer.toBinaryString;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static java.lang.String.format;
import static java.lang.System.out;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static myapi.utils.OfData.PaddingPosition.PADDING_LEFT;
import static myapi.utils.OfData.convertCollectionToArray;
import static myapi.utils.OfData.pad;
import static myapi.utils.OfGUI.testPanel;
import static myapi.utils.OfMath.roundTo;
import static myapi.utils.OfMath.sigma;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import java.awt.Dimension;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import myapi.gui.panels.MyBorderLayoutPanel;
import myapi.gui.panels.MyTablePanel;
public class MyRandomGenerator{
	public static class PanelForArray extends MyTablePanel<ArrayList<ArrayList<String>>>{
		private static final long serialVersionUID=1640153203960494936L;

		public PanelForArray(ArrayList<ArrayList<String>>arrays){super(arrays);}
		@Override public String[]llenarEncabezados(ArrayList<ArrayList<String>>arrays){
			ArrayList<String>array=arrays.remove(0);
			return convertCollectionToArray(array,new String[array.size()]);
		}
		@Override public Object[][]llenarDatos(ArrayList<ArrayList<String>>arrays){
			/**/java.util.HashSet<String>set=new java.util.HashSet<String>();/**/
			Object output[][]=new Object[arrays.size()][arrays.get(0).size()];
			int i=0,j=0;
			for(ArrayList<String>array:arrays){
				for(String string:array){output[i][j++]=string;}
				/**/set.add(array.get(array.size()-1));/**/
				i++;j=0;
			}
			out.println(set.size()+":de:"+arrays.size());
			return output;
		}
		@Override public void agregado(){}
		@Override public void removido(){}
	}
	public static class PanelForData extends MyTablePanel<ArrayList<ArrayList<String>>>{
		private static final long serialVersionUID=-8573909108859388851L;

		public PanelForData(ArrayList<ArrayList<String>>arrays){super(arrays);}
		@Override public String[]llenarEncabezados(ArrayList<ArrayList<String>>arrays){
			int sqr=(int)roundTo(sqrt(arrays.size()),0);
			String headers[]=new String[sqr];
			for(int i=0;i<sqr;i++){headers[i]=""+(i+1);}
			return headers;
		}
		@Override public Object[][]llenarDatos(ArrayList<ArrayList<String>>arrays){
			int sqr=(int)roundTo(sqrt(arrays.size()),0);
			int size=sqr+1;
			Object output[][]=new Object[size][sqr];
			for(int i=0,j=0;arrays.size()>0;i++){
				if(i>0&&i%sqr==0){j++;i=0;}
				ArrayList<String>array=arrays.remove(0);
				output[j][i]=array.get(array.size()-1);
			}
			return output;
		}
		@Override public void agregado(){}
		@Override public void removido(){}
	}
	public static class PanelForCategory extends MyTablePanel<ArrayList<ArrayList<String>>>{
		private static final long serialVersionUID=2741535561484232007L;
		private static final String categoryOfThree[]={"T","1P","TD"};
		private static final String categoryOfFour[]={"P","T|2P","1P","TD"};
		private static final String categoryOfFive[]={"Q","P|TP(FH)","T|2P","1P","TD"};
		private String category[];
		private static final int countForThree[][]={{0},{0},{0}};
		private static final int countForFour[][]={{0},{0,0},{0},{0}};
		private static final int countForFive[][]={{0},{0,0},{0,0},{0},{0}};
		private double percent[][];
		private static final double percentForThree[][]={{14.36},{50.4},{30.24}};
		private static final double percentForFour[][]={{0.45},{7.41,10.98},{50.61},{30.55}};
		private static final double percentForFive[][]={{0.01},{0.45,0.9},{7.2,10.8},{50.4},{30.24}};
		private int count[][],n0,n1;

		public PanelForCategory(ArrayList<ArrayList<String>>arrays){super(arrays);}
		@Override public String[]llenarEncabezados(ArrayList<ArrayList<String>>arrays){
			n0=n1=0;
			int sqr=(int)sqrt(arrays.size());
			String headers[]=new String[sqr];
			for(int i=0;i<sqr;i++){headers[i]=""+(i+1);}
			return headers;
		}
		@Override public Object[][]llenarDatos(ArrayList<ArrayList<String>>arrays){
			int sqr=(int)sqrt(arrays.size());
			int size=sqr+1;
			Object output[][]=new Object[size][sqr];
			for(int i=0,j=0;arrays.size()>0;i++){
				if(i>0&&i%sqr==0){j++;i=0;}
				ArrayList<String>array=arrays.remove(0);
				String number=array.get(array.size()-1);
				if(parseDouble(number)<0.5){n0++;}
				else{n1++;}
				number=number.replaceFirst("0.","");
				if(category==null){
					switch(number.length()){
						case 4:category=categoryOfFour;count=countForFour;break;
						case 5:category=categoryOfFive;count=countForFive;break;
						default:category=categoryOfThree;count=countForThree;
					}
				}
				HashSet<Character>set=new HashSet<Character>();
				for(char c:number.toCharArray()){set.add(new Character(c));}
				int index=set.size();
				String given=category[index-1];
				if(index==2){
					int newLength=number.replaceAll(""+set.iterator().next(),"").length();
					boolean flag=newLength==1||newLength==number.length()-1;
					given=flag?given.split("\\|")[0]:given.split("\\|")[1];
					int dummie=flag?count[index-1][0]++:count[index-1][1]++;dummie=dummie+1;
				}
				else if(index==3&&given.contains("\\|")){
					Iterator<Character>iterator=set.iterator();
					Character tryChar;
					boolean flag=false;
					while(iterator.hasNext()&&
							(tryChar=iterator.next())!=null&&
							!(flag=number.matches("\\d*"+tryChar+"\\d*"+tryChar+"\\d*"+tryChar+"\\d*"))){}
					given=flag?given.split("\\|")[0]:given.split("\\|")[1];
					int dummie=flag?count[index-1][0]++:count[index-1][1]++;dummie=dummie+1;
				}
				else{count[index-1][0]++;}
				output[j][i]=given;
			}
			return output;
		}
		@Override public void agregado(){}
		@Override public void removido(){}
		public String getStats(){
			String output="Categoria\tOi\tEi\t(Ei-Oi)^2/Ei";
			double total=0;
			switch(count.length){
				case 5:percent=percentForFive;break;
				case 4:percent=percentForFour;break;
				default:percent=percentForThree;
			}
			for(int i=0;i<count.length;i++){
				for(int j=0;j<count[i].length;j++){
					double ei=percent[i][j],oi=count[i][j],a;
					total+=(a=oi>0?pow(ei-oi,2)/ei:ei);
					output+='\n'+(count[i].length>1?category[i].split("\\|")[j]:category[i])+'\t'+count[i][j]+'\t'+ei+'\t'+a;
				}
			}
			return output+'\n'+'\t'+'\t'+'\u03A3'+'='+total;
		}
		public String getMiddleStats(){return"Datos\nn0="+n0+"\nn1="+n1;}
	}
	public static void createAndShowGUI(String args[]){
		if(args==null||args.length==0){args=new String[]{"-m1","5,4,322","-m2","20,4,3659,4847","-m3","15,4,4739,8757"};}
		for(int i=0;i<args.length;i+=2){
			if(i==args.length-1){out.println("Use -m# n,k,seed,... for specifyng the desired method");return;}
			String operands[]=args[i+1].split(",");
			if("-m1".equals(args[i])){
				try{
				int n=parseInt(operands[0]),k=parseInt(operands[1]),seed=parseInt(operands[2]);
				testPanel(new PanelForArray(middleMethod(n,k,seed)),"Metodo de cuadrado medios",EXIT_ON_CLOSE);
				}
				catch(Throwable e){out.println("Use n,k,seed for -m1");}
			}
			else if("-m2".equals(args[i])){
				try{
				int n=parseInt(operands[0]),k=parseInt(operands[1]),seed1=parseInt(operands[2]),seed2=parseInt(operands[3]);
				testPanel(new PanelForArray(multiplyMethod(n,k,seed1,seed2)),"Metodo de producto de medios",EXIT_ON_CLOSE);
				}
				catch(Throwable e){out.println("Use n,k,seed1,seed2 for -m2");}
			}
			else if("-m3".equals(args[i])){
				try{
				int n=parseInt(operands[0]),k=parseInt(operands[1]),seed=parseInt(operands[2]),y=parseInt(operands[3]);
				testPanel(new PanelForArray(constantMethod(n,k,seed,y)),"Metodo de multiplicacion de constante",EXIT_ON_CLOSE);
				}
				catch(Throwable e){out.println("Use n,k,seed,y for -m3");}
			}
			else if("-m4".equals(args[i])){
				try{
				int n=parseInt(operands[0]),a=parseInt(operands[1]),c=parseInt(operands[2]),seed=parseInt(operands[3]),
						m=parseInt(operands[4]);
				testPanel(new PanelForArray(mixedMethod(n,a,c,seed,m)),"Metodo congruencial mixto",EXIT_ON_CLOSE);
				}
				catch(Throwable e){out.println("Use n,a,c,seed,m for -m4");}
			}
			else if("-m5".equals(args[i])){
				try{
				int n=parseInt(operands[0]),a=parseInt(operands[1]),seed=parseInt(operands[2]);
				testPanel(new PanelForArray(decimalConguentMethod(n,a,seed)),"Metodo de congruencia-decimal",EXIT_ON_CLOSE);
				}
				catch(Throwable e){out.println("Use n,a,seed for -m5");}
			}
			else if("-m6".equals(args[i])){
				try{
				int n=parseInt(operands[0]),a=parseInt(operands[1]),seed=parseInt(operands[2]),m=parseInt(operands[3]);
				testPanel(new PanelForArray(binaryMethod(n,a,seed,m)),"Metodo binario",EXIT_ON_CLOSE);
				}
				catch(Throwable e){out.println("Use n,a,seed,m for -m6");}
			}
			else if("-m7".equals(args[i])){
				try{
					int n=parseInt(operands[0]),k=parseInt(operands[1]),seed=parseInt(operands[2]);
					String seed2=new StringBuilder(operands[2]).reverse().toString();
					ArrayList<ArrayList<String>>xArrays=ownMethod(n,k,seed),yArrays=ownMethod(n,k,parseInt(seed2)),
						copy=new ArrayList<ArrayList<String>>(),x=new ArrayList<ArrayList<String>>(),
						y=new ArrayList<ArrayList<String>>();
					MyBorderLayoutPanel panel=new MyBorderLayoutPanel(new Dimension(800,600),false);
					PanelForArray xaPanel=new PanelForArray(xArrays);
					PanelForArray yaPanel=new PanelForArray(yArrays);
					x.addAll(xArrays);
					y.addAll(yArrays);
					copy.addAll(xArrays);
					PanelForData xPanel=new PanelForData(copy);
					copy.clear();copy.addAll(yArrays);
					PanelForData yPanel=new PanelForData(copy);
					panel.add(xPanel,CENTER);
					panel.add(yPanel,CENTER);
					PanelForCategory xcPanel=new PanelForCategory(xArrays);
					panel.add(xcPanel,CENTER);
					PanelForCategory ycPanel=new PanelForCategory(yArrays);
					panel.add(ycPanel,CENTER);
					panel.add(xaPanel,CENTER);
					panel.add(yaPanel,CENTER);
					Object xarr[][]=xPanel.llenarDatos(x),yarr[][]=yPanel.llenarDatos(y);
					JPanel squarePanel=new JPanel(null);
					JLabel labels[][]=drawSquare(xarr,yarr,5);
					for(JLabel[]arr:labels){for(JLabel label:arr){squarePanel.add(label);}}
					squarePanel.setPreferredSize(new Dimension(5*labels.length,5*labels.length));
					squarePanel.setSize(squarePanel.getPreferredSize());
					panel.add(drawChart(xarr,yarr),WEST);
					panel.add(squarePanel,WEST);
					panel.setSize(panel.getPreferredSize());
					testPanel(panel,"Metodo Propio",EXIT_ON_CLOSE);
					myapi.MyOptionPane.showMessage(xcPanel.getStats()+'\n'+xcPanel.getMiddleStats(),"Prueba Estadistica");
					myapi.MyOptionPane.showMessage(ycPanel.getStats()+'\n'+ycPanel.getMiddleStats(),"Prueba Estadistica");
				}
				catch(Throwable e){out.println("Use n,k,seed for -m7");e.printStackTrace();}
			}
		}
	}
	public static ArrayList<ArrayList<String>>middleMethod(int n,int k,int seed){
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new java.util.ArrayList<String>();
		row.add("n");
		row.add("Semilla (X0)^2");
		row.add("Resultado");
		row.add("Digitos Tomados");
		row.add("Numero Aleatorio");
		output.add(row);
		for(int i=0;i<n;i++){
			double result=pow(seed,2);
			String sResult=""+(int)result;
			if(result<100){sResult='0'+sResult;}
			if(result<10){sResult='0'+sResult;}
			int start=sResult.length()/2-k/2;
			if(sResult.length()%2!=0&&k%2==0){start++;}
			String taken=sResult.substring(start,start+k);

			row=new ArrayList<String>();
			row.add(""+(i+1));
			row.add("("+seed+")^2");
			row.add(format("%.0f",new Double(result)));
			row.add(taken);
			row.add("0."+taken);
			output.add(row);

			seed=parseInt(taken);
		}
		return output;
	}
	public static ArrayList<ArrayList<String>>multiplyMethod(int n,int k,int seed1,int seed2){
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new ArrayList<String>();
		row.add("n");
		row.add("Semilla (X0*X1)");
		row.add("Resultado");
		row.add("Digitos Tomados");
		row.add("Numero Aleatorio");
		output.add(row);
		for(int i=0;i<n;i++){
			double result=seed1*seed2;
			String sResult=""+(int)result;
			if(result<100){sResult='0'+sResult;}
			if(result<10){sResult='0'+sResult;}
			int start=sResult.length()/2-k/2;
			if(sResult.length()%2!=0&&k%2==0){start++;}
			String taken=sResult.substring(start,start+k);

			row=new ArrayList<String>();
			row.add(""+(i+1));
			row.add("("+seed1+")("+seed2+')');
			row.add(format("%.0f",new Double(result)));
			row.add(taken);
			row.add("0."+taken);
			output.add(row);

			seed1=seed2;
			seed2=parseInt(taken);
		}
		return output;
	}
	public static ArrayList<ArrayList<String>>constantMethod(int n,int k,int seed,int y){
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new ArrayList<String>();
		row.add("n");
		row.add("Semilla (i*X0)");
		row.add("Resultado");
		row.add("Digitos Tomados");
		row.add("Numero Aleatorio");
		output.add(row);
		for(int i=0;i<n;i++){
			double result=y*seed;
			String sResult=""+(int)result;
			if(result<100){sResult='0'+sResult;}
			if(result<10){sResult='0'+sResult;}
			int start=sResult.length()/2-k/2;
			if(sResult.length()%2!=0&&k%2==0){start++;}
			String taken=sResult.substring(start,start+k);

			row=new ArrayList<String>();
			row.add(""+(i+1));
			row.add("("+y+")("+seed+')');
			row.add(format("%.0f",new Double(result)));
			row.add(taken);
			row.add("0."+taken);
			output.add(row);

			seed=parseInt(taken);
		}
		return output;
	}
	public static ArrayList<ArrayList<String>>mixedMethod(int n,int a,int c,int seed,int m){
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new ArrayList<String>();
		row.add("n");
		row.add("Xn");
		row.add("(aXn+C)mod m");
		row.add("Xn+1");
		row.add("Numero Uniforme");
		row.add("Numero Aleatorio");
		output.add(row);
		for(int i=0;i<=n;i++){
			double result=a*seed+c;
			int iResult=(int)(result/m);
			result%=m;

			row=new ArrayList<String>();
			row.add(""+i);
			row.add(""+seed);
			row.add(iResult+" + "+format("%.0f",new Double(result))+"/"+m);
			row.add(""+(seed=(int)result));
			row.add(format("%.0f",new Double(result))+"/"+m);
			row.add(""+result/m);
			output.add(row);
		}
		return output;
	}
	public static ArrayList<ArrayList<String>>decimalConguentMethod(int n,int a,int seed){
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new ArrayList<String>();
		row.add("i");
		row.add("ani");
		row.add("ni+1");
		row.add("ri+1");
		output.add(row);
		for(int i=0;i<n;i++){
			double result=a*seed;
			String sResult=pad(format("%.0f",new Double(result)),8,'0',PADDING_LEFT);

			row=new ArrayList<String>();
			row.add(""+i);
			row.add("("+a+")("+seed+")="+sResult);
			row.add(""+(seed=parseInt(sResult.substring(sResult.length()-4))));
			row.add("0."+seed);
			output.add(row);
		}
		return output;
	}
	public static ArrayList<ArrayList<String>>binaryMethod(int n,int a,int seed,int m){
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new ArrayList<String>();
		row.add("i");
		row.add("ani");
		row.add("ni+1");
		row.add("ri+1");
		output.add(row);
		for(int i=0;i<n;i++){
			double result=a*seed;
			String sResult=pad(toBinaryString((int)result),8,'0',PADDING_LEFT);
			String binary1=pad(toBinaryString(a),4,'0',PADDING_LEFT);
			String binary2=pad(toBinaryString(seed),4,'0',PADDING_LEFT);
			seed=parseInt(sResult.substring(sResult.length()-4),2);

			row=new ArrayList<String>();
			row.add(""+i);
			row.add("("+binary1+")("+binary2+")="+sResult);
			row.add(pad(toBinaryString(seed),4,'0',PADDING_LEFT));
			row.add(""+(double)seed/(double)m);
			output.add(row);
		}
		return output;
	}
	public static ArrayList<ArrayList<String>>ownMethod(int n,int k,int iseed){
		BigInteger seed=BigInteger.valueOf(iseed);
		ArrayList<ArrayList<String>>output=new ArrayList<ArrayList<String>>();
		ArrayList<String>row=new ArrayList<String>();
		row.add("n");
		row.add("xi");
		row.add("\u03A3xi");
		row.add("(\u03A3xi mod 3)+3");
		row.add("m");
		row.add("xi^m+\u03A3xi");
		row.add("xi+1");
		row.add("r");
		output.add(row);
		for(int i=0;i<n;i++){
			String sResult=pad(format("%d",seed),k,'0',PADDING_LEFT);
			String last=sResult.substring(sResult.length()-k);
			Double sigma=sigma(last);
			int m=sigma.intValue()%3+3;
			seed=new BigInteger(last).pow(m).add(BigInteger.valueOf((int)sigma.doubleValue()));

			row=new ArrayList<String>();
			row.add(""+(i+1));
			row.add(last);
			row.add(format("%.0f",sigma));
			row.add("("+sigma+" mod 3)+3");
			row.add(""+m);

			sResult=pad(format("%d",seed),k,'0',PADDING_LEFT);
			last=sResult.substring(sResult.length()-k);
			seed=new BigInteger(last);

			row.add(sResult);
			row.add(last);
			row.add("0."+last);
			output.add(row);
		}
		return output;
	}
	public static ChartPanel drawChart(Object x[][],Object y[][]){
		if(x.length!=y.length){return null;}
		XYSeries serie=new XYSeries("Coordenadas");
		for(int i=0;i<x.length;i++){
			Object xArray[]=x[i],yArray[]=y[i];
			if(xArray.length!=yArray.length){return null;}
			for(int j=0;j<xArray.length;j++){
				Object xValue,yValue;
				if((xValue=xArray[j])!=null&&(yValue=yArray[j])!=null){
					serie.add(parseDouble(xValue.toString()),parseDouble(yValue.toString()));
				}
			}
		}
		XYSeriesCollection dataset=new XYSeriesCollection();
		dataset.addSeries(serie);
		JFreeChart chart=createXYLineChart("Datos Aleatorios","Eje X","Eje Y",dataset,VERTICAL,true,true,false);
		ChartPanel panel=new ChartPanel(chart);
		panel.setSize(panel.getPreferredSize());
		return panel;
	}
	public static JLabel[][]drawSquare(Object x[][],Object y[][],int both){return drawSquare(x,y,both,both);}
	public static JLabel[][]drawSquare(Object x[][],Object y[][],int width,int height){
		return drawSquare(x,y,new Dimension(width,height));
	}
	public static JLabel[][]drawSquare(Object x[][],Object y[][],Dimension size){
		ArrayList<Double>xs=new ArrayList<Double>(),ys=new ArrayList<Double>();
		for(Object[]arr:x){for(Object obj:arr){if(obj!=null){xs.add(valueOf(obj.toString()));}}}
		for(Object[]arr:y){for(Object obj:arr){if(obj!=null){ys.add(valueOf(obj.toString()));}}}
		int length=xs.size();
		HashSet<Double>sx=new HashSet<Double>(),sy=new HashSet<Double>();
		for(int i=0;i<xs.size();i++){
			sx.add(new Double(xs.get(i).doubleValue()*length));
			if(sx.size()==xs.size()/2){break;}
		}
		for(int i=0;i<ys.size();i++){
			sy.add(new Double(ys.get(i).doubleValue()*length));
			if(sy.size()==ys.size()/2){break;}
		}
		JLabel output[][]=new JLabel[length][length];
		for(int i=0;i<length;i++){for(int j=0;j<length;j++){
			output[i][j]=new JLabel();
			output[i][j].setBounds(i*size.width,j*size.height,size.width,size.height);
		}}
		Iterator<Double>xit=sx.iterator(),yit=sy.iterator();
		while(xit.hasNext()&&yit.hasNext()){
			int xi=xit.next().intValue(),yi=yit.next().intValue();
			output[xi][yi].setBackground(black);
			output[xi][yi].setOpaque(true);
		}
		return output;
	}
}