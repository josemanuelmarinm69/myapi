package myapi.utils;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;
import static java.util.Calendar.getInstance;
import static org.joda.time.Weeks.weeksBetween;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.AbstractAction;
import org.joda.time.DateTime;
public class OfTime{
	private static final Timer timer=new Timer();

	public static Calendar getCalendar(Date date){
		Calendar c=getInstance();
		c.setTime(date);
		return c;
	}
	public static Date[]getWeekBefore(Date date){
		Date week[]=new Date[7];
		week[0]=date;
		for(int i=1;i<7;i++){week[i]=getNthDay(date,-i);}
		return week;
	}
	public static Date[]getWeekAfter(Date date){
		Date week[]=new Date[7];
		week[0]=date;
		for(int i=1;i<7;i++){week[i]=getNthDay(date,i);}
		return week;
	}
	public static Date getDate(){return new Date();}
	public static Date getDate(int year,int month,int day){return createDate(year,month,day);}
	public static Date getDate(int year,String month,int day){return createDate(year,month,day);}
	public static Date getNthDay(Date date,int n){
		Calendar c=getCalendar(date);
		c.add(DAY_OF_MONTH,n);
		return c.getTime();
	}
	public static Date getDayAfter(Date date){return getNthDay(date,1);}
	public static Date getDayBefore(Date date){return getNthDay(date,-1);}
	public static int getYear(Date date){return getCalendar(date).get(YEAR);}
	public static int getMonth(Date date){return getCalendar(date).get(MONTH)+1;}
	public static int getDay(Date date){return getCalendar(date).get(DAY_OF_MONTH);}
	public static int getHour(Date date){return getCalendar(date).get(HOUR_OF_DAY);}
	public static int getMinute(Date date){return getCalendar(date).get(MINUTE);}
	public static int getSecond(Date date){return getCalendar(date).get(SECOND);}
	public static String dateToStringWithNumber(Date date){return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);}
	public static String dateToStringWithName(Date date){
		String output=dateToStringWithNumber(date);
		String month=null;
		int monthNumber=getMonth(date);
		if(monthNumber==1){month="Enero";}
		else if(monthNumber==2){month="Febrero";}
		else if(monthNumber==3){month="Marzo";}
		else if(monthNumber==4){month="Abril";}
		else if(monthNumber==5){month="Mayo";}
		else if(monthNumber==6){month="Junio";}
		else if(monthNumber==7){month="Julio";}
		else if(monthNumber==8){month="Agosto";}
		else if(monthNumber==9){month="Septiembre";}
		else if(monthNumber==10){month="Octubre";}
		else if(monthNumber==11){month="Noviembre";}
		else if(monthNumber==12){month="Diciembre";}
		return output.substring(0,5)+month+output.substring(7);
	}
	public static boolean dateIsBeforeTo(Date date1,Date date2){return compareDates(date1,date2)<0;}
	public static boolean dateIsAfterTo(Date date1,Date date2){return compareDates(date1,date2)>0;}
	public static int compareDates(Date date1,Date date2){
		if(getYear(date1)<getYear(date2)){return -1;}
		else if(getYear(date1)>getYear(date2)){return 1;}
		else if(getMonth(date1)<getMonth(date2)){return -1;}
		else if(getMonth(date1)>getMonth(date2)){return 1;}
		else if(getDay(date1)<getDay(date2)){return -1;}
		else if(getDay(date1)>getDay(date2)){return 1;}
		return 0;
	}
	public static int getWeeksBetween(Date start,Date end){return weeksBetween(new DateTime(start),new DateTime(end)).getWeeks();}
	public static Date stringToDate(String date,boolean inverted,boolean hourless){
		if(inverted){
			String elements[]=date.split("/");
			date=elements[2]+"/"+elements[1]+"/"+elements[0];
		}
		if(hourless){date+=" 00:00:00";}
		try{return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(date);}
		/*--ErrorConsole--*/catch(ParseException e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
		return null;
	}
	public static Date createDate(int year,int month,int day){return stringToDate(year+"/"+month+"/"+day,false,true);}
	public static Date createDate(int year,String month,int day){
		int monthNumber=1;
		if(month.equalsIgnoreCase("feb")||month.equalsIgnoreCase("febrero")||month.equalsIgnoreCase("february")){monthNumber=2;}
		else if(month.equalsIgnoreCase("mar")||month.equalsIgnoreCase("marzo")||month.equalsIgnoreCase("march")){monthNumber=3;}
		else if(month.equalsIgnoreCase("abr")||month.equalsIgnoreCase("abril")||month.equalsIgnoreCase("apr")||month.equalsIgnoreCase("april")){
			monthNumber=4;
		}
		else if(month.equalsIgnoreCase("may")||month.equalsIgnoreCase("mayo")){monthNumber=5;}
		else if(month.equalsIgnoreCase("jun")||month.equalsIgnoreCase("junio")||month.equalsIgnoreCase("june")){monthNumber=6;}
		else if(month.equalsIgnoreCase("jul")||month.equalsIgnoreCase("julio")||month.equalsIgnoreCase("july")){monthNumber=7;}
		else if(month.equalsIgnoreCase("ago")||month.equalsIgnoreCase("agosto")||month.equalsIgnoreCase("aug")||month.equalsIgnoreCase("august")){
			monthNumber=8;
		}
		else if(month.equalsIgnoreCase("sep")||month.equalsIgnoreCase("septiembre")||month.equalsIgnoreCase("september")){monthNumber=9;}
		else if(month.equalsIgnoreCase("oct")||month.equalsIgnoreCase("octubre")||month.equalsIgnoreCase("october")){monthNumber=10;}
		else if(month.equalsIgnoreCase("nov")||month.equalsIgnoreCase("noviembre")||month.equalsIgnoreCase("nomvember")){monthNumber=11;}
		else if(month.equalsIgnoreCase("dic")||month.equalsIgnoreCase("diciembre")||month.equalsIgnoreCase("dec")||month.equalsIgnoreCase("december")){
			monthNumber=12;
		}
		return stringToDate(year+"/"+monthNumber+"/"+day,false,true);
	}
	public static java.sql.Date convertDate(Date date){return new java.sql.Date(date.getTime());}
	public static Date convertDate(java.sql.Date date){return new Date(date.getTime());}
	public static void scheduleAction(final AbstractAction action,Date date){
		timer.schedule(new TimerTask(){
			public void run(){action.actionPerformed(null);}
		},date);
	}
	public static void scheduleAction(final AbstractAction action,String date){
		try{scheduleAction(action,new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(date));}
		catch(ParseException e){}
	}
}