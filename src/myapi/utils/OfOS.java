package myapi.utils;
import static java.lang.Runtime.getRuntime;
import static java.lang.System.getProperty;
import static java.lang.System.out;
import static java.lang.System.err;
import static java.lang.System.in;
import static java.lang.System.setOut;
import static java.lang.System.setErr;
import static java.lang.System.setIn;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
public class OfOS{
	private static PrintStream oldOut;
	private static PrintStream oldErr;
	private static InputStream oldIn;

	public static String getOperatingSystem(){return getProperty("os.name");}
	public static boolean isWindowsOperatingSystem(){return getOperatingSystem().contains("Windows");}
	public static boolean isUnixOperatingSystem(){return getOperatingSystem().contains("Unix");}
	public static boolean execute(String comand){return execute(comand);}
	public static boolean execute(String...comands){
		try{
			Process process=getRuntime().exec(comands);
			BufferedReader output=new BufferedReader(new InputStreamReader(process.getInputStream()));
			BufferedReader errors=new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String line;boolean error=false;
			while((line=output.readLine())!=null){/*--MessageConsole--*/myapi.MessageConsole.addText(line);/*--MessageConsole--*/}
			while((line=errors.readLine())!=null){/*--ErrorConsole--*/myapi.ErrorConsole.addText(line);/*--ErrorConsole--*/error=true;}
			return!error;
		}
		catch(IOException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return false;
	}
	public static boolean openFile(File file){return openFile(file.getAbsolutePath());}
	public static boolean openFile(String fileName){
		if(isWindowsOperatingSystem()){return execute("rundll32","url.dll,FileProtocolHandler","\""+fileName+"\"");}
		return false;
	}
	public static ByteArrayOutputStream setNewOutputStream(ByteArrayOutputStream stream){
		if(oldOut!=null){return null;}
		oldOut=out;
		setOut(new PrintStream(stream));
		return stream;
	}
	public static ByteArrayOutputStream setNewOutputStream(){return setNewOutputStream(new ByteArrayOutputStream());}
	public static PrintStream restoreOutputStream(){
		out.flush();
		PrintStream ps=out;
		setOut(oldOut);
		return ps;
	}
	public static ByteArrayOutputStream setNewErrorStream(ByteArrayOutputStream stream){
		if(oldErr!=null){return null;}
		oldErr=err;
		setErr(new PrintStream(stream));
		return stream;
	}
	public static ByteArrayOutputStream setNewErrorStream(){return setNewErrorStream(new ByteArrayOutputStream());}
	public static PrintStream restoreErrorStream(){
		err.flush();
		PrintStream ps=err;
		setErr(oldErr);
		return ps;
	}
	public static ByteArrayInputStream setNewInputStream(ByteArrayInputStream stream){
		if(oldIn!=null){return null;}
		oldIn=in;
		setIn(stream);
		return stream;
	}
	public static InputStream restoreInputStream(){
		InputStream is=in;
		setIn(oldIn);
		return is;
	}
}