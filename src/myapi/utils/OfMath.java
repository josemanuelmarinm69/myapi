package myapi.utils;
import static java.lang.Double.parseDouble;
import static java.lang.Double.valueOf;
import static java.lang.Integer.parseInt;
import static java.lang.Math.pow;
import static java.lang.String.format;
import static java.math.RoundingMode.DOWN;
import static java.math.RoundingMode.HALF_UP;
import static myapi.utils.OfData.configureFormatter;
import static myapi.utils.OfData.isEmptyString;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Stack;
import java.util.Vector;
public class OfMath{
	private static Vector<String>targetVector;
	private static Random random;
	private static final HashMap<Character,Integer>priority;
	private static ArrayList<String>posfixExpression;
	private static ArrayList<String>infixExpression;

	static{
		priority=new HashMap<Character,Integer>();
		priority.put(new Character('+'),new Integer(1));
		priority.put(new Character('-'),new Integer(1));
		priority.put(new Character('*'),new Integer(2));
		priority.put(new Character('/'),new Integer(2));
		priority.put(new Character('^'),new Integer(3));
	}
	public static long getFactorial(long n)throws ArithmeticException{
		if(n<0){throw new ArithmeticException();}
		long f=1;
		for(int i=1;i<=n;i++){f*=i;}
		return f;
	}
	public static double getFactorial(double n){return(n==0)?1:n*getFactorial(n-1);}
	public static double trunkTo(double value,int integerDigits,int decimalDigits){
		return parseDouble(configureFormatter(integerDigits,decimalDigits).format(value));
	}
	public static double trunkTo(double value,int digits){return roundTo(value,digits,DOWN);}
	public static double roundTo(double value,int digits){return roundTo(value,digits,HALF_UP);}
	public static double roundTo(double value,int digits,RoundingMode mode){
		String valueString=""+value;
		BigDecimal decimal=new BigDecimal(valueString);
		decimal=decimal.setScale(digits,mode);
		return decimal.doubleValue();
	}
	public static Double sigma(String number){
		number=format("%.0f",valueOf(number));
		double array[]=new double[number.length()];
		for(int i=0;i<number.length();i++){array[i]=parseDouble(""+number.charAt(i));}
		return sigma(array);
	}
	public static Double sigma(double array[]){
		if(array==null||array.length<1){return null;}
		Double sigma=new Double(array[0]);
		array[0]=0;
		for(double item:array){sigma=new Double(sigma.doubleValue()+item);}
		return sigma;
	}
	public static Vector<String>permute(String string){
		random=new Random();
		targetVector=new Vector<String>();
		long size=getFactorial(string.length());
		for(int i=0;i<size;i++){
			int intArray[]=new int[string.length()];
			for(int j=0;j<intArray.length;j++){intArray[j]=-1;}
			
			for(int j=0;j<intArray.length;j++){
				boolean continueFlag=true;
				do{
					boolean repeat=false;
					int randomNumber=random.nextInt(string.length());
					for(int k=0;k<intArray.length;k++){
						if(randomNumber==intArray[k]){repeat=true;}
					}
					if(repeat){continueFlag=true;}
					else{intArray[j]=randomNumber;continueFlag=false;}
				}while(continueFlag);
			}
			String temp="";
			for(int j=0;j<intArray.length;j++){temp+=intArray[j];}
			boolean repeat=false;
			for(int j=0;j<targetVector.size();j++){
				if(temp.equals(targetVector.get(j))){repeat=false;}
			}
			if(repeat){i--;}
			else{targetVector.add(temp);}
		}
		for(int i=0;i<targetVector.size();i++){
			String temp="";
			char charArray[]=targetVector.get(i).toCharArray();
			for(int j=0;j<charArray.length;j++){temp+=string.charAt(parseInt(""+charArray[j]));}
			targetVector.set(i,temp);
		}
		return targetVector;
	}
	public static ArrayList<String>getPosfixExpression(){return posfixExpression;}
	public static ArrayList<String>getInfixExpression(){return infixExpression;}
	public static synchronized ArrayList<String>convertToPosfix(String exp){
		posfixExpression=new ArrayList<String>();
		infixExpression=new ArrayList<String>();
		Stack<Stack<Character>>stacks=new Stack<Stack<Character>>();
		String operand="";
		Stack<Character>stack=new Stack<Character>();
		for(char c:exp.toCharArray()){
			switch(c){
				case'+':
				case'-':
				case'*':
				case'/':
				case'^':
					if(!isEmptyString(operand)){
						posfixExpression.add(operand);
						infixExpression.add(operand);
						operand="";
					}
					if(stack.size()>0){
						Character operator;
						while(stack.size()>0&&!(priority.get(new Character(c)).intValue()>priority.get((operator=stack.peek())).intValue())){
							operator=stack.pop();
							posfixExpression.add(""+operator);
						}
					}
					stack.push(new Character(c));
					infixExpression.add(""+c);
					break;
				case'(':
					stacks.push(stack);
					stack=new Stack<Character>();
					infixExpression.add(""+c);
					break;
				case')':
					if(!isEmptyString(operand)){
						posfixExpression.add(operand);
						infixExpression.add(operand);
						operand="";
					}
					while(stack.size()>0){
						String string=""+stack.pop();
						posfixExpression.add(string);
					}
					stack=stacks.pop();
					infixExpression.add(""+c);
					break;
				default:operand+=c;
			}
		}
		if(!isEmptyString(operand)){
			posfixExpression.add(operand);
			infixExpression.add(operand);
		}
		while(stack.size()>0){posfixExpression.add(""+stack.pop());}
		return posfixExpression;
	}
	public static synchronized ArrayList<String>convertToPosfix(ArrayList<String>exp){
		String output="";
		for(String string:exp){output+=string;}
		return convertToPosfix(output);
	}
	public static double evaluate(ArrayList<String>posfixExpression){
		Stack<Double>operands=new Stack<Double>();
		for(String item:posfixExpression){
			try{operands.push(valueOf(item));}
			catch(NumberFormatException e){
				double b=operands.pop().doubleValue();
				double a=operands.pop().doubleValue();
				switch(item.charAt(0)){
					case'+':operands.push(new Double(a+b));break;
					case'-':operands.push(new Double(a-b));break;
					case'*':operands.push(new Double(a*b));break;
					case'/':operands.push(new Double(a/b));break;
					case'^':operands.push(new Double(pow(a,b)));break;
					default:throw e;
				}
			}
		}
		return operands.pop().doubleValue();
	}
	public static double evaluate(){return evaluate(posfixExpression);}
	public static int convertToPercent(int advance,int total){
		double advanceD=advance,totalD=total;
		return(int)(advanceD/totalD*100);
	}
}