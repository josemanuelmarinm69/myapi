package myapi.utils;
import static java.awt.Font.BOLD;
import static java.awt.Font.ITALIC;
import static java.awt.MouseInfo.getPointerInfo;
import static java.awt.Toolkit.getDefaultToolkit;
import static java.util.Locale.getAvailableLocales;
import static javax.swing.BoxLayout.X_AXIS;
import static javax.swing.BoxLayout.Y_AXIS;
import static javax.swing.SwingConstants.CENTER;
import static javax.swing.UIManager.get;
import static javax.swing.UIManager.getDefaults;
import static javax.swing.UIManager.put;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import static myapi.MyImageHelper.getScaledIcon;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Locale;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.plaf.FontUIResource;
import org.jdesktop.swingworker.SwingWorker;
import javax.swing.text.JTextComponent;
import myapi.gui.MyWindow;
public class OfGUI{
	public static final Color fluorescent=new Color(169,219,0);
	public static final Color silver=new Color(177,177,177);
	public static final Font verdana=new Font("Verdana",ITALIC,12);
	public static final Font dialog=new Font("Dialog",BOLD,12);
	public static final int tabSize=22;
	public static class MyWorker extends SwingWorker<Void,Void>{
		private Method process;
		private Method finish;
		private Object instance;

		public MyWorker(Method process,Method finish,Object instance){
			this.process=process;
			this.finish=finish;
			this.instance=instance;
		}
		@Override public Void doInBackground(){
			try{process.invoke(instance,(Object[])null);}
			catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(InvocationTargetException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			return null;
		}
		@Override public void done(){
			try{finish.invoke(instance,(Object[])null);}
			catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(InvocationTargetException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		}
		public final void updateProgress(int progress){setProgress(progress);}
	}

	public static String convertTabToSpace(String string){
		String lines[]=string.split("\n"),output="";
		for(String line:lines){
			String tabs[]=line.split("\t");
			int spaces=0;
			for(String tab:tabs){
				output+=tab;
				if(tab.length()>tabSize){spaces=tab.length()%tabSize;}
				else{spaces=tabSize-tab.length();}
				for(int i=0;i<spaces;i++){output+=" ";}
			}
			output=output.substring(0,output.length()-spaces);
			output+="\n";
		}
		output=output.substring(0,output.length()-1);
		return output;
	}
	public static int getTextWidth(Component c){
		if(c instanceof JLabel){return getTextWidth(((JLabel)c).getText(),c);}
		else if(c instanceof AbstractButton){return getTextWidth(((AbstractButton)c).getText(),c);}
		else if(c instanceof JTextComponent){return getTextWidth(((JTextComponent)c).getText(),c);}
		return 0;
	}
	public static int getTextWidth(String text,Component c){
		int width=getTextWidth(text,c.getFontMetrics(c.getFont()),c instanceof JTable||c instanceof JLabel||c instanceof AbstractButton);
		if(c instanceof JRadioButton||c instanceof JCheckBox){width+=15;}
		if(c instanceof Container){
			Insets insets=((Container)c).getInsets();
			width+=insets.left+insets.right;
		}
		return width;
	}
	public static int getTextWidth(String text,Font font,Component c){
		int width=getTextWidth(text,c.getFontMetrics(font),c instanceof JTable||c instanceof JLabel||c instanceof AbstractButton);
		if(c instanceof JRadioButton||c instanceof JCheckBox){width+=15;}
		if(c instanceof Container){
			Insets insets=((Container)c).getInsets();
			width+=insets.left+insets.right;
		}
		return width;
	}
	private static int getTextWidth(String text,FontMetrics fontMetrics,boolean isEllipsisLikely){
		text=convertTabToSpace(text);
		int width=0;
		String lines[]=text.split("\n");
		for(String line:lines){
			int x=fontMetrics.stringWidth(line);
			if(isEllipsisLikely){x+=fontMetrics.stringWidth("   ");}
			if(x>width){width=x;}
		}
		return width;
	}
	public static int getTextHeight(Component c){
		if(c instanceof JLabel){return getTextHeight(((JLabel)c).getText(),c);}
		else if(c instanceof AbstractButton){return getTextHeight(((AbstractButton)c).getText(),c);}
		else if(c instanceof JTextComponent){return getTextHeight(((JTextComponent)c).getText(),c);}
		return 0;
	}
	public static int getTextHeight(String text,Component c){return getTextHeight(text,c.getFont(),c);}
	public static int getTextHeight(String text,Font font,Component c){
		FontMetrics fontMetrics=c.getFontMetrics(font);
		return(int)fontMetrics.getStringBounds(text,c.getGraphics()).getHeight();
	}
	public static void setTitleFont(Font font){
		Enumeration<Object>keys=getDefaults().keys();
		while(keys.hasMoreElements()){
			Object key=keys.nextElement();
			Object value=get(key);
			if(value instanceof FontUIResource){put(key,new FontUIResource(font));}
			else if(value instanceof Font){put(key,new FontUIResource(font));}
		}
	}
	public static void setFileChooserFont(JFileChooser chooser,Font font){setFileChooserFont(chooser.getComponents(),font);}
	private static void setFileChooserFont(Component components[],Font font){
		for(int x=0;x<components.length;x++){
			if(components[x]instanceof Container){setFileChooserFont(((Container)components[x]).getComponents(),font);}
			components[x].setFont(font);
		}
	}
	public static Point getCenter(Component component){
		Dimension d=getDefaultToolkit().getScreenSize();
		return new Point((d.width/2)-(component.getWidth()/2),(d.height/2)-(component.getHeight()/2));
	}
	public static boolean mouseOver(Component component){
		if(!component.isShowing()||!component.isVisible()){return false;}
		Point b=component.getLocationOnScreen();
		return mouseOverArea(b,component.getWidth(),component.getHeight());
	}
	public static boolean mouseOverArea(Point b,int width,int height){
		Point a=mouseLocation();
		if(a.x>=b.x&&a.x<=b.x+width&&a.y>=b.y&&a.y<=b.y+height){return true;}
		return false;
	}
	public static Point mouseLocation(){return getPointerInfo().getLocation();}
	public static MyWindow testPanel(JPanel panel,String title,Point location,boolean resizable,int closeOperation){
		if(title==null){title="Prueba de panel";}
		MyWindow frame=new MyWindow(null,resizable,new Dimension(panel.getWidth()+5,panel.getHeight()+27));
		frame.setDefaultCloseOperation(closeOperation);
		frame.setTitle(title);
		frame.setContentPane(panel);
		frame.pack();
		if(location==null){location=getCenter(panel);}
		frame.setLocation(location);
		frame.setVisible(true);
		return frame;
	}
	public static MyWindow testPanel(JPanel panel,String title,Point location,boolean resizable){
		return testPanel(panel,title,location,resizable,DISPOSE_ON_CLOSE);
	}
	public static MyWindow testPanel(JPanel panel,String title,Point location,int close){return testPanel(panel,title,location,true,close);}
	public static MyWindow testPanel(JPanel panel,String title,Point location){return testPanel(panel,title,location,true);}
	public static MyWindow testPanel(JPanel panel,String title,boolean resizable,int close){return testPanel(panel,title,null,resizable,close);}
	public static MyWindow testPanel(JPanel panel,String title,boolean resizable){return testPanel(panel,title,null,resizable);}
	public static MyWindow testPanel(JPanel panel,Point location,boolean resizable,int close){return testPanel(panel,null,location,resizable,close);}
	public static MyWindow testPanel(JPanel panel,Point location,boolean resizable){return testPanel(panel,null,location,resizable);}
	public static MyWindow testPanel(JPanel panel,String title,int close){return testPanel(panel,title,null,true,close);}
	public static MyWindow testPanel(JPanel panel,String title){return testPanel(panel,title,null,true);}
	public static MyWindow testPanel(JPanel panel,Point location,int close){return testPanel(panel,null,location,true,close);}
	public static MyWindow testPanel(JPanel panel,Point location){return testPanel(panel,null,location,true);}
	public static MyWindow testPanel(JPanel panel,boolean resizable,int close){return testPanel(panel,null,null,resizable,close);}
	public static MyWindow testPanel(JPanel panel,boolean resizable){return testPanel(panel,null,null,resizable);}
	public static MyWindow testPanel(JPanel panel){return testPanel(panel,null,null,true);}
	public static MyWindow testPanel(JPanel panel,int close){return testPanel(panel,null,null,true,close);}
	public static JPanel createPanelWithBoxLayout(boolean horizontal){
		JPanel panel=new JPanel(null);
		panel.setLayout(new BoxLayout(panel,horizontal?X_AXIS:Y_AXIS));
		return panel;
	}
	public static MyWorker createWorker(String processMethod,String finishMethod,Class<?>methodsClass){
		Method process=null;
		Method finish=null;
		try{
			process=methodsClass.getMethod(processMethod,(Class<?>[])null);
			finish=methodsClass.getMethod(finishMethod,(Class<?>[])null);
		}
		catch(NoSuchMethodException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(SecurityException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return new MyWorker(process,finish,null);
	}
	public static MyWorker createWorker(String processMethod,String finishMethod,Object instance){
		Class<?>methodsClass=instance.getClass();
		Method process=null;
		Method finish=null;
		try{
			process=methodsClass.getMethod(processMethod,(Class<?>[])null);
			finish=methodsClass.getMethod(finishMethod,(Class<?>[])null);
		}
		catch(NoSuchMethodException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(SecurityException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return new MyWorker(process,finish,instance);
	}
	public static void syncJProgressBarWithWorker(final JProgressBar bar,SwingWorker<?,?>task){
		task.addPropertyChangeListener(new PropertyChangeListener(){
			public void propertyChange(PropertyChangeEvent e){
				if(e.getPropertyName()=="progress"){bar.setValue(((Integer)e.getNewValue()).intValue());}
			}
		});
		
	}
	public static void addButtonIcons(AbstractButton button,ImageIcon icon,ImageIcon overIcon,
			ImageIcon pressedIcon,ImageIcon selectedIcon,ImageIcon disabledIcon,boolean adjust){
		if(adjust){
			button.setSize(pressedIcon.getIconWidth(),pressedIcon.getIconHeight());
			button.setPreferredSize(new Dimension(button.getWidth(),button.getHeight()));
		}
		else{
			Dimension size=getGreaterDimension(button.getSize(),button.getPreferredSize());
			icon=getScaledIcon(icon,size);
			overIcon=getScaledIcon(overIcon,size);
			pressedIcon=getScaledIcon(pressedIcon,size);
			selectedIcon=getScaledIcon(selectedIcon,size);
			disabledIcon=getScaledIcon(disabledIcon,size);
			button.setSize(size);
			button.setPreferredSize(size);
		}
		if(icon!=null){button.setIcon(icon);}
		if(overIcon!=null){button.setRolloverIcon(overIcon);}
		if(pressedIcon!=null){button.setPressedIcon(pressedIcon);}
		if(selectedIcon!=null){button.setSelectedIcon(selectedIcon);}
		if(disabledIcon!=null){button.setDisabledIcon(disabledIcon);}
		button.setFocusPainted(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setHorizontalTextPosition(CENTER);
	}
	public static void addToggleButtonIcons(JToggleButton button,final ImageIcon onIcon,final ImageIcon offIcon){
		button.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0){
				if(arg0.getStateChange()==ItemEvent.SELECTED){((AbstractButton)arg0.getSource()).setIcon(onIcon);}
				else{((AbstractButton)arg0.getSource()).setIcon(offIcon);}
			}
		});
		button.setIcon(onIcon);
		button.setFocusable(false);
		button.setFocusPainted(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setSize(onIcon.getIconWidth(),onIcon.getIconHeight());
		button.setPreferredSize(new Dimension(button.getWidth(),button.getHeight()));
	}
	public static Dimension getGreaterDimension(Dimension dimension1,Dimension dimension2){
		if(isGreater(dimension1,dimension2)){return dimension1;}
		return dimension2;
	}
	public static Dimension modifyDimension(Dimension dimension,int both){return modifyDimension(dimension,both,both);}
	public static Dimension modifyDimension(Dimension dimension,int width,int height){return new Dimension(dimension.width+width,dimension.height+height);}
	public static boolean isGreater(Dimension dimension1,Dimension dimension2){
		return dimension1.width>dimension2.width&&dimension1.height>dimension2.height;
	}
	public static Locale getFirstLocaleByCountry(String country){
		Locale locales[]=getAvailableLocales();
		for(Locale locale:locales){if(locale.getCountry().equalsIgnoreCase(country)){return locale;}}
		return null;
	}
	public static Locale getLocaleByLanguageAndCountry(String language,String country){
		Locale locales[]=getAvailableLocales();
		for(Locale locale:locales){
			if(locale.getCountry().equalsIgnoreCase(country)&&locale.getLanguage().equalsIgnoreCase(language)){return locale;}
		}
		return null;
	}
}