package myapi.utils;
import static java.awt.event.KeyEvent.CHAR_UNDEFINED;
import static java.lang.Character.isISOControl;
import static java.lang.Character.UnicodeBlock.SPECIALS;
import static java.lang.Character.UnicodeBlock.of;
import static java.lang.Math.min;
import static java.lang.System.arraycopy;
import static java.lang.reflect.Array.newInstance;
import static java.util.Arrays.asList;
import static java.util.Arrays.binarySearch;
import static myapi.utils.OfData.PaddingPosition.PADDING_RIGHT;
import java.lang.Character.UnicodeBlock;
import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
public class OfData{
	public static enum PaddingPosition{PADDING_RIGHT,PADDING_LEFT}
	public static enum TrunkPosition{TRUNK_RIGHT,TRUNK_LEFT}
	public static<T>List<T>convertArrayToList(T array[]){return new LinkedList<T>(asList(array));}
	public static<T>T[]convertCollectionToArray(Collection<T>collection,T array[]){return collection.toArray(array);}
	public static<T>T[]deleteDuplicates(T array[]){
		HashSet<T>set=new HashSet<T>();
		for(int i=0;i<array.length;i++){set.add(array[i]);}
		return convertCollectionToArray(set,array);
	}
	public static<T>T[]copyOf(T[]array){return copyOf(array,array.length);}
	@SuppressWarnings("unchecked")
	public static<T>T[]copyOf(T[]array,int length){
		T[]newArray=(T[])newInstance(array.getClass().getComponentType(),length);
		arraycopy(array,0,newArray,0,min(array.length,newArray.length));
		return newArray;
	}
	public static boolean contains(String[]array,String element){
		return contains(array,element,new Comparator<String>(){
			public int compare(String element1,String element2){return element1==element2?0:element1!=null?element1.compareToIgnoreCase(element2):-1;}
		});
	}
	public static<T>boolean contains(T[]array,T element,Comparator<T>comparator){return array!=null&&comparator!=null&&binarySearch(array,element,comparator)>=0;}
	public static int[]bubbleSort(int array[]){
		for(int i=0;i<array.length-1;i++){
			//order
			if(array[i]>array[i+1]){
				int move=array[i];
				array[i]=array[i+1];
				array[i+1]=move;
				//start again
				i=-1;
			}
		}
		return array;
	}
	public static int[]shellSort(int array[]){
		int gap=array.length;
		//decreasing gap by half
		while((gap/=2)>0){
			boolean restart=false;
			for(int i=0;i+gap<array.length;){
				//order
				if(array[i]>array[i+gap]){
					int move=array[i];
					array[i]=array[i+gap];
					array[i+gap]=move;
					restart=true;
				}
				//make another pass
				if(++i+gap>=array.length&&restart){restart=(i=0)>0;}
			}
		}
		return array;
	}
	public static int[]mergeSort(int array[]){
		if(array.length<2){return array;}
		//divide
		int leftSize=array.length/2,rightSize=array.length-leftSize,index=0;
		int left[]=new int[leftSize],right[]=new int[rightSize];
		//filling
		for(int i=0;i<leftSize;i++){left[i]=array[index++];}
		for(int i=0;i<rightSize;i++){right[i]=array[index++];}
		//ordering divided
		left=mergeSort(left);
		right=mergeSort(right);
		int i,j,k;i=j=k=0;
		//merging
		for(;i<leftSize&&j<rightSize;){array[k++]=left[i]<right[j]?left[i++]:right[j++];}
		//lasted
		for(;i<leftSize;){array[k++]=left[i++];}
		for(;j<rightSize;){array[k++]=right[j++];}
		return array;
	}
	public static int[]quickSort(int array[]){
		if(array.length<2){return array;}
		//select pivot
		int pivotIndex=new Random().nextInt(array.length),pivot=array[pivotIndex];
		//divide
		int left[]=new int[array.length],right[]=new int[array.length];
		int k,leftSize,rightSize,data;leftSize=rightSize=k=0;
		//filling
		for(;k<pivotIndex;){
			if((data=array[k++])<pivot){left[leftSize++]=data;}
			else{right[rightSize++]=data;}
		}
		for(k++;k<array.length;){
			if((data=array[k++])<pivot){left[leftSize++]=data;}
			else{right[rightSize++]=data;}
		}
		//ordering divided
		int newLeft[]=new int[leftSize],newRight[]=new int[rightSize];
		for(int i=0;i<leftSize;){newLeft[i]=left[i++];}
		for(int i=0;i<rightSize;){newRight[i]=right[i++];}
		left=quickSort(left=newLeft);
		right=quickSort(right=newRight);
		//merging
		k=0;
		for(int i=0;i<leftSize;){array[k++]=left[i++];}
		array[k++]=pivot;
		for(int i=0;i<rightSize;){array[k++]=right[i++];}
		return array;
	}
	public static int[]heapSort(int array[]){
		int length=array.length;
		//build heap
		for(int i=length/2-1;i>=0;i--){heapify(array,length,i);}
		//extract from heap
		for(int i=length-1;i>=0;i--){
			//root to end
			int move=array[0];
			array[0]=array[i];
			array[i]=move;
			//max heapify on reduced heap
			heapify(array,i,0);
		}
		return array;
	}
	public static void heapify(int array[],int length,int root){
		int largest=root;
		int left=2*root+1;
		int right=2*root+2;
		//left child larger than root
		if(left<length&&array[left]>array[largest]){largest=left;}
		//right child larger than largest
		if(right<length&&array[right]>array[largest]){largest=right;}
		//largest not root
		if(largest!=root){
			int swap=array[root];
			array[root]=array[largest];
			array[largest]=swap;
			//heapify sub-tree
			heapify(array,length,largest);
		}
	}
	public static String getPrintableChars(String chars){
		String output="";
		for(int i=0;i<chars.length();i++){
			char c=chars.charAt(i);
			if(isPrintableChar(c)){output+=c;}
		}
		return output;
	}
	public static boolean isPrintableChar(char c){
		UnicodeBlock block=of(c);
		return(!isISOControl(c))&&c!=CHAR_UNDEFINED&&block!=null&&block!=SPECIALS;
	}
	public static boolean isEmptyString(String string){return string.length()==0;}
	public static ArrayList<String>splitByCharNum(String string,int num){
		ArrayList<String>strings=new ArrayList<String>();
		int index=0;
		while(index<string.length()){
			strings.add(string.substring(index,min(index+num,string.length())));
			index+=num;
		}
		return strings;
	}
	public static String pad(String string,int length,char character,PaddingPosition position){
		if(string==null||string.length()>=length){return string;}
		StringBuilder builder=new StringBuilder();
		for(int i=0;i<length-string.length();i++){builder.append(character);}
		switch(position){
			case PADDING_RIGHT:return new StringBuilder(string).append(builder.toString()).toString();
			case PADDING_LEFT:return builder.append(string).toString();
		}
		return string;
	}
	public static String trunk(String string,int length,TrunkPosition position){
		if(string==null||string.length()<=length){return string;}
		switch(position){
			case TRUNK_RIGHT:return string.substring(0,length);
			case TRUNK_LEFT:return string.substring(string.length()-length);
		}
		return string;
	}
	public static String assertLength(String string,int length,char character,TrunkPosition trunk,PaddingPosition padding){
		string=string==null||"NULL".equalsIgnoreCase(string)?"":string;
		return string.length()==length?string:string.length()>length?trunk(string,length,trunk):pad(string,length,character,padding);
	}
	public static DecimalFormat configureFormatter(int integerDigits,int decimalDigits)throws InvalidParameterException{
		if(integerDigits<1||decimalDigits<1){throw new InvalidParameterException("Integer and Decimal Digits must be natural numbers above zero");}
		DecimalFormat formatter=new DecimalFormat(pad("",integerDigits,'0',PADDING_RIGHT)+'.'+pad("",decimalDigits,'0',PADDING_RIGHT));
		return formatter;
	}
	public static ArrayList<String>parseURL(String url,String firstSegment){
		if(url==null||isEmptyString(url)){return null;}
		ArrayList<String>list=new ArrayList<String>();
		for(String part:url.split("/")){if(part!=null&&!isEmptyString(part)){list.add(part);}}
		if(firstSegment!=null&&list.size()>1&&!firstSegment.equalsIgnoreCase(list.get(0))){list.remove(0);}
		return list;
	}
	//public static String[]getBundleStringArray(ResourceBundle bundle,String key){return null;}
}