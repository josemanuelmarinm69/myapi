package myapi.utils;
import static darrylbu.util.SwingUtils.getDescendantOfType;
import static java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager;
import static java.awt.event.ActionEvent.ACTION_PERFORMED;
import static java.awt.event.KeyEvent.KEY_PRESSED;
import static java.awt.event.KeyEvent.KEY_RELEASED;
import static java.awt.event.KeyEvent.VK_CONTROL;
import static java.awt.event.KeyEvent.VK_E;
import static java.awt.event.KeyEvent.VK_M;
import static java.awt.event.KeyEvent.VK_SHIFT;
import static java.lang.Character.isDigit;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import static java.lang.Long.parseLong;
import static java.util.Arrays.asList;
import static myapi.MyFileHelper.loadStrings;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfGUI.mouseOver;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.KeyEventDispatcher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.PlainDocument;
public class OfEvent{
	private static boolean keyListenerSetUp=false;
	private static KeySetListener keySetListener;
	private abstract static class KeyStreamListener extends KeyAdapter{
		private String string;

		public KeyStreamListener(String string){this.string=string;}
		public KeyStreamListener(){this.string="";}
		public void setString(String string){this.string=string;}
		public String getString(){return string;}
		public void appendToString(char c){string+=c;}
	}
	private static class KeySetListener implements KeyEventDispatcher{
		private HashMap<int[],AbstractAction>actions;
		private ArrayList<Integer>pressed;
		private int lastCode;
		private int lastId;

		public KeySetListener(){actions=new HashMap<int[],AbstractAction>();pressed=new ArrayList<Integer>();}
		public void addAction(AbstractAction action,int...codes){
			if(codes.length>0){actions.put(codes,action);}
		}
		public synchronized boolean dispatchKeyEvent(KeyEvent e){
			int currentCode=e.getKeyCode();
			int currentId=e.getID();
			/*Ignore holding and key typed events*/
			if((lastCode==currentCode&&lastId==currentId)||currentCode==0){return false;}
			/*Update last code and last id*/
			lastCode=currentCode;
			lastId=currentId;
			handling:
			switch(currentId){
				/*When pressed try to locate current codes action and fire or add to the pressed codes set*/
				case KEY_PRESSED:
					if(!pressed.isEmpty()){
						int pressedCodes[]=new int[pressed.size()+1];
						int i=0;
						for(Integer integer:pressed){pressedCodes[i]=integer.intValue();i++;}
						pressedCodes[i]=currentCode;
						Set<Entry<int[],AbstractAction>>set=actions.entrySet();
						/*Search for an action that matches the current key codes*/
						search:
						for(Entry<int[],AbstractAction>entry:set){
							int actionCodes[]=entry.getKey();
							/*It can't compares*/
							if(actionCodes.length!=pressedCodes.length){continue;}
							/*If any character not matched, stop matching and continue with search*/
							for(int j=0;j<actionCodes.length;j++){if(actionCodes[j]!=pressedCodes[j]){continue search;}}
							AbstractAction action=entry.getValue();
							/*If action is found, fires it*/
							if(action!=null){action.actionPerformed(new ActionEvent(e.getSource(),ACTION_PERFORMED,"accelerator matched"));break handling;}
						}
					}
					pressed.add(new Integer(currentCode));
					break;
				/*When released just remove from the pressed codes set*/
				case KEY_RELEASED:
					pressed.remove(new Integer(currentCode));
					break;
			}
			return false;
		}
	}
	private static class LimitedDocument extends PlainDocument{
		private static final long serialVersionUID=638604376077857655L;
		private long limit;

		public LimitedDocument(long limit){super();this.limit=limit;}
		@Override public void insertString(int offset,String string,AttributeSet set)throws BadLocationException{
			if(string==null){return;}
			if((getLength()+string.length())<=limit){super.insertString(offset,string,set);}
		}
	}
	private static class CharacterSetDocument extends PlainDocument{
		private static final long serialVersionUID=2629886487792863667L;
		private char validCharacters[];

		public CharacterSetDocument(char array[]){validCharacters=array;}
		@Override public void insertString(int offset,String string,AttributeSet set)throws BadLocationException{
			if(string==null){return;}
			for(int i=0;i<string.length();i++){
				boolean matched=false;
				for(int j=0;j<validCharacters.length&&!matched;j++){matched=string.charAt(i)==validCharacters[j];}
				if(!matched){return;}
			}
			super.insertString(offset,string.toUpperCase(),set);
		}
	}
	private static final class MessageAction extends AbstractAction{
		private static final long serialVersionUID=2372376561525000408L;

		public synchronized void actionPerformed(ActionEvent e){myapi.MessageConsole.setVisible(true);}
	}
	private static final class ErrorAction extends AbstractAction{
		private static final long serialVersionUID=-5877870396432802737L;

		public synchronized void actionPerformed(ActionEvent e){myapi.ErrorConsole.setVisible(true);}
	}

	static{
		if(!keyListenerSetUp){
			keySetListener=new KeySetListener();
			getCurrentKeyboardFocusManager().addKeyEventDispatcher(keySetListener);
			addGlobalKeyEvent(new ErrorAction(),VK_CONTROL,VK_SHIFT,VK_E);
			addGlobalKeyEvent(new MessageAction(),VK_CONTROL,VK_SHIFT,VK_M);
			keyListenerSetUp=true;
		}
	}
	public static void addListenerToChoice(final Choice choice){
		choice.addKeyListener(new KeyStreamListener(){
			@Override public void keyPressed(KeyEvent e){
				if(e.getKeyChar()=='\b'){setString("");}
				else{
					appendToString(e.getKeyChar());
					for(int i=1;i<choice.getItemCount();i++){
						/*String item=choice.getItem(i);
						if(obtenerNombre(item,getTecleado()).equalsIgnoreCase(getTecleado())
								||obtenerClave(item,getTecleado()).equalsIgnoreCase(getTecleado())){
							choice.select(i);
							if(choice.getItemListeners().length>0){
								choice.getItemListeners()[0].itemStateChanged(new ItemEvent(choice,
										ItemEvent.ITEM_STATE_CHANGED,
										choice.getItem(i),
										ItemEvent.SELECTED));
							}
							break;
						}*/
					}
				}
			}
		});
	}
	/**
	@Description
	Adds an only-number listener to the <em><strong>spinner</em></strong> and changes its width to fit the longer <em><strong>limit</em></strong>, plus
	request focus when mouse is over.<br>Due to the API limitations, it can only use <em><strong>integer</em></strong> or <em><strong>double</em></strong>
	to limit the value range.
	@Warning
	Be careful that you set up the spinner's model the same way that you add the listener (integer or double number type in the model and in the listener).
	<br>Or you can use the <em><strong>createJSpinnerWithListener</em></strong> method.
	@Equals_To
	addListenerToJSpinner(spinner,<strong>java.lang.Integer.MIN_VALUE,java.lang.Integer.MAX_VALUE</strong>).
	*/
	public static void addListenerToJSpinner(JSpinner spinner){addListenerToJSpinner(spinner,MIN_VALUE,MAX_VALUE);}
	/**
	@Description
	Adds an only-number listener to the <em><strong>spinner</em></strong> and changes its width to fit the longer <em><strong>limit</em></strong>, plus
	request focus when mouse is over.<br>Due to the API limitations, it can only use <em><strong>integer</em></strong> or <em><strong>double</em></strong>
	to limit the value range.
	@Warning
	Be careful that you set up the spinner's model the same way that you add the listener (integer or double number type in the model and in the listener).
	<br>Or you can use the <em><strong>createJSpinnerWithListener</em></strong> method.
	@Equals_To
	addListenerToJSpinner(spinner,<strong>0</strong>,upperLimit).
	*/
	public static void addListenerToJSpinner(JSpinner spinner,int upperLimit){addListenerToJSpinner(spinner,0,upperLimit);}
	/**
	@Description
	Adds an only-number listener to the <em><strong>spinner</em></strong> and changes its width to fit the longer <em><strong>limit</em></strong>, plus
	request focus when mouse is over.<br>Due to the API limitations, it can only use <em><strong>integer</em></strong> or <em><strong>double</em></strong>
	to limit the value range.
	@Warning
	Be careful that you set up the spinner's model the same way that you add the listener (integer or double number type in the model and in the listener).
	<br>Or you can use the <em><strong>createJSpinnerWithListener</em></strong> method.
	*/
	public static void addListenerToJSpinner(final JSpinner spinner,final int lowerLimit,final int upperLimit){
		final JFormattedTextField field=((DefaultEditor)spinner.getEditor()).getTextField();
		DefaultFormatter formatter=(DefaultFormatter)field.getFormatter();
		formatter.setCommitsOnValidEdit(false);
		field.setEditable(false);
		field.addKeyListener(new KeyStreamListener(spinner.getValue().toString()){
			@Override public void keyPressed(KeyEvent e){
				char key=e.getKeyChar();
				/*Reset the spinner*/
				if(key=='\b'){setString("");}
				else if(isDigit(key)){
					appendToString(key);
					long value=parseLong(getString());
					if(value>upperLimit||value<lowerLimit){
						setString(""+lowerLimit);
						value=parseLong(getString());
					}
					/*Change listeners will be fired*/
					spinner.setValue(new Integer(new Long(value).intValue()));
				}
				else if(key=='-'&&getString().length()>0){
					long value=parseLong(getString())*(-1);
					if(value>upperLimit||value<lowerLimit){value=lowerLimit;}
					setString(""+value);
					/*Change listeners will be fired*/
					spinner.setValue(new Integer(new Long(value).intValue()));
				}
				field.setText(getString());
			}
		});
		field.addMouseListener(new MouseAdapter(){
			@Override public void mouseEntered(MouseEvent e){if(spinner.isEnabled()){field.requestFocus();}}
		});
		spinner.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				long value=parseLong(spinner.getModel().getValue().toString());
				if(value>upperLimit||value<lowerLimit){spinner.setValue(new Integer(lowerLimit));}
				field.setText(spinner.getValue().toString());
				/*Firing key-stream listeners*/
				KeyListener listeners[]=field.getKeyListeners();
				for(KeyListener listener:listeners){
					if(listener instanceof KeyStreamListener){((KeyStreamListener)listener).setString(field.getText());}
				}
			}
		});
		spinner.addMouseListener(new MouseAdapter(){
			@Override public void mouseEntered(MouseEvent e){if(spinner.isEnabled()){field.requestFocus();}}
		});
		Insets insets=spinner.getInsets();
		int text1Size=getTextWidth(""+upperLimit,field),text2Size=getTextWidth(""+lowerLimit,field),textSize;
		if(text1Size>text2Size){textSize=text1Size;}
		else{textSize=text2Size;}
		int width=textSize+insets.left+insets.right;
		JButton arrowButton=getDescendantOfType(JButton.class,spinner,"Enabled",new Boolean(true));
		if(arrowButton!=null){width+=arrowButton.getPreferredSize().width;}
		spinner.setSize(width,spinner.getHeight());
	}
	/**
	@Description
	Creates a <em><strong>JSpinner</em></strong> instance and adds it an only-number listener through the <em><strong>addListenerToJSpinner</em></strong>
	method and a <em><strong>SpinnerNumberModel</em></strong> to work properly.
	@Equals_To
	createJSpinnerWithListener(<strong>1</strong>).
	*/
	public static JSpinner createJSpinnerWithListener(){return createJSpinnerWithListener(1);}
	/**
	@Description
	Creates a <em><strong>JSpinner</em></strong> instance and adds it an only-number listener through the <em><strong>addListenerToJSpinner</em></strong>
	method and a <em><strong>SpinnerNumberModel</em></strong> to work properly.
	@Equals_To
	createJSpinnerWithListener(<strong>java.lang.Integer.MAX_VALUE</strong>,step).
	*/
	public static JSpinner createJSpinnerWithListener(int step){return createJSpinnerWithListener(MAX_VALUE,step);}
	/**
	@Description
	Creates a <em><strong>JSpinner</em></strong> instance and adds it an only-number listener through the <em><strong>addListenerToJSpinner</em></strong>
	method and a <em><strong>SpinnerNumberModel</em></strong> to work properly.
	@Equals_To
	createJSpinnerWithListener(<strong>0</strong>,upperLimit,step).
	*/
	public static JSpinner createJSpinnerWithListener(int upperLimit,int step){return createJSpinnerWithListener(0,upperLimit,step);}
	/**
	@Description
	Creates a <em><strong>JSpinner</em></strong> instance and adds it an only-number listener through the <em><strong>addListenerToJSpinner</em></strong>
	method and a <em><strong>SpinnerNumberModel</em></strong> to work properly.
	*/
	public static JSpinner createJSpinnerWithListener(int lowerLimit,int upperLimit,int step){
		JSpinner spinner=new JSpinner(new SpinnerNumberModel(lowerLimit,lowerLimit,upperLimit,step));
		addListenerToJSpinner(spinner,lowerLimit,upperLimit);
		return spinner;
	}
	public static void addListenerToJRadioButton(final JRadioButton radio){
		radio.addKeyListener(new KeyAdapter(){@Override public void keyPressed(KeyEvent e){if(e.getKeyChar()=='\n'){
			radio.setSelected(!radio.isSelected());
		}}});
	}
	public static void addEnterButtonListener(final AbstractButton button){
		button.addKeyListener(new KeyAdapter(){@Override public void keyPressed(KeyEvent e){if(e.getKeyChar()=='\n'){button.doClick();}}});
	}
	public static void addEnterButtonListener(final Button button){
		button.addKeyListener(new KeyAdapter(){@Override public void keyPressed(KeyEvent e){if(e.getKeyChar()=='\n'){
			ActionListener listeners[]=button.getActionListeners();
			ActionEvent event=new ActionEvent(button,ACTION_PERFORMED,"button pressed");
			for(ActionListener listener:listeners){listener.actionPerformed(event);}
		}}});
	}
	public static void addOverListener(final AbstractButton button,final Font font,final Color color,final Font overFont,final Color overColor){
		button.setFont(font);
		button.setForeground(color);
		button.getModel().addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e){
				ButtonModel model=(ButtonModel)e.getSource();
				if(model.isRollover()){
					button.setFont(overFont);
					button.setForeground(overColor);
				}
				else{
					button.setFont(font);
					button.setForeground(color);
				}
			}
		});
	}
	public static void addListenerToJCheckBox(final JCheckBox box,final char[] selectKeys,final char[] deselectKeys){
		box.addKeyListener(new KeyAdapter(){@Override public void keyPressed(KeyEvent e){
			Character c=new Character(e.getKeyChar());
			boolean select=asList(selectKeys).contains(c);
			boolean deselect=!(asList(deselectKeys).contains(c));
			if(select){box.setSelected(select);}
			else if(deselect){box.setSelected(deselect);}
			else{return;}
			ActionListener listeners[]=box.getActionListeners();
			ActionEvent event=new ActionEvent(box,ACTION_PERFORMED,"checkbox pressed");
			for(ActionListener listener:listeners){listener.actionPerformed(event);}
		}});
	}
	public static void addFocusListener(final Component component){
		component.addFocusListener(new FocusListener(){
			public void focusLost(FocusEvent e){cleanListeners(false);}
			public void focusGained(FocusEvent e){cleanListeners(true);}
			private void cleanListeners(boolean hasFocus){
				for(KeyListener listener:component.getKeyListeners()){
					if(listener instanceof KeyStreamListener){((KeyStreamListener)listener).setString("");}
				}
				if(component instanceof AbstractButton){((AbstractButton)component).setSelected(hasFocus);}
			}
		});
	}
	public static void addLimitToJTextField(JTextField textField,int limit){if(limit>0){textField.setDocument(new LimitedDocument(limit));}}
	public static void addCharacterSetToJTextField(JTextField textField,char set[]){textField.setDocument(new CharacterSetDocument(set));}
	public static ArrayList<String>addListenerToJComboBox(final JComboBox comboBox,String archivo){
		for(KeyListener listener:comboBox.getKeyListeners()){comboBox.removeKeyListener(listener);}
		comboBox.addKeyListener(new KeyStreamListener(){
			@Override public void keyPressed(KeyEvent e){
				if(e.getKeyChar()=='\b'){setString("");}
				else{
					appendToString(e.getKeyChar());
					for(int i=1;i<comboBox.getItemCount();i++){
						/*String item=comboBox.getItemAt(i);
						if(obtenerNombre(item,getTecleado()).equalsIgnoreCase(getTecleado())
								||obtenerClave((String)item,getTecleado()).equalsIgnoreCase(getTecleado())){
							comboBox.setSelectedIndex(i);
							if(comboBox.getItemListeners().length>0){
								comboBox.getItemListeners()[0].itemStateChanged(new ItemEvent(comboBox,
										ItemEvent.ITEM_STATE_CHANGED,
										comboBox.getItemAt(i),
										ItemEvent.SELECTED));
							}
							break;
						}*/
					}
				}
			}
		});
		addFocusListener(comboBox);
		ArrayList<String>array=fetchArrayIntoJComboBox(comboBox,archivo);
		try{
			BasicComboPopup popup=getBasicComboPopup(comboBox);
			addListenerToJComboBox(comboBox,popup);
		}
		/*--ErrorConsole--*/catch(Exception e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
		return array;
	}
	private static void addListenerToJComboBox(final JComboBox comboBox,final BasicComboPopup popup)throws NoSuchFieldException,IllegalAccessException{
		comboBox.addMouseListener(new MouseAdapter(){
			@Override public void mouseEntered(MouseEvent e){if(comboBox.isEnabled()){comboBox.requestFocus();comboBox.showPopup();}}
			@Override public void mouseExited(MouseEvent e){if(comboBox.isEnabled()&&!mouseOver(popup)){comboBox.hidePopup();}}
		});
		final JScrollPane pane=getJScrollPane(popup);
		final JScrollBar bar=pane.getVerticalScrollBar();
		pane.getViewport().getView().addMouseListener(new MouseAdapter(){
			@Override public void mouseExited(MouseEvent e){if(!mouseOver(bar)){comboBox.hidePopup();}}
		});
		bar.addMouseListener(new MouseAdapter(){
			@Override public void mouseExited(MouseEvent e){if(!mouseOver(pane)){comboBox.hidePopup();}}
		});
	}
	public static ArrayList<String>fetchArrayIntoJComboBox(JComboBox comboBox,String file){
		if(file!=null){
			ArrayList<String>array=loadStrings(file);
			fetchArrayIntoJComboBox(comboBox,array);
			return array;
		}
		return null;
	}
	public static void fetchArrayIntoJComboBox(JComboBox comboBox,Iterable<String>array){
		comboBox.addItem("");int i=0,j=0;
		for(String item:array){
			comboBox.addItem(item);
			int width=getTextWidth(item,comboBox);
			if(j<width){j=width;}
			i++;
		}
		if(i==0){comboBox.removeAllItems();}
		else if(i>0&&i<=14){comboBox.setMaximumRowCount(i+1);}
		else{comboBox.setMaximumRowCount(16);}
		if(j>0&&comboBox.getItemCount()>0){comboBox.setSize(j,comboBox.getHeight());}
	}
	public static BasicComboPopup getBasicComboPopup(JComboBox comboBox)throws NoSuchFieldException,IllegalAccessException{
		Field popupBasic=BasicComboBoxUI.class.getDeclaredField("popup");
		popupBasic.setAccessible(true);
		return(BasicComboPopup)popupBasic.get(comboBox.getUI());
	}
	public static JScrollPane getJScrollPane(BasicComboPopup popup)throws NoSuchFieldException,IllegalAccessException{
		Field scrollerBasic=BasicComboPopup.class.getDeclaredField("scroller");
		scrollerBasic.setAccessible(true);
		return(JScrollPane)scrollerBasic.get(popup);
	}
	/**
	@Description
	Adds a listener to the <em><strong>container</em></strong> that expands it when a component is added; according to the space the component needs
	to be displayed completely.
	@Warning
	Be careful that the listener will use preferred size. If you need to change the size, you should overwrite the
	<em><strong>setPreferredSize</em></strong> method to modify it alongside.
	*/
	public static void addResizableContainerListener(Container container){
		container.addContainerListener(new ContainerListener(){
			public void componentRemoved(ContainerEvent e){}
			public void componentAdded(ContainerEvent e){expandIfNeeded(e.getContainer(),e.getChild());}
		});
	}
	/**
	@Description
	Adds a listener to the <em><strong>component</em></strong> that will try to expand it's container when the <em><strong>component</em></strong> is
	moved or resized.
	@Warning
	Be careful that the listener will use preferred size. If you need to change the size, you should overwrite the
	<em><strong>setPreferredSize</em></strong> method to modify it alongside.
	*/
	public static void addSizeToParentListener(Component component){
		component.addComponentListener(new ComponentAdapter(){
			@Override public void componentResized(ComponentEvent e){
				Component child=e.getComponent();
				Container parent=child.getParent();
				if(parent!=null){expandIfNeeded(parent,child);}
			}
			@Override public void componentMoved(ComponentEvent e){
				Component child=e.getComponent();
				Container parent=child.getParent();
				if(parent!=null){expandIfNeeded(parent,child);}
			}
		});
	}
	private static void expandIfNeeded(Component parent,Component child){
		Dimension preferred=parent.getPreferredSize();
		int width=preferred.width,height=preferred.height;
		Dimension dimension=new Dimension(child.getX()+child.getWidth(),child.getY()+child.getHeight());
		if(dimension.width<width){dimension.width=width;}
		if(dimension.height<height){dimension.height=height;}
		parent.setPreferredSize(dimension);
/*--MessageConsole--*/myapi.MessageConsole.addText("Expandiendo "+parent.getClass().getName()+" a "+parent.getPreferredSize());/*--MessageConsole--*/
	}
	public static void addGlobalKeyEvent(AbstractAction action,int...keyCodes){
		if(keySetListener==null){return;}
		keySetListener.addAction(action,keyCodes);
	}
}