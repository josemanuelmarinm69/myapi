package myapi.xml;
import static java.io.File.separatorChar;
import static java.lang.System.out;
import static java.util.Arrays.asList;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static myapi.MyFileChooser.xmlFileFilter;
import static myapi.MyFileChooser.xmlFileView;
import static myapi.utils.OfData.isEmptyString;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfFile.getExtension;
import static myapi.utils.OfGUI.createWorker;
import static myapi.utils.OfGUI.syncJProgressBarWithWorker;
import static myapi.utils.OfGUI.testPanel;
import static myapi.utils.OfMath.convertToPercent;
import static myapi.utils.OfOS.restoreOutputStream;
import static myapi.utils.OfOS.setNewOutputStream;
import static myapi.xml.MyXMLEditor.getNodesFrom;
import static myapi.xml.MyXMLEditor.modifyAll;
import static org.apache.commons.io.FileUtils.copyFile;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import myapi.MyFileChooser;
import myapi.generics.MyEntry;
import myapi.utils.OfGUI.MyWorker;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
public class MyXMLDemo{
	private String extension;
	private String unitFileName;
	private String newWeaponName;
	private ArrayList<List<String>>forUnits;
	private ArrayList<List<String>>forWeapons;
	private ArrayList<MyEntry<List<String>,String>>filters;
	private ResourceBundle bundle;
	private HashSet<String>weaponsSet;
	private ByteArrayOutputStream stream;
	private MyWorker task;
	private JTextArea area;
	private JPopupMenu menu;

	public MyXMLDemo(ResourceBundle b)throws Exception{
		bundle=b;
		stream=setNewOutputStream();
		JPanel panel=new JPanel(null);
		addResizableContainerListener(panel);
		JProgressBar bar=new JProgressBar();
		addSizeToParentListener(bar);
		bar.setBounds(0,0,200,30);
		area=new JTextArea();
		area.setEditable(false);
		JScrollPane pane=new JScrollPane(area);
		addSizeToParentListener(pane);
		pane.setBounds(0,31,500,500);
		panel.add(bar);
		panel.add(pane);
		menu=createMenu();
		area.addMouseListener(createAdapter());
		task=createWorker("xmlTest","finish",this);
		syncJProgressBarWithWorker(bar,task);
		panel.setSize(panel.getPreferredSize());
		testPanel(panel,"MyXMLEditor",false,EXIT_ON_CLOSE);
		task.execute();
	}
	private JPopupMenu createMenu(){
		JPopupMenu menu=new JPopupMenu();
		JMenuItem item=new JMenuItem("Save log");
		item.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				MyFileChooser chooser=new MyFileChooser(xmlFileFilter,xmlFileView,false,true);
				if(!chooser.create()){return;}
				File file=chooser.getSelectedFile();
				try{
					FileWriter writer=new FileWriter(file);
					writer.write(area.getText());
					writer.flush();
					writer.close();
				}
				catch(IOException ex){myapi.ErrorConsole.addText(ex);}
			}
		});
		menu.add(item);
		return menu;
	}
	private MouseAdapter createAdapter(){
		return new MouseAdapter(){
			@Override public void mousePressed(MouseEvent e){if(e.isPopupTrigger()){doPop(e);}}
			@Override public void mouseReleased(MouseEvent e){if(e.isPopupTrigger()){doPop(e);}}
			private void doPop(MouseEvent e){menu.show(e.getComponent(),e.getX(),e.getY());}
		};
	}
	public void finish(){
		restoreOutputStream();
		area.setText(stream.toString());
	}
	public void xmlTest()throws Exception{
		weaponsSet=new HashSet<String>();
		/*Load game version specifics*/
		extension=bundle.getString("extension");
		unitFileName=bundle.getString("unitfilename");
		newWeaponName=bundle.getString("newweaponname");
		/*Get every file for the modified units*/
		ArrayList<File>unitFiles=createUnitsFiles(unitFileName+'.'+extension);
		/*Get data to be modified and perform on each file*/
		forUnits=new ArrayList<List<String>>();
		for(String toModify:asList(bundle.getString("forunits").split(","))){
			forUnits.add(asList(toModify.split("\\.")));
		}
		String forweapons=bundle.getString("forweapons");
		if(forweapons!=null&&!isEmptyString(forweapons)){
			forWeapons=new ArrayList<List<String>>();
			for(String toModify:asList(forweapons.split(","))){
				forWeapons.add(asList(toModify.split("\\.")));
			}
		}
		filters=new ArrayList<MyEntry<List<String>,String>>();
		for(String filter:asList(bundle.getString("filters").split(","))){
			String value=filter.substring(filter.indexOf("=")+1);
			filter=filter.substring(0,filter.indexOf("="));
			List<String>list=asList(filter.split("\\."));
			MyEntry<List<String>,String>entry=new MyEntry<List<String>,String>(list,value);
			filters.add(entry);
		}
		/*Setting up needed documents and writers*/
		DocumentBuilder builder=DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Transformer transformer=TransformerFactory.newInstance().newTransformer();
		int a=1,b=unitFiles.size();
		for(File file:unitFiles){
			Document document=builder.parse(file);
			document.getDocumentElement().normalize();
			modifyUnit(document);
			StreamResult result=new StreamResult(file);
			transformer.transform(new DOMSource(document),result);
			task.updateProgress(convertToPercent(a++,b));
		}
	}
	private ArrayList<File>createUnitsFiles(String fileName){
		String basePath=bundle.getString("basepath");
		String technicsPath=bundle.getString("technicspath");
		ArrayList<File>unitFiles=new ArrayList<File>();
		if(technicsPath!=null&&!isEmptyString(technicsPath)){
			technicsPath=basePath+technicsPath;
			for(String category:bundle.getString("categories").split(",")){
				File folder=new File(technicsPath+separatorChar+category);
				for(String file:folder.list()){
					if(getExtension(file)!=null){continue;}
					unitFiles.add(new File(folder.getAbsolutePath()+separatorChar+file+separatorChar+fileName));
				}
			}
		}
		String humansPath=bundle.getString("technicspath");
		if(humansPath!=null&&!isEmptyString(humansPath)){
			for(String path:humansPath.split(",")){
				path=basePath+path;
				File folder=new File(path);
				for(String file:folder.list()){
					if(getExtension(file)!=null){continue;}
					unitFiles.add(new File(folder.getAbsolutePath()+separatorChar+File.separatorChar+fileName));
				}
			}
		}
		return unitFiles;
	}
	private void modifyUnit(Document document)throws Exception{
		String weaponsPath=bundle.getString("basepath")+bundle.getString("weaponspath");
		NodeList nodes=document.getChildNodes();
		/*Modify what is needed*/
		ArrayList<Node>resultNodes=getNodesFrom(nodes,"KeyName",new String[]{"base","RPG"});
		String unitName=resultNodes.get(0).getTextContent();
		out.print('\n'+document.getDocumentURI()+'\n'+unitName);
		if(forWeapons!=null){
			/*Check for weapons*/
			resultNodes=getNodesFrom(nodes,"Weapon",new String[]{"base","RPG","Guns","item"});
			if(resultNodes!=null){
				for(Node node:resultNodes){
					weaponTest(weaponsPath,node.getTextContent());
					node.setTextContent(node.getTextContent()+newWeaponName);
				}
			}
			else{out.print("\nNo weapons found");}
		}
		modifyAll(nodes,filters,forUnits,bundle,false);
	}
	private void weaponTest(String weaponsPath,String weaponName)throws Exception{
		if(weaponsSet.contains(weaponName)){return;}
		File source=new File(weaponsPath+separatorChar+weaponName+'.'+extension);
		if(!source.exists()){return;}
		File target=new File(weaponsPath+separatorChar+weaponName+newWeaponName+'.'+extension);
		out.print("\nCopying "+source+" To "+target);
		copyFile(source,target);
		DocumentBuilder builder=DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Transformer transformer=TransformerFactory.newInstance().newTransformer();
		Document document=builder.parse(target);
		document.getDocumentElement().normalize();
		modifyWeapon(document);
		StreamResult result=new StreamResult(target);
		transformer.transform(new DOMSource(document),result);
		weaponsSet.add(weaponName);
	}
	private void modifyWeapon(Document document){
		NodeList nodes=document.getChildNodes();
		/*Modify what is needed*/
		ArrayList<Node>resultNodes=getNodesFrom(nodes,"KeyName",new String[]{"base","RPG"});
		String unitName=resultNodes.get(0).getTextContent();
		out.print('\n'+document.getDocumentURI()+'\n'+unitName);
		modifyAll(nodes,filters,forWeapons,bundle,false);
	}
}