package myapi.xml;
import static java.lang.Double.parseDouble;
import static java.lang.Double.valueOf;
import static java.lang.System.out;
import static myapi.utils.OfData.convertArrayToList;
import static myapi.utils.OfMath.trunkTo;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import myapi.generics.MyEntry;
public class MyXMLEditor{
	public static ArrayList<Node>getNodesFrom(NodeList nodes,String name,String parents[]){
		return getNodesFrom(nodes,name,convertArrayToList(parents));
	}
	public static ArrayList<Node>getNodesFrom(NodeList nodes,String name,List<String>parents){
		if(parents!=null&&!(parents instanceof LinkedList)){parents=new LinkedList<String>(parents);}
		if(parents!=null&&!parents.isEmpty()){
			/*Take first parent name and delete it from the array*/
			String first=parents.remove(0);
			/*Search nodes with that parent node name*/
			ArrayList<Node>parentList=getNodesFrom(nodes,first,(List<String>)null);
			if(parentList==null){return parentList;}
			ArrayList<Node>foundNodesList=new ArrayList<Node>();
			for(Node parent:parentList){
				ArrayList<Node>list=getNodesFrom(parent.getChildNodes(),name,parents);
				if(list!=null){for(Node found:list){foundNodesList.add(found);}}
				else{return list;}
			}
			/*Return the list with found nodes, no return of empty list*/
			return(foundNodesList.size()>0?foundNodesList:null);
		}
		ArrayList<Node>nodesList=new ArrayList<Node>();
		for(int i=0;i<nodes.getLength();i++){
			Node node=nodes.item(i);
			if(node.getNodeName().equalsIgnoreCase(name)){nodesList.add(node);}
		}
		/*Return the list with found nodes, no return of empty list*/
		return(nodesList.size()>0?nodesList:null);
	}
	public static ArrayList<Node>getAttributesFrom(Node node,String attributeName){
		ArrayList<Node>list=new ArrayList<Node>();
		NamedNodeMap attributes=node.getAttributes();
		for(int i=0;i<attributes.getLength();i++){
			Node attribute=attributes.item(i);
			if(attributeName.equalsIgnoreCase(attribute.getNodeName())){list.add(attribute);}
		}
		return list.size()>0?list:null;
	}
	public static void modifyNode(Node node,double modifier,boolean overwrite){
		String text=node.getNodeValue();
		out.print("\n\t"+node.getNodeName()+" found value:"+text);
		double value=parseDouble(text);
		String newText;
		/*Relative value*/
		if(!overwrite){newText=""+trunkTo(value*modifier,2);}
		/*Absolute value*/
		else{newText=""+modifier;}
		out.print("\n\t"+node.getNodeName()+" written value:"+newText);
		node.setNodeValue(newText);
	}
	public static void modifyAttributes(Node node,String attributeName,double modifier,boolean overwrite){
		NamedNodeMap attributes=node.getAttributes();
		for(int i=0;i<attributes.getLength();i++){
			Node attribute=attributes.item(i);
			if(attributeName.equalsIgnoreCase(attribute.getNodeName())){modifyNode(attribute,modifier,overwrite);}
		}
	}
	public static void modifyAll(NodeList nodes,ArrayList<MyEntry<List<String>,String>>filters,
			List<List<String>>toModify,ResourceBundle bundle,boolean tag){
		checkFilter(nodes,filters,bundle);
		/*For each attribute to be modified*/
		for(List<String>list:toModify){
			int last=list.size()-2;
			boolean overwrite=false;
			Double value;
			String attribute=list.get(last+1);
			/*Get value for multiply or replace*/
			try{value=valueOf(bundle.getString(attribute));}
			catch(NumberFormatException e){
				overwrite=true;
				value=valueOf(bundle.getString(attribute).replaceAll("\\|",""));
			}
			ArrayList<Node>resultNodes=getNodesFrom(nodes,list.get(last),list.subList(0,last));
			if(resultNodes!=null){
				for(Node node:resultNodes){
					if(tag){modifyNode(node,value.doubleValue(),overwrite);}
					else{modifyAttributes(node,attribute,value.doubleValue(),overwrite);}
				}
			}
		}
	}
	public static void checkFilter(NodeList nodes,ArrayList<MyEntry<List<String>,String>>filters,ResourceBundle bundle){
		/*Check for filters*/
		for(MyEntry<List<String>,String>entry:filters){
			List<String>list=entry.getKey();
			int last=list.size()-2;
			String attribute=list.get(last+1);
			ArrayList<Node>resultNodes=getNodesFrom(nodes,list.get(last),list.subList(0,last));
			if(resultNodes!=null){
				for(Node node:resultNodes){
					/*Filter match tags located*/
					for(Node attributeNode:getAttributesFrom(node,attribute)){
						/*Attribute and value match*/
						if(attributeNode.getNodeValue().equals(entry.getValue())){
							String key="";
							for(String element:list){key+=element+'.';}
							key=key.substring(0,key.length()-1)+'+'+entry.getValue();
							/*Values to modify in different way*/
							for(String specific:bundle.getString(key).split(",")){
								int index=specific.indexOf("=");
								String listValue=specific.substring(0,index);
								List<String>list2=convertArrayToList(listValue.split("\\."));
								String refValue=list2.remove(list2.size()-1);
								String refValue2=list2.remove(list2.size()-1);
								boolean overwrite=false;
								Double value;
								/*Get value for multiply or replace*/
								try{value=valueOf(specific.substring(index+1));}
								catch(NumberFormatException e){
									overwrite=true;
									value=valueOf(specific.substring(index+1).replaceAll("\\|",""));
								}
								/*Search to modify nodes*/
								resultNodes=getNodesFrom(nodes,refValue2,list2);
								if(resultNodes!=null){
									for(Node foundNode:resultNodes){
										modifyAttributes(foundNode,refValue,value.doubleValue(),overwrite);
									}
								}
							}
							return;
						}
					}
				}
			}
		}
	}
}