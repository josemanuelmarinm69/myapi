package myapi;
public class MyFileHelper{
	private static java.io.FileInputStream loadFileStream(java.io.File file)throws java.io.FileNotFoundException{
		java.net.URI uri=file.toURI();
		if(uri!=null){return new java.io.FileInputStream(uri.getPath());}
		return null;
	}
	public static java.io.FileInputStream loadFileStream(String name){
		try{return loadFileStream(new java.io.File(myapi.utils.OfFile.getFilesFolder()+"/"+name));}
		catch(java.io.FileNotFoundException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			try{return loadFileStream(new java.io.File(name));}
			catch(java.io.FileNotFoundException e1){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e1);/*--ErrorConsole--*/
			}
		}
		return null;
	}
	private static java.util.ArrayList<String>loadStrings(java.io.FileInputStream stream){
		java.util.ArrayList<String>array=new java.util.ArrayList<String>();
		java.util.Scanner scanner=new java.util.Scanner(stream);
		while(scanner.hasNextLine()){array.add(scanner.nextLine());}
		scanner.close();
		return array;
	}
	public static java.util.ArrayList<String>loadStrings(String name){return loadStrings(loadFileStream(name));}
	public static java.util.ArrayList<String>loadStrings(java.io.File file){
		try{return loadStrings(loadFileStream(file));}
		catch(java.io.FileNotFoundException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
		return null;
	}
	public static java.io.InputStream convertImage(java.awt.image.BufferedImage image,String imageExtension)throws java.io.IOException{
		java.io.ByteArrayOutputStream outputStream=new java.io.ByteArrayOutputStream();
		javax.imageio.ImageIO.write(image,imageExtension,outputStream);
		return new java.io.ByteArrayInputStream(outputStream.toByteArray());
	}
}