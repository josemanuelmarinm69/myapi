package myapi.esf.core;
public enum EsfType{
	ABCE(0xABCEL),
	ABCD(0xABCDL),
	ABCA(0xABCAL);

	private long value;

	private EsfType(long value){this.value=value;}
	public long getValue(){return value;}
	public static EsfType getType(long value){
		for(EsfType type:values()){
			if(type.getValue()==value){return type;}
		}
		return null;
	}
}