package myapi.esf.core;
import static myapi.esf.core.EsfType.ABCA;
import static myapi.esf.core.EsfType.getType;
import java.io.IOException;
import java.util.ArrayList;
import myapi.generics.MyEntry;
import myapi.io.MyBinaryReaderWriter;
public class EsfHeader{
	private long id;
	private long unknown1;
	private long editTime;
	private ArrayList<String>nodeNames;
	public ArrayList<MyEntry<String,Integer>>utf16NodeNames;
	public ArrayList<MyEntry<String,Integer>>asciiNodeNames;

	public EsfHeader(MyBinaryReaderWriter reader)throws IOException{
		reader.seek(0);
		id=reader.readUInt32();
		unknown1=reader.readUInt32();
		editTime=reader.readUInt32();
		long nodeNamesOffset=reader.readUInt32();
		long position=reader.position();
		reader.seek(nodeNamesOffset);
		readNodeNames(reader);
		reader.seek(position);
	}
	private void readNodeNames(MyBinaryReaderWriter reader)throws IOException{
		nodeNames=new ArrayList<String>();
		short count=reader.readInt16();
		for(short i=0;i<count;i++){nodeNames.add(reader.readAsciiString(reader.readUInt16()));}
		if(id==ABCA.getValue()){
			utf16NodeNames=new ArrayList<MyEntry<String,Integer>>();
			int newCount=reader.readInt32();
			for(int i=0;i<newCount;i++){
				utf16NodeNames.add(new MyEntry<String,Integer>(reader.readUtf16String(reader.readUInt16()),new Integer(reader.readInt32())));
			}
			asciiNodeNames=new ArrayList<MyEntry<String,Integer>>();
			newCount=reader.readInt32();
			for(int i=0;i<newCount;i++){
				asciiNodeNames.add(new MyEntry<String,Integer>(reader.readAsciiString(reader.readUInt16()),new Integer(reader.readInt32())));
			}
		}
	}
	public long getId(){return id;}
	public long getUnknown1(){return unknown1;}
	public long getEditTime(){return editTime;}
	public void setEditTime(long editTime){this.editTime=editTime;}
	public ArrayList<String>getNodeNames(){return nodeNames;}
	@Override public String toString(){return"EsfType:"+getType(id)+"\n\tID:"+id+"\n\tUnknown1:"+unknown1+"\n\tEdit Time:"+editTime;}
}