package myapi.esf.core;
public enum EsfNodeType{
	BOOLEAN((short)1),
	UINT((short)8),
	STRING((short)14),
	ARRAY((short)0),
	RECORD((short)128),
	RECORDARRAY((short)129);

	private short value;

	private EsfNodeType(short value){this.value=value;}
	public short getValue(){return value;}
	public static EsfNodeType getType(short value){
		for(EsfNodeType type:values()){
			if(type.getValue()==value){return type;}
		}
		return null;
	}
}