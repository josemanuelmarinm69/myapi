package myapi.esf.nodes;
import java.io.IOException;
import myapi.esf.core.EsfHeader;
import myapi.io.MyBinaryReaderWriter;
public class EsfStringNode extends EsfMemoryMappedValueNode<String>{
	public EsfStringNode(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{super(reader,header);}
	@Override public String readValue(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{return reader.readUtf16String(reader.readUInt16());}
}