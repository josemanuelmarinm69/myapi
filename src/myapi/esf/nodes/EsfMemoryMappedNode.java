package myapi.esf.nodes;
public abstract class EsfMemoryMappedNode{
	protected long start,size;

	public long getStart(){return start;}
	public long getSize(){return size;}
}