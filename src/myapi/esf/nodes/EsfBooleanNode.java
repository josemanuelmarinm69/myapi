package myapi.esf.nodes;
import java.io.IOException;
import myapi.esf.core.EsfHeader;
import myapi.io.MyBinaryReaderWriter;
public class EsfBooleanNode extends EsfMemoryMappedValueNode<Boolean>{
	public EsfBooleanNode(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{super(reader,header);}
	@Override public Boolean readValue(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{return new Boolean(reader.readBoolean());}
}