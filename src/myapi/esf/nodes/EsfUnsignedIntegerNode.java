package myapi.esf.nodes;
import java.io.IOException;
import myapi.esf.core.EsfHeader;
import myapi.io.MyBinaryReaderWriter;
public class EsfUnsignedIntegerNode extends EsfMemoryMappedValueNode<Long>{
	public EsfUnsignedIntegerNode(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{super(reader,header);}
	@Override public Long readValue(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{return new Long(reader.readUInt32());}
}