package myapi.esf.nodes;
import static myapi.esf.gui.EsfMainForm.recursive;
import static myapi.esf.gui.EsfMainForm.limit;
import static myapi.esf.parser.EsfParser.readAdvance;
import static myapi.esf.parser.EsfParser.readRecodInfo;
import static myapi.esf.parser.EsfParser.readToOffset;
import java.io.IOException;
import java.util.ArrayList;
import myapi.esf.core.EsfHeader;
import myapi.generics.MyEntry;
import myapi.io.MyBinaryReaderWriter;
public class EsfMemoryMappedRecordNode extends EsfMemoryMappedNode{
	private boolean readChildren,readParents;
	private short version;
	private long advance;
	private String name;
	private ArrayList<EsfMemoryMappedNode>children;
	//private ArrayList<EsfMemoryMappedNode>leafs;
	private ArrayList<EsfMemoryMappedRecordNode>parents;
	private MyBinaryReaderWriter reader;
	private EsfHeader header;

	public EsfMemoryMappedRecordNode(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{this(reader,header,true);}
	public EsfMemoryMappedRecordNode(MyBinaryReaderWriter reader,EsfHeader header,boolean readChildren)throws IOException{
		this.reader=reader;
		this.header=header;
		start=reader.position()-1;
		MyEntry<String,Short>entry=readRecodInfo(reader,header);
		name=entry.getKey();
		version=entry.getValue().shortValue();
		advance=readAdvance(reader,header);
		size=reader.position()-start;
		long byteCount=advance+size;

		java.lang.System.out.println("MemoryMappedRecordNode\n\tStart:"+start+"\n\tSize:"+size+"\n\tName:"+name+"\n\tVersion:"+version+
				"\n\tAdvance:"+advance+"\n\tByte Count:"+byteCount);

		if(this.readChildren=(readChildren&++recursive<limit)){children=readToOffset(reader,header,reader.position()+advance);}
		else{reader.seek(reader.position()+advance);}
		recursive--;
	}
	public short getVersion(){return version;}
	public String getName(){return name;}
	public ArrayList<EsfMemoryMappedNode>getChildren(){
		if(!readChildren){
			if(recursive++<limit){
				try{
					java.lang.System.out.println("reading");
					reader.seek(start+size);
					children=readToOffset(reader,header,reader.position()+advance);
					readChildren=true;
				}
				catch(IOException e){
					e.printStackTrace();
					recursive--;
					return null;
				}
			}
			recursive--;
		}
		return children;
	}
	public ArrayList<EsfMemoryMappedRecordNode>getParents(){
		if(!readParents){
			ArrayList<EsfMemoryMappedNode>children=getChildren();
			readParents=true&&readChildren;
			if(children==null){return null;}
			ArrayList<EsfMemoryMappedRecordNode>list=new ArrayList<EsfMemoryMappedRecordNode>();
			for(EsfMemoryMappedNode node:children){if(node instanceof EsfMemoryMappedRecordNode){list.add((EsfMemoryMappedRecordNode)node);}}
			parents=list.isEmpty()?null:list;
		}
		return parents;
	}
	@Override public String toString(){return name;}
}