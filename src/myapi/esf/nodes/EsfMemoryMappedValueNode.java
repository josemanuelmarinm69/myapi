package myapi.esf.nodes;
import java.io.IOException;
import myapi.esf.core.EsfHeader;
import myapi.io.MyBinaryReaderWriter;
public abstract class EsfMemoryMappedValueNode<T> extends EsfMemoryMappedNode{
	private T value;

	public EsfMemoryMappedValueNode(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{
		start=reader.position()-1;
		value=readValue(reader,header);
		size=reader.position()-start;

		java.lang.System.out.println("MemoryMappedValueNode\n\tStart:"+start+"\n\tSize:"+size+"\n\tValue:"+value);
	}
	public T getValue(){return value;}
	public abstract T readValue(MyBinaryReaderWriter reader,EsfHeader header)throws IOException;
	@Override public String toString(){return value.getClass().getName()+value.toString();}
}