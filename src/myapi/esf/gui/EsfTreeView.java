package myapi.esf.gui;
import java.util.ArrayList;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import myapi.esf.nodes.EsfMemoryMappedNode;
import myapi.esf.nodes.EsfMemoryMappedRecordNode;
public class EsfTreeView extends JTree implements TreeSelectionListener{
	private static final long serialVersionUID=9157678672625863784L;

	public EsfTreeView(EsfMemoryMappedNode rootNode){
		super(new DefaultMutableTreeNode(rootNode));
		addTreeSelectionListener(this);
	}
	private static void fill(DefaultMutableTreeNode root){
		ArrayList<EsfMemoryMappedRecordNode>parents=((EsfMemoryMappedRecordNode)root.getUserObject()).getParents();
		if(parents==null){return;}
		for(EsfMemoryMappedRecordNode node:parents){root.add(new DefaultMutableTreeNode(node));}
	}
	public void valueChanged(TreeSelectionEvent e){
		DefaultMutableTreeNode selected;
		if((selected=(DefaultMutableTreeNode)getLastSelectedPathComponent())==null){return;}
		fill(selected);
	}
}