package myapi.esf.gui;
import static java.lang.Integer.parseInt;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import static myapi.MyFileChooser.esfFileFilter;
import static myapi.MyFileChooser.esfFileView;
import static myapi.MyOptionPane.showMessage;
import static myapi.utils.OfData.isEmptyString;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import myapi.MyFileChooser;
import myapi.esf.parser.EsfParser;
public class EsfMainForm{
	public static int recursive,limit=1;
	public static void main(String[]args){
		try{
			if(args==null||args.length<2){
				args=new String[2];
				args[0]="-main";
				args[1]="1";
			}
			if(!args[0].equals("-main")){args[1]="0";}
			switch(parseInt(args[1])){
				case 0:
					showMessage("-main #","Parameters");
					break;
				case 2:
					mainEsfEditor();
					break;
				default:
					if(!myapi.MyOptionPane.receiveString("Enter text","Text to convert")){break;}
					String text=myapi.MyOptionPane.getString();
					myapi.MyOptionPane.showMessage(toHexUnicode(text),"Unicode of "+text);
			}
		}
		catch(Exception e){/*Consola de Errores*/myapi.ErrorConsole.addText(e);myapi.ErrorConsole.setVisible(true);/*Consola de Errores*/}
	}
	public static void mainEsfEditor()throws Exception{
		MyFileChooser chooser=new MyFileChooser(esfFileFilter,esfFileView);
		if(!chooser.select()){return;}
		JScrollPane pane=new JScrollPane(new EsfTreeView(new EsfParser(chooser.getSelectedFile().getAbsolutePath()).getRoot()));
		pane.setBounds(0,0,500,500);
		JPanel panel=new JPanel(null);
		panel.add(pane);
		JFrame frame=new JFrame("EditEsf Test");
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setBounds(0,0,500,500);
		frame.setContentPane(panel);
		frame.setVisible(true);
	}
	public static java.util.ArrayList<String>toHexUnicode(String text){
		if(text==null||isEmptyString(text)){return null;}
		java.util.ArrayList<String>unicodes=new java.util.ArrayList<String>();
		for(int i=0;i<text.length();i++){unicodes.add(java.lang.Integer.toHexString(text.charAt(i)));}
		return unicodes;
	}
}