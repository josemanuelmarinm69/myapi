package myapi.esf.parser;
import static myapi.esf.core.EsfNodeType.ARRAY;
import static myapi.esf.core.EsfNodeType.BOOLEAN;
import static myapi.esf.core.EsfNodeType.UINT;
import static myapi.esf.core.EsfNodeType.RECORD;
import static myapi.esf.core.EsfNodeType.RECORDARRAY;
import static myapi.esf.core.EsfNodeType.STRING;
import static myapi.esf.core.EsfType.ABCA;
import java.io.IOException;
import java.util.ArrayList;
import myapi.esf.core.EsfHeader;
import myapi.esf.nodes.EsfBooleanNode;
import myapi.esf.nodes.EsfMemoryMappedNode;
import myapi.esf.nodes.EsfMemoryMappedRecordNode;
import myapi.esf.nodes.EsfStringNode;
import myapi.esf.nodes.EsfUnsignedIntegerNode;
import myapi.generics.MyEntry;
import myapi.io.MyBinaryReaderWriter;
public class EsfParser{
	private MyBinaryReaderWriter reader;
	private EsfHeader header;
	private EsfMemoryMappedNode root;

	public EsfParser(String filePath)throws Exception{
		java.lang.System.out.println(header=new EsfHeader(reader=new MyBinaryReaderWriter(filePath)));
		root=readUnknownNode(reader,header);
	}
	public EsfMemoryMappedNode getRoot(){return root;}
	public static MyEntry<String,Short>readRecodInfo(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{
		return new MyEntry<String,Short>(header.getNodeNames().get(reader.readUInt16()),new Short(reader.readByte()));
	}
	public static long readAdvance(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{
		if(header.getId()==ABCA.getValue()){
			short read;
			long advance=0;
			while(((read=reader.readByte())&0x80)!=0){advance=(advance<<7)+(read&(short)0x7F);}
			return(advance=(advance<<7)+(read&(short)0x7F));
			//could be advance=(advance*2)+(read%128); as long as read is unsigned and advance is 0
		}
		return reader.readInt32()-reader.position();
	}
	public static ArrayList<EsfMemoryMappedNode>readToOffset(MyBinaryReaderWriter reader,EsfHeader header,long offset)throws IOException{
		ArrayList<EsfMemoryMappedNode>list=new ArrayList<EsfMemoryMappedNode>();
		while(reader.position()<offset){
			EsfMemoryMappedNode node;
			if((node=readUnknownNode(reader,header))!=null){list.add(node);}
		}
		return(list.isEmpty()?null:list);
	}
	public static EsfMemoryMappedNode readUnknownNode(MyBinaryReaderWriter reader,EsfHeader header)throws IOException{
		short code=reader.readByte();
		java.lang.System.out.println("Node type code:"+code);
		if(code==ARRAY.getValue()){}
		else if(code==RECORD.getValue()){return new EsfMemoryMappedRecordNode(reader,header);}
		else if(code==RECORDARRAY.getValue()){return new EsfMemoryMappedRecordNode(reader,header,false);}
		else{
			if(code==BOOLEAN.getValue()){return new EsfBooleanNode(reader,header);}
			else if(code==UINT.getValue()){return new EsfUnsignedIntegerNode(reader,header);}
			else if(code==STRING.getValue()){return new EsfStringNode(reader,header);}
			else{}
		}
		java.lang.System.out.println("I don't have coded it yet");
		return null;
	}
}