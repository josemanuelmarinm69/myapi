/*For a symmetric encryption scheme see http://stackoverflow.com/questions/25061297/rsa-how-can-the-client-in-server-context-encrypt-decrypt-incoming-data*/
package myapi.security;
import static java.math.BigInteger.valueOf;
import java.math.BigInteger;
import java.util.Random;
public class MyRSADecoder{
	int bitLength;
	private BigInteger n,q,p,phi,e,d;
	private MyRSAEncoder encoder;

	public MyRSADecoder(){this(10);}
	public MyRSADecoder(int bitLength){
		this.bitLength=bitLength;
		/*Generate p and q*/
		generatePrimes();
		/*Generate e and d*/
		generateKeys();
	}
	public MyRSADecoder(BigInteger p,BigInteger q,int bitLength){
		this.bitLength=bitLength;
		this.p=p;
		this.q=q;
		/*Generate e and d*/
		generateKeys();
	}
	public BigInteger getN(){return n;}
	public BigInteger getE(){return e;}
	private void generatePrimes(){
		p=new BigInteger(bitLength,100,new Random());
		do{
			q=new BigInteger(bitLength,100,new Random());
		}while(q.compareTo(p)==0);
	}
	private void generateKeys(){
		/*n=p*q*/
		n=p.multiply(q);
		/*toltient=(p-1)*(q-1)*/
		phi=p.subtract(valueOf(1));
		phi=phi.multiply(q.subtract(valueOf(1)));
		/*Lower and co-prime e from n*/
		do{
			e=new BigInteger(bitLength*2,new Random());
		}while((e.compareTo(phi)!=-1)||(e.gcd(phi).compareTo(valueOf(1))!=0));
		/*d=e^1 mod totient*/
		d=e.modInverse(phi);
		encoder=new MyRSAEncoder(n,e);
	}
	/**Encode the <em><strong>message</strong></em> using the public key.
	@param	message - The message to be encoded.
	@return	An array with the codes.*/
	public BigInteger[]encode(String message){return encoder.encode(message);}
	/**Decode the <em><strong>codes</strong></em> using the private key.
	@param	codes - An array with codes to be decoded.
	@return	The decoded plain text.*/
	public String decode(BigInteger[]codes){
		BigInteger[]digits=new BigInteger[codes.length];
		for(int i=0;i<digits.length;i++){digits[i]=codes[i].modPow(d,n);}
		char[]chars=new char[digits.length];
		for(int i=0;i<chars.length;i++){chars[i]=(char)(digits[i].intValue());}
		return new String(chars);
	}
}