package myapi.security;
import java.math.BigInteger;
public class MyRSAEncoder{
	private BigInteger n,e;

	public MyRSAEncoder(BigInteger n,BigInteger e){
		this.n=n;
		this.e=e;
	}
	/**Encode the <em><strong>message</strong></em> using the public key.
	@param	message - The message to be encoded.
	@return	An array with the codes.*/
	public BigInteger[]encode(String message){
		int i;
		byte[]bytes=message.getBytes();
		BigInteger[]digits=new BigInteger[bytes.length];
		for(i=0;i<digits.length;i++){digits[i]=new BigInteger(new byte[]{bytes[i]});}
		BigInteger[]codes=new BigInteger[digits.length];
		for(i=0;i<digits.length;i++){codes[i]=digits[i].modPow(e,n);}
		return codes;
	}
}