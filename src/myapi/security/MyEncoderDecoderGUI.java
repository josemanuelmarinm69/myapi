package myapi.security;
import static java.awt.event.InputEvent.CTRL_MASK;
import static java.awt.event.KeyEvent.VK_1;
import static java.awt.event.KeyEvent.VK_2;
import static java.awt.event.KeyEvent.VK_3;
import static java.awt.event.KeyEvent.VK_C;
import static java.awt.event.KeyEvent.VK_CONTROL;
import static java.awt.event.KeyEvent.VK_N;
import static java.awt.event.KeyEvent.VK_S;
import static java.lang.Integer.valueOf;
import static java.util.Locale.getDefault;
import static javax.swing.KeyStroke.getKeyStroke;
import static myapi.MyFileChooser.textFileFilter;
import static myapi.MyFileChooser.textFileView;
import static myapi.MyFileHelper.loadStrings;
import static myapi.MyOptionPane.getLogin;
import static myapi.MyOptionPane.getString;
import static myapi.MyOptionPane.receiveString;
import static myapi.MyOptionPane.showLogin;
import static myapi.MyOptionPane.showMessage;
import static myapi.MyOptionPane.showSystemRegistryLogin;
import static myapi.security.MyUserSystemRegistry.addUser;
import static myapi.security.MyUserSystemRegistry.getUsers;
import static myapi.utils.OfData.getPrintableChars;
import static myapi.utils.OfData.isEmptyString;
import static myapi.utils.OfEvent.addGlobalKeyEvent;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfFile.getFilesResourceBundle;
import static myapi.utils.OfGUI.getFirstLocaleByCountry;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfGUI.modifyDimension;
import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.SocketTimeoutException;
import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import myapi.MyFileChooser;
import myapi.gui.MyWindow;
import myapi.gui.panels.Addable;
import myapi.gui.panels.MyBorderLayoutPanel;
import myapi.io.MyFileDragDropListener;
import myapi.net.MyClient;
import myapi.net.MyServer;
public class MyEncoderDecoderGUI extends MyWindow{
	private static final long serialVersionUID=-2360106106996789953L;
	private static final int port=9090;

	private Dimension gap;
	private ResourceBundle bundle;
	private HashMap<JRadioButtonMenuItem,Locale>languages;
	private JMenu languageMenu,optionsMenu;
	private JRadioButtonMenuItem languageItems[];
	private JMenuItem encodeItem,decodeItem,addUserItem;
	private EncodePanel encodePanel;
	private DecodePanel decodePanel;
	private MyBorderLayoutPanel contentPane;

	private class EncodePanel extends JPanel{
		private static final long serialVersionUID=-7471635916766085082L;
		private JRadioButton fileOption,editOption;
		private JLabel fileLabel,localhostLabel,publicKeyLabel;
		private JButton fileButton,sendButton;
		private JTextArea textArea,encodedArea;
		private JScrollPane encodeScroll;
		private MyClient client;
		private MyRSAEncoder encoder;

		public EncodePanel(final int componentHeight){
			/*Encode panel that expands the window*/
			super(null);
			addResizableContainerListener(this);
			client=new MyClient(){
				@Override public void sendRequests(BufferedReader reader,DataOutputStream output)throws IOException{
					/*Get public key and set up encoder*/
					BigInteger n=new BigInteger(getPrintableChars(reader.readLine()));
					BigInteger e=new BigInteger(getPrintableChars(reader.readLine()));
					encoder=new MyRSAEncoder(n,e);
					publicKeyLabel.setText("N:"+n+" - E:"+e);
					publicKeyLabel.setSize(getTextWidth(publicKeyLabel),componentHeight);
					publicKeyLabel.setVisible(true);
					/*Encode and write the BigInteger array like strings; also prepare coded output*/
					BigInteger[]codes=encoder.encode(textArea.getText());
					String textOutput="";
					int i=0;
					for(BigInteger code:codes){
						output.writeChars(code.toString()+'\n');
						textOutput+=code.toString();
						if(i==codes.length-1){}
						else if(i>0&&i<9){textOutput+=',';}
						else if(i==9){textOutput+='\n';i=-1;}
						i++;
					}
					/*Write \0 to mark the codes finish*/
					output.writeChars("\0");
					/*Display the codes in the text component*/
					encodedArea.setText(textOutput);
				}
			};

			/*Radio buttons with its action and button group*/
			ActionListener optionsListener=new ActionListener(){
				public void actionPerformed(ActionEvent e){
					fileButton.setEnabled(fileOption.isSelected());
					textArea.setEditable(editOption.isSelected());
					if(fileOption.isSelected()){fileLabel.setText("");textArea.setText("");}
				}
			};
			fileOption=new JRadioButton(bundle.getString("fileOptionText"));
			fileOption.setBounds(gap.width,gap.height,getTextWidth(fileOption),componentHeight);
			fileOption.addActionListener(optionsListener);
			editOption=new JRadioButton(bundle.getString("editOptionText"));
			editOption.setBounds(gap.width+fileOption.getX()+fileOption.getWidth(),gap.height,getTextWidth(editOption),componentHeight);
			editOption.addActionListener(optionsListener);
			editOption.setSelected(true);
			ButtonGroup buttonGroup=new ButtonGroup();
			buttonGroup.add(fileOption);
			buttonGroup.add(editOption);

			/*Select file area, with display component and button to choose the file. It also sets the file text in the edit area*/
			fileLabel=new javax.swing.JLabel();
			fileLabel.setBounds(gap.width+editOption.getX()+editOption.getWidth(),gap.height,100,componentHeight);
			fileButton=new javax.swing.JButton(bundle.getString("openButtonText"));
			fileButton.setBounds(gap.width+fileLabel.getX()+fileLabel.getWidth(),gap.height,getTextWidth(fileButton),componentHeight);
			fileButton.setEnabled(false);
			addSizeToParentListener(fileButton);
			fileButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					MyFileChooser chooser=new MyFileChooser(textFileFilter,textFileView,false,true);
					if(!chooser.select()){return;}
					displayText(chooser.getSelectedFile());
				}
			});

			/*Editable text area*/
			textArea=new JTextArea();
			new DropTarget(textArea,new MyFileDragDropListener(new AbstractAction(){
				private static final long serialVersionUID=-4831231817079313926L;

				public void actionPerformed(ActionEvent e){
					MyFileDragDropListener listener=(MyFileDragDropListener)e.getSource();
					List<File>fileList=listener.getList();
					if(fileList==null||fileList.isEmpty()){return;}
					fileOption.doClick();
					displayText(fileList.get(fileList.size()-1));
				}
			}));
			JScrollPane textScroll=new JScrollPane(textArea);
			textScroll.setBounds(gap.width,gap.height+fileOption.getY()+fileOption.getHeight(),200,200);

			/*Encode and send buttons between the text areas. Encode button will encode the first area text and displays it in the second area*/
			sendButton=new JButton(bundle.getString("sendButtonText"));
			sendButton.setBounds(gap.width+textScroll.getX()+textScroll.getWidth(),textScroll.getY(),getTextWidth(sendButton),componentHeight);
			sendButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					if(textArea.getText().length()<1){return;}
					try{
						if(!receiveString(bundle.getString("inputHostMessage"),bundle.getString("inputHostTitle"))){client.connect(port,1000);}
						else{client.connect(getString(),port,1000);}
					}
					catch(SocketTimeoutException ex){showMessage(bundle.getString("serverNotFoundMessage"),bundle.getString("serverNotFoundTitle"));}
					catch(IOException e1){}
				}
			});

			/*Prepare the label to show the public key numbers and local host tip label*/
			localhostLabel=new JLabel(bundle.getString("localhostMessage"));
			localhostLabel.setBounds(sendButton.getX(),gap.height+sendButton.getY()+sendButton.getHeight(),getTextWidth(localhostLabel),componentHeight);
			publicKeyLabel=new JLabel();
			publicKeyLabel.setLocation(localhostLabel.getX(),gap.height+localhostLabel.getY()+localhostLabel.getHeight());
			publicKeyLabel.setVisible(false);
			addSizeToParentListener(publicKeyLabel);

			/*Encoded text area*/
			encodedArea=new JTextArea();
			encodedArea.setEditable(false);
			encodeScroll=new JScrollPane(encodedArea);
			encodeScroll.setBounds(gap.width+localhostLabel.getX()+localhostLabel.getWidth(),
					textScroll.getY(),textScroll.getWidth(),textScroll.getHeight());

			/*Add the components and expand the panel with the both gap*/
			add(fileOption);
			add(editOption);
			add(fileLabel);
			add(fileButton);
			add(textScroll);
			add(sendButton);
			add(publicKeyLabel);
			add(localhostLabel);
			add(encodeScroll);
			Dimension encodeDimension=getSize();
			encodeDimension=modifyDimension(encodeDimension,gap.width,gap.height);
			setPreferredSize(encodeDimension);

			addGlobalKeyEvent(new AbstractAction(){
				private static final long serialVersionUID=5840004611298311561L;

				public void actionPerformed(ActionEvent arg0){if(sendButton!=null){sendButton.doClick();}}
			},VK_CONTROL,VK_N);
		}
		private synchronized void displayText(File textFile){
			fileLabel.setText(textFile.getAbsolutePath());
			fileLabel.setSize(getTextWidth(fileLabel),fileLabel.getHeight());
			fileButton.setLocation(gap.width+fileLabel.getX()+fileLabel.getWidth(),fileButton.getY());
			ArrayList<String>list=loadStrings(textFile);
			String text="";
			for(int i=0;i<list.size();i++){
				String string=list.get(i);
				text+=string;
				if(i<(list.size()-1)){text+='\n';}
			}
			textArea.setText(text);
		}
		private synchronized void updateLocale(){
			fileOption.setText(bundle.getString("fileOptionText"));
			fileOption.setSize(getTextWidth(fileOption),fileOption.getHeight());
			editOption.setText(bundle.getString("editOptionText"));
			editOption.setBounds(gap.width+fileOption.getX()+fileOption.getWidth(),gap.height,getTextWidth(editOption),editOption.getHeight());
			fileLabel.setLocation(gap.width+editOption.getX()+editOption.getWidth(),gap.height);
			fileButton.setText(bundle.getString("openButtonText"));
			fileButton.setSize(getTextWidth(fileButton),fileButton.getHeight());
			sendButton.setText(bundle.getString("sendButtonText"));
			sendButton.setSize(getTextWidth(sendButton),sendButton.getHeight());
			localhostLabel.setText(bundle.getString("localhostMessage"));
			localhostLabel.setSize(getTextWidth(localhostLabel),localhostLabel.getHeight());
			encodeScroll.setLocation(gap.width+localhostLabel.getX()+localhostLabel.getWidth(),encodeScroll.getY());
			repaint();
		}
		@Override public synchronized final void setPreferredSize(Dimension dimension){super.setPreferredSize(dimension);setSize(dimension);}
	}
	private class DecodePanel extends JPanel implements Addable{
		private static final long serialVersionUID=-8481326232603391531L;
		private Dimension textSize;
		private JTabbedPane textPane,decodedPane;
		private JButton decodeButton;
		private HashMap<Integer,JTextArea>decodedAreas;
		private HashMap<Integer,BigInteger[]>codes;
		private MyRSADecoder decoder;
		private MyServer server;
		private int received;

		public DecodePanel(int componentHeight){this(componentHeight,245,222);}
		public DecodePanel(int componentHeight,int textWidth,int textHeight){
			/*Encode panel that expands the window*/
			super(null);
			addResizableContainerListener(this);
			textSize=new Dimension(textWidth,textHeight);
			decodedAreas=new HashMap<Integer,JTextArea>();
			codes=new HashMap<Integer,BigInteger[]>();
			decoder=new MyRSADecoder();
			server=new MyServer(port){
				@Override public void dispatchClients(BufferedReader reader,DataOutputStream output)throws IOException{
					/*Send public key*/
					output.writeChars(decoder.getN().toString()+'\n');
					output.writeChars(decoder.getE().toString()+'\n');
					/*Read BigInteger codes and decode*/
					ArrayList<BigInteger>codes=new ArrayList<BigInteger>();
					String code=null;
					while(!isEmptyString((code=getPrintableChars(reader.readLine())))){codes.add(new BigInteger(code));}
					/*Show message*/
					addReceivedCodes(codes.toArray(new BigInteger[codes.size()]));
				}
			};

			/*Encoded text areas tab pane*/
			textPane=new JTabbedPane();
			textPane.setBounds(gap.width,gap.height,250,250);

			/*Decode the text area and show in the decoded text area*/
			decodeButton=new JButton(bundle.getString("decodeButtonText"));
			decodeButton.setBounds(gap.width+textPane.getX()+textPane.getWidth(),textPane.getY(),getTextWidth(decodeButton),componentHeight);
			decodeButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){
					int index=textPane.getSelectedIndex();
					if(index<0){return;}
					Integer key=valueOf(textPane.getTitleAt(index));
					String decodedText=decoder.decode(codes.get(key));
					decodedAreas.get(key).setText(decodedText);
				}
			});

			/*Private key numbers label*/
			JLabel privateKeyLabel1=new JLabel("N:"+decoder.getN());
			privateKeyLabel1.setBounds(decodeButton.getX(),gap.height+decodeButton.getY()+decodeButton.getHeight(),
					getTextWidth(privateKeyLabel1),componentHeight);
			JLabel privateKeyLabel2=new JLabel("E:"+decoder.getE());
			privateKeyLabel2.setBounds(privateKeyLabel1.getX(),gap.height+privateKeyLabel1.getY()+privateKeyLabel1.getHeight(),
					getTextWidth(privateKeyLabel2),componentHeight);

			/*Decoded text area tab pane*/
			decodedPane=new JTabbedPane();
			decodedPane.setBounds(gap.width+decodeButton.getX()+decodeButton.getWidth(),textPane.getY(),textPane.getWidth(),textPane.getHeight());

			/*Add the components and expand the panel with the both gap*/
			add(textPane);
			add(decodeButton);
			add(privateKeyLabel1);
			add(privateKeyLabel2);
			add(decodedPane);
			Dimension encodeDimension=getSize();
			encodeDimension=modifyDimension(encodeDimension,gap.width,gap.height);
			setPreferredSize(encodeDimension);

			addGlobalKeyEvent(new AbstractAction(){
				private static final long serialVersionUID=5840004611298311561L;

				public void actionPerformed(ActionEvent arg0){added();}
			},VK_CONTROL,VK_S);
			addGlobalKeyEvent(new AbstractAction(){
				private static final long serialVersionUID=5840004611298311561L;

				public void actionPerformed(ActionEvent arg0){removed();}
			},VK_CONTROL,VK_C);
		}
		private synchronized void updateLocale(){
			decodeButton.setText(bundle.getString("decodeButtonText"));
			decodeButton.setSize(getTextWidth(decodeButton),decodeButton.getHeight());
			decodedPane.setLocation(gap.width+decodeButton.getX()+decodeButton.getWidth(),decodedPane.getY());
			repaint();
		}
		private synchronized final void addReceivedCodes(BigInteger codes[]){
			/*Prepare coded text to show*/
			String textOutput="";
			int i=0;
			for(BigInteger integer:codes){
				textOutput+=integer.toString();
				if(i==codes.length-1){}
				else if(i>0&&i<9){textOutput+=',';}
				else if(i==9){textOutput+='\n';i=-1;}
				i++;
			}
			if(textPane.getTabCount()>0&&textPane.getTabCount()%6==0){
				textPane.removeTabAt(0);
				decodedPane.removeTabAt(0);
			}
			JTextArea textArea=new JTextArea(textOutput);
			textArea.setEditable(false);
			JScrollPane textScroll=new JScrollPane(textArea);
			textScroll.setBounds(0,0,textSize.width,textSize.height);
			JPanel textPanel=new JPanel(null);
			addResizableContainerListener(textPanel);
			textPanel.add(textScroll);
			textPanel.setSize(textPanel.getPreferredSize());
			textPane.addTab(""+(received+1),textPanel);
			JTextArea decodedArea=new JTextArea();
			decodedArea.setEditable(false);
			JScrollPane decodedScroll=new JScrollPane(decodedArea);
			decodedScroll.setBounds(0,0,textSize.width,textSize.height);
			JPanel decodedPanel=new JPanel(null);
			addResizableContainerListener(decodedPanel);
			decodedPanel.add(decodedScroll);
			decodedPanel.setSize(textPanel.getPreferredSize());
			decodedPane.addTab(""+(received+1),decodedPanel);
			decodedAreas.put(new Integer(received+1),decodedArea);
			this.codes.put(new Integer(received+1),codes);
			received++;
		}
		public synchronized final void added(){
			try{server.startup();}
			catch(IOException e){}
		}
		public synchronized final void removed(){
			try{server.shutdown();}
			catch(IOException e){}
		}
		@Override public synchronized final void setPreferredSize(Dimension dimension){super.setPreferredSize(dimension);setSize(dimension);}
	}

	public MyEncoderDecoderGUI(boolean invitedUser,int componentHeight,int horizontalGap,int verticalGap){
		super(null,false);
		gap=new Dimension(horizontalGap,verticalGap);
		bundle=getFilesResourceBundle("encoder",getLocale());
		languages=new HashMap<JRadioButtonMenuItem,Locale>();

		/*Sets the displayed panels*/
		contentPane=new MyBorderLayoutPanel(new Dimension(600,300),true);
		encodePanel=new EncodePanel(componentHeight);
		decodePanel=new DecodePanel(componentHeight);

		/*Sets language support options and interface*/
		JMenuBar menuBar=new JMenuBar();
		optionsMenu=new JMenu(bundle.getString("optionsMenuText"));
		menuBar.add(optionsMenu);
		encodeItem=new JMenuItem(bundle.getString("encodeMenuText"));
		encodeItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){contentPane.add(encodePanel);}
		});
		encodeItem.setAccelerator(getKeyStroke(VK_1,CTRL_MASK));
		optionsMenu.add(encodeItem);
		decodeItem=new JMenuItem(bundle.getString("decodeMenuText"));
		decodeItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){contentPane.add(decodePanel);}
		});
		decodeItem.setAccelerator(getKeyStroke(VK_2,CTRL_MASK));
		decodeItem.setEnabled(!invitedUser);
		optionsMenu.add(decodeItem);
		addUserItem=new JMenuItem(bundle.getString("addUserMenuText"));
		addUserItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(!showLogin(bundle.getString("addUserMessage"),bundle.getString("addUserTitle"))){return;}
				addUser(getLogin());
			}
		});
		addUserItem.setAccelerator(getKeyStroke(VK_3,CTRL_MASK));
		addUserItem.setEnabled(!invitedUser);
		optionsMenu.add(addUserItem);
		languageMenu=new JMenu(bundle.getString("languageMenuText"));
		menuBar.add(languageMenu);
		String languageTexts[]=bundle.getString("languages").split(",");
		languageItems=new JRadioButtonMenuItem[languageTexts.length];
		ButtonGroup languageOptions=new ButtonGroup();
		for(int i=0;i<languageTexts.length;i++){
			languageItems[i]=new JRadioButtonMenuItem(languageTexts[i]);
			languageItems[i].addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e){updateLocale(languages.get(e.getSource()));}
			});
			languageMenu.add(languageItems[i]);
			languageOptions.add(languageItems[i]);
			java.util.Locale locale=getFirstLocaleByCountry(languageTexts[i]);
			if(locale.getDisplayLanguage().equalsIgnoreCase(getLocale().getDisplayLanguage())){languageItems[i].setSelected(true);}
			languages.put(languageItems[i],locale);
		}

		/*Sets the window properties and shows it*/
		setTitle(bundle.getString("title"));
		setJMenuBar(menuBar);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setContentPane(contentPane);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);

		addSizeToParentListener(decodePanel);
		addResizableContainerListener(decodePanel);
	}
	public synchronized void updateLocale(Locale locale){
		if(locale!=null){
			bundle=getFilesResourceBundle("encoder",locale);
			setTitle(bundle.getString("title"));
			optionsMenu.setText(bundle.getString("optionsMenuText"));
			languageMenu.setText(bundle.getString("languageMenuText"));
			String languageTexts[]=bundle.getString("languages").split(",");
			for(int i=0;i<languageTexts.length;i++){languageItems[i].setText(languageTexts[i]);}
			encodeItem.setText(bundle.getString("encodeMenuText"));
			decodeItem.setText(bundle.getString("decodeMenuText"));
			addUserItem.setText(bundle.getString("addUserMenuText"));
			encodePanel.updateLocale();
			decodePanel.updateLocale();
		}
	}
	public static void createAndShowGUI(){
		ResourceBundle bundle=getFilesResourceBundle("encoder",getDefault());
		if(!showSystemRegistryLogin(bundle.getString("selectUserMessage"),bundle.getString("selectUserTitle"))){return;}
		Entry<String,String>login=getLogin();
		String password=getUsers().get(login.getKey());
		if(!login.getValue().equals(password)){
			showMessage(bundle.getString("invalidPasswordMessage"),bundle.getString("invalidPasswordTitle"));
			return;
		}
		new MyEncoderDecoderGUI(login.getKey().contains("invitado"),20,20,20);
	}
}