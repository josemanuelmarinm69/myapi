package myapi.security;
import static java.util.prefs.Preferences.userNodeForPackage;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
public class MyUserSystemRegistry{
	public static boolean userExists(String user){
		Preferences preferences=userNodeForPackage(MyUserSystemRegistry.class);
		try{
			for(String key:preferences.keys()){
				String userName=key.replaceFirst("u_","");
				if(key.contains("u_")&&userName.equalsIgnoreCase(user)){return true;}
			}
		}
		/*--ErrorConsole--*/catch(BackingStoreException e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
		return false;
	}
	public static void addUser(Entry<String,String>login){addUser(login.getKey(),login.getValue());}
	public static void addUser(String user,String password){
		Preferences preferences=userNodeForPackage(MyUserSystemRegistry.class);
		preferences.put("u_"+user.toLowerCase(),password);
	}
	public static HashMap<String,String>getUsers(){
		Preferences preferences=userNodeForPackage(MyUserSystemRegistry.class);
		try{
			HashMap<String,String>users=new HashMap<String,String>();
			for(String key:preferences.keys()){
				String userName=key.replaceFirst("u_","");
				String userPassword=preferences.get(key,null);
				if(key.contains("u_")){users.put(userName,userPassword);}
			}
			if(users.size()==0){users=null;}
			return users;
		}
		/*--ErrorConsole--*/catch(BackingStoreException e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
		return null;
	}
}