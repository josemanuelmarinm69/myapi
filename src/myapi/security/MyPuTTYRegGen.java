package myapi.security;
import static java.util.Locale.ENGLISH;
import static myapi.MyFileChooser.awsFileFilter;
import static myapi.MyFileChooser.awsFileView;
import static myapi.MyFileChooser.regFileFilter;
import static myapi.MyFileChooser.regFileView;
import static myapi.MyFileHelper.loadStrings;
import static myapi.utils.OfFile.getFilesFolder;
import static myapi.utils.OfFile.getFilesResourceBundle;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import myapi.MyFileChooser;
public class MyPuTTYRegGen{
	public MyPuTTYRegGen()throws IOException{
		MyFileChooser chooser=new MyFileChooser(awsFileFilter,awsFileView,true,getFilesFolder(),true);
		if(!chooser.select()){return;}
		File source=chooser.getSelectedFile();
		chooser=new MyFileChooser(regFileFilter,regFileView,true,getFilesFolder(),true);
		if(!chooser.create()){return;}
		File target=chooser.getSelectedFile();
		PrintWriter writer=new PrintWriter(target);
		writer.write(generatePuTTY(loadStrings(source.getName()),getFilesResourceBundle("PuTTYRegGen",ENGLISH)));
		writer.close();
	}
	public static String generatePuTTY(ArrayList<String>lines,ResourceBundle bundle){
		String output=bundle.getString("head");
		for(String line:lines){
			String fields[]=line.split("\\t");
			String name=fields[0];
			String ip=fields[1];
			String key=fields[2];
			String entry=bundle.getString("entry");
			if(!"-".equalsIgnoreCase(ip)){
				output+=entry.replaceAll("\\$name","centos@"+ip+" ("+name+")").replaceAll("\\$host","centos@"+ip).replaceAll("\\$key",bundle.getString(key)).replaceAll(" ","_");
				output+=entry.replaceAll("\\$name",name).replaceAll("\\$host","centos@"+ip).replaceAll("\\$key",bundle.getString(key)).replaceAll(" ","_");
			}
		}
		output+=bundle.getString("tail");
		return output;
	}
}