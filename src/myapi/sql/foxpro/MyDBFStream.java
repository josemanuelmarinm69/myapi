package myapi.sql.foxpro;
import java.util.ArrayList;
import com.hexiong.jdbf.JDBFException;
public class MyDBFStream{
	private String headers[];
	private Object data[][];
	private String fileName;

	public MyDBFStream(String fileName)throws JDBFException{
		this.fileName=fileName;
		try{
			MyDBFReader reader=new MyDBFReader(fileName);
			int i;headers=new String[reader.getFieldCount()];
			for(i=0;i<headers.length;i++){headers[i]=reader.getField(i).getName();}
			ArrayList<Object[]>records=new ArrayList<Object[]>();
			while(reader.hasNextRecord()){Object objects[]=reader.nextRecord();records.add(objects);}
			data=new Object[records.size()][getColumns().length];
			for(int j=0;j<records.size();j++){data[j]=records.get(j);}
		}
		/*--ErrorConsole--*/catch(JDBFException e){myapi.ErrorConsole.addText(e);throw e;}/*--ErrorConsole--*/
	}
	public String[]getColumns(){return headers;}
	public Object[][]getRows(){return data;}
	public String getFileName(){return fileName;}
}