package myapi.sql.foxpro;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import com.hexiong.jdbf.JDBFException;
public class MyDBFReader{
	private DataInputStream stream;
	private String charset;
	private MyDBFField[]fields;
	private byte[]nextRecord;
	private int nFieldCount;

	public MyDBFReader(String s)throws JDBFException{this(s,null);}
	public MyDBFReader(String s,String charset)throws JDBFException{
		stream=null;
		fields=null;
		nextRecord=null;
		nFieldCount=0;
		this.charset=charset;
		try{init(new FileInputStream(s));}
		catch(FileNotFoundException e){throw new JDBFException(e);}
	}
	public MyDBFReader(InputStream inputStream)throws JDBFException{this(inputStream,null);}
	public MyDBFReader(InputStream inputStream,String charset)throws JDBFException{
		stream=null;
		fields=null;
		nextRecord=null;
		this.charset=charset;
		init(inputStream);
	}
	private void init(InputStream inputStream)throws JDBFException{
		try{
			stream=new DataInputStream(inputStream);
			int i=readHeader();
			fields=new MyDBFField[i];
			int j=1;
			for(int k=0;k<i;k++){fields[k]=readFieldHeader();if(fields[k]!=null){nFieldCount+=1;j+=fields[k].getLength();}}
			nextRecord=new byte[j];
			try{stream.readFully(nextRecord);}
			catch(EOFException e){nextRecord=null;stream.close();}
			int pos=0;
			for(int p=0;p<j;p++){if((nextRecord[p]==32)||(nextRecord[p]==42)){pos=p;break;}}
			if(pos>0){
				byte[]others=new byte[pos];
				stream.readFully(others);
				for(int p=0;p<j-pos;p++){nextRecord[p]=nextRecord[(p+pos)];}
				for(int p=0;p<pos;p++){nextRecord[(j-p-1)]=others[(pos-p-1)];}
			}
		}
		catch(IOException e){throw new JDBFException(e);}
	}
	private int readHeader()throws IOException,JDBFException{
		byte[]bytes=new byte[16];
		try{stream.readFully(bytes);}
		catch(EOFException e){throw new JDBFException("Unexpected end of file reached.");}
		int i=bytes[8];
		if(i<0){i+=256;}
		i+=256*bytes[9];i--;i/=32;i--;
		try{stream.readFully(bytes);}
		catch(EOFException e1){throw new JDBFException("Unexpected end of file reached.");}
		return i;
	}
	private MyDBFField readFieldHeader()throws IOException,JDBFException{
		byte[]bytes=new byte[16];
		try{stream.readFully(bytes);}
		catch(EOFException e){throw new JDBFException("Unexpected end of file reached.");}
		if((bytes[0]==13)||(bytes[0]==0)){stream.readFully(bytes);return null;}
		StringBuffer stringBuffer=new StringBuffer(10);
		int i=0;
		for(i=0;i<10;i++){if(bytes[i]==0){break;}}
		stringBuffer.append(new String(bytes,0,i));
		char c=(char)bytes[11];
		try{stream.readFully(bytes);}
		catch(EOFException e1){throw new JDBFException("Unexpected end of file reached.");}
		int j=bytes[0],k=bytes[1];
		if(j<0){j+=256;}if(k<0){k+=256;}
		return new MyDBFField(stringBuffer.toString(),c,j,k);
	}
	public int getFieldCount(){return nFieldCount;}
	public MyDBFField getField(int i){return fields[i];}
	public boolean hasNextRecord(){return nextRecord!=null;}
	public Object[]nextRecord()throws JDBFException{
		if(!hasNextRecord()){throw new JDBFException("No more records available.");}
		Object[]objects=new Object[nFieldCount];
		int i=1;
		for(int j=0;j<objects.length;j++){
			int k=fields[j].getLength();
			StringBuffer stringBuffer=new StringBuffer(k);
			try{stringBuffer.append(new String(nextRecord,i,k,charset!=null?charset:"ISO_8859_1"));}
			catch(UnsupportedEncodingException e){}
			objects[j]=fields[j].parse(stringBuffer.toString());
			i+=fields[j].getLength();
		}
		try{stream.readFully(nextRecord);}
		catch(EOFException e){nextRecord=null;}
		catch(IOException e){throw new JDBFException(e);}
		return objects;
	}
	public void close()throws JDBFException{
		nextRecord=null;
		try{stream.close();}
		catch(IOException e){throw new JDBFException(e);}
	}
}