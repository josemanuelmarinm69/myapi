package myapi.sql;
import static java.lang.reflect.Modifier.isPrivate;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import myapi.generics.MyStructure;
import myapi.gui.panels.Listable;
public class MySQLTable<T extends MySQLRow>implements MyStructure<T>{
	private Class<?>dataClass;
	private LinkedHashMap<String,String>map;
	private LinkedHashMap<String,String>parameters;
	private Field fields[];
	private Constructor<?>constructor;
	private String table;
	private String primaryKey;
	private Connection connection;

	@SuppressWarnings("unchecked")
	public MySQLTable(Connection connection,Class<?>dataClass){
		this.dataClass=dataClass;
		this.connection=connection;
		constructor=dataClass.getConstructors()[0];
		constructor.setAccessible(true);
		try{
			Object instance=constructor.newInstance((Object[])null);
			Method mapMethod=dataClass.getMethod("map",(Class<?>[])null);
			map=(LinkedHashMap<String,String>)mapMethod.invoke(instance,(Object[])null);
			if(instance instanceof Listable){
				Method parameterMethod=dataClass.getMethod("parametrizar",(Class<?>[])null);
				parameters=(LinkedHashMap<String,String>)parameterMethod.invoke(instance,(Object[])null);
			}
		}
		catch(InstantiationException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(InvocationTargetException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(NoSuchMethodException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(SecurityException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		fields=dataClass.getDeclaredFields();
		String name=dataClass.getName().toLowerCase();
		table=name.substring(name.lastIndexOf(".")+1,name.length());
		primaryKey=map.get(fields[0].getName());
/*--MessageConsole--*/myapi.MessageConsole.addText("primary key "+primaryKey);/*--MessageConsole--*/
	}
	public void insert(T row)throws SQLException{
		String sentence="insert into "+table+" values(";
		int i=0,total=fields.length;
		for(Entry<String,String>entry:map.entrySet()){
			try{
				Field field=dataClass.getDeclaredField(entry.getKey());
				field.setAccessible(true);
				Object value=field.get(row);
				if(value==null){sentence+=""+value;}
				else if(value instanceof String||value instanceof Date||value instanceof Time||value instanceof Character){sentence+="'"+value+"'";}
				else{sentence+=""+value;}
			}
			catch(NoSuchFieldException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			if(i==total-1){sentence+=")";}
			else{sentence+=",";}
			i++;
		}
/*--MessageConsole--*/myapi.MessageConsole.addText(sentence);/*--MessageConsole--*/
		connection.createStatement().executeUpdate(sentence);
	}
	@SuppressWarnings("unchecked")
	public ArrayList<T>list(String condition){
		String sentence="select*from "+table;
		if(condition!=null){sentence+=" "+condition;}
		try{
/*--MessageConsole--*/myapi.MessageConsole.addText(sentence);/*--MessageConsole--*/
			ResultSet rs=connection.createStatement().executeQuery(sentence);
			ArrayList<T>lista=new ArrayList<T>();
			while(rs.next()){
				Object instance=constructor.newInstance((Object[])null);
				for(Entry<String,String>entry:map.entrySet()){
					Field field=dataClass.getDeclaredField(entry.getKey());
					if(!isPrivate(field.getModifiers())){field.set(instance,rs.getObject(map.get(field.getName())));}
					else{
						String name=field.getName().substring(0,1).toUpperCase()+field.getName().substring(1,field.getName().length());
						Method setter=dataClass.getMethod("set"+name,new Class<?>[]{field.getType()});
						Object object=rs.getObject(map.get(field.getName()));
/*--MessageConsole--*/myapi.MessageConsole.addText("Setter set"+name+"("+field.getType().getName()+")");/*--MessageConsole--*/
/*--MessageConsole--*/myapi.MessageConsole.addText("Atribute "+map.get(field.getName()));/*--MessageConsole--*/
if(object!=null){/*--MessageConsole--*/myapi.MessageConsole.addText("Parameter "+object.getClass());/*--MessageConsole--*/}
						setter.invoke(instance,new Object[]{object});
					}
				}
				lista.add((T)instance);
			}
			return lista;
		}
		catch(SQLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(InstantiationException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(InvocationTargetException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(NoSuchMethodException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		catch(NoSuchFieldException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return null;
	}
	public ArrayList<T>list(){return list(null);}
	public ResultSet execute(String sentence)throws SQLException{
/*--MessageConsole--*/myapi.MessageConsole.addText(sentence);/*--MessageConsole--*/
		ResultSet set=null;
		PreparedStatement pst=this.connection.prepareStatement(sentence);
		if(sentence.split(" ")[0].contains("select")){
			set=pst.executeQuery();
			set.next();
		}
		else{pst.execute();}
		return set;
	}
	public ArrayList<ResultSet>executeProcedure(String procedure,Object...arguments)throws SQLException{
		String sentence="{call "+procedure+"(";
		for(int i=0;i<arguments.length;i++){
			if(i==arguments.length-1){sentence+=" ?";}
			else{sentence+=" ? ,";}
		}
		sentence+=")}";
/*--MessageConsole--*/myapi.MessageConsole.addText(sentence);/*--MessageConsole--*/
		CallableStatement cst=this.connection.prepareCall(sentence);
		int i=1;
		for(Object argument:arguments){
			if(argument==null){cst.setObject(i,argument);}
			else if(argument instanceof String||argument instanceof Date||argument instanceof Time){cst.setString(i,argument.toString());}
			else{cst.setObject(i,argument);}
			i++;
		}
		boolean results=cst.execute();
/*--MessageConsole--*/myapi.MessageConsole.addText(new Boolean(results));/*--MessageConsole--*/
		ArrayList<ResultSet>sets=new ArrayList<ResultSet>();
		while(results){
			ResultSet set=cst.getResultSet();
			set.next();
			sets.add(set);
			results=cst.getMoreResults();
		}
		return sets;
	}
	public boolean isEmpty(){
		String sentence="select*from "+table;
		try{return!connection.createStatement().executeQuery(sentence).next();}
		catch(SQLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return true;
	}
	public T search(Object primaryKey){
		try{
			String condition="where "+primaryKey+"=";
			if(primaryKey==null){condition+=""+primaryKey;}
			else if(primaryKey instanceof String||primaryKey instanceof Date||primaryKey instanceof Time||primaryKey instanceof Character){condition+="'"+primaryKey+"'";}
			else{condition+=""+primaryKey;}
			ArrayList<T>array=list(condition);
			if(!array.isEmpty()){return array.get(0);}
		}
		catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return null;
	}
	public void overwrite(T row)throws SQLException{
		String sentence="update "+table+" set ";
		Object primary=null;
		int i=0,total=fields.length;
		for(Entry<String,String>entry:map.entrySet()){
			try{
				Field field=dataClass.getDeclaredField(entry.getKey());
				field.setAccessible(true);
				Object value=field.get(row);
				if(i==0){primary=value;}
				sentence+=map.get(field.getName())+"=";
				if(value==null){sentence+=""+value;}
				else if(value instanceof String||value instanceof Date||value instanceof Time||value instanceof Character){sentence+="'"+value+"'";}
				else{sentence+=""+value;}
			}
			catch(IllegalArgumentException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(NoSuchFieldException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			if(i==total-1){sentence+=" ";}
			else{sentence+=",";}
			i++;
		}
		sentence+="where "+primaryKey+"=";
		if(primary==null){sentence+=""+primary;}
		else if(primary instanceof String||primary instanceof Date||primary instanceof Time){sentence+="'"+primary+"'";}
		else{sentence+=""+primary;}
/*--MessageConsole--*/myapi.MessageConsole.addText(sentence);/*--MessageConsole--*/
		connection.createStatement().executeUpdate(sentence);
	}
	public void delete(Object primaryKey)throws SQLException{
		String sentence="delete from "+table+" where "+primaryKey+"=";
		if(primaryKey==null){sentence+=""+primaryKey;}
		else if(primaryKey instanceof String||primaryKey instanceof Date||primaryKey instanceof Time){sentence+="'"+primaryKey+"'";}
		else{sentence+=""+primaryKey;}
		connection.createStatement().executeUpdate(sentence);
	}
	public Integer getAutoIncrementValue(String database)throws SQLException{
		try{
			String sentence="select AUTO_INCREMENT from information_schema.tables where TABLE_SCHEMA='"+
				database+"'and TABLE_NAME='"+table+"'";
/*--MessageConsole--*/myapi.MessageConsole.addText(sentence);/*--MessageConsole--*/
			ResultSet rs=connection.createStatement().executeQuery(sentence);
			rs.next();
			return new Integer(rs.getInt("AUTO_INCREMENT"));
		}
		catch(SQLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/throw e;}
	}
	public boolean getFlag(String name){
		try{
			ResultSet rs=connection.createStatement().executeQuery("select @"+name+" as Error");
			rs.next();
			Integer result=(Integer)rs.getObject("Error");
			if(result!=null){return result.intValue()==1;}
		}
		catch(SQLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return false;
	}
	public Object[]fetchInstance(T instance,boolean full){
		LinkedHashMap<String,String>map;
		if(full){map=this.map;}
		else{map=parameters;}
		Object values[]=new Object[map.values().size()];
		int i=0;
		for(Entry<String,String>entry:map.entrySet()){
			if(full){values[i]=entry.getKey();}
			else{values[i]=entry.getValue();}
			try{
				Field field=dataClass.getDeclaredField(values[i].toString());
				field.setAccessible(true);
				values[i]=field.get(instance);
			}
			catch(NoSuchFieldException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			catch(IllegalAccessException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			i++;
		}
		return values;
	}
	public Class<?>getDataClass(){return dataClass;}
	public String getTableName(){return table;}
	public LinkedHashMap<String,String>getParameters(){return parameters;}
	public LinkedHashMap<String,String>getMap(){return map;}
}