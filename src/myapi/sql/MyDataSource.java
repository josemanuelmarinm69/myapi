package myapi.sql;
public enum MyDataSource{
	Firebird,
	MySQL,
	SQLServer,
	Sybase,
	Oracle,
	DB2,
	ODBC
}