package myapi.sql;
import static java.lang.Class.forName;
import static java.sql.DriverManager.getConnection;
import static myapi.utils.OfOS.execute;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
public class MyConnection{
	private MyDataSource source;
	private String user;
	private String password;

	public MyConnection(MyDataSource source,String user,String password){
		this.source=source;
		this.user=user;
		this.password=password;
	}
	public Connection connectTo(String db)throws SQLException{return connectTo("localhost",db);}
	public Connection connectTo(String host,String db)throws SQLException{
		try{
			switch(source){
				case MySQL:
					forName("com.mysql.jdbc.Driver");
					return getConnection("jdbc:mysql://"+host+"/"+db,user,password);
				case Firebird:
					forName("org.firebirdsql.jdbc.FBDriver");
					return getConnection("jdbc:firebirdsql:"+host+"/3050:"+db,user,password);
				default:myapi.MessageConsole.addText("Data source not yet supported");
			}
		}
		catch(ClassNotFoundException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return null;
	}
	public boolean executeScript(File file){return executeScript(file.getAbsolutePath());}
	public boolean executeScript(String fileName){
		switch(source){
			case MySQL:return execute("mysql","-u",user,"--password="+password,"-e","source "+fileName);
			default:return false;
		}
	}
	public static void closeConnection(Connection connection){
		if(connection!=null){
			try{connection.close();}
			catch(SQLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		}
	}
	public static boolean columnInSet(ResultSet set,String column){
		try{
			ResultSetMetaData metaData=set.getMetaData();
			int columns=metaData.getColumnCount();
			for(int i=1;i<=columns;i++){
				if(column.equals(metaData.getColumnName(i))){return true;}
			}
		}
		catch(SQLException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		return false;
	}
}