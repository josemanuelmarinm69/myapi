package myapi.regex;
import static java.util.regex.Pattern.compile;
import static myapi.utils.OfData.isEmptyString;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import myapi.regex.MyAbstractSyntaxTree.Token;
public class MyLexicalAnalyzer{
	private ResourceBundle bundle;
	private ArrayList<Token>idsSet;
	private ArrayList<Token>tokenOutput;
	private ArrayList<String>errorOutput;
	private Pattern id;
	private HashMap<Pattern,String>keywords;
	private HashMap<Pattern,String>literals;

	public MyLexicalAnalyzer(ResourceBundle bundle){
		this.bundle=bundle;
		id=compile(bundle.getString("id"));
		keywords=new HashMap<Pattern,String>();
		String keywordNames[]=bundle.getString("keywords").split(",");
		for(String keywordName:keywordNames){keywords.put(compile(bundle.getString(keywordName)),keywordName);}
		literals=new HashMap<Pattern,String>();
		String literalNames[]=bundle.getString("literals").split(",");
		for(String literalName:literalNames){literals.put(compile(bundle.getString(literalName)),literalName);}
	}
	public ArrayList<Token>getIdsSet(){return idsSet;}
	public ArrayList<String>getErrorList(){return errorOutput;}
	public synchronized ArrayList<Token>analyze(String text){
		int lineNumber=1;
		idsSet=new ArrayList<Token>();
		tokenOutput=new ArrayList<Token>();
		errorOutput=new ArrayList<String>();
		String lines[]=text.split("\n");
		lineloop:
		while(lineNumber<=lines.length){
			String line=lines[lineNumber-1]+' ';
			String currentToken="";
			for(int i=0;i<line.length()&&i>=0;i++){
				char c=line.charAt(i);
				if(c==' '||c=='\t'||c=='('||c==')'||c=='='||c==','||c==';'||c=='{'||c=='}'||c=='/'||c=='*'){
					if(c=='/'&&i<line.length()-1&&line.charAt(i+1)=='*'){
						saveToken(currentToken,"/*",lineNumber);
						currentToken="";
						int startLine=lineNumber;
						line=line.substring(i);
						while(!line.contains("*/")&&lineNumber<lines.length){line=lines[lineNumber]+' ';lineNumber++;}
						if(!line.contains("*/")){break lineloop;}
						if(startLine==lineNumber){i=line.indexOf("*/",i)+1;}
						else{i=line.indexOf("*/")+1;}
						continue;
					}
					else if(c=='/'&&i<line.length()-1&&line.charAt(i+1)=='/'){
						saveToken(currentToken,"//",lineNumber);
						lineNumber++;
						continue lineloop;
					}
					saveToken(currentToken,""+c,lineNumber);
					currentToken="";
					continue;
				}
				currentToken+=c;
			}
			lineNumber++;
		}
		
		return tokenOutput;
	}
	private void saveToken(String token,String breaker,int lineNumber){
		if(id.matcher(token).matches()){
			Token data=new Token();
			data.setName("id");
			data.setLexema(token);
			data.setLine(lineNumber);
			tokenOutput.add(data);
			idsSet.add(data);
		}
		else{
			boolean tokenRegistered=false;
			for(Entry<Pattern,String>entry:keywords.entrySet()){
				if(entry.getKey().matcher(token).matches()){
					Token data=new Token();
					data.setName(entry.getValue());
					data.setLexema(token);
					data.setLine(lineNumber);
					tokenOutput.add(data);
					tokenRegistered=true;
					break;
				}
			}
			if(!tokenRegistered){
				for(Entry<Pattern,String>entry:literals.entrySet()){
					if(entry.getKey().matcher(token).matches()){
						Token data=new Token();
						data.setName(entry.getValue());
						data.setLexema(token);
						data.setLine(lineNumber);
						tokenOutput.add(data);
						tokenRegistered=true;
						break;
					}
				}
				if(!tokenRegistered&&!isEmptyString(token)){
					String errorFormat=bundle.getString("errorFormat").replaceFirst("#",""+lineNumber).replaceFirst("#",'\''+token+'\'');
					errorOutput.add(errorFormat);
				}
			}
		}
		if(breaker.length()>1){
			if(breaker.charAt(1)=='*'){
				String commentName=bundle.getString("multilineCommentary");
				Token data=new Token();
				data.setName(commentName);
				data.setLexema("...");
				data.setLine(lineNumber);
				tokenOutput.add(data);
			}
			else{
				String commentString=bundle.getString("lineCommentary");
				Token data=new Token();
				data.setName(commentString);
				data.setLexema("...");
				data.setLine(lineNumber);
				tokenOutput.add(data);
			}
		}
		else{
			char c=breaker.charAt(0);
			if(!(c==' '||c=='\t')){
				Token data=new Token();
				data.setName(breaker);
				data.setLexema(breaker);
				data.setLine(lineNumber);
				tokenOutput.add(data);
			}
		}
	}
}