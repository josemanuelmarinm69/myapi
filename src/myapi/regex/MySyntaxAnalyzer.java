package myapi.regex;
import static java.util.Arrays.asList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Stack;
import myapi.generics.MyEntry;
import myapi.regex.MyAbstractSyntaxTree.MyAbstractSyntaxTreeNode;
import myapi.regex.MyAbstractSyntaxTree.Token;
public class MySyntaxAnalyzer{
	private class MyRuleMap extends HashMap<Stack<Entry<String,MyAbstractSyntaxTreeNode>>,Entry<String,MyAbstractSyntaxTreeNode>>{
		private static final long serialVersionUID=-2919069309446826332L;

		@SuppressWarnings("unchecked")
		public Entry<String,MyAbstractSyntaxTreeNode>get(Object key,int delayedLineNumber,int currentLineNumber,boolean end){
			Entry<String,MyAbstractSyntaxTreeNode>value=null;
			if(key instanceof Stack){
				Stack<Entry<String,MyAbstractSyntaxTreeNode>>stack=(Stack<Entry<String,MyAbstractSyntaxTreeNode>>)key;
				if(stack.isEmpty()){return value;}
				String error=null;
				for(Entry<Stack<Entry<String,MyAbstractSyntaxTreeNode>>,Entry<String,MyAbstractSyntaxTreeNode>>entry:entrySet()){
					String status=compare(stack,entry,end);
					if("0".equals(status)){
						MyAbstractSyntaxTreeNode first=stack.size()>=1?stack.get(0).getValue():null;
						MyAbstractSyntaxTreeNode second=stack.size()>=2?stack.get(1).getValue():null;
						MyAbstractSyntaxTreeNode third=stack.size()>=3?stack.get(2).getValue():null;
						MyAbstractSyntaxTreeNode fourth=stack.size()>=4?stack.get(3).getValue():null;
						MyAbstractSyntaxTreeNode root=new MyAbstractSyntaxTreeNode(first,second,third,fourth,entry.getValue().getKey());
						value=new MyEntry<String,MyAbstractSyntaxTreeNode>(root.getData().getName(),root);
						error=null;
						break;
					}
					else if(status!=null&&"-1".equals(status)){
						error="wrongOrder";value=new MyEntry<String,MyAbstractSyntaxTreeNode>(null,null);continue;
					}
					else if(status!=null&&status.contains("missing")){
						error=status;value=new MyEntry<String,MyAbstractSyntaxTreeNode>(null,null);continue;
					}
				}
				if(error!=null){
					if(!"wrongOrder".equals(error)&&!end){currentLineNumber=delayedLineNumber;}
					errorOutput.add(bundle.getString("errorFormat").replaceFirst("#",""+currentLineNumber).replaceFirst("#",bundle.getString(error)));
				}
			}
			return value;
		}
		private String compare(Stack<Entry<String,MyAbstractSyntaxTreeNode>>stack,
				Entry<Stack<Entry<String,MyAbstractSyntaxTreeNode>>,Entry<String,MyAbstractSyntaxTreeNode>>entry,boolean end){
			Stack<Entry<String,MyAbstractSyntaxTreeNode>>entryStack=entry.getKey();
			int size1=stack.size(),size2=entryStack.size();
			boolean order=size1==size2;
			ArrayList<String>elementList=new ArrayList<String>();
			for(int i=0;i<size2;i++){elementList.add(entryStack.get(i).getKey());}
			for(int i=0;i<size1;i++){
				if(order){order=elementList.get(0).equals(stack.get(i).getKey());}
				elementList.remove(stack.get(i).getKey());
			}
			if(order){return"0";}
			else if(size1==size2&&elementList.isEmpty()){return"-1";}
			else if(end&&stack.size()>1&&elementList.size()==1&&elementList.contains("}")){return"missingCloseBrace";}
			else if(elementList.size()==1&&size1==size2+1&&stack.get(0).getKey().equals(entryStack.get(0).getKey())){
				if(elementList.contains("{")){return"missingOpenBrace";}
				if(elementList.contains(";")){return"missingSemicolon";}
				if(elementList.contains(",")){return"missingComma";}
				if(elementList.contains("(")||elementList.contains(")")){return"missingParenthesis";}
				if(elementList.contains("id")||elementList.contains("E")){
					String ruleName=entry.getValue().getKey();
					if("A".equals(ruleName)||"ID".equals(ruleName)||("OP".equals(ruleName)&&"id".equals(entryStack.get(0).getKey()))){
						if(!"(".equals(stack.get(size1-1).getKey())){return"missingOperand";}
					}
					else{return"missingOperand";}
				}
			}
			return null;
		}
	}
	private MyRuleMap rules;
	private ResourceBundle bundle;
	private List<String>typeKeywords;
	private ArrayList<Token>idsSet;
	private ArrayList<String>errorOutput;

	public MySyntaxAnalyzer(ResourceBundle bundle){
		this.bundle=bundle;
		rules=new MyRuleMap();
		String ruleNames[]=bundle.getString("rules").split("\\|");
		for(String ruleName:ruleNames){
			String ruleValues[]=bundle.getString(ruleName).split("\\|");
			for(String ruleValue:ruleValues){
				Stack<Entry<String,MyAbstractSyntaxTreeNode>>ruleStack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
				String ruleNodes[]=ruleValue.split(" ");
				for(String ruleNode:ruleNodes){ruleStack.push(new MyEntry<String,MyAbstractSyntaxTreeNode>(ruleNode,null));}
				rules.put(ruleStack,new MyEntry<String,MyAbstractSyntaxTreeNode>(ruleName,null));
			}
		}
		typeKeywords=asList(bundle.getString("typeKeywords").split(","));
	}
	public ArrayList<Token>getIdsSet(){return idsSet;}
	public ArrayList<String>getErrorList(){return errorOutput;}
	public synchronized MyAbstractSyntaxTree build(ArrayList<Token>tokenList,ArrayList<Token>idsSet){
		this.idsSet=idsSet;
		errorOutput=new ArrayList<String>();
		Stack<Entry<String,MyAbstractSyntaxTreeNode>>stack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
		Stack<Entry<String,MyAbstractSyntaxTreeNode>>auxStack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
		ArrayList<Token>pendingIds=null;
		tokenloop:
		for(int i=0;i<tokenList.size();i++){
			Token data=tokenList.get(i);
			String token=data.getName();
			if(typeKeywords.contains(token)){pendingIds=new ArrayList<Token>();}
			if(pendingIds!=null&&"id".equals(token)){pendingIds.add(data);}
			int delayedLineNumber=tokenList.get(i>1?i-2:i).getLine();
			int currentLineNumber=tokenList.get(i).getLine();
			stack.push(new MyEntry<String,MyAbstractSyntaxTreeNode>(token,new MyAbstractSyntaxTreeNode(data)));
			while(!stack.isEmpty()){
				auxStack.add(0,stack.pop());
				Entry<String,MyAbstractSyntaxTreeNode>rule=rules.get(auxStack,delayedLineNumber,currentLineNumber,i==tokenList.size()-1);
				if(rule!=null&&rule.getKey()!=null){
					stack.push(rule);
					auxStack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
					if("N1".equals(rule.getKey())){
						MyAbstractSyntaxTreeNode node=rule.getValue();
						while(node!=null&&!"T".equals(node.getData().getName())){node=node.getFirst();}
						if(node!=null&&pendingIds!=null){
							for(Token pendingId:pendingIds){
								for(Token idData:idsSet){
									if(idData.getLexema().equals(pendingId.getLexema())){
										idData.setType(node.getFirst().getData().getName());
										pendingId.setType(idData.getType());
									}
								}
							}
							pendingIds=null;
						}
					}
				}
				else if(rule!=null&&rule.getKey()==null){
					stack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
					if(!errorOutput.get(errorOutput.size()-1).contains(bundle.getString("wrongOrder"))&&auxStack.size()>1){
						Entry<String,MyAbstractSyntaxTreeNode>last=auxStack.pop();
						Entry<String,MyAbstractSyntaxTreeNode>beforeLast=auxStack.pop();
						stack.push(beforeLast);
						stack.push(last);
					}
					auxStack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
					continue tokenloop;
				}
			}
			if(stack.isEmpty()){
				stack=auxStack;
				auxStack=new Stack<Entry<String,MyAbstractSyntaxTreeNode>>();
			}
		}
		return stack.size()==1?new MyAbstractSyntaxTree(stack.pop().getValue(),bundle.getString("nodeDisplayName"),bundle.getString("nullDisplayName")):null;
	}
}