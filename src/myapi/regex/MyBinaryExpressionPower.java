package myapi.regex;
import static myapi.regex.MyBinaryExpressionTree.random;
import java.util.ArrayList;
public class MyBinaryExpressionPower implements MyBinaryExpressionTreeNode{
	private int size;
	private char baseValue;
	private char powerOperator;
	private ArrayList<MyBinaryExpressionTreeNode>childs;

	public MyBinaryExpressionPower(int size){this.size=size;}
	public char getBaseValue(){return this.baseValue;}
	public void setBaseValue(char baseValue){this.baseValue=baseValue;}
	public char getPowerOperator(){return this.powerOperator;}
	public void setPowerOperator(char powerOperator){this.powerOperator=powerOperator;}
	public void addChild(MyBinaryExpressionTreeNode child){
		if(childs==null){childs=new ArrayList<MyBinaryExpressionTreeNode>();}
		childs.add(child);
	}
	public String[]getStrings(){
		if(childs==null){
			switch(powerOperator){
				case'*':
				case'+':
					return generateStrings(powerOperator=='*');
				default:
					return new String[]{""+baseValue};
			}
		}
		else{
			ArrayList<String[]>arrays=new ArrayList<String[]>();
			for(MyBinaryExpressionTreeNode node:childs){arrays.add(node.getStrings());}
			int size=1;
			for(String array[]:arrays){size*=array.length;}
			String array[]=new String[size];
			for(int i=0;i<size;i++){array[i]="";}
			for(String strings[]:arrays){
				int j=0;
				for(int i=0;i<size;i++){
					array[i]+=strings[j];
					if(j==strings.length-1){j=0;}
					else{j++;}
				}
			}
			if(powerOperator!='\u0000'){
				String completeArray[]=new String[array.length*this.size];
				for(int i=0;i<completeArray.length;i++){
					int length=random.nextInt(size);
					int index=random.nextInt(array.length);
					String temp="";
					for(int j=0;j<length;j++){temp+=array[index];}
					completeArray[i]=temp;
				}
				/*int index=0;
				for(String string:array){
					String powerArray[]=generateStrings(powerOperator=='*',string);
					for(String powerString:powerArray){
						completeArray[index]=powerString;index++;
					}
				}
				for(int i=0;i<completeArray.length;){
					for(int j=0;j<powerArray.length;i++,j++){
						completeArray[i]=powerArray[j];
						if(j==powerArray.length-1){powerArray=generateStrings(powerOperator=='*',array[index++]);j=0;}
					}
				}*/
				array=completeArray;
			}
			return array;
		}
	}
	private String[]generateStrings(boolean includeZero){return generateStrings(includeZero,""+baseValue);}
	private String[]generateStrings(boolean includeZero,String baseValue){
		String array[]=new String[size];
		int i;
		if(includeZero){array[0]="";i=1;}
		else{i=0;}
		for(;i<size;i++){
			String string="";
			for(int j=0;j<i;j++){string+=baseValue;}
			array[i]=string;
		}
		return array;
	}
}