package myapi.regex;
public class UndefinedLanguageCharacterException extends Exception{
	private static final long serialVersionUID=-2155910480539035463L;

	public UndefinedLanguageCharacterException(){}
	public UndefinedLanguageCharacterException(String message){super(message);}
}