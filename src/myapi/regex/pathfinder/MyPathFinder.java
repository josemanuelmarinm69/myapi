package myapi.regex.pathfinder;
import static java.awt.Color.WHITE;
import static java.awt.Color.BLACK;
import static java.awt.Color.BLUE;
import static java.awt.Color.RED;
import static java.awt.Color.GREEN;
import static javax.swing.BorderFactory.createLineBorder;
import static myapi.regex.pathfinder.MyPath.shortest;
import static myapi.regex.pathfinder.MySquare.selected;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
public class MyPathFinder extends JFrame{
	private static final long serialVersionUID=6832432744569309018L;
 	private static final String letters[]={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P",
			"Q","R","S","T","U","V","W","X","Y","Z"};
	private static final int blocks[][]=null;//{{0,0},{0,1},{1,1},{2,2},{2,4},{2,8},{3,0},{3,3},{4,4},{5,5},
			//{6,6},{7,7},{8,8},{9,9}};
	private static final int waters[][]=null;//{{3,6},{2,7},{3,1},{8,2},{4,2},{0,9},{3,5}};
	public static MySquare start,end;
	private final JPanel panel;

	public MyPathFinder(){
		int height=3,width=3,squareHeight=20,squareWidth=20;
		Color normalBackground=WHITE,border=BLACK,block=BLACK,water=BLUE;
		MySquare.height=squareHeight;
		MySquare.width=squareWidth;
		MySquare.border=border;
		MySquare.normalBackground=normalBackground;
		selected=RED;
		MySquare.water=water;
		shortest=GREEN;
		panel=new JPanel(null);
		panel.setSize(((width+1)*squareWidth),((height+2)*squareHeight));
		panel.setPreferredSize(panel.getSize());
		setContentPane(panel);
		MySquare squares[][]=new MySquare[width][height];
		for(int i=0;i<width;i++){
			for(int j=0;j<height;j++){
				squares[i][j]=new MySquare();
				squares[i][j].setName(letters[i%letters.length]+(j+1));
				JLabel label=squares[i][j].getLabel();
				label.setLocation((i+1)*squareWidth,(j+1)*squareHeight);
				panel.add(label);
			}
		}
		for(int i=0;i<width;i++){
			for(int j=0;j<height;j++){
				MySquare up=(i>0)?squares[i-1][j]:null;
				MySquare upRight=(i>0&&j<height-1)?squares[i-1][j+1]:null;
				MySquare right=(j<height-1)?squares[i][j+1]:null;
				MySquare downRight=(i<width-1&&j<height-1)?squares[i+1][j+1]:null;
				MySquare down=(i<width-1)?squares[i+1][j]:null;
				MySquare downLeft=(i<width-1&&j>0)?squares[i+1][j-1]:null;
				MySquare left=(j>0)?squares[i][j-1]:null;
				MySquare upLeft=(i>0&&j>0)?squares[i-1][j-1]:null;
				squares[i][j].setNeighbors(up,upRight,right,downRight,down,downLeft,left,upLeft);
				squares[i][j].setCostos(1,1,1,1,1,1,1,1);
			}
		}
		if(blocks!=null){
			for(int coordinate[]:blocks){
				MySquare square=squares[coordinate[1]][coordinate[0]];
				square.setBackground(block);
				square.setExtraCost(-1);
			}
		}
		if(waters!=null){
			for(int coordinate[]:waters){
				MySquare square=squares[coordinate[1]][coordinate[0]];
				square.setBackground(water);
				square.setExtraCost(2);
			}
		}
		for(int i=0;i<width;i++){
			JLabel label=new JLabel(letters[i%letters.length]);
			label.setSize(squareWidth,squareHeight);
			label.setOpaque(true);
			label.setBackground(normalBackground);
			label.setBorder(createLineBorder(border));
			label.setLocation((i+1)*squareWidth,0);
			panel.add(label);
		}
		for(int j=0;j<height;j++){
			JLabel label=new JLabel(""+(j+1));
			label.setSize(squareWidth,squareHeight);
			label.setOpaque(true);
			label.setBackground(normalBackground);
			label.setBorder(createLineBorder(border));
			label.setLocation(0,(j+1)*squareHeight);
			panel.add(label);
		}
		JButton button=new JButton("Find");
		button.setSize(squareWidth*4,squareHeight);
		button.setLocation(panel.getWidth()/2-button.getWidth()/2,panel.getHeight()-button.getHeight());
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(start!=null&&end!=null&&start!=end){
					MyPath path=new MyPath(start,end);
					path.checkPath(path.getShortestPath());
				}
			}
		});
		panel.add(button);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Shortest Path");
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	public static void createAndShowGUI(){new MyPathFinder();}
}