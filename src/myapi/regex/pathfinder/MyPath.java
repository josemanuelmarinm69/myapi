package myapi.regex.pathfinder;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.awt.Color;
public class MyPath {
	public static Color shortest;
	public static int currentMaxCost=0;
	private ArrayList<LinkedHashSet<MySquare>>paths;
	private ArrayList<Integer>costs;
	private Integer shortestIndex;

	public MyPath(MySquare start,MySquare end){
		LinkedHashSet<MySquare>path=new LinkedHashSet<MySquare>();
		path.add(start);
		paths=start.getPaths(path,end,0);
		out.println(paths.size());
	}
	public LinkedHashSet<MySquare>getShortestPath(){
		if(shortestIndex==null){
			costs=new ArrayList<Integer>();
			shortestIndex=new Integer(0);
			for(int i=0;i<paths.size();i++){
				costs.add(new Integer(calculateCost(paths.get(i))));
				if(costs.get(i).intValue()<costs.get(shortestIndex.intValue()).intValue()){
					shortestIndex=new Integer(i);
				}
			}
		}
		return paths.get(shortestIndex.intValue());
	}
	private int calculateCost(LinkedHashSet<MySquare>path){
		int cost=0;
		MySquare nodes[]=path.toArray(new MySquare[path.size()]);
		for(int i=0;i<nodes.length-1;i++){
			MySquare node=nodes[i],siguiente=nodes[i+1];
			cost+=node.getCost(siguiente)+siguiente.getExtraCost();
		}
		return cost;
	}
	public void checkPath(LinkedHashSet<MySquare>path){for(MySquare node:path){node.check(shortest);}}
}