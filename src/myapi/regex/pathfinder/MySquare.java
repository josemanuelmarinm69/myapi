package myapi.regex.pathfinder;
import static java.lang.System.out;
import static javax.swing.BorderFactory.createLineBorder;
import static myapi.regex.pathfinder.MyPath.currentMaxCost;
import static myapi.regex.pathfinder.MyPathFinder.start;
import static myapi.regex.pathfinder.MyPathFinder.end;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import javax.swing.JLabel;
public class MySquare{
	public static Color normalBackground,border,water,selected;
	public static int width,height;
	private JLabel label;
	private Color background;
	private MySquare neighbors[];
	private int costs[];
	private int extraCost;
	private String name;

	public MySquare(){
		label=new JLabel();
		label.setOpaque(true);
		label.setSize(width,height);
		label.setBackground(background=normalBackground);
		label.setBorder(createLineBorder(border));
		label.addMouseListener(new MouseListener(){
			public void mouseReleased(MouseEvent e){}
			public void mousePressed(MouseEvent e){}
			public void mouseExited(MouseEvent e){}
			public void mouseEntered(MouseEvent e){}
			public void mouseClicked(MouseEvent e){
				if(extraCost==-1){return;}
				if(start!=end){start.uncheck();}
				start=start!=null?end:MySquare.this;
				end=MySquare.this;
				check(selected);
			}
		});
		neighbors=new MySquare[8];
		costs=new int[8];
	}
	public MySquare(MySquare up,MySquare upRight,MySquare right,MySquare downRight,MySquare down,
			MySquare downLeft,MySquare left,MySquare upLeft){
		this();
		neighbors[0]=up;
		neighbors[1]=upRight;
		neighbors[2]=right;
		neighbors[3]=downRight;
		neighbors[4]=down;
		neighbors[5]=downLeft;
		neighbors[6]=left;
		neighbors[7]=upLeft;
	}
	public MySquare(MySquare neighbors[]){
		this();
		if(neighbors==null||neighbors.length!=8){throw new IllegalArgumentException("Not enough neighbors");}
		this.neighbors=neighbors;
	}
	public void setNeighbors(MySquare up,MySquare upRight,MySquare right,MySquare downRight,MySquare down,
			MySquare downLeft,MySquare left,MySquare upLeft){
		neighbors[0]=up;
		neighbors[1]=upRight;
		neighbors[2]=right;
		neighbors[3]=downRight;
		neighbors[4]=down;
		neighbors[5]=downLeft;
		neighbors[6]=left;
		neighbors[7]=upLeft;
	}
	public JLabel getLabel(){return label;}
	public void setBackground(Color color){label.setBackground(background=color);}
	public void check(Color color){label.setBackground(color);}
	public void uncheck(){label.setBackground(background);}
	public void setCostos(int up,int upRight,int right,int downRight,int down,int downLeft,int left,
			int upLeft){
		costs[0]=up;
		costs[1]=upRight;
		costs[2]=right;
		costs[3]=downRight;
		costs[4]=down;
		costs[5]=downLeft;
		costs[6]=left;
		costs[7]=upLeft;
	}
	public int getCost(MySquare neighbor){
		return costs[(neighbor==neighbors[0]?0:
			neighbor==neighbors[1]?1:
			neighbor==neighbors[2]?2:
			neighbor==neighbors[3]?3:
			neighbor==neighbors[4]?4:
			neighbor==neighbors[5]?5:
			neighbor==neighbors[6]?6:7)];
	}
	public void setExtraCost(int extraCost){this.extraCost=extraCost;}
	public int getExtraCost(){return extraCost;}
	public ArrayList<LinkedHashSet<MySquare>>getPaths(LinkedHashSet<MySquare>path,MySquare end,
			int currentCost){
		ArrayList<LinkedHashSet<MySquare>>list=null;
		for(MySquare neighbor:neighbors){
			if(neighbor!=null&&neighbor.extraCost!=-1&&path.add(neighbor)){
				currentCost+=getCost(neighbor)+neighbor.extraCost;
				if(currentMaxCost>0&&currentCost>=currentMaxCost){continue;}
				list=new ArrayList<LinkedHashSet<MySquare>>();
				if(neighbor==end){
					currentMaxCost=currentCost;
					out.println("Found with "+currentMaxCost);
					list.add(path);
					return list;
				}
				ArrayList<LinkedHashSet<MySquare>>news=neighbor.getPaths(
						new LinkedHashSet<MySquare>(path),end,currentCost);
				if(news!=null){list.addAll(news);}
				/*Thread thread=new Thread(){
					@Override public void run(){}
				};
				thread.start();*/
			}
		}
		return list;
	}
	public void setName(String name){this.name=name;}
	public String getName(){return name;}
	@Override public String toString(){return name;}
}