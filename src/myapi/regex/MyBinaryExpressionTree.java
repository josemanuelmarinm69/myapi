package myapi.regex;
import static myapi.utils.OfGUI.createWorker;
import static myapi.utils.OfGUI.syncJProgressBarWithWorker;
import static myapi.utils.OfData.deleteDuplicates;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;
import javax.swing.JProgressBar;
import myapi.utils.OfGUI.MyWorker;
public class MyBinaryExpressionTree{
	private int size;
	private MyBinaryExpressionTreeNode root;
	private MyWorker task;
	public static final Random random=new Random();

	public MyBinaryExpressionTree(int size){
		this.size=size;
		root=new MyBinaryExpressionPower(size);
		task=createWorker("parse","finish",this);
	}
	public MyBinaryExpressionTree(String expression,int size)throws ParseException{this(expression,null,size);}
	public MyBinaryExpressionTree(JProgressBar bar,int size)throws ParseException{this(null,bar,size);}
	public MyBinaryExpressionTree(String expression,JProgressBar bar,int size)throws ParseException{
		this(size);
		if(bar!=null){syncJProgressBarWithWorker(bar,task);}
		if(expression!=null){parse(expression);}
	}
	public void parse(String expression)throws ParseException{
		Stack<MyBinaryExpressionTreeNode>stack=new Stack<MyBinaryExpressionTreeNode>();
		MyBinaryExpressionTreeNode currentNode=root;
		ArrayList<MyBinaryExpressionPower>tempArray=new ArrayList<MyBinaryExpressionPower>();
		for(int i=0;i<expression.length();i++){
			char c=expression.charAt(i);
			switch(c){
				case'0':
				case'1':{
					MyBinaryExpressionPower newNode=new MyBinaryExpressionPower(size);
					newNode.setBaseValue(c);
					tempArray.add(newNode);
					break;}
				case'*':
				case'+':
					if(currentNode instanceof MyBinaryExpressionPower){
						((MyBinaryExpressionPower)currentNode).setPowerOperator(c);
						if(!stack.isEmpty()){currentNode=stack.pop();}
					}
					else{throw new ParseException("Bad sintax in operator "+c,i);}
					break;
				case'(':{
					if(tempArray.size()>0&&currentNode instanceof MyBinaryExpressionPower){
						for(MyBinaryExpressionPower tempNode:tempArray){((MyBinaryExpressionPower)currentNode).addChild(tempNode);}
						tempArray=new ArrayList<MyBinaryExpressionPower>();
					}
					MyBinaryExpressionPower newNode=new MyBinaryExpressionPower(size);
					if(currentNode instanceof MyBinaryExpressionPower){((MyBinaryExpressionPower)currentNode).addChild(newNode);}
					else{((MyBinaryExpressionOr)currentNode).setLeft(newNode);}
					stack.push(currentNode);
					currentNode=newNode;
					break;}
				case')':
					if(currentNode instanceof MyBinaryExpressionOr){
						MyBinaryExpressionPower right=new MyBinaryExpressionPower(size);
						((MyBinaryExpressionOr)currentNode).setRight(right);
						for(MyBinaryExpressionPower tempNode:tempArray){right.addChild(tempNode);}
						tempArray=new ArrayList<MyBinaryExpressionPower>();
					}
					currentNode=stack.pop();
					break;
				case'|':
					if(tempArray.size()>0){
						MyBinaryExpressionOr newNode=new MyBinaryExpressionOr();
						if(currentNode instanceof MyBinaryExpressionPower){((MyBinaryExpressionPower)currentNode).addChild(newNode);}
						else{((MyBinaryExpressionOr)currentNode).setLeft(newNode);}
						MyBinaryExpressionPower left=new MyBinaryExpressionPower(size);
						newNode.setLeft(left);
						for(MyBinaryExpressionPower tempNode:tempArray){left.addChild(tempNode);}
						tempArray=new ArrayList<MyBinaryExpressionPower>();
						stack.push(currentNode);
						currentNode=newNode;
					}
					else{throw new ParseException("Left operand not found for "+c,i);}
					break;
				default:throw new ParseException("Unsupported character "+c,i);
			}
			task.updateProgress(((i+1)*100)/expression.length());
		}
	}
	public void finish(){}
	public void syncWithJProgressBar(JProgressBar bar){syncJProgressBarWithWorker(bar,task);}
	public String[]getStrings(){return deleteDuplicates(root.getStrings());}
}