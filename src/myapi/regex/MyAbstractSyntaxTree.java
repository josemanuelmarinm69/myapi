package myapi.regex;
import java.util.ArrayList;
import java.util.Map.Entry;
import myapi.generics.MyEntry;
public class MyAbstractSyntaxTree{
	public static class Token{
		private String name;
		private String lexema;
		private String type;
		private String value;
		private int line;

		public String getName(){return name;}
		public void setName(String name){this.name=name;}
		public String getLexema(){return lexema;}
		public void setLexema(String lexema){this.lexema=lexema;}
		public String getType(){return type;}
		public void setType(String type){this.type=type;}
		public String getValue(){return value;}
		public void setValue(String value){this.value=value;}
		public int getLine(){return line;}
		public void setLine(int line){this.line=line;}
		@Override public String toString(){return name+"|"+lexema+"|"+line;}
	}
	public static class MyAbstractSyntaxTreeNode{
		private MyAbstractSyntaxTreeNode first;
		private MyAbstractSyntaxTreeNode second;
		private MyAbstractSyntaxTreeNode third;
		private MyAbstractSyntaxTreeNode fourth;
		private Token data;

		public MyAbstractSyntaxTreeNode(String name){this(name,null);}
		public MyAbstractSyntaxTreeNode(String name,String lexema){
			data=new Token();
			data.setName(name);
			data.setLexema(lexema);
		}
		public MyAbstractSyntaxTreeNode(Token data){this.data=data;}
		public MyAbstractSyntaxTreeNode(MyAbstractSyntaxTreeNode first,MyAbstractSyntaxTreeNode second,
				MyAbstractSyntaxTreeNode third,MyAbstractSyntaxTreeNode fourth,Token data){
			this.first=first;
			this.second=second;
			this.third=third;
			this.fourth=fourth;
			this.data=data;
		}
		public MyAbstractSyntaxTreeNode(MyAbstractSyntaxTreeNode first,MyAbstractSyntaxTreeNode second,
				MyAbstractSyntaxTreeNode third,MyAbstractSyntaxTreeNode fourth,String name){
			this.first=first;
			this.second=second;
			this.third=third;
			this.fourth=fourth;
			data=new Token();
			data.setName(name);
		}
		public MyAbstractSyntaxTreeNode getFirst(){return this.first;}
		public void setFirst(MyAbstractSyntaxTreeNode first){this.first=first;}
		public MyAbstractSyntaxTreeNode getSecond(){return this.second;}
		public void setSecond(MyAbstractSyntaxTreeNode second){this.second=second;}
		public MyAbstractSyntaxTreeNode getThird(){return this.third;}
		public void setThird(MyAbstractSyntaxTreeNode third){this.third=third;}
		public MyAbstractSyntaxTreeNode getFourth(){return this.fourth;}
		public void setFourth(MyAbstractSyntaxTreeNode fourth){this.fourth=fourth;}
		public Token getData(){return data;}
		public void setData(Token data){this.data=data;}
		public ArrayList<Entry<MyAbstractSyntaxTreeNode,int[]>>toArray(ArrayList<Entry<MyAbstractSyntaxTreeNode,int[]>>array){
			int numbers[]={-1,-1,-1,-1};
			if(first!=null){array=first.toArray(array);numbers[0]=array.size()-1;}
			if(second!=null){array=second.toArray(array);numbers[1]=array.size()-1;}
			if(third!=null){array=third.toArray(array);numbers[2]=array.size()-1;}
			if(fourth!=null){array=fourth.toArray(array);numbers[3]=array.size()-1;}
			array.add(new MyEntry<MyAbstractSyntaxTreeNode,int[]>(this,numbers));
			return array;
		}
	}
	private MyAbstractSyntaxTreeNode root;
	private String nodeDisplayName;
	private String nullDisplayName;
	private String outputFormat;

	public MyAbstractSyntaxTree(MyAbstractSyntaxTreeNode root,String nodeDisplayName,String nullDisplayName){
		this(root,nodeDisplayName,nullDisplayName,"=\'#\' name=\'#\' type=\'#\' value=\'#\' line=\'#\' # # # #");
	}
	public MyAbstractSyntaxTree(MyAbstractSyntaxTreeNode root,String nodeDisplayName,String nullDisplayName,String outputFormat){
		this.root=root;
		this.nodeDisplayName=nodeDisplayName;
		this.nullDisplayName=nullDisplayName;
		this.outputFormat=outputFormat;
	}
	public String getDisplayText(){
		String output="";
		int i=0;
		for(Entry<MyAbstractSyntaxTreeNode,int[]>entry:root.toArray(new ArrayList<Entry<MyAbstractSyntaxTreeNode,int[]>>())){
			MyAbstractSyntaxTreeNode node=entry.getKey();
			int numbers[]=entry.getValue();
			String node1=numbers[0]>=0?nodeDisplayName+(numbers[0]+1):nullDisplayName;
			String node2=numbers[1]>=0?nodeDisplayName+(numbers[1]+1):nullDisplayName;
			String node3=numbers[2]>=0?nodeDisplayName+(numbers[2]+1):nullDisplayName;
			String node4=numbers[3]>=0?nodeDisplayName+(numbers[3]+1):nullDisplayName;
			if(i>0){output+='\n';}
			String name=node.data.name==null?"null":node.data.name;
			String lexema=node.data.lexema==null?"null":node.data.lexema;
			String type=node.data.type==null?"null":node.data.type;
			String value=node.data.value==null?"null":node.data.value;
			String line=outputFormat.replaceFirst("#",name).replaceFirst("#",lexema);
			line=line.replaceFirst("#",type).replaceFirst("#",value).replaceFirst("#",""+node.data.line);
			output+=nodeDisplayName+(i+1)+line.replaceFirst("#",node1).replaceFirst("#",node2).replaceFirst("#",node3).replaceFirst("#",node4);
			i++;
		}
		return output;
	}
	@Override public String toString(){return getDisplayText();}
}