package myapi.regex;
import static java.lang.Character.isDigit;
import static java.lang.Integer.parseInt;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.ResourceBundle;
import myapi.regex.MyAbstractSyntaxTree.MyAbstractSyntaxTreeNode;
import myapi.regex.MyAbstractSyntaxTree.Token;
public class MySemanticAnalyzer{
	private ResourceBundle syntaxBundle;
	private ResourceBundle semanticBundle;
	private ArrayList<MyAbstractSyntaxTreeNode>nodePool;
	private ArrayList<String[]>nodePoolRefs;
	private ArrayList<String>errorOutput;

	public MySemanticAnalyzer(ResourceBundle syntaxBundle,ResourceBundle semanticBundle,String fileName)throws IOException{
		this.syntaxBundle=syntaxBundle;
		this.semanticBundle=semanticBundle;
		nodePool=new ArrayList<MyAbstractSyntaxTreeNode>();
		nodePoolRefs=new ArrayList<String[]>();
		BufferedReader reader=new BufferedReader(new FileReader(fileName));
		String line;
		while((line=reader.readLine())!=null){
			String values[]=line.split(" ");
			values[0]=values[0].replaceAll("'"," ");
			String name=values[0].split(" ")[1];
			values[1]=values[1].replaceAll("'"," ");
			String lexema=values[1].split(" ")[1];
			values[2]=values[2].replaceAll("'"," ");
			String type=values[2].split(" ")[1];
			if(type!=null&&type.contains("null")){type=null;}
			values[3]=values[3].replaceAll("'"," ");
			String value=values[3].split(" ")[1];
			if(value!=null&&value.contains("null")){value=null;}
			values[4]=values[4].replaceAll("'"," ");
			String lineNumber=values[4].split(" ")[1];
			Token data=new Token();
			data.setName(name);
			data.setLexema(lexema);
			data.setType(type);
			data.setValue(value);
			data.setLine(parseInt(lineNumber));
			MyAbstractSyntaxTreeNode node=new MyAbstractSyntaxTreeNode(data);
			nodePool.add(node);
			nodePoolRefs.add(new String[]{values[5],values[6],values[7],values[8]});
		}
		reader.close();
	}
	public ArrayList<String>getErrorOutput(){return errorOutput;}
	public synchronized MyAbstractSyntaxTree decorate(ArrayList<Token>idsSet){
		String nullValue=syntaxBundle.getString("nullDisplayName");
		errorOutput=new ArrayList<String>();
		LinkedHashSet<String>definedVars=new LinkedHashSet<String>();
		for(int i=nodePool.size()-1;i>0;i--){
			MyAbstractSyntaxTreeNode node=nodePool.get(i);
			if(node==null){continue;}
			String refs[]=nodePoolRefs.get(i);
			if(!nullValue.equals(refs[0])&&nullValue.equals(refs[1])&&nullValue.equals(refs[2])&&nullValue.equals(refs[3])){
				int index=i;
				while(!nullValue.equals(refs[0])&&nullValue.equals(refs[1])&&nullValue.equals(refs[2])&&nullValue.equals(refs[3])){
					nodePool.set(index,null);
					String number="";
					for(char c:refs[0].toCharArray()){if(isDigit(c)){number+=c;}}
					index=parseInt(number)-1;
					refs=nodePoolRefs.get(index);
				}
				nodePool.set(i,nodePool.get(index));
				nodePoolRefs.set(i,refs);
			}
		}
		for(Token data:idsSet){
			String token=data.getName();
			if("id".equals(token)){
				String type=data.getType();
				String lexema=data.getLexema();
				int line=data.getLine();
				if(type==null){errorOutput.add(semanticBundle.getString("missingDec").replaceFirst("#",""+line).replaceFirst("#",lexema));}
				else{definedVars.add(lexema);}
			}
		}
		MyAbstractSyntaxTreeNode lastNode=null;
		for(int i=0;i<nodePool.size();i++){
			MyAbstractSyntaxTreeNode node=nodePool.get(i);
			if(node==null){continue;}
			String refs[]=nodePoolRefs.get(i);
			int indexes[]={-1,-1,-1,-1};
			for(int j=0;j<indexes.length;j++){
				if(nullValue.equals(refs[j])){continue;}
				String number="";
				for(char c:refs[j].toCharArray()){if(isDigit(c)){number+=c;}}
				indexes[j]=parseInt(number)-1;
			}
			MyAbstractSyntaxTreeNode first=indexes[0]<0?null:nodePool.get(indexes[0]);
			MyAbstractSyntaxTreeNode second=indexes[1]<0?null:nodePool.get(indexes[1]);
			MyAbstractSyntaxTreeNode third=indexes[2]<0?null:nodePool.get(indexes[2]);
			MyAbstractSyntaxTreeNode fourth=indexes[3]<0?null:nodePool.get(indexes[3]);
			node.setFirst(first);
			node.setSecond(second);
			node.setThird(third);
			node.setFourth(fourth);
			String name=node.getData().getName();
			String lexema=node.getData().getLexema();
			if("P".equals(name)){lastNode=node;}
			else if("id".equals(name)&&node.getData().getType()!=null&&definedVars.contains(lexema)){definedVars.remove(lexema);}
			else if("id".equals(name)&&node.getData().getType()!=null&&!definedVars.contains(lexema)){
				errorOutput.add(semanticBundle.getString("alreadyDefined").replaceFirst("#",""+node.getData().getLine()).replaceFirst("#",lexema));
			}
			else if("A".equals(name)&&"id".equals(node.getThird().getData().getName())){
				String name1=node.getFirst().getData().getLexema();
				String name2=node.getThird().getData().getLexema();
				String type1=null;
				String type2=null;
				for(Token data:idsSet){
					String dataName=data.getLexema();
					if(dataName.equals(name1)){type1=data.getType();}
					if(dataName.equals(name2)){type2=data.getType();}
				}
				int line=node.getFirst().getData().getLine();
				if(type1!=null&&!type1.equals(type2)){errorOutput.add(semanticBundle.getString("wrongType").replaceFirst("#",""+line).replaceFirst("#",name1));}
			}
		}
		return new MyAbstractSyntaxTree(lastNode,syntaxBundle.getString("nodeDisplayName"),nullValue);
	}
}