package myapi.regex;
public class MyBinaryExpressionOr implements MyBinaryExpressionTreeNode{
	private MyBinaryExpressionTreeNode left;
	private MyBinaryExpressionTreeNode right;

	public MyBinaryExpressionTreeNode getLeft(){return this.left;}
	public void setLeft(MyBinaryExpressionTreeNode left){this.left=left;}
	public MyBinaryExpressionTreeNode getRight(){return this.right;}
	public void setRight(MyBinaryExpressionTreeNode right){this.right=right;}
	public String[]getStrings(){
		String leftArray[]=left.getStrings();
		String rightArray[]=right.getStrings();
		String array[]=new String[leftArray.length+rightArray.length];
		int i=0;
		for(int j=0;j<leftArray.length;j++,i++){array[i]=leftArray[j];}
		for(int j=0;j<rightArray.length;j++,i++){array[i]=rightArray[j];}
		return array;
	}
}