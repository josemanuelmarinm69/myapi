package myapi.regex;
import java.util.HashMap;
public class MyDeterministicFiniteAutomaton{
	private DeterministicFiniteAutomatonState currentState;
	public static class DeterministicFiniteAutomatonState{
		private boolean isAcceptState;
		private HashMap<Character,DeterministicFiniteAutomatonState>transitions;

		public DeterministicFiniteAutomatonState(){this(false);}
		public DeterministicFiniteAutomatonState(boolean isAcceptState){
			this.isAcceptState=isAcceptState;
			transitions=new HashMap<Character,DeterministicFiniteAutomatonState>();
		}
		public boolean isAcceptState(){return isAcceptState;}
		public void addTransition(DeterministicFiniteAutomatonState state,char...chars){
			for(int i=0;i<chars.length;i++){transitions.put(new Character(chars[i]),state);}
		}
		private DeterministicFiniteAutomatonState advance(char c){return transitions.get(new Character(c));}
	}

	public MyDeterministicFiniteAutomaton(DeterministicFiniteAutomatonState initialState){currentState=initialState;}
	public synchronized boolean isAccepted(String expression)throws UndefinedLanguageCharacterException{
		if(!expression.contains("$")){expression+='$';}
		for(int i=0;;i++){
			if(expression.charAt(i)=='$'){break;}
			char c=expression.charAt(i);
			currentState=currentState.advance(c);
			if(currentState==null){throw new UndefinedLanguageCharacterException("Undefined "+c);}
		}
		return currentState.isAcceptState;
	}
	public static MyDeterministicFiniteAutomaton getAutomaton(){
		DeterministicFiniteAutomatonState q0=new DeterministicFiniteAutomatonState();
		DeterministicFiniteAutomatonState q1=new DeterministicFiniteAutomatonState();
		DeterministicFiniteAutomatonState q2=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q3=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q4=new DeterministicFiniteAutomatonState();
		DeterministicFiniteAutomatonState q5=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q6=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q7=new DeterministicFiniteAutomatonState();
		DeterministicFiniteAutomatonState q8=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q9=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q10=new DeterministicFiniteAutomatonState(true);
		DeterministicFiniteAutomatonState q11=new DeterministicFiniteAutomatonState(true);
		q0.addTransition(q4,'0');
		q0.addTransition(q1,'1');
		q1.addTransition(q7,'0');
		q1.addTransition(q2,'1');
		q2.addTransition(q3,'0');
		q2.addTransition(q1,'1');
		q3.addTransition(q9,'0');
		q3.addTransition(q7,'1');
		q4.addTransition(q5,'0');
		q4.addTransition(q7,'1');
		q5.addTransition(q4,'0');
		q5.addTransition(q6,'1');
		q6.addTransition(q7,'0');
		q6.addTransition(q11,'1');
		q7.addTransition(q10,'0');
		q7.addTransition(q8,'1');
		q8.addTransition(q9,'0');
		q8.addTransition(q7,'1');
		q9.addTransition(q8,'0');
		q9.addTransition(q10,'1');
		q10.addTransition(q7,'0');
		q10.addTransition(q11,'1');
		q11.addTransition(q8,'0');
		q11.addTransition(q10,'1');
		return new MyDeterministicFiniteAutomaton(q0);
	}
	public static boolean DFATest(String expression){
		MyDeterministicFiniteAutomaton dfa=getAutomaton();
		try{return dfa.isAccepted(expression);}
		catch(UndefinedLanguageCharacterException e){myapi.ErrorConsole.addText(e);}
		return false;
	}
}