package myapi.regex;
import static java.lang.Double.valueOf;
import static java.util.Locale.US;
import static myapi.utils.OfData.isEmptyString;
import static myapi.utils.OfEvent.addCharacterSetToJTextField;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfEvent.addSizeToParentListener;
import static myapi.utils.OfFile.getFilesResourceBundle;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfGUI.modifyDimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Stack;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import myapi.generics.MyEntry;
import myapi.gui.MyWindow;
public class MyPosfixConverter extends MyWindow{
	private static final long serialVersionUID=-3320100198899226533L;
	private final HashMap<Character,Integer>priority;
	private ArrayList<String>expression;
	private ArrayList<MyCuartet>cuartets;
	private Stack<Stack<Character>>stacks;
	private double evaluation;
	private JPanel panel;
	private JLabel label;
	private JTextField field;
	private JButton button;
	private JScrollPane pane;
	private JTextArea area;
	private class MyCuartet{
		private char operator;
		private String firstOperand;
		private String secondOperand;
		private String destiny;

		public MyCuartet(char operator,String firstOperand,String secondOperand,String destiny){
			this.operator=operator;
			this.firstOperand=firstOperand;
			this.secondOperand=secondOperand;
			this.destiny=destiny;
		}
		@Override public String toString(){return""+'('+operator+','+firstOperand+','+secondOperand+','+destiny+')';}
	}

	public MyPosfixConverter(){
		super(US);
		addResizableContainerListener(panel);
		addSizeToParentListener(panel);
		addCharacterSetToJTextField(field,new char[]{'0','1','2','3','4','5','6','7','8','9','(',')','+','-','*','/'});
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					convert(field.getText());
					area.setText(getExpression()+'\n'+getEvaluation()+'\n'+getCuartets());
				}
				catch(Exception ex){myapi.ErrorConsole.addText(ex);area.setText(bundle.getString("errorMessage"));}
			}
		});
		area.setEditable(false);
		
		panel.add(label);
		panel.add(field);
		panel.add(button);
		panel.add(pane);
		panel.setPreferredSize(modifyDimension(panel.getPreferredSize(),label.getX()));
		panel.setSize(panel.getPreferredSize());
		setContentPane(panel);

		priority=new HashMap<Character,Integer>();
		priority.put(new Character('+'),new Integer(1));
		priority.put(new Character('-'),new Integer(1));
		priority.put(new Character('*'),new Integer(2));
		priority.put(new Character('/'),new Integer(2));

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	@Override public synchronized void init(){
		panel=new JPanel(null);
		label=new JLabel();
		field=new JTextField();
		button=new JButton();
		pane=new JScrollPane(area=new JTextArea());
	}
	@Override public synchronized void updateBundle(Locale locale){bundle=getFilesResourceBundle("posfix",locale);}
	@Override public synchronized void updateLocale(){
		super.updateLocale();
		label.setText(bundle.getString("inputLabel"));
		label.setBounds(10,10,getTextWidth(label),20);
		field.setBounds(label.getX(),label.getY()*2+label.getHeight(),200,20);
		button.setText(bundle.getString("convertText"));
		button.setBounds(label.getX(),label.getY()+field.getY()+field.getHeight(),getTextWidth(button),20);
		pane.setBounds(label.getX(),label.getY()+button.getY()+button.getHeight(),200,200);
	}
	public String getExpression(){
		String output="";
		int i=0;
		for(String item:expression){
			if(i++>0){output+=',';}
			output+=item;
		}
		return output;
	}
	public String getCuartets(){
		String output="";
		int i=0;
		for(MyCuartet cuartet:cuartets){
			if(i++>0){output+='\n';}
			output+=cuartet;
		}
		return output;
	}
	public double getEvaluation(){return evaluation;}
	public synchronized void convert(String exp){
		expression=new ArrayList<String>();
		cuartets=new ArrayList<MyCuartet>();
		stacks=new Stack<Stack<Character>>();
		String operand="";
		Stack<Character>stack=new Stack<Character>();
		for(char c:exp.toCharArray()){
			switch(c){
				case'+':
				case'-':
				case'*':
				case'/':
					if(!isEmptyString(operand)){
						expression.add(operand);
						operand="";
					}
					if(stack.size()>0){
						Character operator;//=stack.peek();
						/*if(!(priority.get(new Character(c)).intValue()>priority.get(operator).intValue())){
							while(stack.size()>0){expression.add(""+stack.pop());}
						}*/
						while(stack.size()>0&&!(priority.get(new Character(c)).intValue()>priority.get((operator=stack.peek())).intValue())){
							operator=stack.pop();
							expression.add(""+operator);
						}
					}
					stack.push(new Character(c));
					break;
				case'(':
					stacks.push(stack);
					stack=new Stack<Character>();
					break;
				case')':
					if(!isEmptyString(operand)){
						expression.add(operand);
						operand="";
					}
					while(stack.size()>0){expression.add(""+stack.pop());}
					stack=stacks.pop();
					break;
				default:operand+=c;
			}
		}
		if(!isEmptyString(operand)){expression.add(operand);}
		while(stack.size()>0){expression.add(""+stack.pop());}
		Stack<MyEntry<Double,String>>operands=new Stack<MyEntry<Double,String>>();
		int temNumber=1;
		for(String item:expression){
			try{operands.push(new MyEntry<Double,String>(valueOf(item),null));}
			catch(NumberFormatException e){
				MyEntry<Double,String>entry1=operands.pop();
				MyEntry<Double,String>entry2=operands.pop();
				double b=entry1.getKey().doubleValue();
				double a=entry2.getKey().doubleValue();
				String tempName="t"+temNumber;
				switch(item.charAt(0)){
					case'+':operands.push(new MyEntry<Double,String>(new Double(a+b),tempName));break;
					case'-':operands.push(new MyEntry<Double,String>(new Double(a-b),tempName));break;
					case'*':operands.push(new MyEntry<Double,String>(new Double(a*b),tempName));break;
					case'/':operands.push(new MyEntry<Double,String>(new Double(a/b),tempName));
				}
				String firstOperand=entry2.getValue()==null?""+a:entry2.getValue();
				String secondOperand=entry1.getValue()==null?""+b:entry1.getValue();
				cuartets.add(new MyCuartet(item.charAt(0),firstOperand,secondOperand,"t"+temNumber++));
			}
		}
		evaluation=operands.pop().getKey().doubleValue();
	}
	public static void createAndShowGUI(){new MyPosfixConverter();}
}