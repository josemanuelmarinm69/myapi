package myapi.regex;
import static java.lang.Double.valueOf;
import static java.lang.Integer.parseInt;
import static java.util.Locale.US;
import static myapi.MyFileHelper.loadStrings;
import static myapi.utils.OfData.isEmptyString;
import static myapi.utils.OfFile.getFilesResourceBundle;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfMath.convertToPosfix;
import static myapi.utils.OfMath.evaluate;
import static myapi.utils.OfMath.getInfixExpression;
import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import myapi.generics.MyEntry;
import myapi.gui.MyWindow;
import myapi.io.MyFileDragDropListener;
public class MyCodeOptimizer extends MyWindow{
	private static final long serialVersionUID=-373199162624478402L;
	private JPanel contentPane;
	private JTextArea textArea;
	private JTextArea resultArea;
	private JButton button;
	private ArrayList<MyEntry<String,ArrayList<String>>>list;

	public MyCodeOptimizer(Dimension dimension){
		super(US,false,dimension);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		contentPane=new JPanel(null);
		new DropTarget(contentPane,new MyFileDragDropListener(new AbstractAction(){
			private static final long serialVersionUID=-4831231817079313926L;

			public void actionPerformed(ActionEvent e){
				MyFileDragDropListener handler=(MyFileDragDropListener)e.getSource();
				List<File>fileList=handler.getList();
				if(fileList==null||fileList.isEmpty()){return;}
				displayText(fileList.get(fileList.size()-1));
			}
		}));
		JScrollPane pane=new JScrollPane(textArea);
		pane.setBounds(5,5,(dimension.width-18)/2,dimension.height-93);
		JScrollPane pane1=new JScrollPane(resultArea);
		pane1.setBounds((dimension.width-18)/2+5,5,(dimension.width-18)/2-5,dimension.height-93);
		contentPane.add(pane);
		contentPane.add(pane1);
		contentPane.add(button);
		setContentPane(contentPane);
		setVisible(true);
	}
	public synchronized void displayText(File file){
		ArrayList<String>strings=loadStrings(file);
		String output="";
		for(int i=0;i<strings.size();i++){
			String string=strings.get(i);
			output+=string;
			if(i!=strings.size()-1){output+='\n';}
		}
		textArea.setText(output);
	}
	@Override public synchronized void init(){
		textArea=new JTextArea();
		resultArea=new JTextArea();
		resultArea.setEditable(false);
		button=new JButton();
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(isEmptyString(textArea.getText())){return;}
				list=new ArrayList<MyEntry<String,ArrayList<String>>>();
				for(String line:textArea.getText().toLowerCase().split("\n")){
					int index=line.indexOf("=");
					if(index>0){
						convertToPosfix(line.substring(index+1,line.length()));
						list.add(new MyEntry<String,ArrayList<String>>(line.substring(0,index),getInfixExpression()));
					}
				}
				deleteDuplicates();
				replaceOperands();
				deleteUnused();
				evaluateExpressions();
				replaceArithmeticIdentity();
				downgrade();
				String output="";
				for(int i=0;i<list.size();i++){
					if(i>0){output+='\n';}
					MyEntry<String,ArrayList<String>>entry=list.get(i);
					output+=entry.getKey()+'=';
					for(String operand:entry.getValue()){output+=operand;}
				}
				resultArea.setText(output);
			}
		});
	}
	@Override public synchronized void updateBundle(Locale locale){bundle=getFilesResourceBundle("optimizer",locale);}
	@Override public synchronized void updateLocale(){
		super.updateLocale();
		float fontSize=parseInt(bundle.getString("fontSize"));
		bar.setFont(bar.getFont().deriveFont(fontSize));
		button.setFont(button.getFont().deriveFont(fontSize));
		textArea.setFont(textArea.getFont().deriveFont(fontSize));
		resultArea.setFont(resultArea.getFont().deriveFont(fontSize));
		button.setText(bundle.getString("textButton"));
		button.setSize(getTextWidth(button),30);
		button.setLocation((getSize().width-18)/2-button.getWidth()/2,getSize().height-88);
	}
	private void deleteDuplicates(){
		boolean checked[]=new boolean[list.size()];
		for(int i=0;i<list.size();i++){
			if(checked[i]){continue;}
			MyEntry<String,ArrayList<String>>entry=list.get(i);
			for(int j=i+1;j<list.size();j++){
				MyEntry<String,ArrayList<String>>entry2=list.get(j);
				if(entry.getValue().contains(entry2.getKey())){break;}
				else if(entry2.getValue().equals(entry.getValue())){
					ArrayList<String>operands=entry2.getValue();
					operands.clear();
					String operand=entry.getValue().size()==1?entry.getValue().get(0):entry.getKey();
					operands.add(operand);
					checked[j]=true;
					replaceOperands(j);
				}
			}
		}
	}
	private void replaceOperands(){for(int i=0;i<list.size();i++){replaceOperands(i);}}
	private void replaceOperands(int index){
		boolean replaced=false;
		MyEntry<String,ArrayList<String>>entry=list.get(index);
		if(entry.getValue().size()==1){
			for(int j=index+1;j<list.size();j++){
				if(list.get(j).getKey().equals(entry.getKey())){break;}
				ArrayList<String>operands=list.get(j).getValue();
				for(int k=0;k<operands.size();k++){
					if(entry.getKey().equals(operands.get(k))){
						operands.remove(k);
						operands.add(k,entry.getValue().get(0));
						replaced=true;
					}
				}
			}
		}
		if(replaced){deleteDuplicates();}
	}
	private void deleteUnused(){
		boolean toDelete[]=new boolean[list.size()];
		mainloop:
		for(int i=toDelete.length-2;i>=0;i--){
			MyEntry<String,ArrayList<String>>entry=list.get(i);
			for(int j=i+1;j<toDelete.length;j++){
				if(toDelete[j]){continue;}
				if(list.get(j).getValue().contains(entry.getKey())){continue mainloop;}
			}
			toDelete[i]=true;
		}
		ArrayList<MyEntry<String,ArrayList<String>>>newList=new ArrayList<MyEntry<String,ArrayList<String>>>();
		for(int i=0;i<toDelete.length;i++){if(!toDelete[i]){newList.add(list.get(i));}}
		list=newList;
	}
	private void evaluateExpressions(){
		boolean evaluated=false;
		for(int i=0;i<list.size();i++){
			MyEntry<String,ArrayList<String>>entry=list.get(i);
			ArrayList<String>operands=new ArrayList<String>();
			if(entry.getValue().size()<2){continue;}
			try{
				operands.add(""+evaluate(convertToPosfix(entry.getValue())));
				entry.setValue(operands);
				evaluated=true;
			}
			catch(Exception e){}
		}
		replaceOperands();
		deleteUnused();
		if(evaluated){evaluateExpressions();}
	}
	private void replaceArithmeticIdentity(){
		boolean replaced=false;
		for(int i=0;i<list.size();i++){
			MyEntry<String,ArrayList<String>>entry=list.get(i);
			ArrayList<String>operands=entry.getValue();
			if(operands.size()==3){
				String op=operands.get(1);
				Double a=null,b=null;
				try{a=valueOf(operands.get(0));}
				catch(Exception e){}
				try{b=valueOf(operands.get(2));}
				catch(Exception e){}
				String first=a==null?operands.get(0):""+a,second=b==null?operands.get(2):""+b;
				if("+".equals(op)&&a!=null&&a.doubleValue()==0){
					operands.clear();
					operands.add(second);
					replaced=true;
				}
				else if("+".equals(op)&&b!=null&&b.doubleValue()==0){
					operands.clear();
					operands.add(first);
					replaced=true;
				}
				else if("-".equals(op)&&b!=null&&b.doubleValue()==0){
					operands.clear();
					operands.add(first);
					replaced=true;
				}
				else if("*".equals(op)&&a!=null&&a.doubleValue()==1){
					operands.clear();
					operands.add(second);
					replaced=true;
				}
				else if("*".equals(op)&&b!=null&&b.doubleValue()==1){
					operands.clear();
					operands.add(first);
					replaced=true;
				}
				else if("/".equals(op)&&b!=null&&b.doubleValue()==1){
					operands.clear();
					operands.add(first);
					replaced=true;
				}
				else if("/".equals(op)&&operands.get(0).equals(operands.get(2))){
					operands.clear();
					operands.add("1");
					replaced=true;
				}
			}
		}
		if(replaced){
			deleteDuplicates();
			evaluateExpressions();
			replaceArithmeticIdentity();
		}
	}
	private void downgrade(){
		boolean replaced=false;
		for(int i=0;i<list.size();i++){
			MyEntry<String,ArrayList<String>>entry=list.get(i);
			ArrayList<String>operands=entry.getValue();
			if(operands.size()==3){
				String op=operands.get(1);
				Double a=null,b=null;
				try{a=valueOf(operands.get(0));}
				catch(Exception e){}
				try{b=valueOf(operands.get(2));}
				catch(Exception e){}
				String first=a==null?operands.get(0):""+a,second=b==null?operands.get(2):""+b;
				if("^".equals(op)&&b!=null&&b.doubleValue()==2){
					operands.clear();
					operands.add(first);
					operands.add("*");
					operands.add(first);
					replaced=true;
				}
				else if("*".equals(op)&&a!=null&&a.doubleValue()==2){
					operands.clear();
					operands.add(second);
					operands.add("+");
					operands.add(second);
					replaced=true;
				}
				else if("*".equals(op)&&b!=null&&b.doubleValue()==2){
					operands.clear();
					operands.add(first);
					operands.add("+");
					operands.add(first);
					replaced=true;
				}
			}
		}
		if(replaced){
			deleteDuplicates();
			evaluateExpressions();
			replaceArithmeticIdentity();
			downgrade();
		}
	}
	public static void createAndShowGUI(ResourceBundle bundle){
		String sizeValues[]=bundle.getString("size").split(",");
		Dimension size;
		try{size=new Dimension(parseInt(sizeValues[0]),parseInt(sizeValues[1]));}
		catch(NumberFormatException e){size=new Dimension(528,396);}
		new MyCodeOptimizer(size);
	}
}