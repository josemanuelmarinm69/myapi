package myapi.regex;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import myapi.generics.MyEntry;
public class MyTuringMachine{
	private TuringMachineBand band;
	private TuringMachineState currentState;
	private class TuringMachineBand{
		private LinkedList<Character>list;
		private int index;

		public TuringMachineBand(char...chars){
			list=new LinkedList<Character>();
			list.addFirst(new Character('\0'));
			for(char c:chars){list.addLast(new Character(c));}
			list.addLast(new Character('\0'));
			index=1;
		}
		public String getBandChars(){
			String bandChars="";
			for(Character c:list){bandChars+=c.charValue();}
			return bandChars.replaceAll("\0","");
		}
		public synchronized char read(){return list.get(index).charValue();}
		public synchronized void write(char c){list.set(index,new Character(c));}
		public synchronized void move(TuringMachineMoves direction){
			switch(direction){
				case R:
					index++;
					if(index==list.size()){
						list.addLast(new Character('\0'));
					}
					break;
				case L:
					index--;
					if(index<0){
						list.addFirst(new Character('\0'));
						index=0;
					}
					break;
				case H:
			}
		}
	}
	public static class TuringMachineState{
		private boolean isAcceptState;
		private HashMap<Character,TuringMachineState>transitions;
		private HashMap<Character,Character>writes;
		private HashMap<Character,TuringMachineMoves>moves;

		public TuringMachineState(){this(false);}
		public TuringMachineState(boolean isAcceptState){
			this.isAcceptState=isAcceptState;
			transitions=new HashMap<Character,TuringMachineState>();
			writes=new HashMap<Character,Character>();
			moves=new HashMap<Character,TuringMachineMoves>();
		}
		public boolean isAcceptState(){return isAcceptState;}
		public void addTransition(TuringMachineState state,char read,char write,TuringMachineMoves move){
			transitions.put(new Character(read),state);
			writes.put(new Character(read),new Character(write));
			moves.put(new Character(read),move);
		}
		private TuringMachineState advance(char c){return transitions.get(new Character(c));}
		private char getWrite(char c){return writes.get(new Character(c)).charValue();}
		private TuringMachineMoves getMove(char c){return moves.get(new Character(c));}
	}
	public static enum TuringMachineMoves{R,L,H}

	public MyTuringMachine(TuringMachineState initialState){currentState=initialState;}
	public synchronized boolean isAccepted(String expression)throws UndefinedLanguageCharacterException{
		band=new TuringMachineBand(expression.toCharArray());
		TuringMachineMoves move=null;
		while(move!=TuringMachineMoves.H){
			char read=band.read();
			char write=currentState.getWrite(read);
			move=currentState.getMove(read);
			currentState=currentState.advance(read);
			if(currentState==null){throw new UndefinedLanguageCharacterException("Undefined "+read);}
			band.write(write);
			band.move(move);
		}
		return currentState.isAcceptState;
	}
	public String getBandChars(){return band.getBandChars();}
	public static MyTuringMachine getMachine(){
		TuringMachineState q0=new TuringMachineState();
		TuringMachineState q1=new TuringMachineState();
		TuringMachineState q2=new TuringMachineState(true);
		TuringMachineState q3=new TuringMachineState();
		TuringMachineState q4=new TuringMachineState();
		TuringMachineMoves r=TuringMachineMoves.R;
		TuringMachineMoves l=TuringMachineMoves.L;
		TuringMachineMoves h=TuringMachineMoves.H;
		q0.addTransition(q0,'0','0',r);
		q0.addTransition(q0,'1','1',r);
		q0.addTransition(q1,'\0','\0',l);
		q1.addTransition(q3,'1','\0',l);
		q1.addTransition(q2,'0','\0',h);
		q3.addTransition(q3,'0','0',l);
		q3.addTransition(q3,'1','1',l);
		q3.addTransition(q4,'\0','\0',r);
		q4.addTransition(q0,'1','\0',r);
		return new MyTuringMachine(q0);
	}
	public static Entry<Boolean,String>TMTest(String expression){
		MyTuringMachine tm=getMachine();
		try{return new MyEntry<Boolean,String>(new Boolean(tm.isAccepted(expression)),tm.getBandChars());}
		catch(UndefinedLanguageCharacterException e){myapi.ErrorConsole.addText(e);}
		return new MyEntry<Boolean,String>(new Boolean(false),tm.getBandChars());
	}
}