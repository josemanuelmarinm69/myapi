package myapi.regex;
import static java.lang.Integer.parseInt;
import static java.io.File.separatorChar;
import static java.util.Locale.US;
import static myapi.MyFileHelper.loadStrings;
import static myapi.utils.OfFile.getFilesFolder;
import static myapi.utils.OfFile.getFilesResourceBundle;
import static myapi.utils.OfGUI.getTextWidth;
import java.awt.Dimension;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import myapi.gui.MyWindow;
import myapi.io.MyFileDragDropListener;
import myapi.regex.MyAbstractSyntaxTree.Token;
public class MyAnalyzer extends MyWindow{
	private static final long serialVersionUID=-2891228666733919736L;
	private JPanel contentPane;
	private JTextArea textArea;
	private JTextArea resultArea;
	private JButton button;
	private MyLexicalAnalyzer la;
	private MySyntaxAnalyzer sya;
	private MySemanticAnalyzer sea;

	public MyAnalyzer(Dimension dimension){
		super(US,false,dimension);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		contentPane=new JPanel(null);
		new DropTarget(contentPane,new MyFileDragDropListener(new AbstractAction(){
			private static final long serialVersionUID=-4831231817079313926L;

			public void actionPerformed(ActionEvent e){
				MyFileDragDropListener handler=(MyFileDragDropListener)e.getSource();
				List<File>fileList=handler.getList();
				if(fileList==null||fileList.isEmpty()){return;}
				displayText(fileList.get(fileList.size()-1));
			}
		}));
		textArea=new JTextArea();
		JScrollPane pane=new JScrollPane(textArea);
		pane.setBounds(5,5,(dimension.width-18)/2,dimension.height-93);
		resultArea=new JTextArea();
		resultArea.setEditable(false);
		JScrollPane pane1=new JScrollPane(resultArea);
		pane1.setBounds((dimension.width-18)/2+5,5,(dimension.width-18)/2-5,dimension.height-93);
		contentPane.add(pane);
		contentPane.add(pane1);
		contentPane.add(button);
		setContentPane(contentPane);
		setVisible(true);
	}
	public synchronized void displayText(File file){
		ArrayList<String>strings=loadStrings(file);
		String output="";
		for(int i=0;i<strings.size();i++){
			String string=strings.get(i);
			output+=string;
			if(i!=strings.size()-1){output+='\n';}
		}
		textArea.setText(output);
	}
	@Override public synchronized void init(){
		button=new JButton();
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					ArrayList<Token>tokenList=la.analyze(textArea.getText());
					String resultOutput="";
					String errorOutput=bundle.getString("errorHeader")+'\n';
					for(String string:la.getErrorList()){errorOutput+=string+'\n';}

					File tokenFile=new File(getFilesFolder()+separatorChar+"tokenlist.txt");//Write of lexical output
					FileWriter writer=new FileWriter(tokenFile);
					String output="";
					for(int i=0;i<tokenList.size();i++){
						Token data=tokenList.get(i);
						if(i>0){output+='\n';}
						if(!data.getLexema().equals("...")){output+=data.getName()+'|'+data.getLexema()+'|'+data.getLine();}
					}
					writer.write(output);
					writer.close();

					BufferedReader reader=new BufferedReader(new FileReader(tokenFile));//Read of lexical output
					ArrayList<Token>tokenList1=new ArrayList<Token>();
					String line;
					while((line=reader.readLine())!=null){
						if(line.contains("|")){
							String values[]=line.split("\\|");
							Token data=new Token();
							data.setName(values[0]);
							data.setLexema(values[1]);
							data.setLine(parseInt(values[2]));
							tokenList1.add(data);
						}
					}
					reader.close();

					MyAbstractSyntaxTree tree=sya.build(tokenList1,la.getIdsSet());//Write of syntax output
					if(tree!=null){
						String treeFile=getFilesFolder()+separatorChar+"tree.txt";
						writer=new FileWriter(new File(treeFile));
						writer.write(tree.getDisplayText());
						writer.close();
						resultOutput+=bundle.getString("idsHeader")+'\n';//Text display
						for(Token data:la.getIdsSet()){resultOutput+=data.getLexema()+'|'+data.getType()+'\n';}
						resultOutput+='\n';

						sea=new MySemanticAnalyzer(getFilesResourceBundle("syntax",getLocale()),getFilesResourceBundle("semantic",getLocale()),treeFile);
						tree=sea.decorate(sya.getIdsSet());
						writer=new FileWriter(new File(getFilesFolder()+separatorChar+"decoratedtree.txt"));//Write of semantic output
						writer.write(tree.getDisplayText());
						writer.close();
						if(!sea.getErrorOutput().isEmpty()){for(String error:sea.getErrorOutput()){errorOutput+=error+'\n';}}
					}
					else{for(String string:sya.getErrorList()){errorOutput+=string+'\n';}}
					resultArea.setText(resultOutput+errorOutput);
				}
				//----ErrorConsole----//
				catch(IOException ex){myapi.ErrorConsole.addText(ex);}
			}
		});
	}
	@Override public synchronized void updateBundle(Locale locale){bundle=getFilesResourceBundle("lexical",locale);}
	@Override public synchronized void updateLocale(){
		super.updateLocale();
		la=new MyLexicalAnalyzer(bundle);
		sya=new MySyntaxAnalyzer(getFilesResourceBundle("syntax",bundle.getLocale()));
		button.setText(bundle.getString("analizeButton"));
		button.setSize(getTextWidth(button),30);
		button.setLocation((getSize().width-18)/2-button.getWidth()/2,getSize().height-88);
	}
	public static void createAndShowGUI(){new MyAnalyzer(new Dimension(528,396));}
}