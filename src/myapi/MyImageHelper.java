package myapi;
public class MyImageHelper{
	public static javax.swing.ImageIcon loadImage(String name){
		String path=myapi.utils.OfFile.getImagesFolder()+"/"+name;
		javax.swing.ImageIcon icon;
		if(myapi.utils.OfFile.isValidFilePath(path)){icon=loadImage(new java.io.File(path).toURI());}
		else{icon=loadImage(MyImageHelper.class.getResource("imagenes/"+name));}
		if(icon==null){icon=new javax.swing.ImageIcon();}
		return icon;
	}
	public static javax.swing.ImageIcon loadImage(String name,java.awt.Dimension size){return getScaledIcon(loadImage(name),size);}
	public static javax.swing.ImageIcon loadButtonImage(String name){return loadImage("botones/"+name);}
	public static javax.swing.ImageIcon loadImage(java.net.URL url){
		javax.swing.ImageIcon icon=new javax.swing.ImageIcon(java.awt.Toolkit.getDefaultToolkit().getImage(url));
		icon.setDescription("path="+url.getPath());
		return icon;
	}
	public static javax.swing.ImageIcon loadImage(java.net.URI uri){
		try{return loadImage(uri.toURL());}
		catch(java.net.MalformedURLException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			return null;
		}
	}
	public static javax.swing.ImageIcon loadImage(java.net.URL url,java.awt.Dimension size){return getScaledIcon(loadImage(url),size);}
	public static javax.swing.ImageIcon loadImage(java.net.URI uri,java.awt.Dimension size){return getScaledIcon(loadImage(uri),size);}
	public static javax.swing.ImageIcon getScaledIcon(javax.swing.ImageIcon icon,java.awt.Dimension size){
		if(icon==null){return null;}
		return new javax.swing.ImageIcon(icon.getImage().getScaledInstance(size.width,size.height,java.awt.Image.SCALE_AREA_AVERAGING));
	}
	public static javax.swing.ImageIcon getRotatedIcon(javax.swing.ImageIcon icon,double degrees){
		java.awt.image.BufferedImage image;
		image=new java.awt.image.BufferedImage(icon.getIconWidth(),icon.getIconHeight(),java.awt.image.BufferedImage.TYPE_INT_ARGB);
		java.awt.Graphics2D g=(java.awt.Graphics2D)image.getGraphics();
		g.rotate(Math.toRadians(degrees),icon.getIconWidth()/2,icon.getIconHeight()/2);
		g.drawImage(icon.getImage(),0,0,null);
		return new javax.swing.ImageIcon(image);
	}
	public static javax.swing.ImageIcon getRotatedIcon(String name,int radians){return getRotatedIcon(loadImage(name),radians);}
	public static java.awt.image.BufferedImage convertImage(java.awt.Image image){
		if(image instanceof java.awt.image.BufferedImage){return(java.awt.image.BufferedImage)image;}
		/*Create a buffered image with transparency*/
		java.awt.image.BufferedImage bufferedImage=new java.awt.image.BufferedImage(
				image.getWidth(null),image.getHeight(null),java.awt.image.BufferedImage.TYPE_INT_ARGB);
		/*Draw the image on to the buffered image*/
		java.awt.Graphics2D graphics=bufferedImage.createGraphics();
		graphics.drawImage(image,0,0,null);
		graphics.dispose();
		return bufferedImage;
	}
	public static java.awt.image.BufferedImage convertImageIcon(javax.swing.ImageIcon icon){return convertImage(icon.getImage());}
}