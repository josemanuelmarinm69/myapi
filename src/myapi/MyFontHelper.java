package myapi;
public class MyFontHelper{
	public static java.awt.Font loadFont(String name,int style,int size){
		try{
			java.net.URI uri=new java.io.File(myapi.utils.OfFile.getFontsFolder()+"/"+name).toURI();
			java.io.BufferedInputStream stream=new java.io.BufferedInputStream(new java.io.FileInputStream(uri.getPath()));
			return java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT,stream).deriveFont(style,size);
		}
		catch(java.awt.FontFormatException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
		catch(java.io.IOException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
		return null;
	}
	public static java.awt.Font changeSize(java.awt.Font font,int size,boolean relative){
		float newSize=relative?font.getSize2D()+size:size;
		return font.deriveFont(new Float(newSize).floatValue());
	}
}