package myapi;
import static java.awt.Color.black;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.parseInt;
import static javax.swing.JOptionPane.DEFAULT_OPTION;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.WARNING_MESSAGE;
import static javax.swing.JOptionPane.YES_OPTION;
import static javax.swing.JOptionPane.getRootFrame;
import static javax.swing.JOptionPane.showOptionDialog;
import static myapi.MyFileHelper.loadStrings;
import static myapi.MyFontHelper.changeSize;
import static myapi.MyImageHelper.loadImage;
import static myapi.security.MyUserSystemRegistry.getUsers;
import static myapi.utils.OfData.isEmptyString;
import static myapi.utils.OfEvent.addFocusListener;
import static myapi.utils.OfEvent.addListenerToJCheckBox;
import static myapi.utils.OfEvent.addListenerToJComboBox;
import static myapi.utils.OfEvent.addOverListener;
import static myapi.utils.OfEvent.createJSpinnerWithListener;
import static myapi.utils.OfEvent.fetchArrayIntoJComboBox;
import static myapi.utils.OfGUI.createWorker;
import static myapi.utils.OfGUI.dialog;
import static myapi.utils.OfGUI.getTextHeight;
import static myapi.utils.OfGUI.getTextWidth;
import static myapi.utils.OfGUI.syncJProgressBarWithWorker;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.plaf.basic.BasicTextAreaUI;
import javax.swing.text.MaskFormatter;
import myapi.utils.OfGUI.MyWorker;
import myapi.generics.MyEntry;
import org.jdesktop.swingworker.SwingWorker;
/**
@String_Compare
For <em><strong>java.lang.String.compareToIgnoreCase</em></strong> it results 0 on "A"&"a", + on "Z"&"A", - on "A"&"Z".
@Unicode_Characters
&#225; \u00E1 &#193; \u00C1
&#233; \u00E9 &#201; \u00C9
&#237; \u00ED &#205; \u00CD
&#243; \u00F3 &#211; \u00D3
&#250; \u00FA &#218; \u00DA
&#241; \u00F1 &#209; \u00D1
&#191; \u00BF &#63; \u003F
&#64; \u0040 &#94; \u005E.
<p>New paragraph.
*/
public class MyOptionPane{
	private static final ImageIcon icon=loadImage("iconopmyoption.png");
	private static final Font font=dialog;
	private static final char yes[]={'S','s'},no[]={'N','n'};
	public static final JButton yesNo[]={new JButton("Si"),new JButton("No")};
	private static MyWorker task;
	private static JProgressBar tasklessBar;
	private static JButton accept[];
	private static JTextField textField;
	private static JPasswordField passwordField;
	private static Font userFont;
	private static String string;
	private static Integer integer;
	private static Entry<String,String>login;
	private static Boolean selections[];
	private static boolean repeat,validInformation;
	static{
		/*Font fuenteOver=fuenteOriginal.deriveFont(new Float(fuenteOriginal.getSize2D()+5));*/
		accept=new JButton[1];
		accept[0]=new JButton("Aceptar");
		accept[0].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){integer=new Integer(0);getRootFrame().dispose();}
		});
		addFocusListener(accept[0]);
		yesNo[0].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){integer=new Integer(0);getRootFrame().dispose();}
		});
		yesNo[1].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){integer=new Integer(1);getRootFrame().dispose();}
		});
		addFocusListener(yesNo[0]);
		addFocusListener(yesNo[1]);
	}

	public static void showMessage(boolean flag,String title){showMessage(""+flag,title);}
	public static void showMessage(long integer,String title){showMessage(""+integer,title);}
	public static void showMessage(double decimal,String title){showMessage(""+decimal,title);}
	public static void showMessage(char character,String title){showMessage(""+character,title);}
	public static void showMessage(Object object,String title){showMessage(object.toString(),title);}
	public static void showMessage(Iterable<?>array,String title){
		String dialog="Datos:";int i=0;
		for(Object object:array){dialog+="\n"+object;i++;}
		if(i==0){showMessage("No hay datos",title);return;}
		showMessage(dialog,title);
	}
	public static void showMessage(Object array[],String title){
		if(array.length==0){showMessage("No hay datos",title);return;}
		String dialog="Datos:";
		for(Object object:array){dialog+="\n"+object;}
		showMessage(dialog,title);
	}
	public static void showMessage(String dialog,String title){
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setPreferredSize(new Dimension(getTextWidth(dialog,area),area.getPreferredSize().height));
		area.setSize(area.getPreferredSize());
		showOptionDialog(null,area,title,WARNING_MESSAGE,YES_OPTION,icon,accept,accept[0]);
	}
	public static boolean receiveString(String dialog,String title){return receiveString(dialog,title,null);}
	public static boolean receiveString(String dialog,String title,final String mask){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Panel size to fit text*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,50));
		/*Formatted or simple text field for the input*/
		textField=null;
		if(mask==null){textField=new JTextField();}
		else{
			try{textField=new JFormattedTextField(new MaskFormatter(mask));}
			catch(ParseException e){textField=new JTextField();/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		}
		textField.setBounds(0,area.getY()+area.getHeight()+5,area.getPreferredSize().width,area.getHeight());
		textField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){accept[0].doClick();}
		});
		panel.add(textField);
		/*Own accept button*/
		JButton accept[]=new JButton[1];
		accept[0]=new JButton("Aceptar");
		if(userFont!=null){
			Font overFont=userFont.deriveFont(new Float(userFont.getSize2D()+5).floatValue());
			addOverListener(accept[0],userFont,black,overFont,black);
		}
		/*Accept button check for valid input*/
		accept[0].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				if(mask!=null){
					int i=mask.length()-1;
					for(;i>0;i--){char c=mask.charAt(i);if(c=='#'||c=='\''||c=='?'||c=='*'||c=='U'||c=='L'||c=='A'||c=='H'){break;}}
					if(textField.getText().charAt(i)!=' '){repeat=!(validInformation=(string=textField.getText())!=null);}
					else{
						if(choose("Do you want to try again","Try again?",yesNo)){repeat=integer.intValue()==0;}
						else{repeat=false;}
					}
				}
				else if(!isEmptyString(textField.getText())){repeat=!(validInformation=(string=textField.getText())!=null);}
				else{
					if(choose("Do you want to try again","Try again?",yesNo)){repeat=integer.intValue()==0;}
					else{repeat=false;}
				}
				integer=new Integer(0);
				getRootFrame().dispose();
			}
		});
		/*Loop when invalid input*/
		do{
			/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
			validInformation=repeat=false;
			showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,accept,textField);
		}while(repeat);
		return validInformation;
	}
	/**
	@Description
	You don't have to worry about closing the modal window neither the bounds of the <em><strong>buttons</em></strong> (in case you send that arguments).
	@Equals_To
	choose(dialog,title,<strong><em>options</em>-based buttons</strong>).
	*/
	public static boolean choose(String dialog,String title,Object[]options){
		/*Set up buttons*/
		final JButton buttons[];
		if(options==null){buttons=accept;}
		else{
			buttons=new JButton[options.length];
			for(int i=0;i<buttons.length;i++){buttons[i]=new JButton(options[i].toString());}
		}
		if(options!=null&&options.length>4){return showMenu(dialog,title,options);}
		return choose(dialog,title,buttons);
	}
	/**
	@Description
	You don't have to worry about closing the modal window neither the bounds of the <em><strong>buttons</em></strong> (in case you send that arguments).
	*/
	public static boolean choose(String dialog,String title,JButton[]buttons){
		/*Accept button on null options or send to menu on five options*/
		if(buttons==null){buttons=accept;}
		if(buttons.length>4){return showMenu(dialog,title,buttons);}
		/*Add needed close and index saving action plus user font*/
		if(buttons!=accept&&buttons!=yesNo){
			final HashMap<JButton,Integer>map=new HashMap<JButton,Integer>();
			for(int i=0;i<buttons.length;i++){
				map.put(buttons[i],new Integer(i));
				if(userFont!=null){buttons[i].setFont(userFont);}
				else{buttons[i].setFont(font);}
				buttons[i].addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){integer=map.get(e.getSource());getRootFrame().dispose();}
				});
			}
		}
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
		integer=null;
		showOptionDialog(null,area,title,WARNING_MESSAGE,YES_OPTION,icon,buttons,buttons[0]);
		return integer!=null;
	}
	/**
	@Description
	This is another type.
	*/
	public static boolean choose(String dialog,String title,String file){return choose(dialog,title,loadStrings(file));}
	/**
	@Description
	This is another type.
	*/
	public static boolean choose(String dialog,String title,ArrayList<String>array){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Set up the options combo box*/
		final JComboBox combo=new JComboBox();
		combo.setBounds(0,area.getY()+area.getHeight()+5,0,20);
		addListenerToJComboBox(combo,null);
		fetchArrayIntoJComboBox(combo,array);
		panel.add(combo);
		/*Panel size to fit major component between combo and area*/
		int comboWidth=combo.getWidth(),areaWidth=area.getPreferredSize().width,width;
		if(comboWidth>areaWidth){width=comboWidth;}
		else{width=areaWidth;}
		panel.setPreferredSize(new Dimension(width+5,50));
		/*Own accept button*/
		JButton accept[]=new JButton[1];
		accept[0]=new JButton("Aceptar");
		if(userFont!=null){
			Font overFont=userFont.deriveFont(new Float(userFont.getSize2D()+5).floatValue());
			addOverListener(accept[0],userFont,black,overFont,black);
		}
		/*Check for valid selection index*/
		accept[0].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(combo.getSelectedIndex()!=0){repeat=!(validInformation=(integer=new Integer(combo.getSelectedIndex()-1))!=null);}
				else{
					if(choose("Do you want to try again","Try again?",yesNo)){repeat=integer.intValue()==0;}
					else{repeat=false;}
				}
				getRootFrame().dispose();
			}
		});
		/*Loop when invalid input*/
		do{
			/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
			validInformation=repeat=false;
			showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,accept,accept[0]);
		}while(repeat);
		return validInformation;
	}
	/**
	@Description
	You don't have to worry about closing the modal window neither the bounds of the <em><strong>buttons</em></strong> (in case you send that arguments).
	*/
	public static boolean showMenu(String dialog,String title,Object[]options){
		/*Set up buttons*/
		final JButton buttons[]=new JButton[options.length];
		for(int i=0;i<buttons.length;i++){
			buttons[i]=new JButton(options[i].toString());
			buttons[i].setSize(buttons[i].getPreferredSize().width,20);
		}
		return showMenu(dialog,title,buttons);
	}
	/**
	@Description
	You don't have to worry about closing the modal window neither the bounds of the <em><strong>buttons</em></strong> (in case you send that arguments).
	*/
	public static boolean showMenu(String dialog,String title,JButton[]buttons){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Add needed close and index saving action plus user font and button location*/
		if(buttons!=accept&&buttons!=yesNo){
			final HashMap<JButton,Integer>map=new HashMap<JButton,Integer>();
			for(int i=0;i<buttons.length;i++){
				map.put(buttons[i],new Integer(i));
				if(userFont!=null){buttons[i].setFont(userFont);}
				else{buttons[i].setFont(font);}
				buttons[i].setBounds(0,(i+1)*(area.getY()+area.getHeight()+5),buttons[i].getPreferredSize().width,20);panel.add(buttons[i]);
				buttons[i].addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){integer=map.get(e.getSource());getRootFrame().dispose();}
				});
			}
		}
		/*Panel size to fit buttons*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,buttons[buttons.length-1].getY()+25));
		/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
		integer=null;
		showOptionDialog(null,panel,title,DEFAULT_OPTION,INFORMATION_MESSAGE,icon,new Object[0],buttons[0]);
		return integer!=null;
	}
	public static boolean select(String dialog,String title,Object[]options){
		/*Without options, it can't proceed*/
		if(options==null){return false;}
		/*Set up check boxes*/
		JCheckBox checks[]=new JCheckBox[options.length];
		for(int i=0;i<options.length;i++){
			checks[i]=new JCheckBox();
			if(options[i]!=null){checks[i].setText(options[i].toString());}
			else{checks[i].setText("No disponible");checks[i].setEnabled(false);}
			addListenerToJCheckBox(checks[i],yes,no);
		}
		return select(dialog,title,checks);
	}
	public static boolean select(String dialog,String title,JCheckBox[]checks){
		/*Without options, it can't proceed*/
		if(checks==null){return false;}
		selections=new Boolean[checks.length];
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Place the check boxes*/
		for(int i=0;i<checks.length;i++){
			checks[i].setBounds(0,(i+1)*(area.getY()+area.getHeight()+5),area.getPreferredSize().width,20);
			if(userFont!=null){checks[i].setFont(userFont);}
			else{checks[i].setFont(font);}
			panel.add(checks[i]);
		}
		/*Panel size to fit components*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,checks[checks.length-1].getY()+25));
		/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
		integer=null;
		showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,accept,accept[0]);
		/*Fill array with selections*/
		if(integer!=null){for(int i=0;i<checks.length;i++){selections[i]=new Boolean(checks[i].isSelected());}}
		return integer!=null;
	}
	public static boolean receiveInteger(String dialog,String title){return receiveInteger(dialog,title,MAX_VALUE);}
	public static boolean receiveInteger(String dialog,String title,int upperLimit){return receiveInteger(dialog,title,0,upperLimit);}
	public static boolean receiveInteger(String dialog,String title,int lowerLimit,int upperLimit){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Set up positive integer restricted spinner*/
		JSpinner spinner=createJSpinnerWithListener(lowerLimit,upperLimit,1);
		spinner.setBounds(0,area.getY()+area.getHeight()+5,spinner.getWidth(),20);
		spinner.requestFocus();
		panel.add(spinner);
		/*Panel size to fit components*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,50));
		/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
		integer=null;
		showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,accept,accept[0]);
		if(integer!=null){integer=(Integer)spinner.getValue();}
		return integer!=null;
	}
	public static boolean receiveInteger(String dialog,String title,String errorMessage){
		do{
			validInformation=repeat=(integer=null)!=null;
			/*Call for input without specific mask*/
			if(!receiveString(dialog,title)){return false;}
			try{repeat=!(validInformation=(integer=new Integer(parseInt(string)))!=null);}
			/*Catch exception and show the error message*/
			catch(NumberFormatException e){
				showMessage(errorMessage,"Dato inv\u00E1lido");
				if(choose("Do you want to try again","Try again?",yesNo)){repeat=integer.intValue()==0;}
				else{return false;}
				validInformation=false;
			}
		}while(repeat);
		return validInformation;
	}
	public static void showProgress(String dialog,String title,String processMethod,String finishMethod,Object instance){
		task=createWorker(processMethod,finishMethod,instance);
		showProgress(dialog,title,task);
		task=null;
	}
	public static void showProgress(String dialog,String title,String processMethod,String finishMethod,Class<?>methodsClass){
		task=createWorker(processMethod,finishMethod,methodsClass);
		showProgress(dialog,title,task);
		task=null;
	}
	public static void showProgress(String dialog,String title,SwingWorker<?,?>task){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Panel size to fit components*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,50));
		/*Progress bar and sync with progress*/
		final JProgressBar bar=new JProgressBar(0,100);
		bar.setBounds(0,area.getY()+area.getHeight()+5,area.getPreferredSize().width,20);
		panel.add(bar);
		syncJProgressBarWithWorker(bar,task);
		/*Start task and show until it finish*/
		task.execute();
		while(!task.isDone()){
			showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,new Object[0],null);
		}
	}
	public static void showTasklessProgress(String dialog,final String title){
		final JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Panel size to fit components*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,50));
		/*Progress bar and sync with progress*/
		tasklessBar=new JProgressBar(0,100);
		tasklessBar.setBounds(0,area.getY()+area.getHeight()+5,area.getPreferredSize().width,20);
		panel.add(tasklessBar);
		new Thread(){
			@Override public void run(){
				showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,new Object[0],null);
				//tasklessBar=null;
			}
		}.start();
	}
	public static boolean showLogin(String dialog,String title){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Panel size to fit text*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,75));
		/*Simple text field for the user*/
		textField=new JTextField();
		textField.setBounds(0,area.getY()+area.getHeight()+5,area.getPreferredSize().width,20);
		textField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){accept[0].doClick();}
		});
		textField.addCaretListener(new CaretListener(){
			public void caretUpdate(CaretEvent e){passwordField.setEnabled(!isEmptyString(textField.getText()));}
		});
		panel.add(textField);
		passwordField=new JPasswordField();
		passwordField.setEnabled(false);
		passwordField.setBounds(0,50,area.getPreferredSize().width,20);
		passwordField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){accept[0].doClick();}
		});
		panel.add(passwordField);
		/*Own accept button*/
		JButton accept[]=new JButton[1];
		accept[0]=new JButton("Aceptar");
		if(userFont!=null){
			Font overFont=userFont.deriveFont(new Float(userFont.getSize2D()+5).floatValue());
			addOverListener(accept[0],userFont,black,overFont,black);
		}
		/*Accept button check for valid input*/
		accept[0].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				if(!isEmptyString(textField.getText())){
					repeat=!(validInformation=(string=textField.getText())!=null);
					login=new MyEntry<String,String>(string,new String(passwordField.getPassword()));
				}
				else{
					if(choose("Do you want to try again","Try again?",yesNo)){repeat=integer.intValue()==0;}
					else{repeat=false;}
				}
				integer=new Integer(0);
				getRootFrame().dispose();
			}
		});
		/*Loop when invalid input*/
		do{
			/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
			validInformation=repeat=(login=null)!=null;
			showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,accept,textField);
		}while(repeat);
		return validInformation;
	}
	public static boolean showSystemRegistryLogin(String dialog,String title){
		JPanel panel=new JPanel(null);
		/*Basic GUI text area containing the dialog string*/
		JTextArea area=new JTextArea(dialog);
		area.setOpaque(false);
		area.setEditable(false);
		area.setFocusable(false);
		area.setUI(new BasicTextAreaUI());
		if(userFont!=null){area.setFont(userFont);}
		else{area.setFont(font);}
		area.setBounds(0,0,area.getPreferredSize().width,getTextHeight(dialog,area));
		panel.add(area);
		/*Panel size to fit text*/
		panel.setPreferredSize(new Dimension(area.getPreferredSize().width+5,75));
		/*Combo box containing the system registry user*/
		final JComboBox comboBox=new JComboBox();
		HashMap<String,String>users=getUsers();
		if(users==null||users.isEmpty()){return false;}
		comboBox.addItem("");
		for(Entry<String,String>entry:users.entrySet()){comboBox.addItem(entry.getKey());}
		comboBox.setBounds(0,area.getY()+area.getHeight()+5,area.getPreferredSize().width,20);
		comboBox.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){passwordField.setEnabled(comboBox.getSelectedIndex()!=0);}
		});
		panel.add(comboBox);
		passwordField=new JPasswordField();
		passwordField.setEnabled(false);
		passwordField.setBounds(0,50,area.getPreferredSize().width,20);
		passwordField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){accept[0].doClick();}
		});
		panel.add(passwordField);
		/*Own accept button*/
		JButton accept[]=new JButton[1];
		accept[0]=new JButton("Aceptar");
		if(userFont!=null){
			Font overFont=userFont.deriveFont(new Float(userFont.getSize2D()+5).floatValue());
			addOverListener(accept[0],userFont,black,overFont,black);
		}
		/*Accept button check for valid input*/
		accept[0].addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				if(comboBox.getSelectedIndex()>0&&comboBox.getSelectedItem()!=null){
					repeat=!(validInformation=(string=comboBox.getSelectedItem().toString())!=null);
					login=new MyEntry<String,String>(string,new String(passwordField.getPassword()));
				}
				else{
					if(choose("Do you want to try again","Try again?",yesNo)){repeat=integer.intValue()==0;}
					else{repeat=false;}
				}
				integer=new Integer(0);
				getRootFrame().dispose();
			}
		});
		/*Loop when invalid input*/
		do{
			/*When using integer,string,selections or validInformation to determine if proceed (the returning value), they must be cleared first*/
			validInformation=repeat=(login=null)!=null;
			showOptionDialog(null,panel,title,WARNING_MESSAGE,YES_OPTION,icon,accept,textField);
		}while(repeat);
		return validInformation;
	}
	public static synchronized void setProgress(int progress){if(task!=null){task.updateProgress(progress);}}
	public static synchronized void setTasklessProgress(long progress){setTasklessProgress((int)progress);}
	public static synchronized void setTasklessProgress(int progress){
		if(tasklessBar!=null){tasklessBar.setValue(progress);}
	}
	public static synchronized void setTasklessTop(long top){setTasklessTop((int)top);}
	public static synchronized void setTasklessTop(int top){
		if(tasklessBar!=null){tasklessBar.setMaximum(top);}
	}
	public static synchronized void setFont(Font userFont,Color color){
		MyOptionPane.userFont=userFont;
		if(color==null){color=black;}
		if(userFont!=null){
			Font overFont=changeSize(userFont,5,true);
			addOverListener(accept[0],userFont,color,overFont,color);
			addOverListener(yesNo[0],userFont,color,overFont,color);
			addOverListener(yesNo[1],userFont,color,overFont,color);
		}
		else{
			Font overFont=font.deriveFont(new Float(font.getSize2D()+5).floatValue());
			addOverListener(accept[0],font,color,overFont,color);
			addOverListener(yesNo[0],font,color,overFont,color);
			addOverListener(yesNo[1],font,color,overFont,color);
		}
	}
	public static String getString(){return string;}
	public static Integer getInteger(){return integer;}
	public static Entry<String,String>getLogin(){return login;}
	public static Boolean[]getSelections(){return selections;}
}