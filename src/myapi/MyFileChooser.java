package myapi;
import static darrylbu.util.SwingUtils.getDescendantOfType;
import static java.io.File.separatorChar;
import static java.util.Locale.ENGLISH;
import static javax.swing.JFileChooser.CANCEL_OPTION;
import static javax.swing.JFileChooser.DIRECTORIES_ONLY;
import static javax.swing.JFileChooser.ERROR_OPTION;
import static javax.swing.JFileChooser.FILES_ONLY;
import static javax.swing.UIManager.get;
import static javax.swing.UIManager.getIcon;
import static myapi.MyImageHelper.loadImage;
import static myapi.MyOptionPane.choose;
import static myapi.MyOptionPane.getInteger;
import static myapi.MyOptionPane.showMessage;
import static myapi.MyOptionPane.yesNo;
import static myapi.utils.OfFile.dat;
import static myapi.utils.OfFile.fileHasExtensionAndDependants;
import static myapi.utils.OfFile.getExtension;
import static myapi.utils.OfFile.getFileName;
import static myapi.utils.OfFile.getFilesFolder;
import static myapi.utils.OfFile.getProgramFolder;
import static myapi.utils.OfFile.getFilesResourceBundle;
import static myapi.utils.OfFile.isValidFilePath;
import static myapi.utils.OfGUI.setFileChooserFont;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;
import myapi.io.MyStream.MyObjectReader;
import myapi.io.MyStream.MyObjectWriter;
public class MyFileChooser{
	private String selectExtensionMessage;
	private String selectExtensionTitle;
	private String notFoundMessage;
	private String notFoundTitle;
	private String overwriteMessage;
	private String overwriteTitle;
	private String selectAgainMessage;
	private String selectAgainTitle;
	private String newFolderName;
	private String openTitle;
	private String openButtonText;
	private String openButtonToolTipText;
	private String saveTitle;
	private String saveButtonText;
	private String saveButtonToolTipText;
	private JFileChooser chooser;
	private MyFileFilter fileFilter;
	private MyFileView fileView;
	private MyFolderFilter folderFilter;
	private MyFolderView folderView;
	private JTextField textField;
	private JLabel lookInLabel;
	private JLabel fileNameLabel;
	private JLabel filesOfTypeLabel;
	private JButton cancelButton;
	private JButton upFolderButton;
	private JButton homeFolderButton;
	private JButton newFolderButton;
	private JToggleButton listViewButton;
	private JToggleButton detailsViewButton;
	private JMenu viewMenuItem;
	private JMenuItem refreshMenuItem;
	private JMenuItem newFolderMenuItem;
	private JRadioButtonMenuItem listMenuItem;
	private JRadioButtonMenuItem detailsMenuItem;
	private boolean validInformation;
	private boolean blocked;
	private boolean fileSelectionMode;
	public static class TempDirStream{
		private final String path;

		public TempDirStream(String extension){path=getFilesFolder()+separatorChar+"temp."+extension;}
		public String read(){
			if(isValidFilePath(path)){
				try{
					MyObjectReader reader=new MyObjectReader(new FileInputStream(path));
					Object object=reader.readObject();
					while(object!=null){
						if(object instanceof String){reader.close();return(String)object;}
						object=reader.readObject();
					}
					reader.close();
				}
				catch(ClassNotFoundException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(ClassCastException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(EOFException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
				catch(IOException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
			}
			return null;
		}
		public void write(String path){
			try{
				MyObjectWriter writer=new MyObjectWriter(new FileOutputStream(this.path));
				writer.writeObject(path);
				writer.close();
			}
			catch(IOException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
		}
	}
	public static class MyFileView extends FileView{
		private ResourceBundle bundle;
		private HashMap<String,String>icons;
		private HashMap<String,String>descriptionKeys;
		private ImageIcon folder;

		public MyFileView(){folder=loadImage("folIcon.png");}
		public MyFileView(HashMap<String,String>icons,HashMap<String,String>descriptionKeys){
			this();
			setIcons(icons);
			setDescriptionKeys(descriptionKeys);
		}
		@Override public final String getName(File f){return null;}
		@Override public final String getDescription(File f){return null;}
		@Override public final Boolean isTraversable(File f){return null;}
		@Override public final String getTypeDescription(File f){
			String type=null;
			String extension=getExtension(f);
			if(extension!=null){type=bundle.getString(descriptionKeys.get(extension.toUpperCase()));}
			return type;
		}
		@Override public final Icon getIcon(File f){
			if(f.isDirectory()){return folder;}
			Icon icon=null;
			String extension=getExtension(f);
			if(extension!=null){icon=loadImage(icons.get(extension.toUpperCase()));}
			return icon;
		}
		public final void setIcons(HashMap<String,String>icons){this.icons=icons;}
		public final void setDescriptionKeys(HashMap<String,String>descriptionKeys){this.descriptionKeys=descriptionKeys;}
		private void setBundle(ResourceBundle bundle){this.bundle=bundle;}
	}
	public static class MyFileFilter extends FileFilter{
		private ResourceBundle bundle;
		private String descriptionKey;
		private String extensions[];
		private boolean blocked;

		public MyFileFilter(String extensions[],String descriptionKey){
			this.extensions=extensions;
			this.descriptionKey=descriptionKey;
		}
		@Override public final String getDescription(){return bundle.getString(descriptionKey);}
		@Override public final boolean accept(File f){
			if(f.isDirectory()&&!blocked){return true;}
			for(String extension:extensions){if(fileHasExtensionAndDependants(f,extension,null)){return true;}}
			return false;
		}
		public final String[]getExtensions(){return extensions.clone();}
		private void block(boolean blocked){this.blocked=blocked;}
		private void setBundle(ResourceBundle bundle){this.bundle=bundle;}
	}
	public static class AllFileView extends FileView{
		private ImageIcon folder;
		private ImageIcon file;

		public AllFileView(){
			folder=loadImage("folIcon.png");
			file=loadImage("fileIcon.png");
		}
		@Override public final String getName(File f){return null;}
		@Override public final String getDescription(File f){return null;}
		@Override public final Boolean isTraversable(File f){return null;}
		@Override public final String getTypeDescription(File f){return null;}
		@Override public final Icon getIcon(File f){
			if(f.isDirectory()){return folder;}
			return file;
		}
	}
	public static class AllFileFilter extends FileFilter{
		//private boolean blocked;

		@Override public final String getDescription(){return null;}
		@Override public final boolean accept(File f){return true;}
		//private void block(boolean blocked){this.blocked=blocked;}
	}
	public static class MyFolderView extends FileView{
		private ResourceBundle bundle;
		private ImageIcon folder;

		public MyFolderView(){folder=loadImage("folIcon.png");}
		@Override public final String getName(File f){return null;}
		@Override public final String getDescription(File f){return null;}
		@Override public final Boolean isTraversable(File f){return null;}
		@Override public final String getTypeDescription(File f){
			if(f.isDirectory()){return bundle.getString("folderViewDescription");}
			return null;
		}
		@Override public final Icon getIcon(File f){
			if(f.isDirectory()){return folder;}
			return null;
		}
		private void setBundle(ResourceBundle bundle){this.bundle=bundle;}
	}
	public static class MyFolderFilter extends FileFilter{
		private ResourceBundle bundle;

		public MyFolderFilter(){}
		@Override public final String getDescription(){return bundle.getString("folderFilterDescription");}
		@Override public final boolean accept(File f){return f.isDirectory();}
		private void setBundle(ResourceBundle bundle){this.bundle=bundle;}
	}
	private class SingleRootFileSystemView extends FileSystemView{
		private File root;
		private File[]roots;

		public SingleRootFileSystemView(File root){
			super();
			this.root=root;
			roots=new File[]{root};
		}
		@Override public File createNewFolder(File containingDir){
			File folder=new File(containingDir,newFolderName);
			folder.mkdir();
			return folder;
		}
		@Override public File getDefaultDirectory(){return root;}
		@Override public File getHomeDirectory(){return root;}
		@Override public File[]getRoots(){return roots;}
	}
	public static final MyFileView datFileView;
	public static final MyFileFilter datFileFilter;
	public static final MyFileView textFileView;
	public static final MyFileFilter textFileFilter;
	public static final MyFileView imageFileView;
	public static final MyFileFilter imageFileFilter;
	public static final MyFileView dbfFileView;
	public static final MyFileFilter dbfFileFilter;
	public static final MyFileView wordFileView;
	public static final MyFileFilter wordFileFilter;
	public static final MyFileView excelFileView;
	public static final MyFileFilter excelFileFilter;
	public static final MyFileView pdfFileView;
	public static final MyFileFilter pdfFileFilter;
	public static final MyFileView xmlFileView;
	public static final MyFileFilter xmlFileFilter;
	public static final MyFileView videoFileView;
	public static final MyFileFilter videoFileFilter;
	public static final MyFileView awsFileView;
	public static final MyFileFilter awsFileFilter;
	public static final MyFileView regFileView;
	public static final MyFileFilter regFileFilter;
	public static final MyFileView esfFileView;
	public static final MyFileFilter esfFileFilter;
	static{
		HashMap<String,String>icons,descriptions;
		icons=new HashMap<String,String>();
		icons.put(dat,"txtIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put(dat,"datViewDescription");
		datFileView=new MyFileView(icons,descriptions);
		datFileFilter=new MyFileFilter(new String[]{dat},"datFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("TXT","txtIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("TXT","textViewDescription");
		textFileView=new MyFileView(icons,descriptions);
		textFileFilter=new MyFileFilter(new String[]{"TXT"},"textFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("PNG","pngIcon.png");
		icons.put("JPG","pngIcon.png");
		icons.put("JPEG","pngIcon.png");
		icons.put("BMP","pngIcon.png");
		icons.put("GIF","pngIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("PNG","pngViewDescription");
		descriptions.put("JPG","jpgViewDescription");
		descriptions.put("JPEG","jpgViewDescription");
		descriptions.put("BMP","bmpViewDescription");
		descriptions.put("GIF","gifViewDescription");
		imageFileView=new MyFileView(icons,descriptions);
		imageFileFilter=new MyFileFilter(new String[]{"PNG","JPG","JPEG","BMP","GIF"},"imageFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("DBF","dbIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("DBF","dbfViewDescription");
		dbfFileView=new MyFileView(icons,descriptions);
		dbfFileFilter=new MyFileFilter(new String[]{"DBF"},"dbfFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("DOCX","wordIcon.png");
		icons.put("DOC","wordIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("DOCX","wordViewDescription");
		descriptions.put("DOC","wordViewDescription");
		wordFileView=new MyFileView(icons,descriptions);
		wordFileFilter=new MyFileFilter(new String[]{"DOC","DOCX"},"wordFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("XLS","xlsIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("XLS","excelViewDescription");
		excelFileView=new MyFileView(icons,descriptions);
		excelFileFilter=new MyFileFilter(new String[]{"XLS"},"excelFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("PDF","pdfIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("PDF","pdfViewDescription");
		pdfFileView=new MyFileView(icons,descriptions);
		pdfFileFilter=new MyFileFilter(new String[]{"PDF"},"pdfFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("XML","txtIcon.png");
		icons.put("XDB","txtIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("XML","xmlViewDescription");
		descriptions.put("XDB","xdbViewDescription");
		xmlFileView=new MyFileView(icons,descriptions);
		xmlFileFilter=new MyFileFilter(new String[]{"XML","XDB"},"xmlFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("MP4","videoIcon.png");
		icons.put("MPG","videoIcon.png");
		icons.put("3GP","videoIcon.png");
		icons.put("AVI","videoIcon.png");
		icons.put("MKV","videoIcon.png");
		icons.put("FLV","videoIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("MP4","mp4ViewDescription");
		descriptions.put("MPG","mpgViewDescription");
		descriptions.put("3GP","3gpViewDescription");
		descriptions.put("AVI","aviViewDescription");
		descriptions.put("MKV","mkvViewDescription");
		descriptions.put("FLV","flvViewDescription");
		videoFileView=new MyFileView(icons,descriptions);
		videoFileFilter=new MyFileFilter(new String[]{"MP4","MPG","3GP","AVI","MKV","FLV"},"videoFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("AWS","txtIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("AWS","awsViewDescription");
		awsFileView=new MyFileView(icons,descriptions);
		awsFileFilter=new MyFileFilter(new String[]{"AWS"},"awsFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("REG","txtIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("REG","regViewDescription");
		regFileView=new MyFileView(icons,descriptions);
		regFileFilter=new MyFileFilter(new String[]{"REG"},"regFilterDescription");
		icons=new HashMap<String,String>();
		icons.put("ESF","txtIcon.png");
		descriptions=new HashMap<String,String>();
		descriptions.put("ESF","esfViewDescription");
		esfFileView=new MyFileView(icons,descriptions);
		esfFileFilter=new MyFileFilter(new String[]{"ESF"},"esfFilterDescription");
	}

	/*public MyFileChooser(){}
	public MyFileChooser(String initialDirectory){this(new AllFileFilter(),new AllFileView(),initialDirectory);}
	public MyFileChooser(boolean fileSelectionMode){}
	public MyFileChooser(boolean blocked,boolean fileSelectionMode){}*/
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView){this(fileFilter,fileView,getProgramFolder());}
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView,String initialDirectory){this(fileFilter,fileView,initialDirectory,true);}
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView,boolean fileSelectionMode){this(fileFilter,fileView,true,fileSelectionMode);}
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView,boolean blocked,boolean fileSelectionMode){
		this(fileFilter,fileView,blocked,getProgramFolder(),fileSelectionMode);
	}
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView,boolean blocked,String initialDirectory){
		this(fileFilter,fileView,blocked,initialDirectory,true);
	}
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView,String initialDirectory,boolean fileSelectionMode){
		this(fileFilter,fileView,true,initialDirectory,fileSelectionMode);
	}
	public MyFileChooser(MyFileFilter fileFilter,MyFileView fileView,boolean blocked,String initialDirectory,boolean fileSelectionMode){
		this.fileSelectionMode=fileSelectionMode;
		this.blocked=fileSelectionMode&&blocked;
		this.fileFilter=fileFilter;
		this.fileView=fileView;
		if(this.blocked){chooser=new JFileChooser(new SingleRootFileSystemView(new File(initialDirectory)));}
		else{chooser=new JFileChooser(initialDirectory);}
		getButtonsLabelsAndField(chooser);
		getPopupMenu();
		if(fileSelectionMode){
			setLocale(ENGLISH);
			chooser.setFileSelectionMode(FILES_ONLY);
			chooser.setFileFilter(fileFilter);
			chooser.setFileView(fileView);
		}
		else{
			chooser.setFileSelectionMode(DIRECTORIES_ONLY);
			this.fileFilter=null;
			this.fileView=null;
			folderFilter=new MyFolderFilter();
			folderView=new MyFolderView();
			setLocale(ENGLISH);
			chooser.setFileFilter(folderFilter);
			chooser.setFileView(folderView);
		}
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setCurrentDirectory(new File(initialDirectory));
		if(fileFilter!=null){fileFilter.block(this.blocked);}
		if(upFolderButton!=null){upFolderButton.setEnabled(!this.blocked);}
		if(newFolderButton!=null){newFolderButton.setEnabled(!this.blocked);}
		if(newFolderMenuItem!=null){newFolderMenuItem.setEnabled(!this.blocked);}
	}
	private void getButtonsLabelsAndField(Container c){
		int len=c.getComponentCount();
		for(int i=0;i<len;i++){
			Component component=c.getComponent(i);
			if(component instanceof JButton){
				JButton button=(JButton)component;
				Icon icon=button.getIcon();
				String text=button.getText();
				if(icon!=null&&icon==getIcon("FileChooser.upFolderIcon")){upFolderButton=button;}
				if(icon!=null&&icon==getIcon("FileChooser.homeFolderIcon")){homeFolderButton=button;}
				if(icon!=null&&icon==getIcon("FileChooser.newFolderIcon")){newFolderButton=button;}
				if(text!=null&&text.equals(get("FileChooser.cancelButtonText"))){cancelButton=button;}
			}
			else if(component instanceof JLabel){
				JLabel label=(JLabel)component;
				String text=label.getText();
				if(text!=null&&text.equals(get("FileChooser.lookInLabelText"))){lookInLabel=label;}
				if(text!=null&&text.equals(get("FileChooser.fileNameLabelText"))){fileNameLabel=label;}
				if(text!=null&&text.equals(get("FileChooser.filesOfTypeLabelText"))){filesOfTypeLabel=label;}
			}
			else if(component instanceof JToggleButton){
				JToggleButton toggle=(JToggleButton)component;
				Icon icon=toggle.getIcon();
				if(icon!=null&&icon==get("FileChooser.listViewIcon")){listViewButton=toggle;}
				if(icon!=null&&icon==get("FileChooser.detailsViewIcon")){detailsViewButton=toggle;}
			}
			else if(component instanceof JTextField){textField=(JTextField)component;}
			else if(component instanceof Container){getButtonsLabelsAndField((Container)component);}
		}
	}
	private void getPopupMenu(){
		JList list=getDescendantOfType(JList.class,chooser,"Enabled",new Boolean(true));
		if(list==null){return;}
		JPopupMenu popup=list.getComponentPopupMenu();
		if(popup==null||popup.getComponentCount()<1){return;}
		viewMenuItem=(JMenu)popup.getComponent(0);
		if(popup.getComponentCount()<2){return;}
		refreshMenuItem=(JMenuItem)popup.getComponent(1);
		if(popup.getComponentCount()<3){return;}
		newFolderMenuItem=(JMenuItem)popup.getComponent(2);
		if(viewMenuItem==null||viewMenuItem.getItemCount()<1){return;}
		listMenuItem=(JRadioButtonMenuItem)viewMenuItem.getItem(0);
		if(viewMenuItem.getItemCount()<2){return;}
		detailsMenuItem=(JRadioButtonMenuItem)viewMenuItem.getItem(1);
	}
	/*Just add temp support*/
	public boolean select(){return select(null);}
	public boolean select(Component parent){
		textField.setEditable(false);
		chooser.setDialogTitle(openTitle);
		chooser.setApproveButtonText(openButtonText);
		chooser.setApproveButtonToolTipText(openButtonToolTipText);
		selectloop:
		do{
			validInformation=false;
			int result=chooser.showOpenDialog(parent);
			if(result==CANCEL_OPTION||result==ERROR_OPTION){return false;}
			else{
				if(fileSelectionMode){
					File selectedFiles[]=chooser.getSelectedFiles();
					if(selectedFiles.length==0){selectedFiles=new File[1];selectedFiles[0]=chooser.getSelectedFile();}
					File files[]=new File[selectedFiles.length];
					int i=0;
					for(File selectedFile:selectedFiles){
						if(getExtension(selectedFile)!=null&&fileFilter.accept(selectedFile)){files[i]=selectedFile;}
						else{
							String fileName=getFileName(selectedFile);
							String extensions[]=fileFilter.getExtensions();
							ArrayList<String>array=new ArrayList<String>();
							for(String extension:extensions){
								File testFile=new File(chooser.getCurrentDirectory().getPath()+separatorChar+fileName+"."+extension);
								if(testFile.exists()&&testFile.isFile()){array.add(extension);}
							}
							if(!array.isEmpty()){
								if(!choose(selectExtensionMessage+'\n'+fileName,selectExtensionTitle,array.toArray())){break selectloop;}
								String extension=array.get(getInteger().intValue());
								files[i]=new File(chooser.getCurrentDirectory().getPath()+separatorChar+fileName+'.'+extension);
							}
							else{
								showMessage(notFoundMessage+'\n'+fileName,notFoundTitle);
								validInformation=false;
								if(choose(selectAgainMessage,selectAgainTitle,yesNo)&&getInteger().intValue()==0){continue selectloop;}
								else{break selectloop;}
							}
						}
						i++;
					}
					chooser.setSelectedFiles(files);
					chooser.setSelectedFile(files[0]);
				}
				File selectedFiles[]=chooser.getSelectedFiles();
				if(selectedFiles.length==0){
					selectedFiles=new File[1];
					selectedFiles[0]=chooser.getSelectedFile();
					chooser.setSelectedFiles(selectedFiles);
					chooser.setSelectedFile(selectedFiles[0]);
				}
				for(File selectedFile:selectedFiles){if(!selectedFile.exists()){
					showMessage(notFoundMessage+'\n'+getFileName(selectedFile),notFoundTitle);
					validInformation=false;
					if(choose(selectAgainMessage,selectAgainTitle,yesNo)&&getInteger().intValue()==0){continue selectloop;}
					else{break selectloop;}
				}}
				validInformation=true;
				break;
			}
		}while(true);
		return validInformation;
	}
	public boolean create(){return create(null);}
	public boolean create(Component parent){
		textField.setEditable(true);
		chooser.setDialogTitle(saveTitle);
		chooser.setApproveButtonText(saveButtonText);
		chooser.setApproveButtonToolTipText(saveButtonToolTipText);
		createloop:
			do{
				validInformation=false;
				int result=chooser.showOpenDialog(parent);
				if(result==CANCEL_OPTION||result==ERROR_OPTION){return false;}
				else{
					if(fileSelectionMode){
						File selectedFiles[]=chooser.getSelectedFiles();
						if(selectedFiles.length==0){selectedFiles=new File[1];selectedFiles[0]=chooser.getSelectedFile();}
						File files[]=new File[selectedFiles.length];
						int i=0;
						for(File selectedFile:selectedFiles){
							if(getExtension(selectedFile)!=null&&fileFilter.accept(selectedFile)){files[i]=selectedFile;}
							else{
								String fileName=getFileName(selectedFile);
								String extensions[]=fileFilter.getExtensions();
								if(!choose(selectExtensionMessage+'\n'+fileName,selectExtensionTitle,extensions)){break createloop;}
								String extension=extensions[getInteger().intValue()];
								files[i]=new File(chooser.getCurrentDirectory().getPath()+separatorChar+fileName+'.'+extension);
							}
							i++;
						}
						chooser.setSelectedFiles(files);
						chooser.setSelectedFile(files[0]);
					}
					File selectedFiles[]=chooser.getSelectedFiles();
					if(selectedFiles.length==0){
						selectedFiles=new File[1];
						selectedFiles[0]=chooser.getSelectedFile();
						chooser.setSelectedFiles(selectedFiles);
						chooser.setSelectedFile(selectedFiles[0]);
					}
					for(File selectedFile:selectedFiles){
						if(selectedFile.exists()){
							if(choose(overwriteMessage+'\n'+getFileName(selectedFile),overwriteTitle,yesNo)&&getInteger().intValue()==0){selectedFile.delete();}
							else{
								validInformation=false;
								if(choose(selectAgainMessage,selectAgainTitle,yesNo)&&getInteger().intValue()==0){continue createloop;}
								else{break createloop;}
							}
						}
						try{selectedFile.createNewFile();}
						catch(IOException e){/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/}
					}
					validInformation=true;
					break;
				}
			}while(true);
			return validInformation;
	}

	/*private File seleccionar(String mensaje){
		int a=JFileChooser.CANCEL_OPTION;
		do{
			a=chooser.showDialog(null,mensaje);
			if(a!=JFileChooser.APPROVE_OPTION){showMessage("No seleccionaste nada","Seleccionar archivo");}
		}while(a!=JFileChooser.APPROVE_OPTION);
		return chooser.getSelectedFile();
	}
	private File guardar(){
		int a=JFileChooser.CANCEL_OPTION;
		do{
			a=chooser.showSaveDialog(null);
			if(a!=JFileChooser.APPROVE_OPTION){showMessage("No seleccionaste nada","Seleccionar archivo");}
		}while(a!=JFileChooser.APPROVE_OPTION);
		return chooser.getSelectedFile();
	}
	public String cargar(String extension){
		File file=seleccionar("Cargar");
		if(getExtension(file)==null){file=new File(file.getPath()+"."+extension);}
		if(file.exists()){new TempDirStream(extension).write(file.getAbsolutePath());return getFileName(file);}
		else{MyOptionPane.showMessage("El archivo no existe","Seleccionar archivo");return null;}
	}
	public String crear(String extension,String extensiones[]){
		File file=guardar();
		if(getExtension(file)==null){file=new File(file.getPath()+"."+extension);}
		File archivos[]=null;
		if(extensiones!=null){
			archivos=new File[extensiones.length];
			for(int i=0;i<extensiones.length;i++){
				archivos[i]=new File("parentDirectory"+"/"+getFileName(file)+"."+extensiones[i]);
			}
		}
		if(file.exists()){
			if(choose("Deseas sobreescribir los datos","Sobreescribir",yesNo)&&getInteger().intValue()==0){
				file.delete();
				if(extensiones!=null){for(int i=0;i<extensiones.length;i++){if(archivos!=null){archivos[i].delete();}}}
			}
			else{return null;}
		}
		try{
			file.createNewFile();
			if(extensiones!=null){for(int i=0;i<extensiones.length;i++){archivos[i].createNewFile();}}
		}
		catch(IOException e){--ErrorConsole--myapi.ErrorConsole.addText(e);--ErrorConsole--}
		new TempDirStream(extension).write(file.getAbsolutePath());
		return getFileName(file);
	}*/

	public void setCurrentDirectory(String directoryPath){setCurrentDirectory(new File(directoryPath));}
	public void setCurrentDirectory(File directory){if(!blocked){chooser.setCurrentDirectory(directory);}}
	public void setMultiSelectionEnabled(boolean enable){chooser.setMultiSelectionEnabled(enable);}
	public File getSelectedFile(){return chooser.getSelectedFile();}
	public File[]getSelectedFiles(){return chooser.getSelectedFiles();}
	public void setLocale(Locale locale){
		ResourceBundle bundle=getFilesResourceBundle("MyFileChooser",locale);
		selectExtensionMessage=bundle.getString("selectExtensionMessage");
		selectExtensionTitle=bundle.getString("selectExtensionTitle");
		notFoundMessage=bundle.getString("notFoundMessage");
		notFoundTitle=bundle.getString("notFoundTitle");
		overwriteMessage=bundle.getString("overwriteMessage");
		overwriteTitle=bundle.getString("overwriteTitle");
		selectAgainMessage=bundle.getString("selectAgainMessage");
		selectAgainTitle=bundle.getString("selectAgainTitle");
		newFolderName=bundle.getString("newFolderName");
		openTitle=bundle.getString("openTitle");
		openButtonText=bundle.getString("openButtonText");
		openButtonToolTipText=bundle.getString("openButtonToolTipText");
		saveTitle=bundle.getString("saveTitle");
		saveButtonText=bundle.getString("saveButtonText");
		saveButtonToolTipText=bundle.getString("saveButtonToolTipText");
		if(lookInLabel!=null){lookInLabel.setText(bundle.getString("lookInLabelText"));}
		if(fileNameLabel!=null){fileNameLabel.setText(bundle.getString("fileNameLabelText"));}
		if(filesOfTypeLabel!=null){filesOfTypeLabel.setText(bundle.getString("filesOfTypeLabelText"));}
		if(upFolderButton!=null){upFolderButton.setToolTipText(bundle.getString("upFolderToolTipText"));}
		if(homeFolderButton!=null){homeFolderButton.setToolTipText(bundle.getString("homeFolderToolTipText"));}
		if(newFolderButton!=null){newFolderButton.setToolTipText(bundle.getString("newFolderToolTipText"));}
		if(listViewButton!=null){listViewButton.setToolTipText(bundle.getString("listViewButtonToolTipTextlist"));}
		if(detailsViewButton!=null){detailsViewButton.setToolTipText(bundle.getString("detailsViewButtonToolTipText"));}
		if(cancelButton!=null){
			cancelButton.setText(bundle.getString("cancelButtonText"));
			cancelButton.setToolTipText(bundle.getString("cancelButtonToolTipText"));
		}
		if(viewMenuItem!=null){viewMenuItem.setText(bundle.getString("viewMenuItemText"));}
		if(refreshMenuItem!=null){refreshMenuItem.setText(bundle.getString("refreshMenuItemText"));}
		if(newFolderMenuItem!=null){newFolderMenuItem.setText(bundle.getString("newFolderMenuItemText"));}
		if(listMenuItem!=null){listMenuItem.setText(bundle.getString("listMenuItemText"));}
		if(detailsMenuItem!=null){detailsMenuItem.setText(bundle.getString("detailsMenuItemText"));}
		if(fileFilter!=null){fileFilter.setBundle(bundle);}
		if(fileView!=null){fileView.setBundle(bundle);}
		if(folderFilter!=null){folderFilter.setBundle(bundle);}
		if(folderView!=null){folderView.setBundle(bundle);}
	}
	public void setFont(Font font){setFileChooserFont(chooser,font);}
}