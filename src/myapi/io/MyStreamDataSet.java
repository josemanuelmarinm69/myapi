package myapi.io;
public class MyStreamDataSet<T,U,V>{
	private T primero;
	private U segundo;
	private V tercero;

	public MyStreamDataSet(){}
	public MyStreamDataSet(T primero,U segundo,V tercero){this.primero=primero;this.segundo=segundo;this.tercero=tercero;}
	public T getPrimero(){return primero;}
	public void setPrimero(T primero){this.primero=primero;}
	public U getSegundo(){return segundo;}
	public void setSegundo(U segundo){this.segundo=segundo;}
	public V getTercero(){return tercero;}
	public void setTercero(V tercero){this.tercero=tercero;}
}