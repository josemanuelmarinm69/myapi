package myapi.io;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import myapi.MyOptionPane;
import myapi.generics.MySimpleNodeStructure;
public class MyStream<T extends MySimpleNodeStructure<?>,U extends MySimpleNodeStructure<?>,V extends MySimpleNodeStructure<?>>{
	public static class MyObjectReader extends ObjectInputStream{
		public MyObjectReader(FileInputStream a)throws StreamCorruptedException,IOException{super(a);}
		@Override protected void readStreamHeader(){}
	}
	public static class MyObjectWriter extends ObjectOutputStream{
		public MyObjectWriter(FileOutputStream a)throws IOException{super(a);}
		@Override protected void writeStreamHeader()throws IOException{reset();}
	}
	private String path;

	public MyStream(String path){setPath(path);}
	public String getPath(){return path;}
	private void setPath(String path){this.path=path;}
	public MyStreamDataSet<T,U,V> crearDatos(T primero,U segundo,V tercero){
		MyStreamDataSet<T,U,V> base=new MyStreamDataSet<T,U,V>();
		String cadena="Se crear\u00E1 una nueva estructura de ";
		if(primero==null){cadena+="\nEquipos";}
		base.setPrimero(primero);
		if(segundo==null){cadena+="\nCompeticiones";}
		base.setSegundo(segundo);
		if(tercero==null){cadena+="\nPartidos";}
		base.setTercero(tercero);
		if(cadena!="Se crear\u00E1 una nueva estructura de "){MyOptionPane.showMessage(cadena,"Datos creados");}
		return base;
	}
	@SuppressWarnings("unchecked")
	public MyStreamDataSet<T,U,V> cargarDatos(MyStreamDataSet<T,U,V> base,boolean preguntar){
		T primero=null;
		U segundo=null;
		V tercero=null;
		if(myapi.utils.OfFile.isValidFilePath(path)){
			java.util.ArrayList<Object> array=new java.util.ArrayList<Object>();
			try{
				MyObjectReader lector=new MyObjectReader(new FileInputStream(path));
				Object leido=lector.readObject();
				while(leido!=null){array.add(leido);leido=lector.readObject();}
				lector.close();
			}
			catch(ClassCastException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			}
			catch(ClassNotFoundException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			}
			catch(EOFException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			}
			catch(NullPointerException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			}
			catch(IOException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
			}
			for(Object objeto:array){
				if(objeto.getClass().equals(base.getPrimero().getClass())
						&&((MySimpleNodeStructure<?>)objeto).getRoot().getData().getClass().equals(base.getPrimero().getRoot().getData().getClass())){
					primero=(T)objeto;
				}
				if(objeto.getClass().equals(base.getSegundo().getClass())
						&&((MySimpleNodeStructure<?>)objeto).getRoot().getData().getClass().equals(base.getSegundo().getRoot().getData().getClass())){
					segundo=(U)objeto;
				}
				if(objeto.getClass().equals(base.getTercero().getClass())
						&&((MySimpleNodeStructure<?>)objeto).getRoot().getData().getClass().equals(base.getTercero().getRoot().getData().getClass())){
					tercero=(V)objeto;
				}
			}
			if(preguntar){
				String selecciones[]={"Equipos","Competiciones","Partidos"};
				if(primero==null){selecciones[0]=null;}
				if(segundo==null){selecciones[1]=null;}
				if(primero==null&&segundo==null){selecciones[2]=null;}
				if(primero==null&&segundo==null&&tercero==null){MyOptionPane.showMessage("No existen datos en el fichero especificado","No hay datos");}
				else{
					if(!MyOptionPane.select("Deseas cargar los registros de una sesi\u00F3n anterior","\u00BFCargar datos?",selecciones)){return null;}
					Boolean resultado[]=MyOptionPane.getSelections();
					if(!resultado[0].booleanValue()){primero=null;}
					if(!resultado[1].booleanValue()){segundo=null;}
					if(!resultado[2].booleanValue()){tercero=null;}
				}
			}
		}
		return crearDatos(primero,segundo,tercero);
	}
	public void guardarDatos(T primero,U segundo,V tercero){
		try{
			MyObjectWriter escritor=new MyObjectWriter(new FileOutputStream(path));
			String cadena="Guard\u00F3 los datos de";
			if(primero!=null){escritor.writeObject(primero);cadena+="\nEquipos";}
			if(segundo!=null){escritor.writeObject(segundo);cadena+="\nCompetencias";}
			if(tercero!=null){escritor.writeObject(tercero);cadena+="\nPartidos";}
			escritor.close();
			MyOptionPane.showMessage(cadena,"Datos guardados");
		}
		catch(IOException e){
			MyOptionPane.showMessage("Error en "+e.getMessage()+"\nNo se guardaron los datos","Error al guardar");
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
	}
	public boolean existenDatos(MyStreamDataSet<T,U,V> base){
		try{
			MyObjectReader lector=new MyObjectReader(new FileInputStream(path));
			Object objeto=lector.readObject();
			while(objeto!=null){
				if(objeto.getClass().equals(base.getPrimero().getClass())||
						objeto.getClass().equals(base.getSegundo().getClass())||
						objeto.getClass().equals(base.getTercero().getClass())){
					lector.close();
					return true;
				}
				objeto=lector.readObject();
			}
			lector.close();
		}
		catch(ClassNotFoundException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
		catch(EOFException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
		catch(IOException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
		}
		return false;
	}
}