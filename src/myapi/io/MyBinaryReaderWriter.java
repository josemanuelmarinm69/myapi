package myapi.io;
import static java.nio.ByteBuffer.wrap;
import static java.nio.ByteOrder.LITTLE_ENDIAN;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
/**@Types
 * <b>byte</b> 8-bit<br>
 * <b>short</b> 16-bit<br>
 * <b>int</b> 32-bit<br>
 * <b>long</b> 64-bit<br>
 * <b>float</b> 32-bit<br>
 * <b>double</b> 64-bit<br>
 * <b>boolean</b> 1-bit<br>
 * <b>char</b> 16-bit (the number represents the binary conversion of the hex unicode character)*/
public class MyBinaryReaderWriter{
	private RandomAccessFile stream;

	public MyBinaryReaderWriter(String filePath)throws FileNotFoundException{stream=new RandomAccessFile(filePath,"rw");}
	/**Close the stream for the file*/
	public void close()throws IOException{stream.close();}
	/**Return the file pointer position as a long*/
	public long position()throws IOException{return stream.getFilePointer();}
	/**Return the file size in bytes as a long*/
	public long length()throws IOException{return stream.length();}
	/**Move the file pointer to <i>position</i>*/
	public void seek(long position)throws IOException{stream.seek(position);}
	/**Read <i>lenght</i> bytes into a byte array and return it*/
	public byte[]readBytes(int length)throws IOException{
		byte bytes[]=new byte[length];
		stream.read(bytes);
		return bytes;
	}
	/**Read all the bytes in the stream from the beginning*/
	public byte[]readAllBytes()throws IOException{
		stream.seek(0);
		return readBytes((int)stream.length());
	}
	/**Read 16-bit character*/
	public char readChar()throws IOException{return wrap(readBytes(2)).order(LITTLE_ENDIAN).getChar();}
	/**Read a 32-bit signed point value*/
	public float readFloat()throws IOException{return wrap(readBytes(4)).order(LITTLE_ENDIAN).getFloat();}
	/**Read a 64-bit signed point value*/
	public double readDouble()throws IOException{return wrap(readBytes(8)).order(LITTLE_ENDIAN).getDouble();}
	/**Read a 8-bit signed int value*/
	public byte readInt8()throws IOException{return wrap(readBytes(1)).order(LITTLE_ENDIAN).get();}
	/**Read a 16-bit signed int value*/
	public short readInt16()throws IOException{return wrap(readBytes(2)).order(LITTLE_ENDIAN).getShort();}
	/**Read a 32-bit signed int value*/
	public int readInt32()throws IOException{return wrap(readBytes(4)).order(LITTLE_ENDIAN).getInt();}
	/**Read a 64-bit signed int value*/
	public long readInt64()throws IOException{return wrap(readBytes(8)).order(LITTLE_ENDIAN).getLong();}
	/**Read a 8-bit unsigned int value (it requires 16-bit store space)*/
	public short readUInt8()throws IOException{return(short)(readInt8()&0xFF);}
	/**Read a 16-bit unsigned int value (it requires 32-bit store space)*/
	public int readUInt16()throws IOException{return readInt16()&0xFFFF;}
	/**Read a 32-bit unsigned int value (it requires 64-bit store space)*/
	public long readUInt32()throws IOException{return readInt32()&0xFFFFFFFFL;}
	/**Read 1-bit boolean value from a byte*/
	public boolean readBoolean()throws IOException{return stream.readBoolean();}
	/**Same as readUInt8*/
	public short readByte()throws IOException{return readUInt8();}
	/**Same as readInt16*/
	public short readShort()throws IOException{return readInt16();}
	/**Same as readInt32*/
	public int readInt()throws IOException{return readInt32();}
	/**Same as readInt64*/
	public long readLong()throws IOException{return readInt64();}
	/**Same as readInt8*/
	public byte readSByte()throws IOException{return readInt8();}
	/**Same as readFloat*/
	public float readSingle()throws IOException {return readFloat();}
	/**Read a <i>length</i> ASCII decoded string*/
	public String readAsciiString(int length)throws IOException{return new String(readBytes(length),"US_ASCII");}
	/**Read a <i>length</i> UTF16 decoded string*/
	public String readUtf16String(int length)throws IOException{return new String(readBytes(length*2),"UTF_16LE");}
}