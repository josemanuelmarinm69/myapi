package myapi.io;
import static java.awt.dnd.DnDConstants.ACTION_COPY;
import static java.awt.event.ActionEvent.ACTION_PERFORMED;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.AbstractAction;
public class MyFileDragDropListener implements DropTargetListener{
	public static final long serialVersionUID=3718524415251248459L;
	private AbstractAction action;
	private List<File>list;

	public MyFileDragDropListener(AbstractAction action){this.action=action;}
	/*public boolean canImport(TransferSupport support){
		if(!support.isDataFlavorSupported(javaFileListFlavor)){return false;}
		if(!((COPY&support.getSourceDropActions())==COPY)){return false;}
		support.setDropAction(COPY);
		return true;
	}
	@SuppressWarnings("unchecked")
	public boolean importData(TransferSupport support){
		if(!canImport(support)){return false;}
		Transferable transferable=support.getTransferable();
		try{
			list=(List<File>)transferable.getTransferData(javaFileListFlavor);
			action.actionPerformed(new ActionEvent(this,ACTION_PERFORMED,"file list transferred"));
			return true;
		}
		catch(UnsupportedFlavorException e){myapi.ErrorConsole.addText(e);}
		catch(IOException e){myapi.ErrorConsole.addText(e);}
		return false;
	}*/
	public void dragEnter(DropTargetDragEvent e){}
	public void dragExit(DropTargetEvent e){}
	public void dragOver(DropTargetDragEvent e){}
	@SuppressWarnings("unchecked")
	public void drop(DropTargetDropEvent e){
		e.acceptDrop(ACTION_COPY);
		Transferable transferable=e.getTransferable();
		DataFlavor flavors[]=e.getCurrentDataFlavors();
		for(DataFlavor flavor:flavors){
			try{
				if(flavor.isFlavorJavaFileListType()){
					list=(List<File>)transferable.getTransferData(flavor);
				}
			}
			/*--ErrorConsole--*/catch(UnsupportedFlavorException ex){myapi.ErrorConsole.addText(ex);}/*--ErrorConsole--*/
			/*--ErrorConsole--*/catch(IOException ex){myapi.ErrorConsole.addText(ex);}/*--ErrorConsole--*/
		}
		e.dropComplete(true);
		action.actionPerformed(new ActionEvent(this,ACTION_PERFORMED,"file list transferred"));
	}
	public void dropActionChanged(DropTargetDragEvent e){}
	public List<File>getList(){return list;}
}