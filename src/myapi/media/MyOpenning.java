package myapi.media;
import static java.awt.Font.BOLD;
import static java.lang.Thread.sleep;
import static myapi.MyFontHelper.loadFont;
import static myapi.utils.OfEvent.addResizableContainerListener;
import static myapi.utils.OfGUI.getTextWidth;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyOpenning extends JFrame{
	private static final long serialVersionUID=1772177261346605021L;

	public MyOpenning(ImageIcon imagen){this(imagen,2);}
	public MyOpenning(ImageIcon imagen,int sleep){this(null,imagen,"Bienvenid\u0040",sleep);}
	public MyOpenning(String titulo,ImageIcon imagen,String texto){this(null,titulo,imagen,texto,2);}
	public MyOpenning(String titulo,ImageIcon imagen,String texto,int sleep){this(null,titulo,imagen,texto,sleep);}
	public MyOpenning(Image icono,String titulo,final ImageIcon imagen,final String texto,int sleep){
		setUndecorated(true);
		if(titulo!=null){setTitle(titulo);}
		if(icono!=null){setIconImage(icono);}
		setContentPane(new JPanel(){
			private static final long serialVersionUID=4057766141097067690L;

			{
				setLayout(null);
				setOpaque(false);
				addResizableContainerListener(this);
				Font fuente=loadFont("Love Letter.ttf",BOLD,35);
				JLabel label=new JLabel(texto);
				label.setFont(fuente);
				label.setForeground(new Color(137,197,75));
				label.setBounds(0,0,getTextWidth(label),label.getPreferredSize().height);
				add(label);
				JLabel icono=new JLabel(imagen);
				icono.setBounds(0,label.getHeight()+1,icono.getPreferredSize().width,icono.getPreferredSize().height);
				add(icono);
			}
		});
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		try{sleep(sleep*1000);}
		/*--ErrorConsole--*/catch(InterruptedException e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
		this.dispose();
	}
}