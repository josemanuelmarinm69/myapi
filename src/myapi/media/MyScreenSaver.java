package myapi.media;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.JFrame;
public class MyScreenSaver extends JFrame{
	private static final long serialVersionUID=-5502761975394024380L;
	private Lienzo lienzo;
	private Random r;
	private int x;
	private int y;
	private int rojo;
	private int verde;
	private int azul;

	public MyScreenSaver(){this(310,310);}
	public MyScreenSaver(java.awt.Dimension tamano){this(tamano.width,tamano.height);}
	public MyScreenSaver(int ancho,int alto){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Lienzo");
		setLocation(100,100);
		setSize(ancho,alto);
		r=new Random();
		lienzo=new Lienzo(0,0,getSize());
		getContentPane().setLayout(null);
		getContentPane().add(lienzo);
		setResizable(false);
		setVisible(true);
		Thread hilo1=new Thread(new Runnable(){
			public void run(){horizontal();}
		});
		Thread hilo2=new Thread(new Runnable(){
			public void run(){vertical();}
		});
		Thread hilo3=new Thread(new Runnable(){
			public void run(){
				while(true){
					try{Thread.sleep(40);lienzo.repaint();}
					catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
					}
				}
			}
		});
		Thread hilo4=new Thread(new Runnable(){
			public void run(){
				while(true){
					lienzo.setColor(new Color(rojo,verde,azul));
					try{Thread.sleep(80);}
					catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
					}
				}
			}
		});
		Thread hilo5=new Thread(new Runnable(){
			public void run(){color();}
		});
		hilo1.start();
		hilo2.start();
		hilo3.start();
		hilo4.start();
		hilo5.start();
	}
	public void horizontal(){
		while(true){
			int rate=r.nextInt(8)+2;
			while(x+50+rate<getWidth()){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				x+=rate;
				lienzo.setX(x);
			}
			rate=r.nextInt(8)+2;
			while(x-rate>0){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				x-=rate;
				lienzo.setX(x);
			}
		}
	}
	public void vertical(){
		while(true){
			int rate=r.nextInt(8)+2;
			while(y+50+rate<getHeight()-20){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				y+=rate;
				lienzo.setY(y);
			}
			rate=r.nextInt(8)+2;
			while(y-rate>0){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				y-=rate;
				lienzo.setY(y);
			}
		}
	}
	public void color(){
		while(true){
			rojo=r.nextInt(255);
			while(rojo<255){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				rojo++;
			}
			while(rojo>19){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				rojo--;
			}
			verde=r.nextInt(255);
			while(verde<255){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				verde++;
			}
			while(verde>19){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				verde--;
			}
			azul=r.nextInt(255);
			while(azul<255){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				azul++;
			}
			while(azul>19){
				try{Thread.sleep(40);}
				catch(InterruptedException e){
/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
				}
				azul--;
			}
		}
	}
}
class Lienzo extends Canvas{
	private static final long serialVersionUID=7980434631804854331L;
	private int x;
	private int y;
	private Color color;

	public Lienzo(java.awt.Point punto,java.awt.Dimension tamano){this(punto.x,punto.y,tamano.width,tamano.height);}
	public Lienzo(int x,int y,java.awt.Dimension tamano){this(x,y,tamano.width,tamano.height);}
	public Lienzo(java.awt.Point punto,int ancho,int alto){this(punto.x,punto.y,ancho,alto);}
	public Lienzo(int x,int y,int ancho,int alto){setBounds(x,y,ancho,alto);}
	@Override public void paint(Graphics g){update(g);}
	@Override public void update(Graphics g){
		java.awt.Image i=createImage(getWidth(),getHeight());
		Graphics gra=i.getGraphics();
		gra.setColor(getBackground());
		gra.drawRect(0,0,getWidth(),getHeight());
		gra.setColor(color);
		gra.fillRoundRect(x,y,50,50,49,49);
		g.drawImage(i,0,0,this);
	}
	public void setX(int x){this.x=x;}
	public void setY(int y){this.y=y;}
	public void setColor(Color color){this.color=color;}
}