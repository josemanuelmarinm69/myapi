//*--ErrorConsole--*/myapi.ErrorConsole.addText("");/*--ErrorConsole--*/
package myapi;
import static myapi.utils.OfOS.setNewErrorStream;
import static myapi.utils.OfData.isEmptyString;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import myapi.gui.MyConsole;
public class ErrorConsole{
	private static MyConsole console;
	private static/*final*/MyErrorConsoleStream stream;
	private static final class MyErrorConsoleStream extends PrintStream{
		private ArrayList<String>list;
		private String current;

		private MyErrorConsoleStream(){
			super(setNewErrorStream());
			list=new ArrayList<String>();
		}
		@Override public void print(String text){
			if(text==null||isEmptyString(text)){return;}
			if(text.startsWith("\\n\\t")){current+=text;}
			else{
				if(current!=null&&!isEmptyString(current)){list.add(current);}
				current="";
			}
			super.print(text);
		}
		@Override public void print(Object object){print(object.toString());}
	}

	static{
		console=new MyConsole("Errores",true);
		//stream=new MyErrorConsoleStream();
	}
	public static void setVisible(boolean visible){if(console!=null){console.setVisible(visible);}}
	public static void addText(String text){
		if(console!=null){console.addText(text);}
		if(stream!=null){stream.print(text);}
	}
	public static void addText(Object object){addText(object.toString());}
	public static void addText(StackTraceElement stack[]){
		String output="";
		for(int i=0;i<stack.length;i++){output+="\n"+stack[i].toString();}
		addText(output);
	}
	public static void addText(Iterable<?>array){String output="";for(Object object:array){output+="\n"+object;}addText(output);}
	public static void addText(Object array[]){String output="";for(Object object:array){output+="\n"+object;}addText(output);}
	public static void addText(Object array[][]){
		String output="";
		for(Object objects[]:array){output+="\n";for(Object object:objects){output+="\t"+object;}}
		addText(output);
	}
	public static void addText(Throwable e){
		if(e instanceof InvocationTargetException){addText(((InvocationTargetException)e).getTargetException());}
		String output="";
		if(e.getMessage()!=null){output+=e.getMessage()+"\n";}
		output+=e.getClass().toString();
		StackTraceElement stack[]=e.getStackTrace();
		for(int i=0;i<stack.length;i++){output+="\n"+stack[i].toString();}
		addText(output);
	}
	public static void addText(SQLException e){
		String output="";
		if(e.getSQLState()!=null){output+=e.getSQLState()+"\n";}
		if(e.getMessage()!=null){output+=e.getMessage()+"\n";}
		output+=e.getClass().toString();
		StackTraceElement stack[]=e.getStackTrace();
		for(int i=0;i<stack.length;i++){output+="\n"+stack[i].toString();}
		addText(output);
	}
}