package myapi.net;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
public abstract class MyServer{
	private int port;
	private ServerSocket serverSocket;
	private class ClientThread extends Thread{
		private Socket clientSocket;

		public ClientThread(Socket clientSocket){this.clientSocket=clientSocket;}
		@Override public void run(){
			BufferedReader reader;
			DataOutputStream output;
			try{
				reader=new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				output=new DataOutputStream(clientSocket.getOutputStream());
				/*--MessageConsole--*/myapi.MessageConsole.addText("Dispatching client");/*--MessageConsole--*/
				dispatchClients(reader,output);
				/*--MessageConsole--*/myapi.MessageConsole.addText("Finished with client");/*--MessageConsole--*/
				reader.close();
				output.close();
				clientSocket.close();
			}
			/*--ErrorConsole--*/catch(IOException e){myapi.ErrorConsole.addText(e);return;}/*--ErrorConsole--*/
		}
	}

	public MyServer(int port){this.port=port;}
	public final synchronized void startup()throws IOException{
		if(serverSocket!=null&&!serverSocket.isClosed()){return;}
		try{serverSocket=new ServerSocket(port);}
		/*--ErrorConsole--*/catch(IOException e){myapi.ErrorConsole.addText(e);throw e;}/*--ErrorConsole--*/
		new Thread(){
			@Override public void run(){
				while(true){
					/*--MessageConsole--*/myapi.MessageConsole.addText("Server waiting for clients");/*--MessageConsole--*/
					Socket clientSocket;
					try{clientSocket=serverSocket.accept();}
					catch(IOException e){
						if("socket closed".equalsIgnoreCase(e.getMessage())||serverSocket.isClosed()){
							/*--MessageConsole--*/myapi.MessageConsole.addText("Socket server closed");/*--MessageConsole--*/
							break;
						}
						/*--ErrorConsole--*/myapi.ErrorConsole.addText(e);/*--ErrorConsole--*/
						continue;
					}
					new ClientThread(clientSocket).start();
					/*--MessageConsole--*/myapi.MessageConsole.addText("Client thread has been started");/*--MessageConsole--*/
				}
				/*--MessageConsole--*/myapi.MessageConsole.addText("Server thread has been finished");/*--MessageConsole--*/
			}
		}.start();
	}
	public final synchronized void shutdown()throws IOException{
		if(serverSocket.isClosed()){return;}
		try{serverSocket.close();}
		/*--ErrorConsole--*/catch(IOException e){myapi.ErrorConsole.addText(e);throw e;}/*--ErrorConsole--*/
	}
	protected abstract void dispatchClients(BufferedReader reader,DataOutputStream output)throws IOException;
}