package myapi.net;
import static java.net.InetAddress.getLocalHost;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
public abstract class MyClient{
	public Socket clientSocket;

	public MyClient(){}
	public void connect(int port,int timeoutMillis)throws IOException{connect(getLocalHost().getCanonicalHostName(),port,timeoutMillis);}
	public void connect(String hostName,int port,int timeoutMillis)throws IOException{connect(new InetSocketAddress(hostName,port),timeoutMillis);}
	public final synchronized void connect(InetSocketAddress address,int timeoutMillis)throws IOException{
		BufferedReader reader;
		DataOutputStream output;
		clientSocket=new Socket();
		try{
			clientSocket.connect(address,timeoutMillis);
			reader=new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			output=new DataOutputStream(clientSocket.getOutputStream());
			/*--MessageConsole--*/myapi.MessageConsole.addText("Client sending requests");/*--MessageConsole--*/
			sendRequests(reader,output);
			/*--MessageConsole--*/myapi.MessageConsole.addText("All client requests have been send");/*--MessageConsole--*/
			reader.close();
			output.close();
			clientSocket.close();
		}
		/*--ErrorConsole--*/catch(IOException e){myapi.ErrorConsole.addText(e);throw e;}/*--ErrorConsole--*/
	}
	protected abstract void sendRequests(BufferedReader reader,DataOutputStream output)throws IOException;
}