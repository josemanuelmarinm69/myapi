package myapi.net;
public class MyArduinoMonitoringGUI{
	private static int humKey;
	private static int tempKey;
	private static int distKey;
	private static int movKey;
	private static myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Float>>humData;
	private static myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Float>>tempData;
	private static myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Integer>>distData;
	private static myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Boolean>>movData;
	private static org.jfree.data.xy.DefaultXYZDataset distDataset;
	private static org.jfree.data.category.DefaultCategoryDataset humDataset;
	private static org.jfree.data.category.DefaultCategoryDataset tempDataset;
	private static org.jfree.data.xy.XYSeries serie;
	private static java.awt.Color humRenderColor;
	private static java.awt.Color tempRenderColor;
	private static java.awt.Color distRenderColor;
	private static java.awt.Color movRenderColor;
	private static java.io.BufferedReader reader;
	private static java.lang.Thread read;
	private static myapi.gui.panels.MyBorderLayoutPanel panel;

	private static void arduinoTest()throws java.lang.Exception{
		java.util.ResourceBundle bundle=myapi.utils.OfFile.getFilesResourceBundle("arduino",java.util.Locale.getDefault());
		java.lang.String portName=bundle.getString("port");
		int limit=java.lang.Integer.parseInt(bundle.getString("dataLimit"));
		boolean simulate="true".equalsIgnoreCase(bundle.getString("simulate"));
		final float ratio=java.lang.Float.parseFloat(bundle.getString("refreshRatio"));
		String colorS1[]=bundle.getString("humRenderColor").split(","),colorS2[]=bundle.getString("tempRenderColor").split(","),
				colorS3[]=bundle.getString("distRenderColor").split(","),colorS4[]=bundle.getString("movRenderColor").split(",");
		int color1[]={java.lang.Integer.parseInt(colorS1[0]),java.lang.Integer.parseInt(colorS1[1]),java.lang.Integer.parseInt(colorS1[2])};
		int color2[]={java.lang.Integer.parseInt(colorS2[0]),java.lang.Integer.parseInt(colorS2[1]),java.lang.Integer.parseInt(colorS2[2])};
		int color3[]={java.lang.Integer.parseInt(colorS3[0]),java.lang.Integer.parseInt(colorS3[1]),java.lang.Integer.parseInt(colorS3[2])};
		int color4[]={java.lang.Integer.parseInt(colorS4[0]),java.lang.Integer.parseInt(colorS4[1]),java.lang.Integer.parseInt(colorS4[2])};
		humRenderColor=new java.awt.Color(color1[0],color1[1],color1[2]);
		tempRenderColor=new java.awt.Color(color2[0],color2[1],color2[2]);
		distRenderColor=new java.awt.Color(color3[0],color3[1],color3[2]);
		movRenderColor=new java.awt.Color(color4[0],color4[1],color4[2]);
		gnu.io.CommPortIdentifier portId=null;
		gnu.io.SerialPort serial=null;
		java.util.Enumeration<?>portEnum=gnu.io.CommPortIdentifier.getPortIdentifiers();
		while(portEnum.hasMoreElements()){
			gnu.io.CommPortIdentifier port=(gnu.io.CommPortIdentifier)portEnum.nextElement();
			if(portName.equals(port.getName())){portId=port;break;}
		}
		if(portId==null){
			/*--ErrorConsole--*/myapi.ErrorConsole.addText(portName+" not found");/*--ErrorConsole--*/
			/*--ErrorConsole--*/myapi.ErrorConsole.setVisible(true);/*--ErrorConsole--*/
			java.lang.Thread.sleep(1500);
			java.lang.System.exit(0);
			return;
		}
		serial=(gnu.io.SerialPort)portId.open(MyArduinoMonitoringGUI.class.getName(),1000);
		serial.setSerialPortParams(9600,gnu.io.SerialPort.DATABITS_8,gnu.io.SerialPort.STOPBITS_1,gnu.io.SerialPort.PARITY_NONE);
		java.io.InputStream input=serial.getInputStream();
		reader=new java.io.BufferedReader(new java.io.InputStreamReader(input));
		if(simulate){reader=simulateReader();}
		/*--MessageConsole--*/myapi.MessageConsole.addText("reader ok "+reader);/*--MessageConsole--*/
		myapi.utils.OfEvent.addGlobalKeyEvent(new javax.swing.AbstractAction(){
			private static final long serialVersionUID=4568081301873513073L;

			public void actionPerformed(java.awt.event.ActionEvent e){
				if(read!=null&&!read.isInterrupted()){return;}
				read=new java.lang.Thread(){
					@Override public void run(){
						try{
							while(true){
								java.lang.Thread.sleep((int)(1000*ratio));
								String output=myapi.utils.OfData.getPrintableChars(reader.readLine());
								/*--MessageConsole--*/myapi.MessageConsole.addText(output);/*--MessageConsole--*/
								if(output!=null&&output.contains("Humedad:")){
									java.lang.Float value=java.lang.Float.valueOf(output.replaceAll("Humedad:",""));
									humData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
										(new java.lang.Integer(humKey),value));
									humKey++;
								}
								else if(output!=null&&output.contains("Temperatura:")){
									java.lang.Float value=java.lang.Float.valueOf(output.replaceAll(",Temperatura:",""));
									tempData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
										(new java.lang.Integer(tempKey),value));
									tempKey++;
								}
								else if(output!=null&&(output.contains(",Entre")||output.contains(",Mas"))){
									int value;
									if(output.contains(" 3 ")&&output.contains(" 6 ")){value=3;}
									else if(output.contains(" 6 ")&&output.contains(" 13 ")){value=7;}
									else if(output.contains(" 13 ")&&output.contains(" 40 ")){value=14;}
									else{value=41;}
									distData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Integer>
										(new java.lang.Integer(distKey),new java.lang.Integer(value)));
									distKey++;
								}
								else if(output!=null&&output.contains("Movimiento")){
									java.lang.Boolean value=new java.lang.Boolean("empezado".equalsIgnoreCase(output.replaceAll(",Movimiento ","")));
									movData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Boolean>
										(new java.lang.Integer(movKey),value));
									movKey++;
								}
								update();
							}
						}
						/*--ErrorConsole--*/catch(java.lang.Exception e){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
					}
				};
				read.start();
			}
		},java.awt.event.KeyEvent.VK_CONTROL,java.awt.event.KeyEvent.VK_S);
		myapi.utils.OfEvent.addGlobalKeyEvent(new javax.swing.AbstractAction(){
			private static final long serialVersionUID=4568081301873513073L;

			public void actionPerformed(java.awt.event.ActionEvent e){
				if(read.isInterrupted()){return;}
				read.interrupt();//try{}
				//*--ErrorConsole--*/catch(java.lang.Exception e1){myapi.ErrorConsole.addText(e);}/*--ErrorConsole--*/
			}
		},java.awt.event.KeyEvent.VK_CONTROL,java.awt.event.KeyEvent.VK_C);
		humData=new myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Float>>(limit);
		tempData=new myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Float>>(limit);
		distData=new myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Integer>>(limit);
		movData=new myapi.generics.MyLimitedList<java.util.Map.Entry<java.lang.Integer,java.lang.Boolean>>(limit);

		if(simulate){
			humData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
				(new java.lang.Integer(humKey),new java.lang.Float(22.2)));
			humKey++;
			humData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
				(new java.lang.Integer(humKey),new java.lang.Float(24.5)));
			humKey++;
			humData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
				(new java.lang.Integer(humKey),new java.lang.Float(30.1)));
			humKey++;
	
			tempData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
				(new java.lang.Integer(tempKey),new java.lang.Float(1.5)));
			tempKey++;
			tempData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
				(new java.lang.Integer(tempKey),new java.lang.Float(4.3)));
			tempKey++;
			tempData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Float>
				(new java.lang.Integer(tempKey),new java.lang.Float(0.1)));
			tempKey++;
	
			distData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Integer>
				(new java.lang.Integer(distKey),new java.lang.Integer(40)));
			distKey++;
			distData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Integer>
				(new java.lang.Integer(distKey),new java.lang.Integer(13)));
			distKey++;
			distData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Integer>
				(new java.lang.Integer(distKey),new java.lang.Integer(3)));
			distKey++;
	
			movData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Boolean>
				(new java.lang.Integer(movKey),new java.lang.Boolean(true)));
			movKey++;
			movData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Boolean>
				(new java.lang.Integer(movKey),new java.lang.Boolean(false)));
			movKey++;
			movData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Boolean>
				(new java.lang.Integer(movKey),new java.lang.Boolean(true)));
		}
		graphicsTest();
		if(simulate){
			java.lang.Thread.sleep(2000);
			movKey++;
			movData.addLast(new myapi.generics.MyEntry<java.lang.Integer,java.lang.Boolean>
				(new java.lang.Integer(movKey),new java.lang.Boolean(false)));
			movKey++;
			update();
		}
	}
	private static void graphicsTest(){
		myapi.gui.MyWindow frame=new myapi.gui.MyWindow(null,false,new java.awt.Dimension(800,600));
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		panel=new myapi.gui.panels.MyBorderLayoutPanel(new java.awt.Dimension(790,562));
		frame.setContentPane(panel);
		distDataset=new org.jfree.data.xy.DefaultXYZDataset();
		humDataset=new org.jfree.data.category.DefaultCategoryDataset();
		tempDataset=new org.jfree.data.category.DefaultCategoryDataset();
		serie=new org.jfree.data.xy.XYSeries("Movimiento");
		update();
		org.jfree.chart.JFreeChart pointChart=org.jfree.chart.ChartFactory.createBubbleChart("Sharp GP2Y0A41SK E","Tiempo","Distancia",distDataset,
				org.jfree.chart.plot.PlotOrientation.VERTICAL,true,true,false);
		org.jfree.chart.renderer.xy.XYItemRenderer pointRenderer=((org.jfree.chart.plot.XYPlot)pointChart.getPlot()).getRenderer();
		pointRenderer.setSeriesPaint(0,distRenderColor);
		org.jfree.chart.ChartPanel chartP=new org.jfree.chart.ChartPanel(pointChart);
		chartP.setSize(chartP.getPreferredSize());

		org.jfree.chart.JFreeChart lineChart=org.jfree.chart.ChartFactory.createLineChart("DHT11","Tiempo","Humedad",humDataset,
				org.jfree.chart.plot.PlotOrientation.VERTICAL,true,true,false);
		org.jfree.chart.renderer.category.CategoryItemRenderer lineRenderer=((org.jfree.chart.plot.CategoryPlot)lineChart.getPlot()).getRenderer();
		lineRenderer.setSeriesPaint(0,humRenderColor);
		org.jfree.chart.ChartPanel chartL=new org.jfree.chart.ChartPanel(lineChart);
		chartL.setSize(chartL.getPreferredSize());

		org.jfree.chart.JFreeChart barChart=org.jfree.chart.ChartFactory.createBarChart("DHT11","Tiempo","Temperatura",tempDataset,
				org.jfree.chart.plot.PlotOrientation.VERTICAL,true,true,false);
		org.jfree.chart.renderer.category.CategoryItemRenderer barRenderer=((org.jfree.chart.plot.CategoryPlot)barChart.getPlot()).getRenderer();
		barRenderer.setSeriesPaint(0,tempRenderColor);
		org.jfree.chart.ChartPanel chartB=new org.jfree.chart.ChartPanel(barChart);
		chartB.setSize(chartB.getPreferredSize());

		org.jfree.data.xy.XYSeriesCollection movDataset=new org.jfree.data.xy.XYSeriesCollection();
		movDataset.addSeries(serie);
		org.jfree.chart.JFreeChart xyChart=org.jfree.chart.ChartFactory.createXYLineChart("PIR","Tiempo","Movimiento",movDataset,
				org.jfree.chart.plot.PlotOrientation.VERTICAL,true,true,false);
		org.jfree.chart.renderer.xy.XYLineAndShapeRenderer xyRenderer=new org.jfree.chart.renderer.xy.XYLineAndShapeRenderer();
		xyRenderer.setSeriesPaint(0,movRenderColor);
		xyRenderer.setSeriesStroke(0,new java.awt.BasicStroke(3.0f));
		xyChart.getXYPlot().setRenderer(xyRenderer);
		org.jfree.chart.ChartPanel chartXY=new org.jfree.chart.ChartPanel(xyChart);
		chartXY.setSize(chartXY.getPreferredSize());

		panel.add(chartP,java.awt.BorderLayout.EAST);
		panel.add(chartL);
		panel.add(chartB);
		panel.add(chartXY,java.awt.BorderLayout.WEST);
		frame.setVisible(true);
	}
	public static void update(){
		double x[]=new double[distData.size()],y[]=new double[distData.size()],z[]=new double[distData.size()];
		for(int i=0;i<distData.size();i++){
			java.util.Map.Entry<java.lang.Integer,java.lang.Integer>entry=distData.get(i);
			x[i]=entry.getKey().doubleValue();
			y[i]=entry.getValue().doubleValue();
			if(y[i]>2&&y[i]<7){z[i]=2;}
			else if(y[i]>6&&y[i]<14){z[i]=4;}
			else if(y[i]>13&&y[i]<41){z[i]=6;}
			else{z[i]=8;}
		}
		double distData[][]={x,y,z};
		distDataset.addSeries("Distancia",distData);

		humDataset.clear();
		for(int i=0;i<humData.size();i++){
			java.util.Map.Entry<java.lang.Integer,java.lang.Float>entry=humData.get(i);
			humDataset.addValue(entry.getValue(),"Humedad",entry.getKey());
		}

		tempDataset.clear();
		for(int i=0;i<tempData.size();i++){
			java.util.Map.Entry<java.lang.Integer,java.lang.Float>entry=tempData.get(i);
			tempDataset.addValue(entry.getValue(),"Temperatura",entry.getKey());
		}

		serie.clear();
		for(int i=0;i<movData.size();i++){
			java.util.Map.Entry<java.lang.Integer,java.lang.Boolean>entry=movData.get(i);
			double val;
			if(entry.getValue().booleanValue()){val=1;}
			else{val=0;}
			serie.add(entry.getKey().doubleValue(),val);
		}
	}
	private static java.io.BufferedReader simulateReader(){
		myapi.utils.OfEvent.addGlobalKeyEvent(new javax.swing.AbstractAction(){
			private static final long serialVersionUID=4568081301873513073L;

			public void actionPerformed(java.awt.event.ActionEvent e){
				java.lang.String messages[]={"Humedad:",",Temperatura:",",Movimiento empezado",",Movimiento acabado",",Mas de 40 cm",",Entre 13 y 40 cm",
						",Entre 6 y 13 cm",",Entre 3 y 6 cm"};
				java.util.Random random=new java.util.Random();
				java.lang.String selected=messages[random.nextInt(8)];
				if(selected==messages[0]||selected==messages[1]){selected+=random.nextInt(99)+"."+random.nextInt(99);}
				reader=new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.ByteArrayInputStream(selected.getBytes())));
				/*--MessageConsole--*/myapi.MessageConsole.addText("send "+selected);/*--MessageConsole--*/
			}
		},java.awt.event.KeyEvent.VK_CONTROL,java.awt.event.KeyEvent.VK_F);
		return null;
	}
	public static void createAndShowGUI()throws java.lang.Exception{arduinoTest();}
}