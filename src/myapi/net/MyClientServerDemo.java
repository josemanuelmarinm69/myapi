package myapi.net;
public class MyClientServerDemo{
	public static void clientTest(){
		myapi.utils.OfEvent.addGlobalKeyEvent(new javax.swing.AbstractAction(){
			private static final long serialVersionUID=1926977978114709075L;

			public synchronized void actionPerformed(java.awt.event.ActionEvent e){
				if(!myapi.MyOptionPane.receiveString("Give the server address","Server IP")){return;}
				try{new myapi.net.MyClient(){
					@Override public void sendRequests(java.io.BufferedReader reader,java.io.DataOutputStream output)throws java.io.IOException{
						myapi.MyOptionPane.showMessage(reader.readLine(),"I got this from server");
						myapi.MyOptionPane.showMessage(reader.readLine(),"And this");
					}
				}.connect(myapi.MyOptionPane.getString(),9090,100);}
				catch(java.net.SocketTimeoutException ex){myapi.MyOptionPane.showMessage("Server not found","Error");}
				catch(java.io.IOException ex){}
			}
		},java.awt.event.KeyEvent.VK_CONTROL,java.awt.event.KeyEvent.VK_N);
	}
	public static void serverTest(){
		final myapi.net.MyServer server=new myapi.net.MyServer(9090){
			@Override public void dispatchClients(java.io.BufferedReader reader,java.io.DataOutputStream output)throws java.io.IOException{
				output.writeChars("Reply from server\n");
				output.writeChars("What else do you want?");
			}
		};
		myapi.utils.OfEvent.addGlobalKeyEvent(new javax.swing.AbstractAction(){
			private static final long serialVersionUID=1926977978114709075L;

			public synchronized void actionPerformed(java.awt.event.ActionEvent e){
				try{server.shutdown();}
				catch(java.io.IOException ex){return;}
				myapi.MyOptionPane.showMessage("Server stoped","Shutdown");
			}
		},java.awt.event.KeyEvent.VK_CONTROL,java.awt.event.KeyEvent.VK_C);
		myapi.utils.OfEvent.addGlobalKeyEvent(new javax.swing.AbstractAction(){
			private static final long serialVersionUID=1926977978114709075L;

			public synchronized void actionPerformed(java.awt.event.ActionEvent e){
				try{server.startup();}
				catch(java.io.IOException ex){return;}
				myapi.MyOptionPane.showMessage("Server started","Startup");
			}
		},java.awt.event.KeyEvent.VK_CONTROL,java.awt.event.KeyEvent.VK_S);
	}
	public static void createAndShowGUI(){
		clientTest();
		serverTest();
		myapi.gui.MyWindow frame=new myapi.gui.MyWindow(null,false,new java.awt.Dimension(300,300));
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}